#include <Strips/Encoder.h>
#include <iostream>
#include <ctime>

using namespace std;
using namespace Strips;

int main() {

  cout << "Test Strips Encoder" << endl;
  
  //create decoder
  Encoder * encoder=new Encoder();

  //add commands
  cout << "Adding commands to the encoder" << endl;
  encoder->AddCommand(new FastCommandReset(1,FastCommandReset::LOGIC_RESET));
  encoder->AddCommand(new WriteRegister(0,0,1,0xF0,0x0F0F0F0F));
  encoder->AddCommand(new WriteRegister(0,0,1,0xE0,0xF0F0F0F0));
  encoder->AddCommand(new WriteRegister(1,1,2,3,4));
  encoder->AddCommand(new WriteRegister(0,0,1,2,3));
  encoder->AddCommand(new WriteRegister(0,0,1,0xFF,0x00000000));
  encoder->AddCommand(new WriteRegister(0,0,1,0xD0,0x55555555));
  encoder->AddCommand(new WriteRegister(0,0,1,0xC0,0xAAAAAAAA));
  encoder->AddCommand(new WriteRegister(0,0,1,0xB0,0xBBBBBBBB));
  encoder->AddCommand(new WriteRegister(0,0,1,0xA0,0xDDDDDDDD));
  encoder->AddCommand(new WriteRegister(0,0,1,0x90,0xFFFFFFFF));
  encoder->AddCommand(new ReadRegister(1,1,2,3));
  encoder->AddCommand(new ReadRegister(0,0,1,2));
  encoder->AddCommand(new L0A(true, 0x8, 1));
  encoder->AddCommand(new L0A(true, 0xF, 0x7F));
  
  //check the contents
  cout << "Initial commands: " << endl;
  for(uint32_t i=0;i<encoder->GetCommands().size();i++){
    cout << "-- " << encoder->GetCommands()[i]->ToString() << endl;
  }
  cout << "Encode" << endl;
  encoder->Encode();
  cout << "Byte string: " << encoder->GetByteString() << endl;
  
  cout << "Get the bytes (from decoder)" << endl;
  uint8_t *bytes = encoder->GetBytes();
  uint32_t len = encoder->GetLength();
  cout << " Length: " << len << endl;

  cout << "Set the bytes (from FELIX)" << endl;
  encoder->SetBytes(bytes,len);

  cout << "Decode" << endl;
  encoder->Decode();
  cout << "Byte string: " << encoder->GetByteString() << endl;
  cout << "Decoded commands: " << endl;
  for(auto command : encoder->GetCommands()){
    cout << "-- " << command->ToString() << endl;
  }


  uint32_t n_test=1E6;
  clock_t start;
  double duration, frequency, bytefreq;

  cout << endl;
  cout << "Statistics:" << endl;
  cout << " Number of Commands: " << encoder->GetCommands().size() << endl;
  cout << " Number of Bytes: " << encoder->GetLength() << endl;
  cout << " Number of Repetitions: " << n_test << endl;
  cout << "Encoding performance: " << endl;
  start = clock();	
  for(uint32_t i=0; i<n_test; i++){
    encoder->Encode();
  }
  duration = ( clock() - start ) / (double) CLOCKS_PER_SEC;
  frequency = (double) n_test / duration;
  bytefreq = encoder->GetLength() * frequency;
  
  cout << " CPU time  [s] : " << duration << endl;
  cout << " Frequency [MHz]: " << frequency/1E6 << endl;
  cout << " Byte Freq [MBps]: " << bytefreq/1E6 << endl;

  
  cout << "Decoding performance" << endl;
  start = clock();	
  for(uint32_t i=0; i<n_test; i++){
    encoder->Decode();
  }

  duration = ( clock() - start ) / (double) CLOCKS_PER_SEC;
  frequency = (double) n_test / duration;
  bytefreq = encoder->GetLength() * frequency;

  cout << " CPU time  [s] : " << duration << endl;
  cout << " Frequency [MHz]: " << frequency/1E6 << endl;
  cout << " Byte Freq [MBps]: " << bytefreq/1E6 << endl;
  
  cout << "Cleaning the house" << endl;
  delete encoder;

  cout << "Have a nice day" << endl;
  
  return 0;
}

