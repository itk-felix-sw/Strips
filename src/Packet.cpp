#include "Strips/Packet.h"
#include <sstream>
#include <iomanip>
#include <bitset>
#include <iostream>
#include <vector>

using namespace std;
using namespace Strips;

map<uint32_t, string> Packet::typeNames = {
    {TYP_NONE, "TYP_NONE"},
    {TYP_PR, "TYP_PR"},
    {TYP_LP, "TYP_LP"},
    {TYP_ABC_RR, "TYP_ABC_RR"},
    {TYP_ABC_TRANSP, "TYP_ABC_TRANSP"},
    {TYP_HCC_RR, "TYP_ABC_RR"},
    {TYP_ABC_FULL, "TYP_ABC_RR"},
    {TYP_ABC_HPR, "TYP_ABC_HPR"},
    {TYP_HCC_HPR, "TYP_HCC_HPR"},
    {TYP_UNKNOWN, "TYP_UNKNOWN"}
};

Packet::Packet(){
  m_type = TYP_UNKNOWN;
}

Packet::~Packet(){}

uint32_t Packet::GetType(){
  return m_type;
}

void Packet::SetType(uint32_t typ){
  m_type = typ;
}

/*


  //HCC or ABC read information
  //

  unsigned char address = 0;
  unsigned int value = 0;
  uint16_t channel_abc = 0; //IC number, for ABC responses only
  uint16_t abc_status = 0;  // Status word from ABC

  std::vector<uint16_t> raw_words;
  int flag = 0;
  int bcid = 0;
  int bcid_parity = 0;
  int l0id = 0;
  std::vector<Cluster> clusters;
  int num_idles = 0;
*/


  // global variables???
  //




/*
//different parsing for different packet type ABC/HCC/PR and LP
//

// raw words   std::vector<uint16_t> raw_words;

  int parse_data_HCC_read(){
    if( raw_words.size() != 8 ){
      if((raw_words.size()-2)/4 == 2) {
        // Transfer of data is 32bit, not including SOP/EOP, so don't have precise length
        //
        logger().debug("HCC readout packet should be 8 ten-bit words, but this is {}. Allowing small mismatch.", raw_words.size());
      } else {
        logger().error("Error, HCC readout packet should be 8 ten-bit words, but this is {}. Not parsing packet.", raw_words.size());
        return 1;
      }
    }

    this->address = ((raw_words[1] & 0xF) << 4) | ((raw_words[2] >> 4) & 0xF);
    this->value = ((raw_words[2] & 0xF) << 28) | ((raw_words[3] & 0xFF) << 20) |
      ((raw_words[4] & 0xFF) << 12) | ((raw_words[5] & 0xFF) << 4) | ((raw_words[6] >> 4) & 0xF) ;

  return 0;
  }


  int parse_data_ABC_read(){
    if( raw_words.size() != 11 ){
      if((raw_words.size()-2)/4 == 3) {
        // Transfer of data is 32bit, not including SOP/EOP, so don't have precise length
        //
       logger().debug("ABC readout packet should be 11 ten-bit words, but this is {}. Allowing small mismatch.", raw_words.size());
      } else {
        logger().error("ABC readout packet should be 11 ten-bit words, but this is {}. Not parsing packet.", raw_words.size());
        return 1;
      }
    }

    this->channel_abc = raw_words[1] & 0xF;
    this->address = raw_words[2] & 0xFF;
    // Top nibble of raw_words[3] should be all 0
    //
    
    this->value = ((raw_words[3] & 0xF) << 28)
                | ((raw_words[4] & 0xFF) << 20)
                | ((raw_words[5] & 0xFF) << 12)
                | ((raw_words[6] & 0xFF) << 4)
                | ((raw_words[7] & 0xF0) >> 4);

    this->abc_status = ((raw_words[7] & 0xF) << 12)
                     | ((raw_words[8] & 0xFF) << 4)
                     | ((raw_words[9] & 0xF0) >> 4);

    return 0;
  }


*/
