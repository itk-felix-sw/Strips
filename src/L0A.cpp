#include <Strips/L0A.h>
#include <Strips/SixToEight.h>
#include <sstream>

using namespace std;
using namespace Strips;

L0A::L0A(){
  m_bcr=0;
  m_l0a=1;
  m_tag=0;
}

L0A::~L0A(){
  //nothing to do
}

L0A::L0A(bool bcr, uint8_t mask, uint8_t tag){
  m_bcr=bcr;
  m_l0a=mask;
  m_tag=tag;
}

L0A::L0A(bool bcr, bool bc1, bool bc2, bool bc3, bool bc4, uint8_t tag){
  m_bcr=bcr;
  m_l0a=0;
  m_tag=tag;
}

L0A::L0A(L0A * copy){
  m_bcr=copy->m_bcr;
  m_l0a=copy->m_l0a;
  m_tag=copy->m_tag;
}

void L0A::SetBCR(bool enable){
  m_bcr=enable;
}

bool L0A::GetBCR(){
  return m_bcr;
}

void L0A::SetL0A(uint8_t mask){
  m_l0a=mask;
}

void L0A::SetL0A(bool bc1, bool bc2, bool bc3, bool bc4){
  m_l0a=0;
  m_l0a|=bc1<<0;
  m_l0a|=bc2<<1;
  m_l0a|=bc3<<2;
  m_l0a|=bc3<<3;
}

void L0A::SetL0A(uint32_t bc, bool enable){
  if(bc>3){return;}
  if(enable){
    m_l0a |= (1<<bc);
  } else {
    m_l0a &= ~(1<<bc);
  }
}

uint32_t L0A::GetL0A(){
  return m_l0a;
}

bool L0A::GetL0A(uint32_t bc){
  return ((m_l0a>>bc) & 0x1);
}
 
void L0A::SetL0Tag(uint32_t tag){
  m_tag=tag;
}

uint32_t L0A::GetL0Tag(){
  return m_tag;
}

string L0A::ToString(){
  ostringstream os;
  os << "L0A "
     << "BCR: " << m_bcr << " "
     << "mask: 0x" << hex << (uint32_t) m_l0a << dec << " "
     << "tag: 0x" << hex << (uint32_t) m_tag << dec;
  return os.str();
}

uint32_t L0A::Unpack (uint8_t * bytes, uint32_t maxlen){
  //we need 2 bytes
  if(maxlen<2) return 0;
  
  //they are not k-words
  if((SixToEight::word2k.find(bytes[0])!=SixToEight::word2k.end()) or 
     (SixToEight::word2k.find(bytes[1])!=SixToEight::word2k.end())){
    return 0;
  }
  
  //decode the bytes into 6bits   
  uint32_t b0=SixToEight::lut8b6b[bytes[0]];
  uint32_t b1=SixToEight::lut8b6b[bytes[1]];
  
  //If there is no enabled bit in the first five, it is not a L0A command
  if((b0&0x3E)==0){return 0;}

  m_bcr = (b0>>5) & 0x1;
  m_l0a = (b0>>1) & 0xF;
  m_tag = ((b0&0x1) << 6) | (b1&0x3f);

  return 2;
}
      
uint32_t L0A::Pack(uint8_t * bytes){
  //If there is no trigger enabled is not a L0A command
  if(m_l0a==0){return 0;}

  //payload
  uint8_t b0=((m_bcr<<5) & 0x20) | ((m_l0a<<1) & 0x1E) | ((m_tag >> 6) & 0x1);
  uint8_t b1=m_tag&0x3F;

  //encode
  bytes[0] = SixToEight::lut6b8b[b0];
  bytes[1] = SixToEight::lut6b8b[b1];
  
  return 2;
}
    
