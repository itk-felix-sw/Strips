#include <Strips/EndeavourWrite.h>
#include <sstream>
#include <iomanip>

using namespace std;
using namespace Strips;

EndeavourWrite::EndeavourWrite(){
  m_type=Endeavour::WRITE;
  m_id=0;
  m_address=0;
  m_data=0;
}

EndeavourWrite::EndeavourWrite(uint8_t id, uint8_t address, uint32_t data){
  m_id=id;
  m_address=address;
  m_data=data;
}

EndeavourWrite::EndeavourWrite(EndeavourWrite * copy){
  m_id=copy->m_id;
  m_address=copy->m_address;
  m_data=copy->m_data;
}

EndeavourWrite::~EndeavourWrite(){
  //nothing to do
}

void EndeavourWrite::SetID(uint8_t id){
  m_id = id;
}

uint8_t EndeavourWrite::GetID(){
  return m_id;
}

void EndeavourWrite::SetAddress(uint8_t address){
  m_address=address;
}

uint8_t EndeavourWrite::GetAddress(){
  return m_address;
}

void EndeavourWrite::SetData(uint32_t data){
  m_data=data;
}

uint32_t EndeavourWrite::GetData(){
  return m_data;
}

string EndeavourWrite::ToString(){
  ostringstream os;
  os << "EndeavourWrite "
     << "ID: " << (uint32_t) m_id << " "
     << "Address: 0x" << hex << setw(2) << setfill('0') << (uint32_t) m_address << dec << " "
     << "Data: 0x" << hex << setw(8) << setfill('0') << m_data << dec;
  return os.str();
}

uint32_t EndeavourWrite::Unpack(uint8_t * bytes, uint32_t maxlen){
  if(maxlen<7) return 0;
  if((bytes[0] & 0xE0) != Endeavour::WRITE){return 0;}
  
  m_id = (bytes[0] & 0x1F);
  m_address = bytes[1];
  m_data = ((bytes[2]&0xFF)<<24) | ((bytes[3]&0xFF)<<16) | ((bytes[4]&0xFF)<<8) | ((bytes[5]&0xFF)<<0);

  return 7;
}
      
uint32_t EndeavourWrite::Pack(uint8_t * bytes){

  bytes[0] = m_type | (m_id & 0x1F);
  bytes[1] = m_address;
  bytes[2] = (m_data >> 24) & 0xFF;
  bytes[3] = (m_data >> 16) & 0xFF;
  bytes[4] = (m_data >>  8) & 0xFF;
  bytes[5] = (m_data >>  0) & 0xFF;
  bytes[6] = CRC(bytes);

  return 7;
}
    
