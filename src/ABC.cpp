#include <Strips/ABC.h>
#include <iomanip>
#include <sstream>
#include <iostream>

using namespace std;
using namespace Strips;

ABC::ABC(){

  m_registers.resize(NUM_ABC_REGISTERS);

  // ANDREA dummy default values = 99 to be fixed 
  m_subregisters[ABCSubRegister::RRFORCE]		= new SubRegister(&m_registers[0],  0,  1, 99);
  m_subregisters[ABCSubRegister::WRITEDISABLE] 		= new SubRegister(&m_registers[0],  1,  1, 99);
  m_subregisters[ABCSubRegister::STOPHPR] 		= new SubRegister(&m_registers[0],  2,  1, 99);
  m_subregisters[ABCSubRegister::TESTHPR] 		= new SubRegister(&m_registers[0],  3,  1, 99);
  m_subregisters[ABCSubRegister::EFUSEL] 		= new SubRegister(&m_registers[0],  4,  1, 99);
  m_subregisters[ABCSubRegister::LCBERRCNTCLR] 		= new SubRegister(&m_registers[0],  5,  1, 99);
  
  m_subregisters[ABCSubRegister::BVREF] 		= new SubRegister(&m_registers[1],  0,  5, 99);
  m_subregisters[ABCSubRegister::BIREF] 		= new SubRegister(&m_registers[1],  8,  5, 99);
  m_subregisters[ABCSubRegister::B8BREF] 		= new SubRegister(&m_registers[1], 16,  5, 99);
  m_subregisters[ABCSubRegister::BTRANGE] 		= new SubRegister(&m_registers[1], 24,  5, 99); 

  m_subregisters[ABCSubRegister::BVT] 			= new SubRegister(&m_registers[2],  0,  8, 99);
  m_subregisters[ABCSubRegister::COMBIAS] 		= new SubRegister(&m_registers[2],  8,  5, 99);
  m_subregisters[ABCSubRegister::BIFEED] 		= new SubRegister(&m_registers[2], 16,  5, 99);
  m_subregisters[ABCSubRegister::BIPRE] 		= new SubRegister(&m_registers[2], 24,  5, 99);

  m_subregisters[ABCSubRegister::STR_DEL_R] 		= new SubRegister(&m_registers[3],  0,  2, 99);
  m_subregisters[ABCSubRegister::STR_DEL] 		= new SubRegister(&m_registers[3],  8,  6, 99);
  m_subregisters[ABCSubRegister::BCAL] 			= new SubRegister(&m_registers[3], 16,  9, 99);
  m_subregisters[ABCSubRegister::BCAL_RANGE]		= new SubRegister(&m_registers[3], 25,  1, 99);

  m_subregisters[ABCSubRegister::ADC_BIAS]		= new SubRegister(&m_registers[4],  0,  4, 99);
  m_subregisters[ABCSubRegister::ADC_CH]		= new SubRegister(&m_registers[4],  4,  4, 99);
  m_subregisters[ABCSubRegister::ADC_ENABLE]		= new SubRegister(&m_registers[4],  8,  1, 99);

  m_subregisters[ABCSubRegister::D_S]			= new SubRegister(&m_registers[6],  0, 16, 99);
  m_subregisters[ABCSubRegister::D_LOW]			= new SubRegister(&m_registers[6], 16,  1, 99);
  m_subregisters[ABCSubRegister::D_EN_CTRL]		= new SubRegister(&m_registers[6], 17,  1, 99);

  m_subregisters[ABCSubRegister::BTMUX]			= new SubRegister(&m_registers[7],  0, 14, 99);
  m_subregisters[ABCSubRegister::BTMUXD]		= new SubRegister(&m_registers[7], 14 , 1, 99);
  m_subregisters[ABCSubRegister::A_S]			= new SubRegister(&m_registers[7], 15, 16, 99);
  m_subregisters[ABCSubRegister::A_EN_CTRL]		= new SubRegister(&m_registers[7], 31,  1, 99);


  m_subregisters[ABCSubRegister::TEST_PULSE_ENABLE]	= new SubRegister(&m_registers[32],  4,  1, 99);
  m_subregisters[ABCSubRegister::ENCOUNT]		= new SubRegister(&m_registers[32],  5,  1, 99);
  m_subregisters[ABCSubRegister::MASKHPR]		= new SubRegister(&m_registers[32],  6,  1, 99);
  m_subregisters[ABCSubRegister::PR_ENABLE]		= new SubRegister(&m_registers[32],  8,  1, 99);
  m_subregisters[ABCSubRegister::LP_ENABLE]		= new SubRegister(&m_registers[32],  9,  1, 99);
  m_subregisters[ABCSubRegister::RRMODE]		= new SubRegister(&m_registers[32], 10,  2, 99);
  m_subregisters[ABCSubRegister::TM]			= new SubRegister(&m_registers[32], 16,  2, 99);
  m_subregisters[ABCSubRegister::TESTPATT_ENABLE]	= new SubRegister(&m_registers[32], 18,  1, 99);
  m_subregisters[ABCSubRegister::TESTPATT1]		= new SubRegister(&m_registers[32], 20,  4, 99);
  m_subregisters[ABCSubRegister::TESTPATT2]		= new SubRegister(&m_registers[32], 24,  4, 99);

  m_subregisters[ABCSubRegister::CURRDRIV]		= new SubRegister(&m_registers[33],  0,  3, 99);
  m_subregisters[ABCSubRegister::CALPULSE_ENABLE]	= new SubRegister(&m_registers[33],  4,  1, 99);
  m_subregisters[ABCSubRegister::CALPULSE_POLARITY]	= new SubRegister(&m_registers[33],  5,  1, 99);

  m_subregisters[ABCSubRegister::LATENCY]		= new SubRegister(&m_registers[34],  0,  9, 99);
  m_subregisters[ABCSubRegister::BCFLAG_ENABLE]		= new SubRegister(&m_registers[34], 23,  1, 99);
  m_subregisters[ABCSubRegister::BCOFFSET]		= new SubRegister(&m_registers[34], 24,  8, 99);

  m_subregisters[ABCSubRegister::DETMODE]		= new SubRegister(&m_registers[35],  0,  2, 99);
  m_subregisters[ABCSubRegister::MAX_CLUSTER]		= new SubRegister(&m_registers[35], 12,  6, 99);
  m_subregisters[ABCSubRegister::MAX_CLUSTER_ENABLE]	= new SubRegister(&m_registers[35], 18,  1, 99);

  m_subregisters[ABCSubRegister::EN_CLUSTER_EMPTY]	= new SubRegister(&m_registers[36],  0,  1, 99);
  m_subregisters[ABCSubRegister::EN_CLUSTER_FULL]	= new SubRegister(&m_registers[36],  1,  1, 99);
  m_subregisters[ABCSubRegister::EN_CLUSTER_OVFL]	= new SubRegister(&m_registers[36],  2,  1, 99);
  m_subregisters[ABCSubRegister::EN_REGFIFO_EMPTY]	= new SubRegister(&m_registers[36],  3,  1, 99);
  m_subregisters[ABCSubRegister::EN_REGFIFO_FULL]	= new SubRegister(&m_registers[36],  4,  1, 99);
  m_subregisters[ABCSubRegister::EN_REGFIFO_OVFL]	= new SubRegister(&m_registers[36],  5,  1, 99);
  m_subregisters[ABCSubRegister::EN_LPFIFO_EMPTY]	= new SubRegister(&m_registers[36],  6,  1, 99);
  m_subregisters[ABCSubRegister::EN_LPFIFO_FULL]	= new SubRegister(&m_registers[36],  7,  1, 99);
  m_subregisters[ABCSubRegister::EN_PRFIFO_EMPTY]	= new SubRegister(&m_registers[36],  8,  1, 99);
  m_subregisters[ABCSubRegister::EN_PRFIFO_FULL]	= new SubRegister(&m_registers[36],  9,  1, 99);
  m_subregisters[ABCSubRegister::EN_LCB_LOCKED]		= new SubRegister(&m_registers[36], 10,  1, 99);
  m_subregisters[ABCSubRegister::EN_LCB_DECODE_ERR]	= new SubRegister(&m_registers[36], 11,  1, 99);
  m_subregisters[ABCSubRegister::EN_LCB_ERRCNT_OVFL]	= new SubRegister(&m_registers[36], 12,  1, 99);
  m_subregisters[ABCSubRegister::EN_LCB_SCMD_ERR]	= new SubRegister(&m_registers[36], 13,  1, 99);

  m_subregisters[ABCSubRegister::LCB_ERRCOUNT_THR]	= new SubRegister(&m_registers[38],  0, 16, 99);
  m_id=0;

  m_registerNames={
      {"SCReg", ABCRegister::SCReg},
      {"ADCS1", ABCRegister::ADCS1},
      {"ADCS2", ABCRegister::ADCS2},
      {"ADCS3", ABCRegister::ADCS3},
      {"ADCS4", ABCRegister::ADCS4},
      {"ADCS5", ABCRegister::ADCS5},
      {"ADCS6", ABCRegister::ADCS6},
      {"ADCS7", ABCRegister::ADCS7},
      {"MaskInput0", ABCRegister::MaskInput0},
      {"MaskInput1", ABCRegister::MaskInput1},
      {"MaskInput2", ABCRegister::MaskInput2},
      {"MaskInput3", ABCRegister::MaskInput3},
      {"MaskInput4", ABCRegister::MaskInput4},
      {"MaskInput5", ABCRegister::MaskInput5},
      {"MaskInput6", ABCRegister::MaskInput6},
      {"MaskInput7", ABCRegister::MaskInput7},
      {"CREG0", ABCRegister::CREG0},
      {"CREG1", ABCRegister::CREG1},
      {"CREG2", ABCRegister::CREG2},
      {"CREG3", ABCRegister::CREG3},
      {"CREG4", ABCRegister::CREG4},
      {"CREG6", ABCRegister::CREG6},
      {"STAT0", ABCRegister::STAT0},
      {"STAT1", ABCRegister::STAT1},
      {"STAT2", ABCRegister::STAT2},
      {"STAT3", ABCRegister::STAT3},
      {"STAT4", ABCRegister::STAT4},
      {"HPR", ABCRegister::HPR},
      {"TrimDAC0", ABCRegister::TrimDAC0},
      {"TrimDAC1", ABCRegister::TrimDAC1},
      {"TrimDAC2", ABCRegister::TrimDAC2},
      {"TrimDAC3", ABCRegister::TrimDAC3},
      {"TrimDAC4", ABCRegister::TrimDAC4},
      {"TrimDAC5", ABCRegister::TrimDAC5},
      {"TrimDAC6", ABCRegister::TrimDAC6},
      {"TrimDAC7", ABCRegister::TrimDAC7},
      {"TrimDAC8", ABCRegister::TrimDAC8},
      {"TrimDAC9", ABCRegister::TrimDAC9},
      {"TrimDAC10", ABCRegister::TrimDAC10},
      {"TrimDAC11", ABCRegister::TrimDAC11},
      {"TrimDAC12", ABCRegister::TrimDAC12},
      {"TrimDAC13", ABCRegister::TrimDAC13},
      {"TrimDAC14", ABCRegister::TrimDAC14},
      {"TrimDAC15", ABCRegister::TrimDAC15},
      {"TrimDAC16", ABCRegister::TrimDAC16},
      {"TrimDAC17", ABCRegister::TrimDAC17},
      {"TrimDAC18", ABCRegister::TrimDAC18},
      {"TrimDAC19", ABCRegister::TrimDAC19},
      {"TrimDAC20", ABCRegister::TrimDAC20},
      {"TrimDAC21", ABCRegister::TrimDAC21},
      {"TrimDAC22", ABCRegister::TrimDAC22},
      {"TrimDAC23", ABCRegister::TrimDAC23},
      {"TrimDAC24", ABCRegister::TrimDAC24},
      {"TrimDAC25", ABCRegister::TrimDAC25},
      {"TrimDAC26", ABCRegister::TrimDAC26},
      {"TrimDAC27", ABCRegister::TrimDAC27},
      {"TrimDAC28", ABCRegister::TrimDAC28},
      {"TrimDAC29", ABCRegister::TrimDAC29},
      {"TrimDAC30", ABCRegister::TrimDAC30},
      {"TrimDAC31", ABCRegister::TrimDAC31},
      {"TrimDAC32", ABCRegister::TrimDAC32},
      {"TrimDAC33", ABCRegister::TrimDAC33},
      {"TrimDAC34", ABCRegister::TrimDAC34},
      {"TrimDAC35", ABCRegister::TrimDAC35},
      {"TrimDAC36", ABCRegister::TrimDAC36},
      {"TrimDAC37", ABCRegister::TrimDAC37},
      {"TrimDAC38", ABCRegister::TrimDAC38},
      {"TrimDAC39", ABCRegister::TrimDAC39},
      {"CalREG0", ABCRegister::CalREG0},
      {"CalREG1", ABCRegister::CalREG1},
      {"CalREG2", ABCRegister::CalREG2},
      {"CalREG3", ABCRegister::CalREG3},
      {"CalREG4", ABCRegister::CalREG4},
      {"CalREG5", ABCRegister::CalREG5},
      {"CalREG6", ABCRegister::CalREG6},
      {"CalREG7", ABCRegister::CalREG7},
      {"HitCountREG0", ABCRegister::HitCountREG0},
      {"HitCountREG1", ABCRegister::HitCountREG1},
      {"HitCountREG2", ABCRegister::HitCountREG2},
      {"HitCountREG3", ABCRegister::HitCountREG3},
      {"HitCountREG4", ABCRegister::HitCountREG4},
      {"HitCountREG5", ABCRegister::HitCountREG5},
      {"HitCountREG6", ABCRegister::HitCountREG6},
      {"HitCountREG7", ABCRegister::HitCountREG7},
      {"HitCountREG8", ABCRegister::HitCountREG8},
      {"HitCountREG9", ABCRegister::HitCountREG9},
      {"HitCountREG10", ABCRegister::HitCountREG10},
      {"HitCountREG11", ABCRegister::HitCountREG11},
      {"HitCountREG12", ABCRegister::HitCountREG12},
      {"HitCountREG13", ABCRegister::HitCountREG13},
      {"HitCountREG14", ABCRegister::HitCountREG14},
      {"HitCountREG15", ABCRegister::HitCountREG15},
      {"HitCountREG16", ABCRegister::HitCountREG16},
      {"HitCountREG17", ABCRegister::HitCountREG17},
      {"HitCountREG18", ABCRegister::HitCountREG18},
      {"HitCountREG19", ABCRegister::HitCountREG19},
      {"HitCountREG20", ABCRegister::HitCountREG20},
      {"HitCountREG21", ABCRegister::HitCountREG21},
      {"HitCountREG22", ABCRegister::HitCountREG22},
      {"HitCountREG23", ABCRegister::HitCountREG23},
      {"HitCountREG24", ABCRegister::HitCountREG24},
      {"HitCountREG25", ABCRegister::HitCountREG25},
      {"HitCountREG26", ABCRegister::HitCountREG26},
      {"HitCountREG27", ABCRegister::HitCountREG27},
      {"HitCountREG28", ABCRegister::HitCountREG28},
      {"HitCountREG29", ABCRegister::HitCountREG29},
      {"HitCountREG30", ABCRegister::HitCountREG30},
      {"HitCountREG31", ABCRegister::HitCountREG31},
      {"HitCountREG32", ABCRegister::HitCountREG32},
      {"HitCountREG33", ABCRegister::HitCountREG33},
      {"HitCountREG34", ABCRegister::HitCountREG34},
      {"HitCountREG35", ABCRegister::HitCountREG35},
      {"HitCountREG36", ABCRegister::HitCountREG36},
      {"HitCountREG37", ABCRegister::HitCountREG37},
      {"HitCountREG38", ABCRegister::HitCountREG38},
      {"HitCountREG39", ABCRegister::HitCountREG39},
      {"HitCountREG40", ABCRegister::HitCountREG40},
      {"HitCountREG41", ABCRegister::HitCountREG41},
      {"HitCountREG42", ABCRegister::HitCountREG42},
      {"HitCountREG43", ABCRegister::HitCountREG43},
      {"HitCountREG44", ABCRegister::HitCountREG44},
      {"HitCountREG45", ABCRegister::HitCountREG45},
      {"HitCountREG46", ABCRegister::HitCountREG46},
      {"HitCountREG47", ABCRegister::HitCountREG47},
      {"HitCountREG48", ABCRegister::HitCountREG48},
      {"HitCountREG49", ABCRegister::HitCountREG49},
      {"HitCountREG50", ABCRegister::HitCountREG50},
      {"HitCountREG51", ABCRegister::HitCountREG51},
      {"HitCountREG52", ABCRegister::HitCountREG52},
      {"HitCountREG53", ABCRegister::HitCountREG53},
      {"HitCountREG54", ABCRegister::HitCountREG54},
      {"HitCountREG55", ABCRegister::HitCountREG55},
      {"HitCountREG56", ABCRegister::HitCountREG56},
      {"HitCountREG57", ABCRegister::HitCountREG57},
      {"HitCountREG58", ABCRegister::HitCountREG58},
      {"HitCountREG59", ABCRegister::HitCountREG59},
      {"HitCountREG60", ABCRegister::HitCountREG60},
      {"HitCountREG61", ABCRegister::HitCountREG61},
      {"HitCountREG62", ABCRegister::HitCountREG62},
      {"HitCountREG63", ABCRegister::HitCountREG63},
  };

  m_subRegisterNames={
      {"TESTHPR", ABCSubRegister::TESTHPR},
  {"ADC_CH", ABCSubRegister::ADC_CH},
  {"TESTPATT1", ABCSubRegister::TESTPATT1},
  {"MAX_CLUSTER_ENABLE", ABCSubRegister::MAX_CLUSTER_ENABLE},
  {"EN_CLUSTER_FULL", ABCSubRegister::EN_CLUSTER_FULL},
  {"EN_LCB_ERRCNT_OVFL", ABCSubRegister::EN_LCB_ERRCNT_OVFL},
  {"EFUSEL", ABCSubRegister::EFUSEL},
  {"LCBERRCNTCLR", ABCSubRegister::LCBERRCNTCLR},
  {"CURRDRIV", ABCSubRegister::CURRDRIV},
  {"EN_REGFIFO_OVFL", ABCSubRegister::EN_REGFIFO_OVFL},
  {"EN_PRFIFO_EMPTY", ABCSubRegister::EN_PRFIFO_EMPTY},
  {"DETMODE", ABCSubRegister::DETMODE},
  {"A_EN_CTRL", ABCSubRegister::A_EN_CTRL},
  {"BVREF", ABCSubRegister::BVREF},
  {"B8BREF", ABCSubRegister::B8BREF},
  {"BTRANGE", ABCSubRegister::BTRANGE},
  {"STR_DEL_R", ABCSubRegister::STR_DEL_R},
  {"EN_LCB_DECODE_ERR", ABCSubRegister::EN_LCB_DECODE_ERR},
  {"EN_LCB_LOCKED", ABCSubRegister::EN_LCB_LOCKED},
  {"TM", ABCSubRegister::TM},
  {"BCAL", ABCSubRegister::BCAL},
  {"D_LOW", ABCSubRegister::D_LOW},
  {"TESTPATT2", ABCSubRegister::TESTPATT2},
  {"EN_LPFIFO_EMPTY", ABCSubRegister::EN_LPFIFO_EMPTY},
  {"MASKHPR", ABCSubRegister::MASKHPR},
  {"LP_ENABLE", ABCSubRegister::LP_ENABLE},
  {"RRMODE", ABCSubRegister::RRMODE},
  {"STR_DEL", ABCSubRegister::STR_DEL},
  {"BTMUX", ABCSubRegister::BTMUX},
  {"EN_REGFIFO_FULL", ABCSubRegister::EN_REGFIFO_FULL},
  {"D_S", ABCSubRegister::D_S},
  {"EN_CLUSTER_OVFL", ABCSubRegister::EN_CLUSTER_OVFL},
  {"TEST_PULSE_ENABLE", ABCSubRegister::TEST_PULSE_ENABLE},
  {"BIREF", ABCSubRegister::BIREF},
  {"EN_PRFIFO_FULL", ABCSubRegister::EN_PRFIFO_FULL},
  {"BCAL_RANGE", ABCSubRegister::BCAL_RANGE},
  {"ADC_BIAS", ABCSubRegister::ADC_BIAS},
  {"EN_LCB_SCMD_ERR", ABCSubRegister::EN_LCB_SCMD_ERR},
  {"EN_REGFIFO_EMPTY", ABCSubRegister::EN_REGFIFO_EMPTY},
  {"D_EN_CTRL", ABCSubRegister::D_EN_CTRL},
  {"TESTPATT_ENABLE", ABCSubRegister::TESTPATT_ENABLE},
  {"BIPRE", ABCSubRegister::BIPRE},
  {"ADC_ENABLE", ABCSubRegister::ADC_ENABLE},
  {"MAX_CLUSTER", ABCSubRegister::MAX_CLUSTER},
  {"RRFORCE", ABCSubRegister::RRFORCE},
  {"BTMUXD", ABCSubRegister::BTMUXD},
  {"CALPULSE_ENABLE", ABCSubRegister::CALPULSE_ENABLE},
  {"CALPULSE_POLARITY", ABCSubRegister::CALPULSE_POLARITY},
  {"EN_LPFIFO_FULL", ABCSubRegister::EN_LPFIFO_FULL},
  {"ENCOUNT", ABCSubRegister::ENCOUNT},
  {"LATENCY", ABCSubRegister::LATENCY},
  {"COMBIAS", ABCSubRegister::COMBIAS},
  {"BCFLAG_ENABLE", ABCSubRegister::BCFLAG_ENABLE},
  {"STOPHPR", ABCSubRegister::STOPHPR},
  {"WRITEDISABLE", ABCSubRegister::WRITEDISABLE},
  {"BVT", ABCSubRegister::BVT},
  {"BCOFFSET", ABCSubRegister::BCOFFSET},
  {"LCB_ERRCOUNT_THR", ABCSubRegister::LCB_ERRCOUNT_THR},
  {"EN_CLUSTER_EMPTY", ABCSubRegister::EN_CLUSTER_EMPTY},
  {"A_S", ABCSubRegister::A_S},
  {"PR_ENABLE", ABCSubRegister::PR_ENABLE},
  {"BIFEED", ABCSubRegister::BIFEED},
  };


}

ABC::~ABC(){
  for(auto it : m_subregisters){
    delete it.second;
  }
  m_subregisters.clear();
}


void ABC::SetDefaults(){
  // Initialize 32-bit register with default values
  GetRegister(ABCRegister::SCReg)->SetValue(0x00000000);

  //Analog and DCS regs
  for (int iReg=ABCRegister::ADCS1; iReg<=ABCRegister::ADCS7; iReg++)
    (&m_registers.at(iReg))->SetValue(0x00000000);

  //configuration registers    
  for (int iReg=ABCRegister::CREG0; iReg<=ABCRegister::CREG6; iReg++) {
    if(iReg == ABCRegister::CREG0 + 5) continue; // Skip CREG5 as it's fuse register 
    (&m_registers.at(iReg))->SetValue(0x00000000);
  }
  
  //mask registers
  for (int iReg=ABCRegister::MaskInput0; iReg<=ABCRegister::MaskInput7; iReg++)
    (&m_registers.at(iReg))->SetValue(0x00000000);
  
  //calibration registers      
  for (unsigned int iReg=ABCRegister::CalREG0; iReg<=ABCRegister::CalREG7; iReg++)
    (&m_registers.at(iReg))->SetValue(0xFFFFFFFF);

  //trim dac registers 
  for (unsigned int iReg=ABCRegister::TrimDAC0; iReg<=ABCRegister::TrimDAC31; iReg++)
    (&m_registers.at(iReg))->SetValue(0xFFFFFFFF);

  for (unsigned int iReg=ABCRegister::TrimDAC0; iReg<=ABCRegister::TrimDAC31; iReg++)
    (&m_registers.at(iReg))->SetValue(0x00000000);


}

/*

const std::vector<abcsubregdef> s_abcsubregdefs_v1 = {
  // SCREG
  {ABCStarSubRegister::RRFORCE,      0, 0, 1},
  {ABCStarSubRegister::WRITEDISABLE, 0, 1, 1},
  {ABCStarSubRegister::STOPHPR,      0, 2, 1},
  {ABCStarSubRegister::TESTHPR,      0, 3, 1},
  {ABCStarSubRegister::EFUSEL,       0, 4, 1},
  {ABCStarSubRegister::LCBERRCNTCLR, 0, 5, 1},
  {ABCStarSubRegister::ADCRESET,     0, 6, 1},

  // DCS1
  {ABCStarSubRegister::BVREF,                1, 0,  5},
  {ABCStarSubRegister::BIREF,                1, 5,  5},
  {ABCStarSubRegister::B8BREF,               1, 10, 5},
  {ABCStarSubRegister::BTRANGE,              1, 15, 5},
  {ABCStarSubRegister::BVT,                  1, 20, 8},
  {ABCStarSubRegister::DIS_CLK,              1, 28, 3},
  {ABCStarSubRegister::LCB_SELF_TEST_ENABLE, 1, 31, 1},

  // DCS2
  {ABCStarSubRegister::STR_DEL_R,     2, 0,  2},
  {ABCStarSubRegister::STR_DEL,       2, 2,  6},
  {ABCStarSubRegister::COMBIAS,       2, 8,  5},
  {ABCStarSubRegister::BCAL,          2, 13, 9},
  {ABCStarSubRegister::DATA_IDLE,     2, 22, 4},
  {ABCStarSubRegister::EN_GLITCH_A,   2, 26, 1},
  {ABCStarSubRegister::EN_GLITCH_B,   2, 27, 1},
  {ABCStarSubRegister::EN_GLITCH_C,   2, 28, 1},
  {ABCStarSubRegister::EN_GLITCH_V,   2, 29, 1},
  {ABCStarSubRegister::EN_GLITCH_ADC, 2, 30, 1},
  {ABCStarSubRegister::RING_OSC_EN,   2, 31, 1},

  // DCS3
  {ABCStarSubRegister::ADC_BIAS,       3, 0,  4},
  {ABCStarSubRegister::ADC_CH,         3, 4,  4},
  {ABCStarSubRegister::ADC_ENABLE,     3, 8,  1},
  {ABCStarSubRegister::BTMUX_DEC,      3, 9,  4},
  {ABCStarSubRegister::BTMUXD,         3, 13, 1},
  {ABCStarSubRegister::A_S_DEC,        3, 14, 5},
  {ABCStarSubRegister::A_LOW,          3, 19, 1},
  {ABCStarSubRegister::A_EN_CTRL,      3, 20, 1},
  {ABCStarSubRegister::D_S_DEC,        3, 21, 5},
  {ABCStarSubRegister::D_LOW,          3, 26, 1},
  {ABCStarSubRegister::D_EN_CTRL,      3, 27, 1},
  {ABCStarSubRegister::EN_OUT_DECODER, 3, 28, 4},

  // CFG0
  {ABCStarSubRegister::TEST_PULSE_ENABLE, 32, 0,  1},
  {ABCStarSubRegister::ENCOUNT,           32, 1,  1},
  {ABCStarSubRegister::MASKHPR,           32, 2,  1},
  {ABCStarSubRegister::PR_ENABLE,         32, 3,  1},
  {ABCStarSubRegister::LP_ENABLE,         32, 4,  1},
  {ABCStarSubRegister::RRMODE,            32, 5,  2},
  {ABCStarSubRegister::TM,                32, 7,  2},
  {ABCStarSubRegister::TESTPATT_ENABLE,   32, 9,  1},
  {ABCStarSubRegister::TESTPATT1,         32, 10, 4},
  {ABCStarSubRegister::TESTPATT2,         32, 14, 4},
  {ABCStarSubRegister::CURRDRIV,          32, 18, 3},
  {ABCStarSubRegister::CALPULSE_ENABLE,   32, 21, 1},
  {ABCStarSubRegister::CALPULSE_POLARITY, 32, 22, 1},
  {ABCStarSubRegister::LATENCY,           32, 23, 9},

  // CFG1
  {ABCStarSubRegister::DTESTOUTSEL,            33, 0,  7},
  {ABCStarSubRegister::DETMODE,                33, 7,  2},
  {ABCStarSubRegister::MAX_CLUSTER,            33, 9,  6},
  {ABCStarSubRegister::MAX_CLUSTER_ENABLE,     33, 15, 1},
  // {ABCStarSubRegister::DOFUSEADDR,             33, 16,  5},
  {ABCStarSubRegister::V0_READOUT_MODE,        33, 21, 1},
  {ABCStarSubRegister::DIS_CLKS_EN,            33, 22, 1},
  {ABCStarSubRegister::LCB_ERRCOUNT_THR,       33, 23, 8},
  {ABCStarSubRegister::READOUT_TIMEOUT_ENABLE, 33, 31, 1},
};

enum


            // New for ABCStarV1
            // SCREG
            ADCRESET,

            // DCS1
            DIS_CLK, LCB_SELF_TEST_ENABLE,

            // DCS2
            DATA_IDLE, EN_GLITCH_A, EN_GLITCH_B, EN_GLITCH_C, EN_GLITCH_V, EN_GLITCH_ADC, RING_OSC_EN,

            // DCS3
            BTMUX_DEC, A_S_DEC, A_LOW, D_S_DEC, EN_OUT_DECODER,

            // CFG1
            DTESTOUTSEL,
            // DOFUSEADDR
            V0_READOUT_MODE, DIS_CLKS_EN,
            READOUT_TIMEOUT_ENABLE

*/
