#include "Strips/Handler.h"
#include "Strips/RunNumber.h"
#include <json.hpp>
#include <iostream>
#include <fstream>
#include <mutex>
#include <chrono>
#include <sstream>
#include <iomanip>
#include "TFile.h"
#include "TROOT.h"
#include "TBufferJSON.h"
#include "TKey.h"

using json=nlohmann::basic_json<std::map, std::vector, std::string, bool, std::int32_t, std::uint32_t, float>;
using namespace std;
using namespace Strips;

Handler::Handler(){
  gROOT->SetBatch(true); //This is a must otherwise runs hang
  string itkpath = getenv("ITK_PATH");
  m_config_path.push_back("");
  m_config_path.push_back("./tuned/");
  m_config_path.push_back("./");
  m_config_path.push_back(itkpath+"/");
  m_config_path.push_back(itkpath+"/tuned/");
  m_config_path.push_back(itkpath+"/config/");
  m_config_path.push_back(itkpath+"/installed/share/data/config/");

  m_verbose = false;
  m_backend = "posix";
  m_retune = false;
  m_rootfile = 0;
  m_outpath = (getenv("ITK_DATA_PATH")?string(getenv("ITK_DATA_PATH")):".");
  m_rn = new RunNumber();
  m_output = true;
  m_fullOutPath = "";
  m_buspath = "/tmp/bus";
  m_storehits = false;
}

Handler::~Handler(){
  while(!m_fes.empty()){
    FrontEnd* fe=m_fes.back();
    m_fes.pop_back();
    delete fe;
  }
  m_fes.clear();
  if(m_rootfile) delete m_rootfile;
  delete m_rn;
}

void Handler::SetVerbose(bool enable){
  m_verbose=enable;
  for(auto fe: m_fes){
    fe->SetVerbose(enable);
  }
}

void Handler::SetContext(string context){
  m_backend = context;
}

void Handler::SetInterface(string interface){
  m_interface = interface;
}

void Handler::SetRetune(bool enable){
  m_retune=enable;
}

bool Handler::GetRetune(){
  return m_retune;
}

void Handler::SetEnableOutput(bool enable){
  m_output=enable;
}

bool Handler::GetEnableOutput(){
  return m_output;
}

void Handler::SetCharge(uint32_t charge){
  m_charge=charge;
}

uint32_t Handler::GetCharge(){
  return m_charge;
}

uint32_t Handler::GetThreshold(){
  return m_threshold;
}

void Handler::SetThreshold(uint32_t threshold){
  m_threshold=threshold;
}

void Handler::SetOutPath(string path){
  m_outpath=path;
}

string Handler::GetOutPath() {
  return m_outpath;
}

string Handler::GetFullOutPath(){
  return m_fullOutPath;
}

void Handler::SetBusPath(std::string buspath){
  m_buspath=buspath;
 }

string Handler::GetBusPath(){
  return m_buspath;
}

FrontEnd* Handler::GetFE(string name){
  return m_fe[name];
}

uint32_t Handler::GetFEDataElink(string name){
  return m_fe_rx[name];
}

uint32_t Handler::GetFECmdElink(string name){
  return m_fe_tx[name];
}

vector<FrontEnd*> Handler::GetFEs(){
  return m_fes;
}



void Handler::SetMapping(string mapping, bool auto_load){

  for(string const& sdir : m_config_path){
   if(m_verbose) cout << "Handler::SetMapping" << " Testing: " << sdir << mapping << endl;
   m_connectivityPath = sdir+mapping;
   std::ifstream fr(m_connectivityPath);
   if(!fr){continue;}
   cout << "Handler::SetMapping" << " Loading: " << m_connectivityPath << endl;
   json config;
   fr >> config;
   for(auto node : config["connectivity"]){
     AddMapping(node["name"],node["config"],node["tx"],node["rx"],node["host"],node["cmd_port"],node["host"],node["data_port"]);
     if(auto_load and node["enable"]!=0){
       cout << "Handler::SetMapping Adding front end " << node["name"] << endl;
       LoadFE(node["name"]);
     }else if(auto_load){
       cout << "Handler::SetMapping Front end is disabled: " << node["name"] << endl;
     }
   }
   fr.close();
   break;
  }

}

void Handler::AddMapping(string name, string config, uint32_t cmd_elink, uint32_t data_elink, string cmd_host, uint32_t cmd_port, string data_host, uint32_t data_port){
  if(m_verbose){
    cout << "Handler::AddMapping Front-End: "
         << "name: " << name << ", "
         << "config: " << config << ", "
         << "cmd_host: " << cmd_host << ", "
         << "cmd_port: " << cmd_port << ", "
         << "cmd_elink: " << cmd_elink << ", "
         << "data_host: " << data_host << ", "
         << "data_port: " << data_port << ", "
         << "data_elink: " << data_elink << " "
         << endl;
  }
  m_fe[name]=0;
  m_enabled[name]=false;
  m_configs[name]=config;
  m_fe_rx[name]=data_elink;
  m_fe_tx[name]=cmd_elink;
  m_data_port[data_elink]=data_port;
  m_data_host[data_elink]=data_host;
  m_cmd_host[cmd_elink]=cmd_host;
  m_cmd_port[cmd_elink]=cmd_port;
}

void Handler::AddFE(string name, FrontEnd* fe){
  fe->SetName(name);
  fe->SetActive(true);
  m_fe[name]=fe;
  m_fes.push_back(fe);
  m_enabled[name]=true;
}

void Handler::LoadFE(string name, string path){
  cout << "Handler::LoadFE" << endl;
  if(path==""){path=m_configs[name];}
  if(path==""){path=name+".json";}

  FrontEnd * fe = 0;
  for(string const& sdir : m_config_path){
    if(m_verbose) cout << "Handler::AddFE" << " Testing: " << sdir << path << endl;
    ifstream fr(sdir+path);
    if(!fr){
      continue;
    }
    cout << "Handler::AddFE" << " Loading: " << sdir << path << endl;
    stringstream buffer;
    buffer << fr.rdbuf();
    AddFE(name, buffer.str());
    fe=m_fe[name];

    //File was found, no need to keep looking for it
    break;
  }
  if(!fe){cout << "Handler::AddFE Front-end not added. File not found: " << path << endl;}
  else{ if(m_verbose){cout << "Handler::AddFE File correctly loaded: " << path << endl;}}
}

void Handler::AddFE(string name, string sconfig){
  cout << "Handler::AddFE Parsing JSON" << endl;
  json config;
  stringstream(sconfig) >> config;
  //Create the front-end
  if(m_verbose) cout << "Handler::AddFE Create front-end" << endl;
  FrontEnd * fe = new FrontEnd();
  fe->SetVerbose(m_verbose);
  fe->SetName(name);

  //Actually read the file
  if(m_verbose) cout << "Handler::AddFE Reading the config" << endl;

  fe->GetHCC()->SetID(config["HCC"]["ID"]);

  fe->SetActive(true);
  //fe->GetHCC()->SetRegisterStrings(config["HCC"]["regs"]);
  for(uint32_t i=0;i<config["ABCs"]["IDs"].size();i++){
    if(m_verbose) std::cout << "Handler::AddFE ABC id: " << config["ABCs"]["IDs"][i] << std::endl;
    fe->AddABC();
    //cout << "ABC id: " << config["ABCs"]["IDs"][i] << endl;
    fe->GetABC(i)->SetID(config["ABCs"]["IDs"][i]);
    fe->GetABC(i)->SetRegisterStrings(config["ABCs"]["common"]);
    for(auto p : config["ABCs"]["subregs"][i].items()){
      if(m_verbose) std::cout << "Handler::AddFE subRegister: " << p.key() << " value: " << p.value()<< std::endl;
      uint32_t val = 0;
      if (p.key().find("_S") != string::npos){
        string v = p.value();
        val = std::stol (v,nullptr,16);
      }
      else {val = p.value();}
      fe->GetABC(i)->GetSubRegister(p.key())->SetValue(val);
      fe->GetABC(i)->GetSubRegister(p.key())->IsGiven(true);

    }
  }

  m_fe[name]=fe;
  m_fes.push_back(fe);
  m_enabled[name]=true;

  if(m_fe_rx.count(name)==0 or m_fe_tx.count(name)==0){
    cout << "Handler::AddFE Configuration error. Connectivity file does not contains FE: " << name << endl;
    m_enabled[name]=false;
  }
}

void Handler::Connect(){

  vector<uint64_t> vec_elinks;
  
  //TX
  for(auto it : m_fe_tx){
    if(m_enabled[it.first]==false){continue;}
    uint64_t tx_elink = it.second;
    if(std::find(vec_elinks.begin(), vec_elinks.end(), tx_elink) == vec_elinks.end()){
      vec_elinks.push_back(tx_elink);
    }
    m_tx_fes[tx_elink].push_back(m_fe[it.first]);
  }

  //RX
  for(auto it : m_fe_rx){
    if(m_enabled[it.first]==false){continue;}
    uint64_t rx_elink = it.second;
    if(std::find(vec_elinks.begin(), vec_elinks.end(), rx_elink) == vec_elinks.end()){
      vec_elinks.push_back(rx_elink);
    }
    m_rx_fe[rx_elink] = m_fe[it.first];
  }

  //NetioNext
  m_NextConfig = new felix_proxy::ClientThread::Config();
  m_NextConfig->property["local_ip_or_interface"] = m_interface;  
  m_NextConfig->property["log_level"] = (m_verbose?"trace":"error");
  m_NextConfig->property["bus_dir"] = m_buspath;
  m_NextConfig->property["bus_group_name"] = "FELIX";

  m_NextConfig->on_init_callback = []() {
    cout<<"Handler::ClientThread::Initialiasing connection"<<endl;
  };

  m_NextConfig->on_connect_callback = [](const std::uint64_t fid){
    cout<<"Handler::ClientThread::Connecting to FID: 0x"<<hex<<fid<<dec<<endl;
  };

  m_NextConfig->on_disconnect_callback = [](const std::uint64_t fid){
    std::cout<<"Handler::ClientThread::Disconnecting from FID: 0x"<<hex<<fid<<dec<<endl;
  };
  
  m_NextConfig->on_data_callback =  [&](const std::uint64_t rx_elink,
					const std::uint8_t* data,
					const std::size_t size,
					const std::uint8_t status){
    
    if(m_rx_fe.find(rx_elink) != m_rx_fe.end()){
      if(m_verbose) cout << "Handler::Connect Received data from FID: 0x" << hex << rx_elink << dec << " size:" << size << endl;
      m_mutex[rx_elink].lock();
      m_rx_fe[rx_elink]->HandleData((uint8_t*)data,size);
      m_mutex[rx_elink].unlock(); 
    }  
  };

  m_NextClient = new felix_proxy::ClientThread(*m_NextConfig);
  m_NextClient->subscribe(vec_elinks);



}

void Handler::InitRun(){
  cout << "Handler::InitRun" << endl;

  //Start a new run
  cout << "Starting run number: " << m_rn->GetNextRunNumberString(6,true) << endl;

  //Create the output directory
  if(m_verbose) cout << "Handler::InitRun Creating output directory (if doesn't exist yet)" << endl;
  ostringstream cmd;
  cmd << "mkdir -p " << m_outpath << "/" << m_rn->GetRunNumberString(6) << "_" << m_scan;
  system(cmd.str().c_str());
  m_fullOutPath = m_outpath + "/" + m_rn->GetRunNumberString(6) + "_" + m_scan;
/*
  //Open the output log file
  ostringstream logFilePath;
  logFilePath << m_outpath << "/" << m_rn->GetRunNumberString(6) << "_" << m_scan << "/" << "logFile.txt";
  m_logFile.open(logFilePath.str().c_str());
  if(m_verbose) cout << "Handler::InitRun Created log file " << logFilePath.str().c_str() << endl;
  m_logFile << "\"exec\": \"" << m_commandLine << "\"" << endl;
  m_logFile << "\"runNumber\": " << m_rn->GetRunNumberString(6) << endl;
  time_t startTime;
  time(&startTime);
  m_logFile << "\"startTime\": " << ctime(&startTime) << endl;

  //Copy the connectivity file to the log!!!! FIXME
  std::string line;
  std::ifstream connectivityFile(m_connectivityPath);
  while(std::getline(connectivityFile, line)){
    m_logFile << line << endl;
  }
  connectivityFile.close();
*/
  //give up if somebody disabled the output
  if(!m_output) return;

  //Open the output ROOT file
  ostringstream opath;
  opath << m_outpath << "/" << m_rn->GetRunNumberString(6) << "_" << m_scan << "/" << "output.root";
  if(m_verbose) cout << "Handler::InitRun Creating root file " << opath.str().c_str() << endl;
  m_rootfile=TFile::Open(opath.str().c_str(),"RECREATE");
  if(m_verbose) cout << "Handler::InitRun Created root file " << opath.str().c_str() << endl;

}

void Handler::Config(){

  cout<< "Handler::Config" << endl;

  for(auto fe : m_fes){
    //fe->Configure();
    //Send(fe);

    fe->Reset();
    Send(fe);
    fe->WriteHCCRegisters();
    Send(fe);
    fe->WriteABCRegisters();
    Send(fe);

  }

}

void Handler::PreRun(){
  cout << "Handler::PreRun is meant to be extended" << endl;
}

void Handler::Run(){
  cout << "Handler::Run is supposed to be extended" << endl;
}

void Handler::Analysis(){
  cout << "Handler::Analysis is meant to be extended" << endl;
}

void Handler::Disconnect(){}

void Handler::PrepareTrigger(uint32_t latency, bool digital){
  cout << "Handler::PrepareTrigger" << endl;
  for(auto it : m_tx_fes){
    if(m_verbose) cout << "Handler::PrepareTrigger: Composing trigger message for tx " << it.first << endl;
    it.second[0]->Trigger(latency,digital);
    it.second[0]->ProcessCommands();
    m_trigger_msgs[it.first]=vector<uint8_t>();
    for(uint32_t i=0;i<it.second[0]->GetLength();i++){m_trigger_msgs[it.first].push_back(it.second[0]->GetBytes()[i]);}
    }
}

void Handler::PrepareTrigger(Encoder * encoder){
  cout << "Handler::PrepareTrigger" << endl;
  for(auto it : m_tx_fes){
    if(m_verbose) cout << "Handler::PrepareTrigger: Composing trigger message for tx " << it.first << endl;
    encoder->Encode();
    m_trigger_msgs[it.first]=vector<uint8_t>();
    for(uint32_t i=0;i<encoder->GetLength();i++){m_trigger_msgs[it.first].push_back(encoder->GetBytes()[i]);}
  }
}

void Handler::Trigger(){
  for(auto it : m_tx_fes){
    if(m_verbose) cout << "Handler::Trigger: Trigger! for tx " << it.first << endl;
    m_NextClient->send_data(it.first,m_trigger_msgs[it.first].data(),m_trigger_msgs[it.first].size(),true);
  }
}

void Handler::Send(FrontEnd * fe){
  //process the commands from the front-end
  fe->ProcessCommands();
  //quick return
  if(fe->GetLength()==0){return;}
  if(!m_enabled[fe->GetName()]){return;}
  //figure out the tx_elink
  uint32_t tx_elink=m_fe_tx[fe->GetName()];
  //copy the message
  vector<uint8_t> payload(fe->GetLength());
  for(uint32_t i=0;i<fe->GetLength();i++){payload[i]=fe->GetBytes()[i];};
  //send message
  m_NextClient->send_data(tx_elink,payload.data(),payload.size(),true);
  fe->Clear(); 
}

string Handler::GetFEConfig(string name){
  FrontEnd * fe = m_fe[name];
  json config;
  config["HCC"]["ID"]=fe->GetHCC()->GetID();
  config["HCC"]["regs"]=fe->GetHCC()->GetRegisterValues();
  for(uint32_t i=0;i<fe->GetABCs().size();i++){
    config["ABCs"]["IDs"][i] = fe->GetABC(i)->GetID();
    config["ABCs"]["common"] = fe->GetABC(i)->GetRegisterValues();
    for(auto p : fe->GetABC(i)->GetGivenSubRegisters()){
      config["ABCs"]["subregs"][i][p.first]=p.second->GetValueAsString();
    }
  }
  return config.dump();
}

void Handler::SaveConfig(string configuration){
  if(!m_output) return;

  if(configuration=="tuned"){system("mkdir -p ./tuned");}

  for(auto fe: m_fes){
    if(!fe->IsActive()){continue;}
    ostringstream os;
    if(configuration!="tuned"){
      os << m_fullOutPath << "/" << m_configs[fe->GetName()] << "_" << configuration;
    }else{
      os << "./tuned/" << m_configs[fe->GetName()];
    }
    if (m_configs[fe->GetName()].find(".json")==string::npos) os<<".json";
    json config;
    //---config.parse(GetFEConfig(fe->GetName()));
    ofstream fw(os.str());
    fw << setw(4) << config;
    fw.close();
  }
}

string Handler::GetResults(){
  string results;
  TIter next(m_rootfile->GetListOfKeys());
  TKey *key;
  while ((key = (TKey*)next())) {
    TString json = TBufferJSON::ConvertToJSON(m_rootfile->Get(key->GetName()));
    results.append(json);
  }
  return results;
}

void Handler::Save(){

  time_t endTime;
  time(&endTime);
  //m_log->Get(Logger::INFO) << "endTime: " << ctime(&endTime);
  //cout << "Save log file: " << m_log->GetName() << endl;
  //m_log->Close();

  if(!m_output) return;

  cout << "Save ROOT file: " << m_rootfile->GetName() << endl;
  m_rootfile->Close();

}


void Handler::SaveFE(FrontEnd * fe, string path){
  json config;
  config["HCC"]["ID"]=fe->GetHCC()->GetID();
  config["HCC"]["regs"]=fe->GetHCC()->GetRegisterValues();
  for(uint32_t i=0;i<fe->GetABCs().size();i++){
    config["ABCs"]["IDs"][i] = fe->GetABC(i)->GetID();
    config["ABCs"]["common"] = fe->GetABC(i)->GetRegisterValues();
    for(auto p : fe->GetABC(i)->GetGivenSubRegisters()){
      config["ABCs"]["subregs"][i][p.first]=p.second->GetValueAsString();
    }
  }
  ofstream fw(path);
  fw << setw(4) << config;
  fw.close();
}

