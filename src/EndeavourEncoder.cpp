#include "Strips/EndeavourEncoder.h"
#include <iostream>
#include <sstream>
#include <iomanip>

using namespace std;
using namespace Strips;

EndeavourEncoder::EndeavourEncoder(){
  m_size = 1e6;
  m_bytes.reserve(m_size);
  m_bytes.resize(m_size,0);
  m_length = 0;
  m_read = new EndeavourRead();
  m_next = new EndeavourNext();
  m_write = new EndeavourWrite();
  m_setid = new EndeavourSetid();
}

EndeavourEncoder::~EndeavourEncoder(){
  m_bytes.clear();
  ClearCommands();
  delete m_read;
  delete m_next;
  delete m_write;
  delete m_setid;
}

void EndeavourEncoder::AddBytes(uint8_t *bytes, uint32_t pos, uint32_t len){
  for(uint32_t i=pos;i<pos+len;i++){
    m_bytes[m_length]=bytes[i];
    m_length++;
  }
}

void EndeavourEncoder::SetBytes(uint8_t *bytes, uint32_t len, bool reversed){
  m_length=len;
  if(len>m_bytes.size()){m_bytes.resize(len);}
  if(!reversed){for(uint32_t i=0;i<len;i++){m_bytes[i]=bytes[i];}}
  else         {for(uint32_t i=0;i<len;i++){m_bytes[len-i-1]=bytes[i];}}
}

void EndeavourEncoder::AddCommand(Endeavour *cmd){
  m_cmds.push_back(cmd);
}
  
void EndeavourEncoder::ClearBytes(){
  m_length=0;
}
  
void EndeavourEncoder::ClearCommands(){
  while(!m_cmds.empty()){
    Endeavour* cmd = m_cmds.back();
    delete cmd;
    m_cmds.pop_back();
  }
}

void EndeavourEncoder::Clear(){
  ClearBytes();
  ClearCommands();
}
  
string EndeavourEncoder::GetByteString(){
  ostringstream os;
  os << hex; 
  for(uint32_t pos=0; pos<m_length; pos++){
    os << setw(2) << setfill('0') << (uint32_t) m_bytes[pos] << " ";
  }
  return os.str();
}

void EndeavourEncoder::GetBytes(uint8_t * bytes, uint32_t & length, bool reversed){
  length=m_length;
  if(!reversed){for(uint32_t i=0;i<m_length;i++){bytes[i]=m_bytes[i];}}
  else         {for(uint32_t i=0;i<m_length;i++){bytes[m_length-i-1]=m_bytes[i];}}
}

uint8_t * EndeavourEncoder::GetBytes(bool reversed){
  if(!reversed){return m_bytes.data();}
  m_rbytes.resize(m_length);
  for(uint32_t i=0;i<m_length;i++){
    m_rbytes[m_length-i-1]=m_bytes[i];
  }
  return m_rbytes.data();
}
  
uint32_t EndeavourEncoder::GetLength(){
  return m_length;
}
  
vector<Endeavour*> & EndeavourEncoder::GetCommands(){
  return m_cmds;
}

void EndeavourEncoder::Encode(){
  uint32_t pos=0;
  for(uint32_t i=0;i<m_cmds.size();i++){
    if(m_size < pos+1000){m_size+=1000;m_bytes.resize(m_size,0);}
    pos+=m_cmds[i]->Pack(&m_bytes[pos]);
  }
  m_length=pos;
}

void EndeavourEncoder::Decode(bool verbose){
  ClearCommands();
  uint32_t pos=0;
  uint32_t nb=0;
  uint32_t tnb=m_length;
  Endeavour * cmd=0;

  while(pos<tnb){
    if((nb=m_read->Unpack(&m_bytes[pos],tnb-pos))>0){
      if(verbose) cout << "EndeavourEncoder::Decode Read" << endl;
      cmd=m_read; m_read=new EndeavourRead();
    }
    else if((nb=m_next->Unpack(&m_bytes[pos],tnb-pos))>0){
      if(verbose) cout << "EndeavourEncoder::Decode Next" << endl;
      cmd=m_next; m_next=new EndeavourNext();
    }
    else if((nb=m_write->Unpack(&m_bytes[pos],tnb-pos))>0){
      if(verbose) cout << "EndeavourEncoder::Decode Write" << endl;
      cmd=m_write; m_write=new EndeavourWrite();
    }
    else if((nb=m_setid->Unpack(&m_bytes[pos],tnb-pos))>0){
      if(verbose) cout << "EndeavourEncoder::Decode Setid" << endl;
      cmd=m_setid; m_setid=new EndeavourSetid();
    }
    else{
      cout << "EndeavourEncoder::Decode Cannot decode byte sequence: "
          << "0x" << hex << setw(2) << setfill('0') << (uint32_t) m_bytes[pos] << dec
          << " at index: " << pos
          << " ...skipping" << endl;
      pos++;
      continue;
    }
    if(!cmd){pos++; continue;}
    m_cmds.push_back(cmd);
    pos+=nb;
    if(verbose) {cout << "EndeavourEncoder::Decode pos=" << pos << " tnb=" << tnb << endl;}
  }
}
