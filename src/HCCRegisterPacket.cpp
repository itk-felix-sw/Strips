#include "Strips/HCCRegisterPacket.h"
#include <iostream>
#include <sstream>

using namespace std;
using namespace Strips;

HCCRegisterPacket::HCCRegisterPacket(){
  m_type=Packet::TYP_HCC_RR;
  m_address=0;
  m_data=0;
}

HCCRegisterPacket::HCCRegisterPacket(uint32_t typ,uint8_t address, uint32_t data){
  m_type=(typ==Packet::TYP_HCC_HPR?Packet::TYP_HCC_HPR:Packet::TYP_HCC_RR);
  m_address=address;
  m_data=data;
}

HCCRegisterPacket::HCCRegisterPacket(HCCRegisterPacket *copy){
  m_type=copy->m_type;
  m_address=copy->m_address;
  m_data=copy->m_data;
}

HCCRegisterPacket::~HCCRegisterPacket(){
  //nothing to do
}

string HCCRegisterPacket::ToString(){
  ostringstream os;
  os << "HCCRegisterPacket "
     << "Type: " << Packet::typeNames[m_type] << " (0x" << hex << m_type << dec << ") "
     << "Address: 0x" << hex << (uint32_t) m_address << dec << ", "
     << "Data: 0x" << hex << m_data << dec;
  return os.str();
}

void HCCRegisterPacket::SetAddress(uint32_t address){
  m_address=address;
}

uint32_t HCCRegisterPacket::GetAddress(){
  return m_address;
}

void HCCRegisterPacket::SetData(uint32_t data){
  m_data=data;
}

uint32_t HCCRegisterPacket::GetData(){
  return m_data;
}

uint32_t HCCRegisterPacket::Pack(uint8_t * bytes){

  bytes[0] = ((m_type & 0x0F) << 4) | ((m_address >> 4) & 0x0F);
  bytes[1] = (m_address & 0x0F) << 4;
  bytes[1] = (m_data >> 28) & 0x0F;
  bytes[2] = (m_data >> 20) & 0xFF;
  bytes[3] = (m_data >> 12) & 0xFF;
  bytes[4] = (m_data >>  4) & 0xFF;
  bytes[5] = ((m_data & 0x0F) << 4);

  return 6;
}

uint32_t HCCRegisterPacket::Unpack(uint8_t * bytes, uint32_t maxlen){
  m_type = (bytes[0]&0xF0) >> 4;

  if(m_type!=Packet::TYP_HCC_HPR and m_type!=Packet::TYP_HCC_RR){
    return 0;
  }
  
  m_address = (bytes[0] & 0x0F) << 4;
  m_address = (bytes[1] & 0xF0) >> 4;
  m_data  = (bytes[1] & 0x0F) << 28;
  m_data |= (bytes[2] & 0xFF) << 20;
  m_data |= (bytes[3] & 0xFF) << 20;
  m_data |= (bytes[4] & 0xFF) <<  4;
  m_data |= (bytes[5] & 0xF0) >>  4;

  return 6;
}


