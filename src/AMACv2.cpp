#include <Strips/AMACv2.h>

using namespace std;
using namespace Strips;


AMACv2::AMACv2(){

  m_registers.resize(172);

  m_registerNames={  
    {"Status",REG_Status},{"HxFlags",REG_HxFlags},{"HyFlags",REG_HyFlags},{"HV0Flags",REG_HV0Flags},{"HV2Flags",REG_HV2Flags},{"DCDCflags",REG_DCDCflags},{"WRNflags",REG_WRNflags},{"SynFlags",REG_SynFlags},{"Value0",REG_Value0},{"Value1",REG_Value1},{"Value2",REG_Value2},{"Value3",REG_Value3},{"Value4",REG_Value4},{"Value5",REG_Value5},{"SerNum",REG_SerNum},{"FlagResets",REG_FlagResets},{"LogicReset",REG_LogicReset},{"HardReset",REG_HardReset},{"CntSet",REG_CntSet},{"CntSetC",REG_CntSetC},{"DCDCen",REG_DCDCen},{"DCDCenC",REG_DCDCenC},{"Ilock",REG_Ilock},{"IlockC",REG_IlockC},{"RstCnt",REG_RstCnt},{"RstCntC",REG_RstCntC},{"AMen",REG_AMen},{"AMenC",REG_AMenC},{"AMpwr",REG_AMpwr},{"AMpwrC",REG_AMpwrC},{"BgCnt",REG_BgCnt},{"AMcnt",REG_AMcnt},{"Dacs0",REG_Dacs0},{"DACbias",REG_DACbias},{"AMACcnt",REG_AMACcnt},{"NTC",REG_NTC},{"LvCurCal",REG_LvCurCal},{"HxICm_cfg",REG_HxICm_cfg},{"HyICm_cfg",REG_HyICm_cfg},{"HV0ICm_cfg",REG_HV0ICm_cfg},{"HV2ICm_cfg",REG_HV2ICm_cfg},{"DCDCICm_cfg",REG_DCDCICm_cfg},{"WRNICm_cfg",REG_WRNICm_cfg},{"HxTlut",REG_HxTlut},{"HxModlut1",REG_HxModlut1},{"HxModlut2",REG_HxModlut2},{"HyTlut",REG_HyTlut},{"HyModlut1",REG_HyModlut1},{"HyModlut2",REG_HyModlut2},{"HV0Tlut",REG_HV0Tlut},{"HV0Modlut1",REG_HV0Modlut1},{"HV0Modlut2",REG_HV0Modlut2},{"HV2Tlut",REG_HV2Tlut},{"HV2Modlut1",REG_HV2Modlut1},{"HV2Modlut2",REG_HV2Modlut2},{"DCDCTlut",REG_DCDCTlut},{"DCDCModlut1",REG_DCDCModlut1},{"DCDCModlut2",REG_DCDCModlut2},{"WRNTlut",REG_WRNTlut},{"WRNModlut1",REG_WRNModlut1},{"WRNModlut2",REG_WRNModlut2},{"HxFlagEn",REG_HxFlagEn},{"HyFlagEn",REG_HyFlagEn},{"HV0FlagEn",REG_HV0FlagEn},{"HV2FlagEn",REG_HV2FlagEn},{"DCDCFlagEn",REG_DCDCFlagEn},{"WRNFlagEn",REG_WRNFlagEn},{"SynFlagEn",REG_SynFlagEn},{"HxLoTh0",REG_HxLoTh0},{"HxLoTh1",REG_HxLoTh1},{"HxLoTh2",REG_HxLoTh2},{"HxLoTh3",REG_HxLoTh3},{"HxLoTh4",REG_HxLoTh4},{"HxLoTh5",REG_HxLoTh5},{"HxHiTh0",REG_HxHiTh0},{"HxHiTh1",REG_HxHiTh1},{"HxHiTh2",REG_HxHiTh2},{"HxHiTh3",REG_HxHiTh3},{"HxHiTh4",REG_HxHiTh4},{"HxHiTh5",REG_HxHiTh5},{"HyLoTh0",REG_HyLoTh0},{"HyLoTh1",REG_HyLoTh1},{"HyLoTh2",REG_HyLoTh2},{"HyLoTh3",REG_HyLoTh3},{"HyLoTh4",REG_HyLoTh4},{"HyLoTh5",REG_HyLoTh5},{"HyHiTh0",REG_HyHiTh0},{"HyHiTh1",REG_HyHiTh1},{"HyHiTh2",REG_HyHiTh2},{"HyHiTh3",REG_HyHiTh3},{"HyHiTh4",REG_HyHiTh4},{"HyHiTh5",REG_HyHiTh5},{"HV0LoTh0",REG_HV0LoTh0},{"HV0LoTh1",REG_HV0LoTh1},{"HV0LoTh2",REG_HV0LoTh2},{"HV0LoTh3",REG_HV0LoTh3},{"HV0LoTh4",REG_HV0LoTh4},{"HV0LoTh5",REG_HV0LoTh5},{"HV0HiTh0",REG_HV0HiTh0},{"HV0HiTh1",REG_HV0HiTh1},{"HV0HiTh2",REG_HV0HiTh2},{"HV0HiTh3",REG_HV0HiTh3},{"HV0HiTh4",REG_HV0HiTh4},{"HV0HiTh5",REG_HV0HiTh5},{"HV2LoTh0",REG_HV2LoTh0},{"HV2LoTh1",REG_HV2LoTh1},{"HV2LoTh2",REG_HV2LoTh2},{"HV2LoTh3",REG_HV2LoTh3},{"HV2LoTh4",REG_HV2LoTh4},{"HV2LoTh5",REG_HV2LoTh5},{"HV2HiTh0",REG_HV2HiTh0},{"HV2HiTh1",REG_HV2HiTh1},{"HV2HiTh2",REG_HV2HiTh2},{"HV2HiTh3",REG_HV2HiTh3},{"HV2HiTh4",REG_HV2HiTh4},{"HV2HiTh5",REG_HV2HiTh5},{"DCDCLoTh0",REG_DCDCLoTh0},{"DCDCLoTh1",REG_DCDCLoTh1},{"DCDCLoTh2",REG_DCDCLoTh2},{"DCDCLoTh3",REG_DCDCLoTh3},{"DCDCLoTh4",REG_DCDCLoTh4},{"DCDCLoTh5",REG_DCDCLoTh5},{"DCDCHiTh0",REG_DCDCHiTh0},{"DCDCHiTh1",REG_DCDCHiTh1},{"DCDCHiTh2",REG_DCDCHiTh2},{"DCDCHiTh3",REG_DCDCHiTh3},{"DCDCHiTh4",REG_DCDCHiTh4},{"DCDCHiTh5",REG_DCDCHiTh5},{"WRNLoTh0",REG_WRNLoTh0},{"WRNLoTh1",REG_WRNLoTh1},{"WRNLoTh2",REG_WRNLoTh2},{"WRNLoTh3",REG_WRNLoTh3},{"WRNLoTh4",REG_WRNLoTh4},{"WRNLoTh5",REG_WRNLoTh5},{"WRNHiTh0",REG_WRNHiTh0},{"WRNHiTh1",REG_WRNHiTh1},{"WRNHiTh2",REG_WRNHiTh2},{"WRNHiTh3",REG_WRNHiTh3},{"WRNHiTh4",REG_WRNHiTh4},{"WRNHiTh5",REG_WRNHiTh5}  };

m_subRegisterNames={
{"StatusAM",StatusAM  },
{"StatusWARN",  StatusWARN},
{"StatusDCDC",  StatusDCDC},
{"StatusHV3",  StatusHV3},
{"StatusHV2",  StatusHV2},
{"StatusHV1",  StatusHV1},
{"StatusHV0",  StatusHV0},
{"StatusY2LDO",  StatusY2LDO},
{"StatusY1LDO",  StatusY1LDO},
{"StatusY0LDO",  StatusY0LDO},
{"StatusX2LDO",  StatusX2LDO},
{"StatusX1LDO",  StatusX1LDO},
{"StatusX0LDO",  StatusX0LDO},
{"StatusGPI",  StatusGPI},
{"StatusPGOOD",  StatusPGOOD},
{"StatusILockWARN",  StatusILockWARN},
{"StatusILockDCDC",  StatusILockDCDC},
{"StatusILockHV2",  StatusILockHV2},
{"StatusILockHV0",  StatusILockHV0},
{"StatusILockYLDO",  StatusILockYLDO},
{"StatusILockxLDO",  StatusILockxLDO},
{"HxFlagsHi",  HxFlagsHi},
{"HxFlagsLo",  HxFlagsLo},
{"HyFlagsHi",  HyFlagsHi},
{"HyFlagsLo",  HyFlagsLo},
{"HV0FlagsHi",  HV0FlagsHi},
{"HV0FlagsLo",  HV0FlagsLo},
{"HV2FlagsHi",  HV2FlagsHi},
{"HV2FlagsLo",  HV2FlagsLo},
{"DCDCflagsHi",  DCDCflagsHi},
{"DCDCflagsLo",  DCDCflagsLo},
{"WRNflagsHi",  WRNflagsHi},
{"WRNflagsLo",  WRNflagsLo},
{"SynFlagsWRN",  SynFlagsWRN},
{"SynFlagsDCDC",  SynFlagsDCDC},
{"SynFlagsHV2",  SynFlagsHV2},
{"SynFlagsHV0",  SynFlagsHV0},
{"SynFlagsHy",  SynFlagsHy},
{"SynFlagsHx",  SynFlagsHx},
{"Value0AMen",  Value0AMen},
{"Ch0Value",  Ch0Value},
{"Ch1Value",  Ch1Value},
{"Ch2Value",  Ch2Value},
{"Value1AMen",  Value1AMen},
{"Ch3Value",  Ch3Value},
{"Ch4Value",  Ch4Value},
{"Ch5Value",  Ch5Value},
{"Value2AMen",  Value2AMen},
{"Ch6Value",  Ch6Value},
{"Ch7Value",  Ch7Value},
{"Ch8Value",  Ch8Value},
{"Value3AMen",  Value3AMen},
{"Ch9Value",  Ch9Value},
{"Ch10Value",  Ch10Value},
{"Ch11Value",  Ch11Value},
{"Value4AMen",  Value4AMen},
{"Ch12Value",  Ch12Value},
{"Ch13Value",  Ch13Value},
{"Ch14Value",  Ch14Value},
{"Value5AMen",  Value5AMen},
{"Ch15Value",  Ch15Value},
{"PadID",  PadID},
{"SerNum",  SerNum},
{"FlagResetWRN",  FlagResetWRN},
{"FlagResetDCDC",  FlagResetDCDC},
{"FlagResetHV2",  FlagResetHV2},
{"FlagResetHV0",  FlagResetHV0},
{"FlagResetXLDO",  FlagResetXLDO},
{"FlagResetYLDO",  FlagResetYLDO},
{"LogicReset",  LogicReset},
{"HardReset",  HardReset},
{"CntSetHV3frq",  CntSetHV3frq},
{"CntSetHV3en",  CntSetHV3en},
{"CntSetHV2frq",  CntSetHV2frq},
{"CntSetHV2en",  CntSetHV2en},
{"CntSetHV1frq",  CntSetHV1frq},
{"CntSetHV1en",  CntSetHV1en},
{"CntSetHV0frq",  CntSetHV0frq},
{"CntSetHV0en",  CntSetHV0en},
{"CntSetHyLDO2en",  CntSetHyLDO2en},
{"CntSetHyLDO1en",  CntSetHyLDO1en},
{"CntSetHyLDO0en",  CntSetHyLDO0en},
{"CntSetHxLDO2en",  CntSetHxLDO2en},
{"CntSetHxLDO1en",  CntSetHxLDO1en},
{"CntSetHxLDO0en",  CntSetHxLDO0en},
{"CntSetWARN",  CntSetWARN},
{"CntSetCHV3frq",  CntSetCHV3frq},
{"CntSetCHV3en",  CntSetCHV3en},
{"CntSetCHV2frq",  CntSetCHV2frq},
{"CntSetCHV2en",  CntSetCHV2en},
{"CntSetCHV1frq",  CntSetCHV1frq},
{"CntSetCHV1en",  CntSetCHV1en},
{"CntSetCHV0frq",  CntSetCHV0frq},
{"CntSetCHV0en",  CntSetCHV0en},
{"CntSetCHyLDO2en",  CntSetCHyLDO2en},
{"CntSetCHyLDO1en",  CntSetCHyLDO1en},
{"CntSetCHyLDO0en",  CntSetCHyLDO0en},
{"CntSetCHxLDO2en",  CntSetCHxLDO2en},
{"CntSetCHxLDO1en",  CntSetCHxLDO1en},
{"CntSetCHxLDO0en",  CntSetCHxLDO0en},
{"CntSetCWARN",  CntSetCWARN},
{"DCDCAdj",  DCDCAdj},
{"DCDCen",  DCDCen},
{"DCDCAdjC",  DCDCAdjC},
{"DCDCenC",  DCDCenC},
{"IlockWRN",  IlockWRN},
{"IlockDCDC",  IlockDCDC},
{"IlockHV2",  IlockHV2},
{"IlockHV0",  IlockHV0},
{"IlockHy",  IlockHy},
{"IlockHx",  IlockHx},
{"IlockCWRN",  IlockCWRN},
{"IlockCDCDC",  IlockCDCDC},
{"IlockCHV2",  IlockCHV2},
{"IlockCHV0",  IlockCHV0},
{"IlockCHy",  IlockCHy},
{"IlockCHx",  IlockCHx},
{"RstCntHyHCCresetB",  RstCntHyHCCresetB},
{"RstCntHxHCCresetB",  RstCntHxHCCresetB},
{"RstCntOF",  RstCntOF},
{"RstCntCHyHCCresetB",  RstCntCHyHCCresetB},
{"RstCntCHxHCCresetB",  RstCntCHxHCCresetB},
{"RstCntCOF",  RstCntCOF},
{"AMzeroCalib",  AMzeroCalib},
{"AMen",  AMen},
{"AMzeroCalibC",  AMzeroCalibC},
{"AMenC",  AMenC},
{"ReqDCDCPGOOD",  ReqDCDCPGOOD},
{"DCDCenToPwrAMAC",  DCDCenToPwrAMAC},
{"ReqDCDCPGOODC",  ReqDCDCPGOODC},
{"DCDCenToPwrAMACC",  DCDCenToPwrAMACC},
{"AMbgen",  AMbgen },
{"AMbg",  AMbg   },
{"VDDbgen",  VDDbgen},
{"VDDbg",  VDDbg  },
{"AMintCalib",  AMintCalib},
{"Ch13Mux",  Ch13Mux},
{"Ch12Mux",  Ch12Mux},
{"Ch5Mux",  Ch5Mux},
{"Ch4Mux",  Ch4Mux},
{"Ch3Mux",  Ch3Mux},
{"DACShunty",  DACShunty},
{"DACShuntx",  DACShuntx},
{"DACCALy",  DACCALy},
{"DACCalx",  DACCalx},
{"DACbias",  DACbias},
{"HVcurGain",  HVcurGain },
{"DRcomMode",  DRcomMode },
{"DRcurr",  DRcurr    },
{"RingOscFrq",  RingOscFrq},
{"NTCpbCal",  NTCpbCal       },
{"NTCpbSenseRange",  NTCpbSenseRange},
{"NTCy0Cal",  NTCy0Cal       },
{"NTCy0SenseRange",  NTCy0SenseRange},
{"NTCx0Cal",  NTCx0Cal       },
{"NTCx0SenseRange",  NTCx0SenseRange},
{"DCDCoOffset",  DCDCoOffset     },
{"DCDCoZeroReading",  DCDCoZeroReading},
{"DCDCoN",  DCDCoN          },
{"DCDCoP",  DCDCoP          },
{"DCDCiZeroReading",  DCDCiZeroReading},
{"DCDCiRangeSW",  DCDCiRangeSW    },
{"DCDCiOffset",  DCDCiOffset     },
{"DCDCiP",  DCDCiP          },
{"DCDCiN",  DCDCiN          },
{"HxLAM",  HxLAM},
{"HxFlagsLatch",  HxFlagsLatch},
{"HxFlagsLogic",  HxFlagsLogic},
{"HxFlagValid",  HxFlagValid},
{"HxFlagsValidConf",  HxFlagsValidConf},
{"HyLAM",  HyLAM},
{"HyFlagsLatch",  HyFlagsLatch},
{"HyFlagsLogic",  HyFlagsLogic},
{"HyFlagValid",  HyFlagValid},
{"HyFlagsValidConf",  HyFlagsValidConf},
{"HV0LAM",  HV0LAM},
{"HV0FlagsLatch",  HV0FlagsLatch},
{"HV0FlagsLogic",  HV0FlagsLogic},
{"HV0FlagValid",  HV0FlagValid},
{"HV0FlagsValidConf",  HV0FlagsValidConf},
{"HV2LAM",  HV2LAM},
{"HV2FlagsLatch",  HV2FlagsLatch},
{"HV2FlagsLogic",  HV2FlagsLogic},
{"HV2FlagValid",  HV2FlagValid},
{"HV2FlagsValidConf",  HV2FlagsValidConf},
{"DCDCLAM",  DCDCLAM},
{"DCDCFlagsLatch",  DCDCFlagsLatch},
{"DCDCFlagsLogic",  DCDCFlagsLogic},
{"DCDCFlagValid",  DCDCFlagValid},
{"DCDCFlagsValidConf",  DCDCFlagsValidConf},
{"WRNLAM",  WRNLAM},
{"WRNFlagsLatch",  WRNFlagsLatch},
{"WRNFlagsLogic",  WRNFlagsLogic},
{"WRNFlagValid",  WRNFlagValid},
{"WRNFlagsValidConf",  WRNFlagsValidConf},
{"HxTlut",  HxTlut},
{"HxModlut1",  HxModlut1},
{"HxModlut2",  HxModlut2},
{"HyTlut",  HyTlut},
{"HyModlut1",  HyModlut1},
{"HyModlut2",  HyModlut2},
{"HV0Tlut",  HV0Tlut},
{"HV0Modlut1",  HV0Modlut1},
{"HV0Modlut2",  HV0Modlut2},
{"HV2Tlut",  HV2Tlut},
{"HV2Modlut1",  HV2Modlut1},
{"HV2Modlut2",  HV2Modlut2},
{"DCDCTlut",  DCDCTlut},
{"DCDCModlut1",  DCDCModlut1},
{"DCDCModlut2",  DCDCModlut2},
{"WRNTlut",  WRNTlut},
{"WRNModlut1",  WRNModlut1},
{"WRNModlut2",  WRNModlut2},
{"HxFlagsEnHi",  HxFlagsEnHi},
{"HxFlagsEnLo",  HxFlagsEnLo},
{"HyFlagsEnHi",  HyFlagsEnHi},
{"HyFlagsEnLo",  HyFlagsEnLo},
{"HV0FlagsEnHi",  HV0FlagsEnHi},
{"HV0FlagsEnLo",  HV0FlagsEnLo},
{"HV2FlagsEnHi",  HV2FlagsEnHi},
{"HV2FlagsEnLo",  HV2FlagsEnLo},
{"DCDCFlagsEnHi",  DCDCFlagsEnHi},
{"DCDCFlagsEnLo",  DCDCFlagsEnLo},
{"WRNFlagsEnHi",  WRNFlagsEnHi},
{"WRNFlagsEnLo",  WRNFlagsEnLo},
{"WRNsynFlagEnHi",  WRNsynFlagEnHi},
{"WRNsynFlagEnLo",  WRNsynFlagEnLo},
{"DCDCsynFlagEnHi",  DCDCsynFlagEnHi},
{"DCDCsynFlagEnLo",  DCDCsynFlagEnLo},
{"HV2synFlagEnHi",  HV2synFlagEnHi},
{"HV2synFlagEnLo",  HV2synFlagEnLo},
{"HV0synFlagEnHi",  HV0synFlagEnHi},
{"HV0synFlagEnLo",  HV0synFlagEnLo},
{"HysynFlagEnHi",  HysynFlagEnHi},
{"HysynFlagEnLo",  HysynFlagEnLo},
{"HxsynFlagEnHi",  HxsynFlagEnHi},
{"HxsynFlagEnLo",  HxsynFlagEnLo},
{"HxLoThCh0",  HxLoThCh0},
{"HxLoThCh1",  HxLoThCh1},
{"HxLoThCh2",  HxLoThCh2},
{"HxLoThCh3",  HxLoThCh3},
{"HxLoThCh4",  HxLoThCh4},
{"HxLoThCh5",  HxLoThCh5},
{"HxLoThCh6",  HxLoThCh6},
{"HxLoThCh7",  HxLoThCh7},
{"HxLoThCh8",  HxLoThCh8},
{"HxLoThCh9",  HxLoThCh9},
{"HxLoThCh10",  HxLoThCh10},
{"HxLoThCh11",  HxLoThCh11},
{"HxLoThCh12",  HxLoThCh12},
{"HxLoThCh13",  HxLoThCh13},
{"HxLoThCh14",  HxLoThCh14},
{"HxLoThCh15",  HxLoThCh15},
{"HxHiThCh0",  HxHiThCh0},
{"HxHiThCh1",  HxHiThCh1},
{"HxHiThCh2",  HxHiThCh2},
{"HxHiThCh3",  HxHiThCh3},
{"HxHiThCh4",  HxHiThCh4},
{"HxHiThCh5",  HxHiThCh5},
{"HxHiThCh6",  HxHiThCh6},
{"HxHiThCh7",  HxHiThCh7},
{"HxHiThCh8",  HxHiThCh8},
{"HxHiThCh9",  HxHiThCh9},
{"HxHiThCh10",  HxHiThCh10},
{"HxHiThCh11",  HxHiThCh11},
{"HxHiThCh12",  HxHiThCh12},
{"HxHiThCh13",  HxHiThCh13},
{"HxHiThCh14",  HxHiThCh14},
{"HxHiThCh15",  HxHiThCh15},
{"HyLoThCh0",  HyLoThCh0},
{"HyLoThCh1",  HyLoThCh1},
{"HyLoThCh2",  HyLoThCh2},
{"HyLoThCh3",  HyLoThCh3},
{"HyLoThCh4",  HyLoThCh4},
{"HyLoThCh5",  HyLoThCh5},
{"HyLoThCh6",  HyLoThCh6},
{"HyLoThCh7",  HyLoThCh7},
{"HyLoThCh8",  HyLoThCh8},
{"HyLoThCh9",  HyLoThCh9},
{"HyLoThCh10",  HyLoThCh10},
{"HyLoThCh11",  HyLoThCh11},
{"HyLoThCh12",  HyLoThCh12},
{"HyLoThCh13",  HyLoThCh13},
{"HyLoThCh14",  HyLoThCh14},
{"HyLoThCh15",  HyLoThCh15},
{"HyHiThCh0",  HyHiThCh0},
{"HyHiThCh1",  HyHiThCh1},
{"HyHiThCh2",  HyHiThCh2},
{"HyHiThCh3",  HyHiThCh3},
{"HyHiThCh4",  HyHiThCh4},
{"HyHiThCh5",  HyHiThCh5},
{"HyHiThCh6",  HyHiThCh6},
{"HyHiThCh7",  HyHiThCh7},
{"HyHiThCh8",  HyHiThCh8},
{"HyHiThCh9",  HyHiThCh9},
{"HyHiThCh10",  HyHiThCh10},
{"HyHiThCh11",  HyHiThCh11},
{"HyHiThCh12",  HyHiThCh12},
{"HyHiThCh13",  HyHiThCh13},
{"HyHiThCh14",  HyHiThCh14},
{"HyHiThCh15",  HyHiThCh15},
{"HV0LoThCh0",  HV0LoThCh0},
{"HV0LoThCh1",  HV0LoThCh1},
{"HV0LoThCh2",  HV0LoThCh2},
{"HV0LoThCh3",  HV0LoThCh3},
{"HV0LoThCh4",  HV0LoThCh4},
{"HV0LoThCh5",  HV0LoThCh5},
{"HV0LoThCh6",  HV0LoThCh6},
{"HV0LoThCh7",  HV0LoThCh7},
{"HV0LoThCh8",  HV0LoThCh8},
{"HV0LoThCh9",  HV0LoThCh9},
{"HV0LoThCh10",  HV0LoThCh10},
{"HV0LoThCh11",  HV0LoThCh11},
{"HV0LoThCh12",  HV0LoThCh12},
{"HV0LoThCh13",  HV0LoThCh13},
{"HV0LoThCh14",  HV0LoThCh14},
{"HV0LoThCh15",  HV0LoThCh15},
{"HV0HiThCh0",  HV0HiThCh0},
{"HV0HiThCh1",  HV0HiThCh1},
{"HV0HiThCh2",  HV0HiThCh2},
{"HV0HiThCh3",  HV0HiThCh3},
{"HV0HiThCh4",  HV0HiThCh4},
{"HV0HiThCh5",  HV0HiThCh5},
{"HV0HiThCh6",  HV0HiThCh6},
{"HV0HiThCh7",  HV0HiThCh7},
{"HV0HiThCh8",  HV0HiThCh8},
{"HV0HiThCh9",  HV0HiThCh9},
{"HV0HiThCh10",  HV0HiThCh10},
{"HV0HiThCh11",  HV0HiThCh11},
{"HV0HiThCh12",  HV0HiThCh12},
{"HV0HiThCh13",  HV0HiThCh13},
{"HV0HiThCh14",  HV0HiThCh14},
{"HV0HiThCh15",  HV0HiThCh15},
{"HV2LoThCh0",  HV2LoThCh0},
{"HV2LoThCh1",  HV2LoThCh1},
{"HV2LoThCh2",  HV2LoThCh2},
{"HV2LoThCh3",  HV2LoThCh3},
{"HV2LoThCh4",  HV2LoThCh4},
{"HV2LoThCh5",  HV2LoThCh5},
{"HV2LoThCh6",  HV2LoThCh6},
{"HV2LoThCh7",  HV2LoThCh7},
{"HV2LoThCh8",  HV2LoThCh8},
{"HV2LoThCh9",  HV2LoThCh9},
{"HV2LoThCh10",  HV2LoThCh10},
{"HV2LoThCh11",  HV2LoThCh11},
{"HV2LoThCh12",  HV2LoThCh12},
{"HV2LoThCh13",  HV2LoThCh13},
{"HV2LoThCh14",  HV2LoThCh14},
{"HV2LoThCh15",  HV2LoThCh15},
{"HV2HiThCh0",  HV2HiThCh0},
{"HV2HiThCh1",  HV2HiThCh1},
{"HV2HiThCh2",  HV2HiThCh2},
{"HV2HiThCh3",  HV2HiThCh3},
{"HV2HiThCh4",  HV2HiThCh4},
{"HV2HiThCh5",  HV2HiThCh5},
{"HV2HiThCh6",  HV2HiThCh6},
{"HV2HiThCh7",  HV2HiThCh7},
{"HV2HiThCh8",  HV2HiThCh8},
{"HV2HiThCh9",  HV2HiThCh9},
{"HV2HiThCh10",  HV2HiThCh10},
{"HV2HiThCh11",  HV2HiThCh11},
{"HV2HiThCh12",  HV2HiThCh12},
{"HV2HiThCh13",  HV2HiThCh13},
{"HV2HiThCh14",  HV2HiThCh14},
{"HV2HiThCh15",  HV2HiThCh15},
{"DCDCLoThCh0",  DCDCLoThCh0},
{"DCDCLoThCh1",  DCDCLoThCh1},
{"DCDCLoThCh2",  DCDCLoThCh2},
{"DCDCLoThCh3",  DCDCLoThCh3},
{"DCDCLoThCh4",  DCDCLoThCh4},
{"DCDCLoThCh5",  DCDCLoThCh5},
{"DCDCLoThCh6",  DCDCLoThCh6},
{"DCDCLoThCh7",  DCDCLoThCh7},
{"DCDCLoThCh8",  DCDCLoThCh8},
{"DCDCLoThCh9",  DCDCLoThCh9},
{"DCDCLoThCh10",  DCDCLoThCh10},
{"DCDCLoThCh11",  DCDCLoThCh11},
{"DCDCLoThCh12",  DCDCLoThCh12},
{"DCDCLoThCh13",  DCDCLoThCh13},
{"DCDCLoThCh14",  DCDCLoThCh14},
{"DCDCLoThCh15",  DCDCLoThCh15},
{"DCDCHiThCh0",  DCDCHiThCh0},
{"DCDCHiThCh1",  DCDCHiThCh1},
{"DCDCHiThCh2",  DCDCHiThCh2},
{"DCDCHiThCh3",  DCDCHiThCh3},
{"DCDCHiThCh4",  DCDCHiThCh4},
{"DCDCHiThCh5",  DCDCHiThCh5},
{"DCDCHiThCh6",  DCDCHiThCh6},
{"DCDCHiThCh7",  DCDCHiThCh7},
{"DCDCHiThCh8",  DCDCHiThCh8},
{"DCDCHiThCh9",  DCDCHiThCh9},
{"DCDCHiThCh10",  DCDCHiThCh10},
{"DCDCHiThCh11",  DCDCHiThCh11},
{"DCDCHiThCh12",  DCDCHiThCh12},
{"DCDCHiThCh13",  DCDCHiThCh13},
{"DCDCHiThCh14",  DCDCHiThCh14},
{"DCDCHiThCh15",  DCDCHiThCh15},
{"WRNLoThCh0",  WRNLoThCh0},
{"WRNLoThCh1",  WRNLoThCh1},
{"WRNLoThCh2",  WRNLoThCh2},
{"WRNLoThCh3",  WRNLoThCh3},
{"WRNLoThCh4",  WRNLoThCh4},
{"WRNLoThCh5",  WRNLoThCh5},
{"WRNLoThCh6",  WRNLoThCh6},
{"WRNLoThCh7",  WRNLoThCh7},
{"WRNLoThCh8",  WRNLoThCh8},
{"WRNLoThCh9",  WRNLoThCh9},
{"WRNLoThCh10",  WRNLoThCh10},
{"WRNLoThCh11",  WRNLoThCh11},
{"WRNLoThCh12",  WRNLoThCh12},
{"WRNLoThCh13",  WRNLoThCh13},
{"WRNLoThCh14",  WRNLoThCh14},
{"WRNLoThCh15",  WRNLoThCh15},
{"WRNHiThCh0",  WRNHiThCh0},
{"WRNHiThCh1",  WRNHiThCh1},
{"WRNHiThCh2",  WRNHiThCh2},
{"WRNHiThCh3",  WRNHiThCh3},
{"WRNHiThCh4",  WRNHiThCh4},
{"WRNHiThCh5",  WRNHiThCh5},
{"WRNHiThCh6",  WRNHiThCh6},
{"WRNHiThCh7",  WRNHiThCh7},
{"WRNHiThCh8",  WRNHiThCh8},
{"WRNHiThCh9",  WRNHiThCh9},
{"WRNHiThCh10",  WRNHiThCh10},
{"WRNHiThCh11",  WRNHiThCh11},
{"WRNHiThCh12",  WRNHiThCh12},
{"WRNHiThCh13",  WRNHiThCh13},
{"WRNHiThCh14",  WRNHiThCh14},
{"WRNHiThCh15",  WRNHiThCh15},
};

  m_subregisters[StatusAM]=new SubRegister(&m_registers[ 0], 31, 1, 0);
  m_subregisters[  StatusWARN]=new SubRegister(&m_registers[ 0], 29, 1, 0);
  m_subregisters[  StatusDCDC]=new SubRegister(&m_registers[ 0], 28, 1, 0);
  m_subregisters[  StatusHV3]=new SubRegister(&m_registers[ 0], 27, 1, 0);
  m_subregisters[  StatusHV2]=new SubRegister(&m_registers[ 0], 26, 1, 0);
  m_subregisters[  StatusHV1]=new SubRegister(&m_registers[ 0], 25, 1, 0);
  m_subregisters[  StatusHV0]=new SubRegister(&m_registers[ 0], 24, 1, 0);
  m_subregisters[  StatusY2LDO]=new SubRegister(&m_registers[ 0], 22, 1, 0);
  m_subregisters[  StatusY1LDO]=new SubRegister(&m_registers[ 0], 21, 1, 0);
  m_subregisters[  StatusY0LDO]=new SubRegister(&m_registers[ 0], 20, 1, 0);
  m_subregisters[  StatusX2LDO]=new SubRegister(&m_registers[ 0], 18, 1, 0);
  m_subregisters[  StatusX1LDO]=new SubRegister(&m_registers[ 0], 17, 1, 0);
  m_subregisters[  StatusX0LDO]=new SubRegister(&m_registers[ 0], 16, 1, 0);
  m_subregisters[  StatusGPI]=new SubRegister(&m_registers[ 0], 12, 1, 0);
  m_subregisters[  StatusPGOOD]=new SubRegister(&m_registers[ 0], 8, 1, 0);
  m_subregisters[  StatusILockWARN]=new SubRegister(&m_registers[ 0], 5, 1, 0);
  m_subregisters[  StatusILockDCDC]=new SubRegister(&m_registers[ 0], 4, 1, 0);
  m_subregisters[  StatusILockHV2]=new SubRegister(&m_registers[ 0], 3, 1, 0);
  m_subregisters[  StatusILockHV0]=new SubRegister(&m_registers[ 0], 2, 1, 0);
  m_subregisters[  StatusILockYLDO]=new SubRegister(&m_registers[ 0], 1, 1, 0);
  m_subregisters[  StatusILockxLDO]=new SubRegister(&m_registers[ 0], 0, 1, 0);
  m_subregisters[  HxFlagsHi]=new SubRegister(&m_registers[ 1], 16, 16, 0);
  m_subregisters[  HxFlagsLo]=new SubRegister(&m_registers[ 1], 0, 16, 0);
  m_subregisters[  HyFlagsHi]=new SubRegister(&m_registers[ 2], 16, 16, 0);
  m_subregisters[  HyFlagsLo]=new SubRegister(&m_registers[ 2], 0, 16, 0);
  m_subregisters[  HV0FlagsHi]=new SubRegister(&m_registers[ 3], 16, 16, 0);
  m_subregisters[  HV0FlagsLo]=new SubRegister(&m_registers[ 3], 0, 16, 0);
  m_subregisters[  HV2FlagsHi]=new SubRegister(&m_registers[ 4], 16, 16, 0);
  m_subregisters[  HV2FlagsLo]=new SubRegister(&m_registers[ 4], 0, 16, 0);
  m_subregisters[  DCDCflagsHi]=new SubRegister(&m_registers[ 5], 16, 16, 0);
  m_subregisters[  DCDCflagsLo]=new SubRegister(&m_registers[ 5], 0, 16, 0);
  m_subregisters[  WRNflagsHi]=new SubRegister(&m_registers[ 6], 16, 16, 0);
  m_subregisters[  WRNflagsLo]=new SubRegister(&m_registers[ 6], 0, 16, 0);
  m_subregisters[  SynFlagsWRN]=new SubRegister(&m_registers[ 7], 20, 2, 0);
m_subregisters[  SynFlagsDCDC]=new SubRegister(&m_registers[ 7], 16, 2, 0);
m_subregisters[  SynFlagsHV2]=new SubRegister(&m_registers[ 7], 12, 2, 0);
m_subregisters[  SynFlagsHV0]=new SubRegister(&m_registers[ 7], 8, 2, 0);
m_subregisters[  SynFlagsHy]=new SubRegister(&m_registers[ 7], 4, 2, 0);
m_subregisters[  SynFlagsHx]=new SubRegister(&m_registers[ 7], 0, 2, 0);

m_subregisters[  Value0AMen]=new SubRegister(&m_registers[ 10], 31, 1, 0);
m_subregisters[  Ch0Value]=new SubRegister(&m_registers[ 10], 0, 10, 0);
m_subregisters[  Ch1Value]=new SubRegister(&m_registers[ 10], 10, 10, 0);
m_subregisters[  Ch2Value]=new SubRegister(&m_registers[ 10], 20, 10, 0);

m_subregisters[  Value1AMen]=new SubRegister(&m_registers[ 11], 31, 1, 0);
m_subregisters[  Ch3Value]=new SubRegister(&m_registers[ 11], 0, 10, 0);
m_subregisters[  Ch4Value]=new SubRegister(&m_registers[ 11], 10, 10, 0);
m_subregisters[  Ch5Value]=new SubRegister(&m_registers[ 11], 20, 10, 0);

m_subregisters[  Value2AMen]=new SubRegister(&m_registers[ 12], 31, 1, 0);
m_subregisters[  Ch6Value]=new SubRegister(&m_registers[ 12], 0, 10, 0);
m_subregisters[  Ch7Value]=new SubRegister(&m_registers[ 12], 10, 10, 0);
m_subregisters[  Ch8Value]=new SubRegister(&m_registers[ 12], 20, 10, 0);

m_subregisters[  Value3AMen]=new SubRegister(&m_registers[ 13], 31, 1, 0);
m_subregisters[  Ch9Value]=new SubRegister(&m_registers[ 13], 0, 10, 0);
m_subregisters[  Ch10Value]=new SubRegister(&m_registers[ 13], 10, 10, 0);
m_subregisters[  Ch11Value]=new SubRegister(&m_registers[ 13], 20, 10, 0);

m_subregisters[  Value4AMen]=new SubRegister(&m_registers[ 14], 31, 1, 0);
m_subregisters[  Ch12Value]=new SubRegister(&m_registers[ 14], 0, 10, 0);
m_subregisters[  Ch13Value]=new SubRegister(&m_registers[ 14], 10, 10, 0);
m_subregisters[  Ch14Value]=new SubRegister(&m_registers[ 14], 20, 10, 0);

m_subregisters[  Value5AMen]=new SubRegister(&m_registers[ 15], 31, 1, 0);
m_subregisters[  Ch15Value]=new SubRegister(&m_registers[ 15], 0, 10, 0);

m_subregisters[  PadID]=new SubRegister(&m_registers[ 31], 24, 5, 0);
m_subregisters[  SerNum]=new SubRegister(&m_registers[ 31], 0, 16, 0);

m_subregisters[  FlagResetWRN]=new SubRegister(&m_registers[ 32], 5, 1, 0);
m_subregisters[  FlagResetDCDC]=new SubRegister(&m_registers[ 32], 4, 1, 0);
m_subregisters[  FlagResetHV2]=new SubRegister(&m_registers[ 32], 3, 1, 0);
m_subregisters[  FlagResetHV0]=new SubRegister(&m_registers[ 32], 2, 1, 0);
m_subregisters[  FlagResetXLDO]=new SubRegister(&m_registers[ 32], 1, 1, 0);
m_subregisters[  FlagResetYLDO]=new SubRegister(&m_registers[ 32], 0, 1, 0);

m_subregisters[  LogicReset]=new SubRegister(&m_registers[ 33], 0, 32, 0);

m_subregisters[  HardReset]=new SubRegister(&m_registers[ 34], 0, 32, 0);

m_subregisters[  CntSetHV3frq]=new SubRegister(&m_registers[ 40], 29, 2, 3);
m_subregisters[  CntSetHV3en]=new SubRegister(&m_registers[ 40], 28, 1, 0);
m_subregisters[  CntSetHV2frq]=new SubRegister(&m_registers[ 40], 25, 2, 3);
m_subregisters[  CntSetHV2en]=new SubRegister(&m_registers[ 40], 24, 1, 0);
m_subregisters[  CntSetHV1frq]=new SubRegister(&m_registers[ 40], 21, 2, 3);
m_subregisters[  CntSetHV1en]=new SubRegister(&m_registers[ 40], 20, 1, 0);
m_subregisters[  CntSetHV0frq]=new SubRegister(&m_registers[ 40], 17, 2, 3);
m_subregisters[  CntSetHV0en]=new SubRegister(&m_registers[ 40], 16, 1, 0);
m_subregisters[  CntSetHyLDO2en]=new SubRegister(&m_registers[ 40], 14, 1, 0);
m_subregisters[  CntSetHyLDO1en]=new SubRegister(&m_registers[ 40], 13, 1, 0);
m_subregisters[  CntSetHyLDO0en]=new SubRegister(&m_registers[ 40], 12, 1, 0);
m_subregisters[  CntSetHxLDO2en]=new SubRegister(&m_registers[ 40], 10, 1, 0);
m_subregisters[  CntSetHxLDO1en]=new SubRegister(&m_registers[ 40], 9, 1, 0);
m_subregisters[  CntSetHxLDO0en]=new SubRegister(&m_registers[ 40], 8, 1, 0);
m_subregisters[  CntSetWARN]=new SubRegister(&m_registers[ 40], 4, 1, 0);

m_subregisters[  CntSetCHV3frq]=new SubRegister(&m_registers[ 41], 29, 2, 3);
m_subregisters[  CntSetCHV3en]=new SubRegister(&m_registers[ 41], 28, 1, 0);
m_subregisters[  CntSetCHV2frq]=new SubRegister(&m_registers[ 41], 25, 2, 3);
m_subregisters[  CntSetCHV2en]=new SubRegister(&m_registers[ 41], 24, 1, 0);
m_subregisters[  CntSetCHV1frq]=new SubRegister(&m_registers[ 41], 21, 2, 3);
m_subregisters[  CntSetCHV1en]=new SubRegister(&m_registers[ 41], 20, 1, 0);
m_subregisters[  CntSetCHV0frq]=new SubRegister(&m_registers[ 41], 17, 2, 3);
m_subregisters[  CntSetCHV0en]=new SubRegister(&m_registers[ 41], 16, 1, 0);
m_subregisters[  CntSetCHyLDO2en]=new SubRegister(&m_registers[ 41], 14, 1, 0);
m_subregisters[  CntSetCHyLDO1en]=new SubRegister(&m_registers[ 41], 13, 1, 0);
m_subregisters[  CntSetCHyLDO0en]=new SubRegister(&m_registers[ 41], 12, 1, 0);
m_subregisters[  CntSetCHxLDO2en]=new SubRegister(&m_registers[ 41], 10, 1, 0);
m_subregisters[  CntSetCHxLDO1en]=new SubRegister(&m_registers[ 41], 9, 1, 0);
m_subregisters[  CntSetCHxLDO0en]=new SubRegister(&m_registers[ 41], 8, 1, 0);
m_subregisters[  CntSetCWARN]=new SubRegister(&m_registers[ 41], 4, 1, 0);

m_subregisters[  DCDCAdj]=new SubRegister(&m_registers[ 42], 4, 2, 0);
m_subregisters[  DCDCen]=new SubRegister(&m_registers[ 42], 0, 1, 0);

m_subregisters[  DCDCAdjC]=new SubRegister(&m_registers[ 43], 4, 2, 0);
m_subregisters[  DCDCenC]=new SubRegister(&m_registers[ 43], 0, 1, 0);

m_subregisters[  IlockWRN]=new SubRegister(&m_registers[ 44], 5, 1, 0);
m_subregisters[  IlockDCDC]=new SubRegister(&m_registers[ 44], 4, 1, 0);
m_subregisters[  IlockHV2]=new SubRegister(&m_registers[ 44], 3, 1, 0);
m_subregisters[  IlockHV0]=new SubRegister(&m_registers[ 44], 2, 1, 0);
m_subregisters[  IlockHy]=new SubRegister(&m_registers[ 44], 1, 1, 0);
m_subregisters[  IlockHx]=new SubRegister(&m_registers[ 44], 0, 1, 0);

m_subregisters[  IlockCWRN]=new SubRegister(&m_registers[ 45], 5, 1, 0);
m_subregisters[  IlockCDCDC]=new SubRegister(&m_registers[ 45], 4, 1, 0);
m_subregisters[  IlockCHV2]=new SubRegister(&m_registers[ 45], 3, 1, 0);
m_subregisters[  IlockCHV0]=new SubRegister(&m_registers[ 45], 2, 1, 0);
m_subregisters[  IlockCHy]=new SubRegister(&m_registers[ 45], 1, 1, 0);
m_subregisters[  IlockCHx]=new SubRegister(&m_registers[ 45], 0, 1, 0);

m_subregisters[  RstCntHyHCCresetB]=new SubRegister(&m_registers[ 46], 16, 1, 0);
m_subregisters[  RstCntHxHCCresetB]=new SubRegister(&m_registers[ 46], 8, 1, 0);
m_subregisters[  RstCntOF]=new SubRegister(&m_registers[ 46], 0, 1, 0);

m_subregisters[  RstCntCHyHCCresetB]=new SubRegister(&m_registers[ 47], 16, 1, 0);
m_subregisters[  RstCntCHxHCCresetB]=new SubRegister(&m_registers[ 47], 8, 1, 0);
m_subregisters[  RstCntCOF]=new SubRegister(&m_registers[ 47], 0, 1, 0);

m_subregisters[  AMzeroCalib]=new SubRegister(&m_registers[ 48], 8, 1, 0);
m_subregisters[  AMen]=new SubRegister(&m_registers[ 48], 0, 1, 1);

m_subregisters[  AMzeroCalibC]=new SubRegister(&m_registers[ 49], 8, 1, 0);
m_subregisters[  AMenC]=new SubRegister(&m_registers[ 49], 0, 1, 1);

m_subregisters[  ReqDCDCPGOOD]=new SubRegister(&m_registers[ 50], 8, 1, 1);
m_subregisters[  DCDCenToPwrAMAC]=new SubRegister(&m_registers[ 50], 0, 1, 0);

m_subregisters[  ReqDCDCPGOODC]=new SubRegister(&m_registers[ 51], 8, 1, 1);
m_subregisters[  DCDCenToPwrAMACC]=new SubRegister(&m_registers[ 51], 0, 1, 0);

m_subregisters[  AMbgen ]=new SubRegister(&m_registers[ 52], 15, 1, 0);
m_subregisters[  AMbg   ]=new SubRegister(&m_registers[ 52],  8, 5, 0);
m_subregisters[  VDDbgen]=new SubRegister(&m_registers[ 52],  7, 1, 0);
m_subregisters[  VDDbg  ]=new SubRegister(&m_registers[ 52],  0, 5, 0);

m_subregisters[  AMintCalib]=new SubRegister(&m_registers[ 53], 24, 4, 0);
m_subregisters[  Ch13Mux]=new SubRegister(&m_registers[ 53], 20, 2, 0);
m_subregisters[  Ch12Mux]=new SubRegister(&m_registers[ 53], 16, 2, 0);
m_subregisters[  Ch5Mux]=new SubRegister(&m_registers[ 53], 13, 2, 0);
m_subregisters[  Ch4Mux]=new SubRegister(&m_registers[ 53], 8, 2, 0);
m_subregisters[  Ch3Mux]=new SubRegister(&m_registers[ 53], 4, 2, 0);

m_subregisters[  DACShunty]=new SubRegister(&m_registers[ 54], 24, 8, 0);
m_subregisters[  DACShuntx]=new SubRegister(&m_registers[ 54], 16, 8, 0);
m_subregisters[  DACCALy]=new SubRegister(&m_registers[ 54], 8, 8, 0);
m_subregisters[  DACCalx]=new SubRegister(&m_registers[ 54], 0, 8, 0);

m_subregisters[  DACbias]=new SubRegister(&m_registers[ 55], 0, 5, 0xD);

m_subregisters[  HVcurGain ]=new SubRegister(&m_registers[ 56], 16, 4, 0);
m_subregisters[  DRcomMode ]=new SubRegister(&m_registers[ 56], 12, 2, 0);
m_subregisters[  DRcurr    ]=new SubRegister(&m_registers[ 56],  8, 3, 4);
m_subregisters[  RingOscFrq]=new SubRegister(&m_registers[ 56],  0, 3, 4);

m_subregisters[  NTCpbCal       ]=new SubRegister(&m_registers[ 57], 19, 1, 1);
m_subregisters[  NTCpbSenseRange]=new SubRegister(&m_registers[ 57], 16, 3, 4);
m_subregisters[  NTCy0Cal       ]=new SubRegister(&m_registers[ 57], 11, 1, 1);
m_subregisters[  NTCy0SenseRange]=new SubRegister(&m_registers[ 57],  8, 3, 4);
m_subregisters[  NTCx0Cal       ]=new SubRegister(&m_registers[ 57],  3, 1, 1);
m_subregisters[  NTCx0SenseRange]=new SubRegister(&m_registers[ 57],  0, 3, 4);

m_subregisters[  DCDCoOffset     ]=new SubRegister(&m_registers[ 58], 20, 4, 4);
m_subregisters[  DCDCoZeroReading]=new SubRegister(&m_registers[ 58], 19, 1, 0);
m_subregisters[  DCDCoN          ]=new SubRegister(&m_registers[ 58], 17, 1, 0);
m_subregisters[  DCDCoP          ]=new SubRegister(&m_registers[ 58], 16, 1, 0);
m_subregisters[  DCDCiZeroReading]=new SubRegister(&m_registers[ 58], 15, 1, 0);
m_subregisters[  DCDCiRangeSW    ]=new SubRegister(&m_registers[ 58], 12, 1, 1);
m_subregisters[  DCDCiOffset     ]=new SubRegister(&m_registers[ 58],  8, 4, 8);
m_subregisters[  DCDCiP          ]=new SubRegister(&m_registers[ 58],  4, 3, 0);
m_subregisters[  DCDCiN          ]=new SubRegister(&m_registers[ 58],  0, 3, 0);

m_subregisters[  HxLAM]=new SubRegister(&m_registers[ 60], 16, 1, 0);
m_subregisters[  HxFlagsLatch]=new SubRegister(&m_registers[ 60], 12, 1, 1);
m_subregisters[  HxFlagsLogic]=new SubRegister(&m_registers[ 60], 8, 1, 0);
m_subregisters[  HxFlagValid]=new SubRegister(&m_registers[ 60], 4, 1, 0);
m_subregisters[  HxFlagsValidConf]=new SubRegister(&m_registers[ 60], 0, 2, 0);

m_subregisters[  HyLAM]=new SubRegister(&m_registers[ 61], 16, 1, 0);
m_subregisters[  HyFlagsLatch]=new SubRegister(&m_registers[ 61], 12, 1, 1);
m_subregisters[  HyFlagsLogic]=new SubRegister(&m_registers[ 61], 8, 1, 0);
m_subregisters[  HyFlagValid]=new SubRegister(&m_registers[ 61], 4, 1, 0);
m_subregisters[  HyFlagsValidConf]=new SubRegister(&m_registers[ 61], 0, 2, 0);

m_subregisters[  HV0LAM]=new SubRegister(&m_registers[ 62], 16, 1, 0);
m_subregisters[  HV0FlagsLatch]=new SubRegister(&m_registers[ 62], 12, 1, 1);
m_subregisters[  HV0FlagsLogic]=new SubRegister(&m_registers[ 62], 8, 1, 0);
m_subregisters[  HV0FlagValid]=new SubRegister(&m_registers[ 62], 4, 1, 0);
m_subregisters[  HV0FlagsValidConf]=new SubRegister(&m_registers[ 62], 0, 2, 0);

m_subregisters[  HV2LAM]=new SubRegister(&m_registers[ 63], 16, 1, 0);
m_subregisters[  HV2FlagsLatch]=new SubRegister(&m_registers[ 63], 12, 1, 1);
m_subregisters[  HV2FlagsLogic]=new SubRegister(&m_registers[ 63], 8, 1, 0);
m_subregisters[  HV2FlagValid]=new SubRegister(&m_registers[ 63], 4, 1, 0);
m_subregisters[  HV2FlagsValidConf]=new SubRegister(&m_registers[ 63], 0, 2, 0);

m_subregisters[  DCDCLAM]=new SubRegister(&m_registers[ 64], 16, 1, 0);
m_subregisters[  DCDCFlagsLatch]=new SubRegister(&m_registers[ 64], 12, 1, 1);
m_subregisters[  DCDCFlagsLogic]=new SubRegister(&m_registers[ 64], 8, 1, 0);
m_subregisters[  DCDCFlagValid]=new SubRegister(&m_registers[ 64], 4, 1, 0);
m_subregisters[  DCDCFlagsValidConf]=new SubRegister(&m_registers[ 64], 0, 2, 0);

m_subregisters[  WRNLAM]=new SubRegister(&m_registers[ 65], 16, 1, 0);
m_subregisters[  WRNFlagsLatch]=new SubRegister(&m_registers[ 65], 12, 1, 1);
m_subregisters[  WRNFlagsLogic]=new SubRegister(&m_registers[ 65], 8, 1, 0);
m_subregisters[  WRNFlagValid]=new SubRegister(&m_registers[ 65], 4, 1, 0);
m_subregisters[  WRNFlagsValidConf]=new SubRegister(&m_registers[ 65], 0, 2, 0);

m_subregisters[  HxTlut]=new SubRegister(&m_registers[ 70], 0, 8, 0);

m_subregisters[  HxModlut1]=new SubRegister(&m_registers[ 71], 0, 32, 0);

m_subregisters[  HxModlut2]=new SubRegister(&m_registers[ 72], 0, 32, 0);

m_subregisters[  HyTlut]=new SubRegister(&m_registers[ 73], 0, 8, 0);

m_subregisters[  HyModlut1]=new SubRegister(&m_registers[ 74], 0, 32, 0);

m_subregisters[  HyModlut2]=new SubRegister(&m_registers[ 75], 0, 32, 0);

m_subregisters[  HV0Tlut]=new SubRegister(&m_registers[ 76], 0, 8, 0);

m_subregisters[  HV0Modlut1]=new SubRegister(&m_registers[ 77], 0, 32, 0);

m_subregisters[  HV0Modlut2]=new SubRegister(&m_registers[ 78], 0, 32, 0);

m_subregisters[  HV2Tlut]=new SubRegister(&m_registers[ 79], 0, 8, 0);

m_subregisters[  HV2Modlut1]=new SubRegister(&m_registers[ 80], 0, 32, 0);

m_subregisters[  HV2Modlut2]=new SubRegister(&m_registers[ 81], 0, 32, 0);

m_subregisters[  DCDCTlut]=new SubRegister(&m_registers[ 82], 0, 8, 0);

m_subregisters[  DCDCModlut1]=new SubRegister(&m_registers[ 83], 0, 32, 0);

m_subregisters[  DCDCModlut2]=new SubRegister(&m_registers[ 84], 0, 32, 0);

m_subregisters[  WRNTlut]=new SubRegister(&m_registers[ 85], 0, 8, 0);

m_subregisters[  WRNModlut1]=new SubRegister(&m_registers[ 86], 0, 32, 0);

m_subregisters[  WRNModlut2]=new SubRegister(&m_registers[ 87], 0, 32, 0);

m_subregisters[  HxFlagsEnHi]=new SubRegister(&m_registers[ 90], 16, 16, 0);
m_subregisters[  HxFlagsEnLo]=new SubRegister(&m_registers[ 90], 16, 16, 0);

m_subregisters[  HyFlagsEnHi]=new SubRegister(&m_registers[ 91], 16, 16, 0);
m_subregisters[  HyFlagsEnLo]=new SubRegister(&m_registers[ 91], 16, 16, 0);

m_subregisters[  HV0FlagsEnHi]=new SubRegister(&m_registers[ 92], 16, 16, 0);
m_subregisters[  HV0FlagsEnLo]=new SubRegister(&m_registers[ 92], 16, 16, 0);

m_subregisters[  HV2FlagsEnHi]=new SubRegister(&m_registers[ 93], 16, 16, 0);
m_subregisters[  HV2FlagsEnLo]=new SubRegister(&m_registers[ 93], 16, 16, 0);

m_subregisters[  DCDCFlagsEnHi]=new SubRegister(&m_registers[ 94], 16, 16, 0);
m_subregisters[  DCDCFlagsEnLo]=new SubRegister(&m_registers[ 94], 16, 16, 0);

m_subregisters[  WRNFlagsEnHi]=new SubRegister(&m_registers[ 95], 16, 16, 0);
m_subregisters[  WRNFlagsEnLo]=new SubRegister(&m_registers[ 95], 16, 16, 0);

m_subregisters[  WRNsynFlagEnHi]=new SubRegister(&m_registers[ 95], 21, 1, 0);
m_subregisters[  WRNsynFlagEnLo]=new SubRegister(&m_registers[ 95], 20, 1, 0);
m_subregisters[  DCDCsynFlagEnHi]=new SubRegister(&m_registers[ 95], 17, 1, 0);
m_subregisters[  DCDCsynFlagEnLo]=new SubRegister(&m_registers[ 95], 16, 1, 0);
m_subregisters[  HV2synFlagEnHi]=new SubRegister(&m_registers[ 95], 13, 1, 0);
m_subregisters[  HV2synFlagEnLo]=new SubRegister(&m_registers[ 95], 12, 1, 0);
m_subregisters[  HV0synFlagEnHi]=new SubRegister(&m_registers[ 95], 9, 1, 0);
m_subregisters[  HV0synFlagEnLo]=new SubRegister(&m_registers[ 95], 8, 1, 0);
m_subregisters[  HysynFlagEnHi]=new SubRegister(&m_registers[ 95], 5, 1, 0);
m_subregisters[  HysynFlagEnLo]=new SubRegister(&m_registers[ 95], 4, 1, 0);
m_subregisters[  HxsynFlagEnHi]=new SubRegister(&m_registers[ 95], 1, 1, 0);
m_subregisters[  HxsynFlagEnLo]=new SubRegister(&m_registers[ 95], 0, 1, 0);

m_subregisters[  HxLoThCh0]=new SubRegister(&m_registers[ 100], 0, 10, 0);
m_subregisters[  HxLoThCh1]=new SubRegister(&m_registers[ 100], 10, 10, 0);
m_subregisters[  HxLoThCh2]=new SubRegister(&m_registers[ 100], 20, 10, 0);

m_subregisters[  HxLoThCh3]=new SubRegister(&m_registers[ 101], 0, 10, 0);
m_subregisters[  HxLoThCh4]=new SubRegister(&m_registers[ 101], 10, 10, 0);
m_subregisters[  HxLoThCh5]=new SubRegister(&m_registers[ 101], 20, 10, 0);

m_subregisters[  HxLoThCh6]=new SubRegister(&m_registers[ 102], 0, 10, 0);
m_subregisters[  HxLoThCh7]=new SubRegister(&m_registers[ 102], 10, 10, 0);
m_subregisters[  HxLoThCh8]=new SubRegister(&m_registers[ 102], 20, 10, 0);

m_subregisters[  HxLoThCh9]=new SubRegister(&m_registers[ 103], 0, 10, 0);
m_subregisters[  HxLoThCh10]=new SubRegister(&m_registers[ 103], 10, 10, 0);
m_subregisters[  HxLoThCh11]=new SubRegister(&m_registers[ 103], 20, 10, 0);

m_subregisters[  HxLoThCh12]=new SubRegister(&m_registers[ 104], 0, 10, 0);
m_subregisters[  HxLoThCh13]=new SubRegister(&m_registers[ 104], 10, 10, 0);
m_subregisters[  HxLoThCh14]=new SubRegister(&m_registers[ 104], 20, 10, 0);

m_subregisters[  HxLoThCh15]=new SubRegister(&m_registers[ 105], 0, 10, 0);

m_subregisters[  HxHiThCh0]=new SubRegister(&m_registers[ 106], 0, 10, 0x3FF);
m_subregisters[  HxHiThCh1]=new SubRegister(&m_registers[ 106], 10, 10, 0x3FF);
m_subregisters[  HxHiThCh2]=new SubRegister(&m_registers[ 106], 20, 10, 0x3FF);

m_subregisters[  HxHiThCh3]=new SubRegister(&m_registers[ 107], 0, 10, 0x3FF);
m_subregisters[  HxHiThCh4]=new SubRegister(&m_registers[ 107], 10, 10, 0x3FF);
m_subregisters[  HxHiThCh5]=new SubRegister(&m_registers[ 107], 20, 10, 0x3FF);

m_subregisters[  HxHiThCh6]=new SubRegister(&m_registers[ 108], 0, 10, 0x3FF);
m_subregisters[  HxHiThCh7]=new SubRegister(&m_registers[ 108], 10, 10, 0x3FF);
m_subregisters[  HxHiThCh8]=new SubRegister(&m_registers[ 108], 20, 10, 0x3FF);

m_subregisters[  HxHiThCh9]=new SubRegister(&m_registers[ 109], 0, 10, 0x3FF);
m_subregisters[  HxHiThCh10]=new SubRegister(&m_registers[ 109], 10, 10, 0x3FF);
m_subregisters[  HxHiThCh11]=new SubRegister(&m_registers[ 109], 20, 10, 0x3FF);

m_subregisters[  HxHiThCh12]=new SubRegister(&m_registers[ 110], 0, 10, 0x3FF);
m_subregisters[  HxHiThCh13]=new SubRegister(&m_registers[ 110], 10, 10, 0x3FF);
m_subregisters[  HxHiThCh14]=new SubRegister(&m_registers[ 110], 20, 10, 0x3FF);

m_subregisters[  HxHiThCh15]=new SubRegister(&m_registers[ 111], 0, 10, 0x3FF);

m_subregisters[  HyLoThCh0]=new SubRegister(&m_registers[ 112], 0, 10, 0);
m_subregisters[  HyLoThCh1]=new SubRegister(&m_registers[ 112], 10, 10, 0);
m_subregisters[  HyLoThCh2]=new SubRegister(&m_registers[ 112], 20, 10, 0);

m_subregisters[  HyLoThCh3]=new SubRegister(&m_registers[ 113], 0, 10, 0);
m_subregisters[  HyLoThCh4]=new SubRegister(&m_registers[ 113], 10, 10, 0);
m_subregisters[  HyLoThCh5]=new SubRegister(&m_registers[ 113], 20, 10, 0);

m_subregisters[  HyLoThCh6]=new SubRegister(&m_registers[ 114], 0, 10, 0);
m_subregisters[  HyLoThCh7]=new SubRegister(&m_registers[ 114], 10, 10, 0);
m_subregisters[  HyLoThCh8]=new SubRegister(&m_registers[ 114], 20, 10, 0);

m_subregisters[  HyLoThCh9]=new SubRegister(&m_registers[ 115], 0, 10, 0);
m_subregisters[  HyLoThCh10]=new SubRegister(&m_registers[ 115], 10, 10, 0);
m_subregisters[  HyLoThCh11]=new SubRegister(&m_registers[ 115], 20, 10, 0);

m_subregisters[  HyLoThCh12]=new SubRegister(&m_registers[ 116], 0, 10, 0);
m_subregisters[  HyLoThCh13]=new SubRegister(&m_registers[ 116], 10, 10, 0);
m_subregisters[  HyLoThCh14]=new SubRegister(&m_registers[ 116], 20, 10, 0);

m_subregisters[  HyLoThCh15]=new SubRegister(&m_registers[ 117], 0, 10, 0);

m_subregisters[  HyHiThCh0]=new SubRegister(&m_registers[ 118], 0, 10, 0x3FF);
m_subregisters[  HyHiThCh1]=new SubRegister(&m_registers[ 118], 10, 10, 0x3FF);
m_subregisters[  HyHiThCh2]=new SubRegister(&m_registers[ 118], 20, 10, 0x3FF);

m_subregisters[  HyHiThCh3]=new SubRegister(&m_registers[ 119], 0, 10, 0x3FF);
m_subregisters[  HyHiThCh4]=new SubRegister(&m_registers[ 119], 10, 10, 0x3FF);
m_subregisters[  HyHiThCh5]=new SubRegister(&m_registers[ 119], 20, 10, 0x3FF);

m_subregisters[  HyHiThCh6]=new SubRegister(&m_registers[ 120], 0, 10, 0x3FF);
m_subregisters[  HyHiThCh7]=new SubRegister(&m_registers[ 120], 10, 10, 0x3FF);
m_subregisters[  HyHiThCh8]=new SubRegister(&m_registers[ 120], 20, 10, 0x3FF);

m_subregisters[  HyHiThCh9]=new SubRegister(&m_registers[ 121], 0, 10, 0x3FF);
m_subregisters[  HyHiThCh10]=new SubRegister(&m_registers[ 121], 10, 10, 0x3FF);
m_subregisters[  HyHiThCh11]=new SubRegister(&m_registers[ 121], 20, 10, 0x3FF);

m_subregisters[  HyHiThCh12]=new SubRegister(&m_registers[ 122], 0, 10, 0x3FF);
m_subregisters[  HyHiThCh13]=new SubRegister(&m_registers[ 122], 10, 10, 0x3FF);
m_subregisters[  HyHiThCh14]=new SubRegister(&m_registers[ 122], 20, 10, 0x3FF);

m_subregisters[  HyHiThCh15]=new SubRegister(&m_registers[ 123], 0, 10, 0x3FF);

m_subregisters[  HV0LoThCh0]=new SubRegister(&m_registers[ 124], 0, 10, 0);
m_subregisters[  HV0LoThCh1]=new SubRegister(&m_registers[ 124], 10, 10, 0);
m_subregisters[  HV0LoThCh2]=new SubRegister(&m_registers[ 124], 20, 10, 0);

m_subregisters[  HV0LoThCh3]=new SubRegister(&m_registers[ 125], 0, 10, 0);
m_subregisters[  HV0LoThCh4]=new SubRegister(&m_registers[ 125], 10, 10, 0);
m_subregisters[  HV0LoThCh5]=new SubRegister(&m_registers[ 125], 20, 10, 0);

m_subregisters[  HV0LoThCh6]=new SubRegister(&m_registers[ 126], 0, 10, 0);
m_subregisters[  HV0LoThCh7]=new SubRegister(&m_registers[ 126], 10, 10, 0);
m_subregisters[  HV0LoThCh8]=new SubRegister(&m_registers[ 126], 20, 10, 0);

m_subregisters[  HV0LoThCh9]=new SubRegister(&m_registers[ 127], 0, 10, 0);
m_subregisters[  HV0LoThCh10]=new SubRegister(&m_registers[ 127], 10, 10, 0);
m_subregisters[  HV0LoThCh11]=new SubRegister(&m_registers[ 127], 20, 10, 0);

m_subregisters[  HV0LoThCh12]=new SubRegister(&m_registers[ 128], 0, 10, 0);
m_subregisters[  HV0LoThCh13]=new SubRegister(&m_registers[ 128], 10, 10, 0);
m_subregisters[  HV0LoThCh14]=new SubRegister(&m_registers[ 128], 20, 10, 0);

m_subregisters[  HV0LoThCh15]=new SubRegister(&m_registers[ 129], 0, 10, 0);

m_subregisters[  HV0HiThCh0]=new SubRegister(&m_registers[ 130], 0, 10, 0x3FF);
m_subregisters[  HV0HiThCh1]=new SubRegister(&m_registers[ 130], 10, 10, 0x3FF);
m_subregisters[  HV0HiThCh2]=new SubRegister(&m_registers[ 130], 20, 10, 0x3FF);

m_subregisters[  HV0HiThCh3]=new SubRegister(&m_registers[ 131], 0, 10, 0x3FF);
m_subregisters[  HV0HiThCh4]=new SubRegister(&m_registers[ 131], 10, 10, 0x3FF);
m_subregisters[  HV0HiThCh5]=new SubRegister(&m_registers[ 131], 20, 10, 0x3FF);

m_subregisters[  HV0HiThCh6]=new SubRegister(&m_registers[ 132], 0, 10, 0x3FF);
m_subregisters[  HV0HiThCh7]=new SubRegister(&m_registers[ 132], 10, 10, 0x3FF);
m_subregisters[  HV0HiThCh8]=new SubRegister(&m_registers[ 132], 20, 10, 0x3FF);

m_subregisters[  HV0HiThCh9]=new SubRegister(&m_registers[ 133], 0, 10, 0x3FF);
m_subregisters[  HV0HiThCh10]=new SubRegister(&m_registers[ 133], 10, 10, 0x3FF);
m_subregisters[  HV0HiThCh11]=new SubRegister(&m_registers[ 133], 20, 10, 0x3FF);

m_subregisters[  HV0HiThCh12]=new SubRegister(&m_registers[ 134], 0, 10, 0x3FF);
m_subregisters[  HV0HiThCh13]=new SubRegister(&m_registers[ 134], 10, 10, 0x3FF);
m_subregisters[  HV0HiThCh14]=new SubRegister(&m_registers[ 134], 20, 10, 0x3FF);

m_subregisters[  HV0HiThCh15]=new SubRegister(&m_registers[ 135], 0, 10, 0x3FF);

m_subregisters[  HV2LoThCh0]=new SubRegister(&m_registers[ 136], 0, 10, 0);
m_subregisters[  HV2LoThCh1]=new SubRegister(&m_registers[ 136], 10, 10, 0);
m_subregisters[  HV2LoThCh2]=new SubRegister(&m_registers[ 136], 20, 10, 0);

m_subregisters[  HV2LoThCh3]=new SubRegister(&m_registers[ 137], 0, 10, 0);
m_subregisters[  HV2LoThCh4]=new SubRegister(&m_registers[ 137], 10, 10, 0);
m_subregisters[  HV2LoThCh5]=new SubRegister(&m_registers[ 137], 20, 10, 0);

m_subregisters[  HV2LoThCh6]=new SubRegister(&m_registers[ 138], 0, 10, 0);
m_subregisters[  HV2LoThCh7]=new SubRegister(&m_registers[ 138], 10, 10, 0);
m_subregisters[  HV2LoThCh8]=new SubRegister(&m_registers[ 138], 20, 10, 0);

m_subregisters[  HV2LoThCh9]=new SubRegister(&m_registers[ 139], 0, 10, 0);
m_subregisters[  HV2LoThCh10]=new SubRegister(&m_registers[ 139], 10, 10, 0);
m_subregisters[  HV2LoThCh11]=new SubRegister(&m_registers[ 139], 20, 10, 0);

m_subregisters[  HV2LoThCh12]=new SubRegister(&m_registers[ 140], 0, 10, 0);
m_subregisters[  HV2LoThCh13]=new SubRegister(&m_registers[ 140], 10, 10, 0);
m_subregisters[  HV2LoThCh14]=new SubRegister(&m_registers[ 140], 20, 10, 0);

m_subregisters[  HV2LoThCh15]=new SubRegister(&m_registers[ 141], 0, 10, 0);

m_subregisters[  HV2HiThCh0]=new SubRegister(&m_registers[ 142], 0, 10, 0x3FF);
m_subregisters[  HV2HiThCh1]=new SubRegister(&m_registers[ 142], 10, 10, 0x3FF);
m_subregisters[  HV2HiThCh2]=new SubRegister(&m_registers[ 142], 20, 10, 0x3FF);

m_subregisters[  HV2HiThCh3]=new SubRegister(&m_registers[ 143], 0, 10, 0x3FF);
m_subregisters[  HV2HiThCh4]=new SubRegister(&m_registers[ 143], 10, 10, 0x3FF);
m_subregisters[  HV2HiThCh5]=new SubRegister(&m_registers[ 143], 20, 10, 0x3FF);

m_subregisters[  HV2HiThCh6]=new SubRegister(&m_registers[ 144], 0, 10, 0x3FF);
m_subregisters[  HV2HiThCh7]=new SubRegister(&m_registers[ 144], 10, 10, 0x3FF);
m_subregisters[  HV2HiThCh8]=new SubRegister(&m_registers[ 144], 20, 10, 0x3FF);

m_subregisters[  HV2HiThCh9]=new SubRegister(&m_registers[ 145], 0, 10, 0x3FF);
m_subregisters[  HV2HiThCh10]=new SubRegister(&m_registers[ 145], 10, 10, 0x3FF);
m_subregisters[  HV2HiThCh11]=new SubRegister(&m_registers[ 145], 20, 10, 0x3FF);

m_subregisters[  HV2HiThCh12]=new SubRegister(&m_registers[ 146], 0, 10, 0x3FF);
m_subregisters[  HV2HiThCh13]=new SubRegister(&m_registers[ 146], 10, 10, 0x3FF);
m_subregisters[  HV2HiThCh14]=new SubRegister(&m_registers[ 146], 20, 10, 0x3FF);

m_subregisters[  HV2HiThCh15]=new SubRegister(&m_registers[ 147], 0, 10, 0x3FF);

m_subregisters[  DCDCLoThCh0]=new SubRegister(&m_registers[ 148], 0, 10, 0);
m_subregisters[  DCDCLoThCh1]=new SubRegister(&m_registers[ 148], 10, 10, 0);
m_subregisters[  DCDCLoThCh2]=new SubRegister(&m_registers[ 148], 20, 10, 0);

m_subregisters[  DCDCLoThCh3]=new SubRegister(&m_registers[ 149], 0, 10, 0);
m_subregisters[  DCDCLoThCh4]=new SubRegister(&m_registers[ 149], 10, 10, 0);
m_subregisters[  DCDCLoThCh5]=new SubRegister(&m_registers[ 149], 20, 10, 0);

m_subregisters[  DCDCLoThCh6]=new SubRegister(&m_registers[ 150], 0, 10, 0);
m_subregisters[  DCDCLoThCh7]=new SubRegister(&m_registers[ 150], 10, 10, 0);
m_subregisters[  DCDCLoThCh8]=new SubRegister(&m_registers[ 150], 20, 10, 0);

m_subregisters[  DCDCLoThCh9]=new SubRegister(&m_registers[ 151], 0, 10, 0);
m_subregisters[  DCDCLoThCh10]=new SubRegister(&m_registers[ 151], 10, 10, 0);
m_subregisters[  DCDCLoThCh11]=new SubRegister(&m_registers[ 151], 20, 10, 0);

m_subregisters[  DCDCLoThCh12]=new SubRegister(&m_registers[ 152], 0, 10, 0);
m_subregisters[  DCDCLoThCh13]=new SubRegister(&m_registers[ 152], 10, 10, 0);
m_subregisters[  DCDCLoThCh14]=new SubRegister(&m_registers[ 152], 20, 10, 0);

m_subregisters[  DCDCLoThCh15]=new SubRegister(&m_registers[ 153], 0, 10, 0);

m_subregisters[  DCDCHiThCh0]=new SubRegister(&m_registers[ 154], 0, 10, 0x3FF);
m_subregisters[  DCDCHiThCh1]=new SubRegister(&m_registers[ 154], 10, 10, 0x3FF);
m_subregisters[  DCDCHiThCh2]=new SubRegister(&m_registers[ 154], 20, 10, 0x3FF);

m_subregisters[  DCDCHiThCh3]=new SubRegister(&m_registers[ 155], 0, 10, 0x3FF);
m_subregisters[  DCDCHiThCh4]=new SubRegister(&m_registers[ 155], 10, 10, 0x3FF);
m_subregisters[  DCDCHiThCh5]=new SubRegister(&m_registers[ 155], 20, 10, 0x3FF);

m_subregisters[  DCDCHiThCh6]=new SubRegister(&m_registers[ 156], 0, 10, 0x3FF);
m_subregisters[  DCDCHiThCh7]=new SubRegister(&m_registers[ 156], 10, 10, 0x3FF);
m_subregisters[  DCDCHiThCh8]=new SubRegister(&m_registers[ 156], 20, 10, 0x3FF);

m_subregisters[  DCDCHiThCh9]=new SubRegister(&m_registers[ 157], 0, 10, 0x3FF);
m_subregisters[  DCDCHiThCh10]=new SubRegister(&m_registers[ 157], 10, 10, 0x3FF);
m_subregisters[  DCDCHiThCh11]=new SubRegister(&m_registers[ 157], 20, 10, 0x3FF);

m_subregisters[  DCDCHiThCh12]=new SubRegister(&m_registers[ 158], 0, 10, 0x3FF);
m_subregisters[  DCDCHiThCh13]=new SubRegister(&m_registers[ 158], 10, 10, 0x3FF);
m_subregisters[  DCDCHiThCh14]=new SubRegister(&m_registers[ 158], 20, 10, 0x3FF);

m_subregisters[  DCDCHiThCh15]=new SubRegister(&m_registers[ 159], 0, 10, 0x3FF);

m_subregisters[  WRNLoThCh0]=new SubRegister(&m_registers[ 160], 0, 10, 0);
m_subregisters[  WRNLoThCh1]=new SubRegister(&m_registers[ 160], 10, 10, 0);
m_subregisters[  WRNLoThCh2]=new SubRegister(&m_registers[ 160], 20, 10, 0);

m_subregisters[  WRNLoThCh3]=new SubRegister(&m_registers[ 161], 0, 10, 0);
m_subregisters[  WRNLoThCh4]=new SubRegister(&m_registers[ 161], 10, 10, 0);
m_subregisters[  WRNLoThCh5]=new SubRegister(&m_registers[ 161], 20, 10, 0);

m_subregisters[  WRNLoThCh6]=new SubRegister(&m_registers[ 162], 0, 10, 0);
m_subregisters[  WRNLoThCh7]=new SubRegister(&m_registers[ 162], 10, 10, 0);
m_subregisters[  WRNLoThCh8]=new SubRegister(&m_registers[ 162], 20, 10, 0);

m_subregisters[  WRNLoThCh9]=new SubRegister(&m_registers[ 163], 0, 10, 0);
m_subregisters[  WRNLoThCh10]=new SubRegister(&m_registers[ 163], 10, 10, 0);
m_subregisters[  WRNLoThCh11]=new SubRegister(&m_registers[ 163], 20, 10, 0);

m_subregisters[  WRNLoThCh12]=new SubRegister(&m_registers[ 164], 0, 10, 0);
m_subregisters[  WRNLoThCh13]=new SubRegister(&m_registers[ 164], 10, 10, 0);
m_subregisters[  WRNLoThCh14]=new SubRegister(&m_registers[ 164], 20, 10, 0);

m_subregisters[  WRNLoThCh15]=new SubRegister(&m_registers[ 165], 0, 10, 0);

m_subregisters[  WRNHiThCh0]=new SubRegister(&m_registers[ 166], 0, 10, 0x3FF);
m_subregisters[  WRNHiThCh1]=new SubRegister(&m_registers[ 166], 10, 10, 0x3FF);
m_subregisters[  WRNHiThCh2]=new SubRegister(&m_registers[ 166], 20, 10, 0x3FF);

m_subregisters[  WRNHiThCh3]=new SubRegister(&m_registers[ 167], 0, 10, 0x3FF);
m_subregisters[  WRNHiThCh4]=new SubRegister(&m_registers[ 167], 10, 10, 0x3FF);
m_subregisters[  WRNHiThCh5]=new SubRegister(&m_registers[ 167], 20, 10, 0x3FF);

m_subregisters[  WRNHiThCh6]=new SubRegister(&m_registers[ 168], 0, 10, 0x3FF);
m_subregisters[  WRNHiThCh7]=new SubRegister(&m_registers[ 168], 10, 10, 0x3FF);
m_subregisters[  WRNHiThCh8]=new SubRegister(&m_registers[ 168], 20, 10, 0x3FF);

m_subregisters[  WRNHiThCh9]=new SubRegister(&m_registers[ 169], 0, 10, 0x3FF);
m_subregisters[  WRNHiThCh10]=new SubRegister(&m_registers[ 169], 10, 10, 0x3FF);
m_subregisters[  WRNHiThCh11]=new SubRegister(&m_registers[ 169], 20, 10, 0x3FF);

m_subregisters[  WRNHiThCh12]=new SubRegister(&m_registers[ 170], 0, 10, 0x3FF);
m_subregisters[  WRNHiThCh13]=new SubRegister(&m_registers[ 170], 10, 10, 0x3FF);
m_subregisters[  WRNHiThCh14]=new SubRegister(&m_registers[ 170], 20, 10, 0x3FF);

m_subregisters[  WRNHiThCh15]=new SubRegister(&m_registers[ 171], 0, 10, 0x3FF);

}

AMACv2::~AMACv2(){
  for(auto it : m_subregisters){
    delete it.second;
  }
  m_subregisters.clear();
}
