#include <Strips/EndeavourNext.h>
#include <sstream>
#include <iomanip>

using namespace std;
using namespace Strips;

EndeavourNext::EndeavourNext(){
  m_type=Endeavour::NEXT;
  m_id=0;
}

EndeavourNext::EndeavourNext(uint8_t id){
  m_id=id;
}

EndeavourNext::EndeavourNext(EndeavourNext * copy){
  m_id=copy->m_id;
}

EndeavourNext::~EndeavourNext(){
  //nothing to do
}

void EndeavourNext::SetID(uint8_t id){
  m_id = id;
}

uint8_t EndeavourNext::GetID(){
  return m_id;
}

string EndeavourNext::ToString(){
  ostringstream os;
  os << "EndeavourNext "
     << "ID: " << (uint32_t) m_id;
  return os.str();
}

uint32_t EndeavourNext::Unpack(uint8_t * bytes, uint32_t maxlen){
  if(maxlen<1) return 0;
  if((bytes[0] & 0xE0) != m_type){return 0;}
  
  m_id = (bytes[0] & 0x1F);
  
  return 1;
}
      
uint32_t EndeavourNext::Pack(uint8_t * bytes){

  bytes[0] = m_type | (m_id & 0x1F);
  
  return 1;
}
    
