#include <Strips/WriteRegister.h>
#include <Strips/SixToEight.h>
#include <sstream>
#include <iostream>
#include <iomanip>

using namespace std;
using namespace Strips;

WriteRegister::WriteRegister(){
  m_type=Command::WRITE;
  m_chip=0;
  m_hccid=0;
  m_abcid=0;
  m_address=0;
  m_value=0;
}

WriteRegister::WriteRegister(bool chip, uint32_t hccid, uint32_t abcid, uint8_t address, uint32_t value){
  m_type=Command::WRITE;
  m_chip=chip;
  m_hccid=hccid;
  m_abcid=abcid;
  m_address=address;
  m_value=value;
}

WriteRegister::WriteRegister(WriteRegister * copy){
  m_type=copy->m_type;
  m_chip=copy->m_chip;
  m_hccid=copy->m_hccid;
  m_abcid=copy->m_abcid;
  m_address=copy->m_address;
  m_value=copy->m_value;
}

WriteRegister::~WriteRegister(){
  //nothing to do
}

void WriteRegister::SetChip(bool chip){
  m_chip=chip;
}

bool WriteRegister::GetChip(){
  return m_chip;
}

void WriteRegister::SetHCCid(uint8_t hccid){
  m_hccid=hccid;
}

uint8_t WriteRegister::GetHCCid(){
  return m_hccid;
}

void WriteRegister::SetABCid(uint8_t abcid){
  m_abcid=abcid;
}

uint8_t WriteRegister::GetABCid(){
  return m_abcid;
}

void WriteRegister::SetAddress(uint8_t address){
  m_address=address;
}

uint8_t WriteRegister::GetAddress(){
  return m_address;
}

void WriteRegister::SetValue(uint32_t value){
  m_value=value;
}

uint32_t WriteRegister::GetValue(){
  return m_value;
}


string WriteRegister::ToString(){
  ostringstream os;
  os << "WriteRegister "
     << "Chip: " << (m_chip==WriteRegister::ABC?"ABC":"HCC") << " (" << m_chip << ") "
     << "HCC id: 0x" << hex << (uint32_t)m_hccid << dec << " "
     << "ABC id: 0x" << hex << (uint32_t)m_abcid << dec << " "
     << "Address: 0x" << hex << setw(2) << setfill('0') << (uint32_t)m_address << dec << " "
     << "Value: 0x" << hex << setw(8) << setfill('0') << (uint32_t)m_value << dec;
  return os.str();
}

uint32_t WriteRegister::Unpack (uint8_t * bytes, uint32_t maxlen){
  //we need 18 bytes
  if(maxlen<18) return 0;

  vector<uint8_t> bb(18,0);

  //decode
  for(uint32_t i=0;i<18;i++){
    bb[i]=SixToEight::decodeLUT[bytes[i]];
    //cout << hex << setw(2) << setfill('0') << (uint32_t)bb[i] << dec;
  }

  //First byte is a K2
  //Second to last is a K2
  if(bb[ 0]!=SixToEight::K2 or
     bb[16]!=SixToEight::K2){
    cout << "hs" << endl;
    return 0;
  }

  //K2 payload has to be Start in the header
  if((bb[ 1]&0x10)!=0x10){cout << "p1" << endl;return 0;}

  //K2 payload has to be End in the trailer
  if((bb[17]&0x10)!=0x00){cout << "p2" << endl;return 0;}
  
  //Should not be a Read Register command
  if(bb[2]!=0){return 0;}
  
  //We could actually check that MSB are 0

  //extract the payload
  m_chip = (bb[1]>>5) & 0x1;
  m_hccid = bb[1] & 0xF;
  m_abcid = (bb[2] >> 2) & 0xF;
  m_address = ((bb[3] & 0x3)<<6) | ( (bb[4]&0x1) << 5 ) | ((bb[5] >> 1) & 0x1F);
  m_value = 0;
  m_value |= (bb[ 7] & 0x0F)<<28;
  m_value |= (bb[ 8] & 0x01)<<27;
  m_value |= (bb[ 9] & 0x3F)<<21;
  m_value |= (bb[10] & 0x01)<<20;
  m_value |= (bb[11] & 0x3F)<<14;
  m_value |= (bb[12] & 0x01)<<13;
  m_value |= (bb[13] & 0x3F)<< 7;
  m_value |= (bb[14] & 0x01)<< 6;
  m_value |= (bb[15] & 0x3F)<< 0;
  
  return 18;
}
      
uint32_t WriteRegister::Pack(uint8_t * bytes){

  //payload
  vector<uint8_t> bb(18,0);

  bb[ 0] = SixToEight::K2;
  bb[ 1] = ((m_chip & 0x1) << 5) | (1 << 4) | (m_hccid & 0xF);
  bb[ 2] = 0;
  bb[ 3] = ((m_abcid & 0xF) << 2 ) | ((m_address >> 6) & 0x3);
  bb[ 4] = (m_address >> 5) & 0x01;
  bb[ 5] = (m_address & 0x1F) << 1;
  bb[ 6] = 0;
  bb[ 7] = (m_value >> 28) & 0x0F;
  bb[ 8] = (m_value >> 27) & 0x01;
  bb[ 9] = (m_value >> 21) & 0x3F;
  bb[10] = (m_value >> 20) & 0x01;
  bb[11] = (m_value >> 14) & 0x3F;
  bb[12] = (m_value >> 13) & 0x01;
  bb[13] = (m_value >>  7) & 0x3F;
  bb[14] = (m_value >>  6) & 0x01;
  bb[15] = (m_value >>  0) & 0x3F;
  bb[16] = SixToEight::K2;
  bb[17] = ((m_chip & 0x1) << 5) | (0 << 4) | (m_hccid & 0xF);

  //encode
  for(uint32_t i=0;i<18;i++){
    bytes[i] = SixToEight::encodeLUT[bb[i]];
    //cout << hex << setw(2) << setfill('0') << (uint32_t)bb[i] << dec;
  }
  
  return 18;
}
    
