#include <Strips/EndeavourRead.h>
#include <sstream>
#include <iomanip>

using namespace std;
using namespace Strips;

EndeavourRead::EndeavourRead(){
  m_type=Endeavour::READ;
  m_id=0;
  m_address=0;
}

EndeavourRead::EndeavourRead(uint8_t id, uint8_t address){
  m_id=id;
  m_address=address;
}

EndeavourRead::EndeavourRead(EndeavourRead * copy){
  m_id=copy->m_id;
  m_address=copy->m_address;
}

EndeavourRead::~EndeavourRead(){
  //nothing to do
}

void EndeavourRead::SetID(uint8_t id){
  m_id = id;
}

uint8_t EndeavourRead::GetID(){
  return m_id;
}

void EndeavourRead::SetAddress(uint8_t address){
  m_address=address;
}

uint8_t EndeavourRead::GetAddress(){
  return m_address;
}

string EndeavourRead::ToString(){
  ostringstream os;
  os << "EndeavourRead "
     << "ID: " << (uint32_t) m_id << " "
     << "Address: 0x" << hex << setw(2) << setfill('0') << (uint32_t) m_address << dec;
  return os.str();
}

uint32_t EndeavourRead::Unpack(uint8_t * bytes, uint32_t maxlen){
  if(maxlen<2) return 0;
  if((bytes[0] & 0xE0) != Endeavour::READ){return 0;}
  
  m_id = (bytes[0] & 0x1F);
  m_address = bytes[1];
  
  return 2;
}
      
uint32_t EndeavourRead::Pack(uint8_t * bytes){

  bytes[0] = m_type | (m_id & 0x1F);
  bytes[1] = m_address;
  
  return 2;
}
    
