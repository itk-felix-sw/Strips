#include "Strips/Decoder.h"
#include <iostream>
#include <sstream>
#include <iomanip>

using namespace std;
using namespace Strips;

Decoder::Decoder(){
  m_size = 1e6;
  m_bytes.reserve(m_size);
  m_bytes.resize(m_size,0);
  m_length = 0;
  m_abc_reg = new ABCRegisterPacket();
  m_hcc_reg = new HCCRegisterPacket();
  m_transp = new TransparentPacket();
  m_lppr = new ABCLPPRPacket();
}

Decoder::~Decoder(){
  m_bytes.clear();
  ClearPackets();
  delete m_abc_reg;
  delete m_hcc_reg;
  delete m_transp;
  delete m_lppr;
}

void Decoder::AddBytes(uint8_t *bytes, uint32_t pos, uint32_t len){
  for(uint32_t i=pos;i<pos+len;i++){
    m_bytes[m_length]=bytes[i];
    m_length++;
  }
}

void Decoder::SetBytes(uint8_t *bytes, uint32_t len, bool reversed){
  m_length=len;
  if(len>m_bytes.size()){m_bytes.resize(len);}
  if(!reversed){for(uint32_t i=0;i<len;i++){m_bytes[i]=bytes[i];}}
  else         {for(uint32_t i=0;i<len;i++){m_bytes[len-i-1]=bytes[i];}}
}

void Decoder::AddPacket(Packet *packet){
  m_packets.push_back(packet);
}
  
void Decoder::ClearBytes(){
  m_length=0;
}
  
void Decoder::ClearPackets(){
  while(!m_packets.empty()){
    Packet* packet = m_packets.back();
    delete packet;
    m_packets.pop_back();
  }
}

void Decoder::Clear(){
  ClearBytes();
  ClearPackets();
}
  
string Decoder::GetByteString(){
  ostringstream os;
  os << hex; 
  for(uint32_t pos=0; pos<m_length; pos++){
    os << setw(2) << setfill('0') << (uint32_t) m_bytes[pos] << " ";
  }
  return os.str();
}

void Decoder::GetBytes(uint8_t * bytes, uint32_t & length, bool reversed){
  length=m_length;
  if(!reversed){for(uint32_t i=0;i<m_length;i++){bytes[i]=m_bytes[i];}}
  else         {for(uint32_t i=0;i<m_length;i++){bytes[m_length-i-1]=m_bytes[i];}}
}

uint8_t * Decoder::GetBytes(bool reversed){
  if(!reversed){return m_bytes.data();}
  m_rbytes.resize(m_length);
  for(uint32_t i=0;i<m_length;i++){
    m_rbytes[m_length-i-1]=m_bytes[i];
  }
  return m_rbytes.data();
}
  
uint32_t Decoder::GetLength(){
  return m_length;
}
  
vector<Packet*> & Decoder::GetPackets(){
  return m_packets;
}

void Decoder::Encode(){
  uint32_t pos=0;
  for(uint32_t i=0;i<m_packets.size();i++){
    if(m_size < pos+1000){m_size+=1000;m_bytes.resize(m_size,0);}
    pos+=m_packets[i]->Pack(&m_bytes[pos]);
  }
  m_length=pos;
}

void Decoder::Decode(bool verbose){
  ClearPackets();
  uint32_t pos=0;
  uint32_t nb=0;
  uint32_t tnb=m_length;
  Packet * packet=0;

  while(pos<tnb){
    if((nb=m_lppr->Unpack(&m_bytes[pos],tnb-pos))>0){
      if(verbose) cout << "Decoder::Decode LPPR" << endl;
      packet=m_lppr; m_lppr=new ABCLPPRPacket();
    }
    else if((nb=m_abc_reg->Unpack(&m_bytes[pos],tnb-pos))>0){
      if(verbose) cout << "Decoder::Decode ABC Register" << endl;
      packet=m_abc_reg; m_abc_reg=new ABCRegisterPacket();
    }
    else if((nb=m_hcc_reg->Unpack(&m_bytes[pos],tnb-pos))>0){
      if(verbose) cout << "Decoder::Decode HCC Register" << endl;
      packet=m_hcc_reg; m_hcc_reg=new HCCRegisterPacket();
    }
    else if((nb=m_transp->Unpack(&m_bytes[pos],tnb-pos))>0){
      if(verbose) cout << "Decoder::Decode Transparent" << endl;
      packet=m_transp; m_transp=new TransparentPacket();
    }

    else{
      cout << "Decoder::Decode Cannot decode byte sequence: "
          << "0x" << hex << setw(2) << setfill('0') << (uint32_t) m_bytes[pos] << dec
          << " at index: " << pos
          << " ...skipping" << endl;
      pos++;
      continue;
    }
    if(!packet){pos++; continue;}
    m_packets.push_back(packet);
    pos+=nb;
    if(verbose) {cout << "Decoder::Decode pos=" << pos << " tnb=" << tnb << endl;}
  }
}
