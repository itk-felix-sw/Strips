#include <Strips/HCC.h>
#include <Strips/Register.h>
#include <Strips/Register.h>
#include <Strips/SubRegister.h>
#include <iostream>
#include <iomanip>

using namespace std;
using namespace Strips;

HCC::HCC(){

  m_registers.resize(NUM_REGISTERS);
  m_backup.resize(NUM_REGISTERS);

  m_subregisters[STOPHPR]               = new SubRegister(&m_registers[16],0,1,0);
  m_subregisters[TESTHPR]               = new SubRegister(&m_registers[16],1,1,0);

  m_subregisters[CFD_BC_FINEDELAY]      = new SubRegister(&m_registers[32],0,4,0);
  m_subregisters[CFD_BC_COARSEDELAY]    = new SubRegister(&m_registers[32],8,4,0);
  m_subregisters[CFD_PRLP_FINEDELAY]    = new SubRegister(&m_registers[32],12,2,0);
  m_subregisters[CFD_PRLP_COARSEDELAY]  = new SubRegister(&m_registers[32],16,4,0);
  m_subregisters[HFD_LCBA_FINEDELAY]    = new SubRegister(&m_registers[32],20,4,0);
  m_subregisters[LCBA_DELAY160]         = new SubRegister(&m_registers[32],24,2,0);
  m_subregisters[FD_DATAIN0_FINEDELAY]  = new SubRegister(&m_registers[33],0,4,0);
  m_subregisters[FD_DATAIN1_FINEDELAY]  = new SubRegister(&m_registers[33],4,4,0);
  m_subregisters[FD_DATAIN2_FINEDELAY]  = new SubRegister(&m_registers[33],8,4,0);
  m_subregisters[FD_DATAIN3_FINEDELAY]  = new SubRegister(&m_registers[33],12,4,0);
  m_subregisters[FD_DATAIN4_FINEDELAY]  = new SubRegister(&m_registers[33],16,4,0);
  m_subregisters[FD_DATAIN5_FINEDELAY]  = new SubRegister(&m_registers[33],20,4,0);
  m_subregisters[FD_DATAIN6_FINEDELAY]  = new SubRegister(&m_registers[33],24,4,0);
  m_subregisters[FD_DATAIN7_FINEDELAY]  = new SubRegister(&m_registers[33],28,4,0);
  m_subregisters[FD_DATAIN8_FINEDELAY]  = new SubRegister(&m_registers[34],0,4,0);
  m_subregisters[FD_DATAIN9_FINEDELAY]  = new SubRegister(&m_registers[34],4,4,0);
  m_subregisters[FD_DATAIN10_FINEDELAY] = new SubRegister(&m_registers[34],8,4,0);

  m_subregisters[EPLLICP]               = new SubRegister(&m_registers[35],0,4,0);
  m_subregisters[EPLLCAP]               = new SubRegister(&m_registers[35],4,2,0);
  m_subregisters[EPLLRES]               = new SubRegister(&m_registers[35],8,4,0);
  m_subregisters[EPLLREFFREQ]           = new SubRegister(&m_registers[35],12,2,0);
  m_subregisters[EPLLENABLEPHASE]       = new SubRegister(&m_registers[35],16,8,0);
  m_subregisters[EPLLPHASE320A]         = new SubRegister(&m_registers[36],0,4,0);
  m_subregisters[EPLLPHASE320B]         = new SubRegister(&m_registers[36],4,4,0);
  m_subregisters[EPLLPHASE320C]         = new SubRegister(&m_registers[36],8,4,0);
  m_subregisters[EPLLPHASE160A]         = new SubRegister(&m_registers[37],0,0,0);
  m_subregisters[EPLLPHASE160B]         = new SubRegister(&m_registers[37],8,4,0);
  m_subregisters[EPLLPHASE160C]         = new SubRegister(&m_registers[37],16,4,0);

  m_subregisters[STVCLKOUTCUR]          = new SubRegister(&m_registers[38],0,3,0);
  m_subregisters[STVCLKOUTEN]           = new SubRegister(&m_registers[38],3,1,0);
  m_subregisters[LCBOUTCUR]             = new SubRegister(&m_registers[38],4,3,0);
  m_subregisters[LCBOUTEN]              = new SubRegister(&m_registers[38],7,1,0);
  m_subregisters[R3L1OUTCUR]            = new SubRegister(&m_registers[38],8,3,0);
  m_subregisters[R3L1OUTEN]             = new SubRegister(&m_registers[38],11,1,0);
  m_subregisters[BCHYBCUR]              = new SubRegister(&m_registers[38],12,3,0);
  m_subregisters[BCHYBEN]               = new SubRegister(&m_registers[38],15,1,0);
  m_subregisters[LCBAHYBCUR]            = new SubRegister(&m_registers[38],16,3,0);
  m_subregisters[LCBAHYBEN]             = new SubRegister(&m_registers[38],19,1,0);
  m_subregisters[PRLPHYBCUR]            = new SubRegister(&m_registers[38],20,3,0);
  m_subregisters[PRLPHYBEN]             = new SubRegister(&m_registers[38],23,1,0);
  m_subregisters[RCLKHYBCUR]            = new SubRegister(&m_registers[38],24,3,0);
  m_subregisters[RCLKHYBEN]             = new SubRegister(&m_registers[38],27,1,0);

  m_subregisters[DATA1CUR]              = new SubRegister(&m_registers[39],0,3,0);
  m_subregisters[DATA1ENPRE]            = new SubRegister(&m_registers[39],3,1,0);
  m_subregisters[DATA1ENABLE]           = new SubRegister(&m_registers[39],4,1,0);
  m_subregisters[DATA1TERM]             = new SubRegister(&m_registers[39],5,1,0);
  m_subregisters[DATACLKCUR]            = new SubRegister(&m_registers[39],16,3,0);
  m_subregisters[DATACLKENPRE]          = new SubRegister(&m_registers[39],19,1,0);
  m_subregisters[DATACLKENABLE]         = new SubRegister(&m_registers[39],20,1,0);

  m_subregisters[ICENABLE]              = new SubRegister(&m_registers[40],0,11,0);
  m_subregisters[ICTRANSSEL]            = new SubRegister(&m_registers[40],16,3,0);


  m_subregisters[TRIGMODE]              = new SubRegister(&m_registers[41], 0, 1, 0);
  m_subregisters[ROSPEED]               = new SubRegister(&m_registers[41], 4, 1, 0);
  m_subregisters[OPMODE]                = new SubRegister(&m_registers[41], 8, 2, 0);
  m_subregisters[MAXNPACKETS]           = new SubRegister(&m_registers[41], 12, 3, 0);
  m_subregisters[ENCODECNTL]            = new SubRegister(&m_registers[41], 16, 1, 0);
  m_subregisters[ENCODE8B10B]           = new SubRegister(&m_registers[41], 17, 1, 1);
  m_subregisters[PRBSMODE]              = new SubRegister(&m_registers[41], 18, 1, 0);

  m_subregisters[TRIGMODEC]             = new SubRegister(&m_registers[42], 0, 1, 0);
  m_subregisters[ROSPEEDC]              = new SubRegister(&m_registers[42], 4, 1, 0);
  m_subregisters[OPMODEC]               = new SubRegister(&m_registers[42], 8, 2, 0);
  m_subregisters[MAXNPACKETSC]          = new SubRegister(&m_registers[42], 12, 3, 0);
  m_subregisters[ENCODECNTLC]           = new SubRegister(&m_registers[42], 16, 1, 0);
  m_subregisters[ENCODE8B10BC]          = new SubRegister(&m_registers[42], 17, 1, 1);
  m_subregisters[PRBSMODEC]             = new SubRegister(&m_registers[42], 18, 1, 0);

  m_subregisters[BGSETTING]             = new SubRegister(&m_registers[43], 0, 5, 0);
  m_subregisters[MASKHPR]               = new SubRegister(&m_registers[43], 8, 1, 0);
  m_subregisters[GPO0]                  = new SubRegister(&m_registers[43], 12, 1, 0);
  m_subregisters[GPO1]                  = new SubRegister(&m_registers[43], 13, 1, 0);
  m_subregisters[EFUSEPROGBIT]          = new SubRegister(&m_registers[43], 16, 5, 0);
  m_subregisters[BCIDRSTDELAY]          = new SubRegister(&m_registers[44], 0, 9, 0);
  m_subregisters[BCMMSQUELCH]           = new SubRegister(&m_registers[44], 16, 11, 0);
  m_subregisters[ABCRESETB]             = new SubRegister(&m_registers[45], 0, 1, 0);
  m_subregisters[AMACSSSH]              = new SubRegister(&m_registers[45], 4, 1, 0);
  m_subregisters[ABCRESETBC]            = new SubRegister(&m_registers[46], 0, 1, 0);
  m_subregisters[AMACSSSHC]             = new SubRegister(&m_registers[46], 4, 1, 0);
  m_subregisters[LCBERRCOUNTTHR]        = new SubRegister(&m_registers[47], 0, 16, 0);
  m_subregisters[R3L1ERRCOUNTTHR]       = new SubRegister(&m_registers[47], 16, 16, 0);
  m_subregisters[AMENABLE]              = new SubRegister(&m_registers[48], 0, 1, 0);
  m_subregisters[AMCALIB]               = new SubRegister(&m_registers[48], 4, 1, 0);
  m_subregisters[AMSW0]                 = new SubRegister(&m_registers[48], 8, 1, 0);
  m_subregisters[AMSW1]                 = new SubRegister(&m_registers[48], 9, 1, 0);
  m_subregisters[AMSW2]                 = new SubRegister(&m_registers[48], 10, 1, 0);
  m_subregisters[AMSW60]                = new SubRegister(&m_registers[48], 12, 1, 0);
  m_subregisters[AMSW80]                = new SubRegister(&m_registers[48], 13, 1, 0);
  m_subregisters[AMSW100]               = new SubRegister(&m_registers[48], 14, 1, 0);
  m_subregisters[ANASET]                = new SubRegister(&m_registers[48], 16, 3, 0);
  m_subregisters[THERMOFFSET]           = new SubRegister(&m_registers[48], 20, 4, 0);
  
  m_registerNames={
      {"Delay1", HCCRegister::Delay1},
      {"Delay2", HCCRegister::Delay2},
      {"Delay3", HCCRegister::Delay3},
      {"PLL1", HCCRegister::PLL1},
      {"PLL2",  HCCRegister::PLL2},
      {"PLL3",  HCCRegister::PLL3},
      {"DRV1",  HCCRegister::DRV1},
      {"DRV2",  HCCRegister::DRV2},
      {"ICenable", HCCRegister::ICenable},
      {"OPmode", HCCRegister::OPmode},
      {"OPmodeC", HCCRegister::OPmodeC},
      {"Cfg1", HCCRegister::Cfg1},
      {"Cfg2", HCCRegister::Cfg2},
      {"ExtRst", HCCRegister::ExtRst},
      {"ExtRstC", HCCRegister::ExtRstC},
      {"ErrCfg", HCCRegister::ErrCfg},
      {"ADCcfg", HCCRegister::ADCcfg},
      {"Pulse", HCCRegister::Pulse}
  };

  m_subRegisterNames={
      {"RCLKHYBCUR", HCCSubRegister::RCLKHYBCUR},
      {"LCBA_DELAY160", HCCSubRegister::LCBA_DELAY160},
      {"DATACLKENPRE", HCCSubRegister::DATACLKENPRE},
      {"EPLLICP", HCCSubRegister::EPLLICP},
      {"BCHYBEN", HCCSubRegister::BCHYBEN},
      {"ROSPEED", HCCSubRegister::ROSPEED},
      {"FD_RCLK_FINEDELAY", HCCSubRegister::FD_RCLK_FINEDELAY},
      {"PRLPHYBEN", HCCSubRegister::PRLPHYBEN},
      {"FD_DATAIN10_FINEDELAY", HCCSubRegister::FD_DATAIN10_FINEDELAY},
      {"DATACLKCUR", HCCSubRegister::DATACLKCUR},
      {"FD_DATAIN9_FINEDELAY", HCCSubRegister::FD_DATAIN9_FINEDELAY},
      {"AMSW60", HCCSubRegister::AMSW60},
      {"BCHYBCUR", HCCSubRegister::BCHYBCUR},
      {"MASKHPR", HCCSubRegister::MASKHPR},
      {"LCBERRCOUNTTHR", HCCSubRegister::LCBERRCOUNTTHR},
      {"FD_DATAIN5_FINEDELAY", HCCSubRegister::FD_DATAIN5_FINEDELAY},
      {"EPLLPHASE320C", HCCSubRegister::EPLLPHASE320C},
      {"FD_DATAIN4_FINEDELAY", HCCSubRegister::FD_DATAIN4_FINEDELAY},
      {"LCBAHYBEN", HCCSubRegister::LCBAHYBEN},
      {"R3L1OUTCUR", HCCSubRegister::R3L1OUTCUR},
      {"EFUSEPROGBIT", HCCSubRegister::EFUSEPROGBIT},
      {"FD_DATAIN0_FINEDELAY", HCCSubRegister::FD_DATAIN0_FINEDELAY},
      {"BCIDRSTDELAY", HCCSubRegister::BCIDRSTDELAY},
      {"DATA1ENABLE", HCCSubRegister::DATA1ENABLE},
      {"LCBOUTEN", HCCSubRegister::LCBOUTEN},
      {"FD_DATAIN7_FINEDELAY", HCCSubRegister::FD_DATAIN7_FINEDELAY},
      {"MAXNPACKETSC", HCCSubRegister::MAXNPACKETSC},
      {"ROSPEEDC", HCCSubRegister::ROSPEEDC},
      {"AMACSSSH", HCCSubRegister::AMACSSSH},
      {"LCBOUTCUR", HCCSubRegister::LCBOUTCUR},
      {"AMSW2", HCCSubRegister::AMSW2},
      {"TRIGMODEC", HCCSubRegister::TRIGMODEC},
      {"ABCRESETB", HCCSubRegister::ABCRESETB},
      {"ICENABLE", HCCSubRegister::ICENABLE},
      {"EPLLPHASE320A", HCCSubRegister::EPLLPHASE320A},
      {"GPO1", HCCSubRegister::GPO1},
      {"R3L1OUTEN", HCCSubRegister::R3L1OUTEN},
      {"EPLLPHASE320B", HCCSubRegister::EPLLPHASE320B},
      {"GPO0", HCCSubRegister::GPO0},
      {"STVCLKOUTEN", HCCSubRegister::STVCLKOUTEN},
      {"ENCODECNTL", HCCSubRegister::ENCODECNTL},
      {"BCMMSQUELCH", HCCSubRegister::BCMMSQUELCH},
      {"TRIGMODE", HCCSubRegister::TRIGMODE},
      {"ENCODE8B10B", HCCSubRegister::ENCODE8B10B},
      {"BGSETTING", HCCSubRegister::BGSETTING},
      {"CFD_BC_COARSEDELAY", HCCSubRegister::CFD_BC_COARSEDELAY},
      {"EPLLCAP", HCCSubRegister::EPLLCAP},
      {"CFD_BC_FINEDELAY", HCCSubRegister::CFD_BC_FINEDELAY},
      {"FD_DATAIN6_FINEDELAY", HCCSubRegister::FD_DATAIN6_FINEDELAY},
      {"RCLKHYBEN", HCCSubRegister::RCLKHYBEN},
      {"HFD_LCBA_FINEDELAY", HCCSubRegister::HFD_LCBA_FINEDELAY},
      {"LCBAHYBCUR", HCCSubRegister::LCBAHYBCUR},
      {"R3L1ERRCOUNTTHR", HCCSubRegister::R3L1ERRCOUNTTHR},
      {"AMSW1", HCCSubRegister::AMSW1},
      {"MAXNPACKETS", HCCSubRegister::MAXNPACKETS},
      {"PRBSMODE", HCCSubRegister::PRBSMODE},
      {"FD_DATAIN8_FINEDELAY", HCCSubRegister::FD_DATAIN8_FINEDELAY},
      {"AMACSSSHC", HCCSubRegister::AMACSSSHC},
      {"EPLLRES", HCCSubRegister::EPLLRES},
      {"CFD_PRLP_COARSEDELAY", HCCSubRegister::CFD_PRLP_COARSEDELAY},
      {"DATA1ENPRE", HCCSubRegister::DATA1ENPRE},
      {"EPLLPHASE160A", HCCSubRegister::EPLLPHASE160A},
      {"PRLPHYBCUR", HCCSubRegister::PRLPHYBCUR},
      {"STOPHPR", HCCSubRegister::STOPHPR},
      {"DATA1CUR", HCCSubRegister::DATA1CUR},
      {"ENCODECNTLC", HCCSubRegister::ENCODECNTLC},
      {"TESTHPR", HCCSubRegister::TESTHPR},
      {"EPLLPHASE160B", HCCSubRegister::EPLLPHASE160B},
      {"EPLLREFFREQ", HCCSubRegister::EPLLREFFREQ},
      {"FD_DATAIN1_FINEDELAY", HCCSubRegister::FD_DATAIN1_FINEDELAY},
      {"FD_DATAIN3_FINEDELAY", HCCSubRegister::FD_DATAIN3_FINEDELAY},
      {"ENCODE8B10BC", HCCSubRegister::ENCODE8B10BC},
      {"THERMOFFSET", HCCSubRegister::THERMOFFSET},
      {"AMSW0", HCCSubRegister::AMSW0},
      {"DATACLKENABLE", HCCSubRegister::DATACLKENABLE},
      {"AMSW80", HCCSubRegister::AMSW80},
      {"STVCLKOUTCUR", HCCSubRegister::STVCLKOUTCUR},
      {"ICTRANSSEL", HCCSubRegister::ICTRANSSEL},
      {"OPMODEC", HCCSubRegister::OPMODEC},
      {"CFD_PRLP_FINEDELAY", HCCSubRegister::CFD_PRLP_FINEDELAY},
      {"ANASET", HCCSubRegister::ANASET},
      {"DATA1TERM", HCCSubRegister::DATA1TERM},
      {"EPLLPHASE160C", HCCSubRegister::EPLLPHASE160C},
      {"PRBSMODEC", HCCSubRegister::PRBSMODEC},
      {"AMENABLE", HCCSubRegister::AMENABLE},
      {"AMSW100", HCCSubRegister::AMSW100},
      {"FD_DATAIN2_FINEDELAY", HCCSubRegister::FD_DATAIN2_FINEDELAY},
      {"ABCRESETBC", HCCSubRegister::ABCRESETBC},
      {"EPLLENABLEPHASE", HCCSubRegister::EPLLENABLEPHASE},
      {"AMCALIB", HCCSubRegister::AMCALIB},
      {"OPMODE", HCCSubRegister::OPMODE},
  };

  m_id=0;
}

HCC::~HCC(){
  for(auto it : m_subregisters){
    delete it.second;
  }
  m_subregisters.clear();
}



/*
const std::vector<hccsubregdef> s_hccsubregdefs_v0 = {
  {HCCStarSubRegister::EPLLPHASE320A            ,36	,0	,4}	,
  {HCCStarSubRegister::EPLLPHASE320B            ,36	,4	,4}	,
  {HCCStarSubRegister::EPLLPHASE320C            ,36	,8	,4}	,
  {HCCStarSubRegister::EPLLPHASE160A            ,37	,0	,4}	,
  {HCCStarSubRegister::EPLLPHASE160B            ,37	,8	,4}	,
  {HCCStarSubRegister::EPLLPHASE160C            ,37	,16	,4}	,
  {HCCStarSubRegister::AMSW0                    ,48	,8	,1}	,
  {HCCStarSubRegister::AMSW1                    ,48	,9	,1}	,
  {HCCStarSubRegister::AMSW2                    ,48	,10	,1}	,
  {HCCStarSubRegister::AMSW60                   ,48	,12	,1}	,
  {HCCStarSubRegister::AMSW80                   ,48	,13	,1}	,
  {HCCStarSubRegister::AMSW100                  ,48	,14	,1}	,
};

const std::vector<hccsubregdef> s_hccsubregdefs_v1 = {
  {HCCStarSubRegister::EPLLPHASE160,  35, 29, 3},
  {HCCStarSubRegister::CLK_DIS_EN,    41, 20, 1},
  {HCCStarSubRegister::CLK_DIS_ENC,   42, 20, 1},
  {HCCStarSubRegister::BG_RANGE_LOW,  43,  5, 1},
  {HCCStarSubRegister::BGVDD_CNT_EN,  43,  7, 1},
  {HCCStarSubRegister::CLK_DIS_PHASE, 43, 24, 2},
  {HCCStarSubRegister::CLK_DIS_SEL,   43, 28, 2},
  {HCCStarSubRegister::AM_INT_SLOPE,  48,  8, 4},
  {HCCStarSubRegister::AM_RANGE,      48, 12, 1},
};


enum 


            // HCCv1 drops:
            //  EPLLPHASE320A, EPLLPHASE320B, EPLLPHASE320C, EPLLPHASE160A, EPLLPHASE160B, EPLLPHASE160C,
            // AMSW0, AMSW1, AMSW2, AMSW60, AMSW80, AMSW100
            // Reg 35
            EPLLPHASE160,
            // Reg 41
            CLK_DIS_EN,
            // Reg 41
            CLK_DIS_ENC,
            // Reg 43
            BG_RANGE_LOW, BGVDD_CNT_EN, CLK_DIS_PHASE, CLK_DIS_SEL,
            // Reg 48
            AM_INT_SLOPE, AM_RANGE

*/
