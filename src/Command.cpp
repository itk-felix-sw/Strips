#include <Strips/Command.h>

using namespace std;
using namespace Strips;

const std::map<uint32_t, std::string> Command::typeNames={
  {NONE,"NONE"},
  {L0A,"L0A"},
  {FAST,"FAST"},
  {READ,"READ"},
  {WRITE,"WRITE"},
  {IDLE,"IDLE"}
};

Command::Command(){
  m_type=NONE;
}

Command::~Command(){
  //nothing to do
}

void Command::SetType(uint32_t typ){
  m_type=typ;
}

uint32_t Command::GetType(){
  return m_type;
}
