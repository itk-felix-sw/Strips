#include <Strips/Cluster.h>
#include <iostream>
#include <iomanip>
#include <sstream>

using namespace std;
using namespace Strips;

Cluster::Cluster(){ 
  m_bcid=0;
  m_l0id=0;
  m_bcid_parity=0;
  m_channel=0;
  m_address=0;
  m_pattern=0;
}

Cluster::~Cluster(){}

Cluster * Cluster::Clone(){
  Cluster* cluster = new Cluster();
  cluster->m_bcid=m_bcid;
  cluster->m_l0id=m_l0id;
  cluster->m_bcid_parity=m_bcid_parity;
  cluster->m_channel=m_channel;
  cluster->m_address=m_address;
  cluster->m_pattern=m_pattern;
  return cluster;
}

void Cluster::Update(uint32_t l0id, bool bcid_parity, uint32_t bcid){
  m_l0id=l0id;
  m_bcid_parity=bcid_parity;
  m_bcid=bcid;
}

void Cluster::Parse(uint16_t rawcls){
  m_channel = (rawcls >> 11) & 0xF;
  m_address = (rawcls >> 3) & 0xFF;
  m_pattern = rawcls & 0x7;
}

vector<uint32_t> Cluster::GetStripNumbers(){
  vector<uint32_t> ret;
  ret.push_back(m_address);
  for(uint32_t i=0; i<3; i++){
    if(!((m_pattern>>i)&0x1)){ret.push_back(m_address+i+1);}
  }
  return ret;
}

uint32_t Cluster::GetChannel(){
  return m_channel;
}

uint32_t Cluster::GetAddress(){
  return m_address;
}

uint32_t Cluster::GetPattern(){
  return m_pattern;
}

uint32_t Cluster::GetBCID(){
  return m_bcid;
}

bool Cluster::GetBCIDParity(){
  return m_bcid_parity;
}

uint32_t Cluster::GetL0ID(){
  return m_l0id;
}

void Cluster::SetChannel(uint32_t channel){
  m_channel=channel;
}

void Cluster::SetAddress(uint32_t address){
  m_address=address;
}

void Cluster::SetPattern(uint32_t pattern){
  m_pattern=pattern;
}

void Cluster::SetBCID(uint32_t bcid){
  m_bcid=bcid;
}

void Cluster::SetBCIDParity(bool bcid_parity){
  m_bcid_parity=bcid_parity;
}

void Cluster::SetL0ID(uint32_t l0id){
  m_l0id=l0id;
}

std::string Cluster::ToString(){
  ostringstream os;
  os << "Cluster "
     << "bcid: " << m_bcid << " "
     << "l0id: " << m_l0id << " "
     << "bcid_parity: " << m_bcid_parity << " "
     << "channel: " << m_channel << " "
     << "address: " << m_address << " "
     << "pattern: " << m_pattern << " ";
  return os.str();
}
