#include "Strips/Encoder.h"
#include <iostream>
#include <sstream>
#include <iomanip>

using namespace std;
using namespace Strips;

Encoder::Encoder(){
  m_size = 1e6;
  m_bytes.reserve(m_size);
  m_bytes.resize(m_size,0);
  m_length = 0;
  m_l0a = new L0A();
  m_read = new ReadRegister();
  m_write = new WriteRegister();
  m_fast = new FastCommandReset();
  m_idle = new Idle();
}

Encoder::~Encoder(){
  m_bytes.clear();
  ClearCommands();
  delete m_l0a;
  delete m_read;
  delete m_write;
  delete m_idle;
  delete m_fast;
}

void Encoder::AddBytes(uint8_t *bytes, uint32_t pos, uint32_t len){
  for(uint32_t i=pos;i<pos+len;i++){
    m_bytes[m_length]=bytes[i];
    m_length++;
  }
}

void Encoder::SetBytes(uint8_t *bytes, uint32_t len, bool reversed){
  m_length=len;
  if(len>m_bytes.size()){m_bytes.resize(len);}
  if(!reversed){for(uint32_t i=0;i<len;i++){m_bytes[i]=bytes[i];}}
  else         {for(uint32_t i=0;i<len;i++){m_bytes[len-i-1]=bytes[i];}}
}

void Encoder::AddCommand(Command *cmd){
  m_cmds.push_back(cmd);
}
  
void Encoder::ClearBytes(){
  m_length=0;
}
  
void Encoder::ClearCommands(){
  while(!m_cmds.empty()){
    Command* cmd = m_cmds.back();
    delete cmd;
    m_cmds.pop_back();
  }
}

void Encoder::Clear(){
  ClearBytes();
  ClearCommands();
}
  
string Encoder::GetByteString(){
  ostringstream os;
  os << hex; 
  for(uint32_t pos=0; pos<m_length; pos++){
    os << setw(2) << setfill('0') << (uint32_t) m_bytes[pos] << " ";
  }
  return os.str();
}

void Encoder::GetBytes(uint8_t * bytes, uint32_t & length, bool reversed){
  length=m_length;
  if(!reversed){for(uint32_t i=0;i<m_length;i++){bytes[i]=m_bytes[i];}}
  else         {for(uint32_t i=0;i<m_length;i++){bytes[m_length-i-1]=m_bytes[i];}}
}

uint8_t * Encoder::GetBytes(bool reversed){
  if(!reversed){return m_bytes.data();}
  m_rbytes.resize(m_length);
  for(uint32_t i=0;i<m_length;i++){
    m_rbytes[m_length-i-1]=m_bytes[i];
  }
  return m_rbytes.data();
}
  
uint32_t Encoder::GetLength(){
  return m_length;
}
  
vector<Command*> & Encoder::GetCommands(){
  return m_cmds;
}

void Encoder::Encode(){
  uint32_t pos=0;
  for(uint32_t i=0;i<m_cmds.size();i++){
    if(m_size < pos+1000){m_size+=1000;m_bytes.resize(m_size,0);}
    pos+=m_cmds[i]->Pack(&m_bytes[pos]);
  }
  m_length=pos;
}

void Encoder::Decode(bool verbose){
  ClearCommands();
  uint32_t pos=0;
  uint32_t nb=0;
  uint32_t tnb=m_length;
  Command * cmd=0;

  while(pos<tnb){
    if((nb=m_idle->Unpack(&m_bytes[pos],tnb-pos))>0){
      if(verbose) cout << "Encoder::Decode Idle" << endl;
      cmd=m_idle; m_idle=new Idle();
    }
    else if((nb=m_fast->Unpack(&m_bytes[pos],tnb-pos))>0){
      if(verbose) cout << "Encoder::Decode Fast" << endl;
      cmd=m_fast; m_fast=new FastCommandReset();
    }
    else if((nb=m_l0a->Unpack(&m_bytes[pos],tnb-pos))>0){
      if(verbose) cout << "Encoder::Decode L0A" << endl;
      cmd=m_l0a; m_l0a=new L0A();
    }
    else if((nb=m_read->Unpack(&m_bytes[pos],tnb-pos))>0){
      if(verbose) cout << "Encoder::Decode Read" << endl;
      cmd=m_read; m_read=new ReadRegister();
    }
    else if((nb=m_write->Unpack(&m_bytes[pos],tnb-pos))>0){
      if(verbose) cout << "Encoder::Decode Write" << endl;
      cmd=m_write; m_write=new WriteRegister();
    }

    else{
      cout << "Encoder::Decode Cannot decode byte sequence: "
          << "0x" << hex << setw(2) << setfill('0') << (uint32_t) m_bytes[pos] << dec
          << " at index: " << pos
          << " ...skipping" << endl;
      pos++;
      continue;
    }
    if(!cmd){pos++; continue;}
    m_cmds.push_back(cmd);
    pos+=nb;
    if(verbose) {cout << "Encoder::Decode pos=" << pos << " tnb=" << tnb << endl;}
  }
}
