#include <Strips/Endeavour.h>

using namespace std;
using namespace Strips;

const std::map<uint32_t, std::string> Endeavour::typeNames={
  {NONE,"NONE"},
  {READ,"READ"},
  {NEXT,"NEXT"},
  {WRITE,"WRITE"},
  {SETID,"SETID"}
};

Endeavour::Endeavour(){
  m_type=NONE;
}

Endeavour::~Endeavour(){
  //nothing to do
}

void Endeavour::SetType(uint32_t typ){
  m_type=typ;
}

uint32_t Endeavour::GetType(){
  return m_type;
}

uint8_t Endeavour::CRC(uint8_t * bytes){
  uint32_t crc=0;
  for(uint32_t i=0;i<8;i++){
    crc|=((
        ((bytes[0]>>i)&1) ^
        ((bytes[1]>>i)&1) ^
        ((bytes[2]>>i)&1) ^
        ((bytes[3]>>i)&1) ^
        ((bytes[4]>>i)&1) ^
        ((bytes[5]>>i)&1))<<i);
  }
  return crc;
}
