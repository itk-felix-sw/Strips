#include "Strips/ABCLPPRPacket.h"
#include <iostream>
#include <sstream>


using namespace std;
using namespace Strips;

ABCLPPRPacket::ABCLPPRPacket(){
  m_type=Packet::TYP_PR;
  m_l0id=0;
  m_flag=0;
  m_bcid=0;
  m_parity=0;
}

ABCLPPRPacket::ABCLPPRPacket(ABCLPPRPacket *copy){
  m_type=m_type;
  m_l0id=copy->m_l0id;
  m_flag=copy->m_flag;
  m_bcid=copy->m_bcid;
  m_parity=copy->m_parity;
  m_clusters=copy->m_clusters;
}

ABCLPPRPacket::ABCLPPRPacket(uint32_t typ, uint32_t l0id, bool flag, uint32_t bcid, bool parity, vector<uint16_t> clusters){
  m_type=(typ==Packet::TYP_LP?Packet::TYP_LP:Packet::TYP_PR);
  m_l0id=l0id;
  m_flag=flag;
  m_bcid=bcid;
  m_parity=parity;
  m_clusters=clusters;
}

ABCLPPRPacket::~ABCLPPRPacket(){
  m_clusters.clear();
}

string ABCLPPRPacket::ToString(){
  ostringstream os;
  os << "ABCLPPRPacket "
     << "Type: " << Packet::typeNames[m_type] << " (0x" << hex << m_type << dec << ") "
     << "L0ID: 0x" << hex << (uint32_t) m_l0id << dec << ", "
     << "flag: " << m_flag << ", "
     << "BCID: 0x" << hex << (uint32_t) m_bcid << dec << ", "
     << "parity: " << m_parity << ", "
     << "cluster size: " <<  m_clusters.size() << dec;

    for (unsigned int i= 0; i< m_clusters.size(); ++i){

      //std::cout << "channel :" << hex << (m_clusters.at(i)) << dec << std::endl;

    } 
  return os.str();

}

void ABCLPPRPacket::SetL0ID(uint32_t l0id){
  m_l0id=m_l0id;
}

uint32_t ABCLPPRPacket::GetL0ID(){
  return m_l0id;
}

void ABCLPPRPacket::SetFlag(bool flag){
  m_flag=flag;
}

bool ABCLPPRPacket::GetFlag(){
  return m_flag;
}

void ABCLPPRPacket::SetBCID(uint32_t bcid){
  m_bcid=m_bcid;
}

uint32_t ABCLPPRPacket::GetBCID(){
  return m_bcid;
}

void ABCLPPRPacket::SetParity(bool parity){
  m_parity=parity;
}

bool ABCLPPRPacket::GetParity(){
  return m_parity;
}

void ABCLPPRPacket::SetClusters(std::vector<uint16_t> &clusters){
  m_clusters=clusters;
}

void ABCLPPRPacket::AddCluster(uint16_t cluster){
  m_clusters.push_back(cluster);
}

vector<uint16_t>& ABCLPPRPacket::GetClusters(){
  return m_clusters;
}

uint32_t ABCLPPRPacket::Pack(uint8_t * bytes){

  bytes[0] = (m_type & 0xF) << 4;
  bytes[0] |= (m_flag & 1) << 3;
  bytes[0] |=  m_l0id & 0x70;
  bytes[1] = (m_l0id & 0xF) << 4;
  bytes[1] |= (m_bcid & 0x7) << 1;
  bytes[1] |=  m_parity & 0x1;
  unsigned int byte_n = 2;

  for (unsigned int i =0; i < m_clusters.size(); ++i){
    // max size protection to be included

    byte_n = 2+2*i;
    bytes[byte_n] = ( m_clusters.at(i) & 0xFF00 ) >> 8;
    bytes[byte_n+1] = ( m_clusters.at(i) & 0x00FF );
  }
  bytes[byte_n+2] = 0x6F;
  bytes[byte_n+3] = 0xED;
  
  return 2+m_clusters.size()*2+2;
  
}

uint32_t ABCLPPRPacket::Unpack(uint8_t * bytes, uint32_t maxlen){

  if(maxlen<4){return 0;}

  m_type = (bytes[0]&0xF0) >> 4;

  if(m_type!=Packet::TYP_LP && m_type!=Packet::TYP_PR){
    return 0;
  }

  m_flag =  (bytes[0] >> 3) & 1;
  m_l0id = ((bytes[0] & 0b111) << 4) | ((bytes[1] >> 4) & 0xF); 
  m_bcid = (bytes[1] >> 1 & 0x7);
  m_parity = (bytes[1] & 0x1);
 
  //* skipping idle conditions for now  */
  
  unsigned int byte_n = 2; //skipping the first 2 bytes dedicated to the header of the physics package
  bool parsing = true;
  
  while (parsing){
    // add protection max size 
    // add check if we have reached the end of the package 

    uint16_t cluster = (bytes[byte_n] << 8) | bytes[byte_n+1];  // reading data 2 bytes at the time, each cluster is described by 15 bits 
    //m_data how to fill the data //|= cluster; 

    if (cluster == 0x6FED ) { parsing = false; } // end of physics packet 
    else {
      byte_n+=2;
      m_clusters.push_back(cluster);
    }
    
  }
  return 2+m_clusters.size()*2+2;  


}

