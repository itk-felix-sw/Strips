#include <Strips/ReadRegister.h>
#include <Strips/SixToEight.h>
#include <sstream>

using namespace std;
using namespace Strips;

ReadRegister::ReadRegister(){
  m_type=Command::READ;
  m_chip=0;
  m_hccid=0;
  m_abcid=0;
  m_address=0;
}

ReadRegister::ReadRegister(bool chip, uint8_t hccid, uint8_t abcid, uint8_t address){
  m_type=Command::READ;
  m_chip=chip;
  m_hccid=hccid;
  m_abcid=abcid;
  m_address=address;
}

ReadRegister::ReadRegister(ReadRegister * copy){
  m_type=copy->m_type;
  m_chip=copy->m_chip;
  m_hccid=copy->m_hccid;
  m_abcid=copy->m_abcid;
  m_address=copy->m_address;
}

ReadRegister::~ReadRegister(){
  //nothing to do
}

void ReadRegister::SetChip(bool chip){
  m_chip=chip;
}

bool ReadRegister::GetChip(){
  return m_chip;
}

void ReadRegister::SetHCCid(uint8_t hccid){
  m_hccid=hccid;
}

uint8_t ReadRegister::GetHCCid(){
  return m_hccid;
}

void ReadRegister::SetABCid(uint8_t abcid){
  m_abcid=abcid;
}

uint8_t ReadRegister::GetABCid(){
  return m_abcid;
}

void ReadRegister::SetAddress(uint8_t address){
  m_address=address;
}

uint8_t ReadRegister::GetAddress(){
  return m_address;
}

string ReadRegister::ToString(){
  ostringstream os;
  os << "ReadRegister "
     << "Chip: " << m_chip << " "
     << "HCC id: 0x" << hex << (uint32_t) m_hccid << dec << " "
     << "ABC id: 0x" << hex << (uint32_t) m_abcid << dec << " "
     << "Address: 0x" << hex << (uint32_t) m_address << dec << " ";
  return os.str();
}

uint32_t ReadRegister::Unpack (uint8_t * bytes, uint32_t maxlen){
  //we need 8 bytes
  if(maxlen<8) return 0;

  vector<uint8_t> bb(8,0);

  //decode
  for(uint32_t i=0;i<8;i++){
    bb[i]=SixToEight::decodeLUT[bytes[i]];
  }

  //First byte is a K2
  //Second to last is a K2
  if(bb[0]!=SixToEight::K2 or
     bb[6]!=SixToEight::K2){
    return 0;
  }

  //top header (K2 payload) has to have the start flag
  if((bb[1]&0x10)!=0x10){return 0;}

  //bottom header (K2 payload) has to have the end flag
  if((bb[5]&0x10)!=0x00){return 0;}

  //Should not be a Write Register command
  if(bb[2]!=1){return 0;}

  //We could actually check that MSB are 0

  //extract the payload
  m_chip = (bb[1]>>5) & 0x1;
  m_hccid = bb[1] & 0xF;
  m_abcid = (bb[2] >> 2) & 0xF;
  m_address = ((bb[3] & 0x3)<<6) | ( (bb[4]&0x1) << 5 ) | ((bb[5] >> 1) & 0x1F);

  return 8;
}
      
uint32_t ReadRegister::Pack(uint8_t * bytes){

  //payload
  vector<uint8_t> bb(8,0);

  bb[0] = SixToEight::K2;
  bb[1] = ((m_chip & 0x1) << 5) | (1 << 4) | (m_hccid & 0xF);
  bb[2] = 1;
  bb[3] = ((m_abcid & 0xF) << 2 ) | ((m_address >> 6) & 0x3);
  bb[4] = (m_address >> 5) & 0x01;
  bb[5] = (m_address & 0x1F) << 1;
  bb[6] = SixToEight::K2;
  bb[7] = ((m_chip & 0x1) << 5) | (0 << 4) | (m_hccid & 0xF);

  //encode
  for(uint32_t i=0;i<8;i++){
    bytes[i] = SixToEight::encodeLUT[bb[i]];
  }
  
  return 8;
}
    
