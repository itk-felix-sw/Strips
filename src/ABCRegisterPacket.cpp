#include "Strips/ABCRegisterPacket.h"
#include <iostream>
#include <sstream>

using namespace std;
using namespace Strips;

ABCRegisterPacket::ABCRegisterPacket(){
  m_type=Packet::TYP_ABC_RR;
  m_channel=0;
  m_address=0;
  m_data=0;
  m_statusbits=0;
}

ABCRegisterPacket::ABCRegisterPacket(uint32_t typ,uint8_t channel,uint8_t address, uint32_t data,uint32_t statusbits){
  m_type=(typ==Packet::TYP_ABC_HPR?Packet::TYP_ABC_HPR:Packet::TYP_ABC_RR);
  m_channel=channel;
  m_address=address;
  m_data=data;
  m_statusbits=statusbits;
}

ABCRegisterPacket::ABCRegisterPacket(ABCRegisterPacket *copy){
  m_type=copy->m_type;
  m_channel=copy->m_channel;
  m_address=copy->m_address;
  m_data=copy->m_data;
  m_statusbits=copy->m_statusbits;
}

ABCRegisterPacket::~ABCRegisterPacket(){
  //nothing to do
}

string ABCRegisterPacket::ToString(){
  ostringstream os;
  os << "ABCRegisterPacket "
     << "Type: " << Packet::typeNames[m_type] << " (0x" << hex << m_type << dec << ") "
     << "Chan: " << m_channel << ", "
     << "Address: 0x" << hex << (uint32_t) m_address << dec << ", "
     << "Data: 0x" << hex << m_data << dec << ", "
     << "Status: 0x" << hex << m_statusbits << dec;
  return os.str();
}

void ABCRegisterPacket::SetChannel(uint32_t channel){
  m_channel=channel;
}

uint32_t ABCRegisterPacket::GetChannel(){
  return m_channel;
}

void ABCRegisterPacket::SetAddress(uint32_t address){
  m_address=address;
}

uint32_t ABCRegisterPacket::GetAddress(){
  return m_address;
}

void ABCRegisterPacket::SetData(uint32_t data){
  m_data=data;
}

uint32_t ABCRegisterPacket::GetData(){
  return m_data;
}

void ABCRegisterPacket::SetStatusBits(uint32_t statusbits){
  m_statusbits=statusbits;
}

uint32_t ABCRegisterPacket::GetStatusBits(){
  return m_statusbits;
}

uint32_t ABCRegisterPacket::Pack(uint8_t * bytes){

  bytes[0] = ((m_type & 0x0F) << 4) | (m_channel & 0x0F);
  bytes[1] = m_address & 0xFF;
  bytes[2] = (m_data >> 28) & 0x0F;
  bytes[3] = (m_data >> 20) & 0xFF;
  bytes[4] = (m_data >> 12) & 0xFF;
  bytes[5] = (m_data >>  4) & 0xFF;
  bytes[6] = ((m_data & 0x0F) << 4) | (m_statusbits & 0x0F);
  bytes[7] = (m_statusbits >>  4) & 0xFF;
  bytes[8] = (m_statusbits >> 12) & 0x0F;

  return 9;
}

uint32_t ABCRegisterPacket::Unpack(uint8_t * bytes, uint32_t maxlen){
  m_type = (bytes[0]&0xF0) >> 4;

  if(m_type!=Packet::TYP_ABC_HPR and m_type!=Packet::TYP_ABC_RR){
    return 0;
  }
  
  m_channel = bytes[0] & 0x0F;
  m_address = bytes[1];
  m_data  = (bytes[2] & 0x0F) << 28;
  m_data |= (bytes[3] & 0xFF) << 20;
  m_data |= (bytes[4] & 0xFF) << 12;
  m_data |= (bytes[5] & 0xFF) <<  4;
  m_data |= (bytes[6] & 0xF0) >>  4;
  m_statusbits  = (bytes[6] & 0x0F) << 0;
  m_statusbits |= (bytes[7] & 0xFF) << 4;
  m_statusbits |= (bytes[8] & 0xF)  << 12;

  return 9;
}

/*
int ABCRegisterPacket::Parse(std::vector<uint16_t> *raw_words){
  if ( raw_words->size() != 11 ){
    if((raw_words->size()-2)/4 == 3){
      // if the number of words is not 11 but 13 we keep the packet due to SOP/EOP 
    }
    else return 1; 
  }
  
  m_channel = raw_words[1] & 0xF;
  m_address = raw_words[2] & 0xFF;

  m_data = ((raw_words[3] & 0xF) << 28)
         | ((raw_words[4] & 0xFF) << 20) 
         | ((raw_words[5] & 0xFF) << 12)
         | ((raw_words[6] & 0xFF) << 4)
         | ((raw_words[7] & 0xF0) >> 4);
  

  m_statusbits = ((raw_words[7] & 0xF) << 1) 
               | ((raw_words[8] & 0xFF) << 4)
               | ((raw_words[9] & 0xF0) >> 4);

  return 0;

}
*/
