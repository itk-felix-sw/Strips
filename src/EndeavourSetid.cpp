#include <Strips/EndeavourSetid.h>
#include <sstream>
#include <iomanip>

using namespace std;
using namespace Strips;

EndeavourSetid::EndeavourSetid(){
  m_type=Endeavour::SETID;
  m_newid=0;
  m_efuseid=0;
  m_padid=0;
}

EndeavourSetid::EndeavourSetid(uint8_t newid, uint32_t efuseid, uint8_t padid){
  m_newid=newid;
  m_efuseid=efuseid;
  m_padid=padid;
}

EndeavourSetid::EndeavourSetid(EndeavourSetid * copy){
  m_newid=copy->m_newid;
  m_efuseid=copy->m_efuseid;
  m_padid=copy->m_padid;
}

EndeavourSetid::~EndeavourSetid(){
  //nothing to do
}

void EndeavourSetid::SetNewID(uint8_t newid){
  m_newid = newid;
}

uint8_t EndeavourSetid::GetNewID(){
  return m_newid;
}

void EndeavourSetid::SetEfuseID(uint32_t efuseid){
  m_efuseid=efuseid;
}

uint32_t EndeavourSetid::GetEfuseID(){
  return m_efuseid;
}

void EndeavourSetid::SetPadID(uint8_t padid){
  m_padid=padid;
}

uint8_t EndeavourSetid::GetPadID(){
  return m_padid;
}

string EndeavourSetid::ToString(){
  ostringstream os;
  os << "EndeavourSetid "
     << "NewID: " << (uint32_t) m_newid << " "
     << "EfuseID: 0x" << hex << (uint32_t) m_efuseid << dec << " "
     << "PadID: 0x" << hex << (uint32_t) m_padid << dec;
  return os.str();
}

uint32_t EndeavourSetid::Unpack(uint8_t * bytes, uint32_t maxlen){
  if(maxlen<7) return 0;
  if((bytes[0] & 0xE0) != Endeavour::SETID){return 0;}
  
  m_newid = bytes[1] & 0x1F;
  m_efuseid = ((bytes[2]&0x0F)<<16) | ((bytes[3]&0xFF)<<8) | ((bytes[4]&0xFF)<<0);
  m_padid = bytes[5] & 0x1F;
  return 7;
}
      
uint32_t EndeavourSetid::Pack(uint8_t * bytes){
  
  bytes[0] = m_type | 0x1F;
  bytes[1] = 0xE0 | (m_newid & 0x1F);
  bytes[2] = 0xF0 | ((m_efuseid >> 16) & 0x0F);
  bytes[3] = (m_efuseid >>  8) & 0xFF;
  bytes[4] = (m_efuseid >>  0) & 0xFF;
  bytes[5] = 0xE0 | (m_padid & 0x1F);
  bytes[6] = CRC(bytes);
  return 7;
}
    
