#include <Strips/FastCommandReset.h>
#include <Strips/SixToEight.h>
#include <sstream>

using namespace std;
using namespace Strips;

map<uint32_t,string> FastCommandReset::typeNames={
    {NONE,"NONE"},
    {RESVD,"Reserved"},
    {LOGIC_RESET,"Logic Reset"},
    {ABC_REG_RESET,"ABC Register Reset"},
    {ABC_SEU_RESET,"ABC SEU Reset"},
    {ABC_CAL_PULSE,"ABC Calibration Pulse"},
    {ABC_DIGITAL_PULSE,"ABC Digital Pulse"},
    {ABC_HIT_COUNT_RESET,"ABC Hit Counter Reset"},
    {ABC_HIT_COUNT_START,"ABC Hit Counter Start"},
    {ABC_HIT_COUNT_STOP,"ABC Hit Counter Stop"},
    {ABC_SLOW_COMMAND_RESET,"ABC Slow Command Reset"},
    {HCC_STOP_PRLP,"Stop PR and LP to ABCStar ASICs"},
    {HCC_REG_RESET,"HCC Register Reset"},
    {HCC_SEU_RESET,"HCC SEU Reset"},
    {HCC_PLL_RESET,"HCC PLL Reset"},
    {HCC_START_PRLP,"Start PR and LP to ABCStar ASICs"}
};

FastCommandReset::FastCommandReset(){
  m_bc=0;
  m_command=0;
}

FastCommandReset::FastCommandReset(uint8_t bc, uint8_t command){
  m_bc=bc;
  m_command=command;
}

FastCommandReset::FastCommandReset(FastCommandReset * copy){
  m_bc=copy->m_bc;
  m_command=copy->m_command;
}

FastCommandReset::~FastCommandReset(){
  //nothing to do
}

void FastCommandReset::SetBC(uint8_t bc){
  m_bc = (bc >3) ? 3 : bc;
}

uint8_t FastCommandReset::GetBC(){
  return m_bc;
}

void FastCommandReset::SetCommand(uint8_t command){
  m_command=command;
}

uint8_t FastCommandReset::GetCommand(){
  return m_command;
}

string FastCommandReset::ToString(){
  ostringstream os;
  os << "FastCommandReset "
     << "BC: " << (uint32_t) m_bc << " "
     << "Command: 0x" << hex << (uint32_t) m_command << dec << " (" << typeNames[m_command] << ")";
  return os.str();
}

uint32_t FastCommandReset::Unpack (uint8_t * bytes, uint32_t maxlen){
  //we need 2 bytes
  if(maxlen<2) return 0;
  
  //decode the bytes into 6bits   
  uint32_t b0=SixToEight::decodeLUT[bytes[0]];
  uint32_t b1=SixToEight::decodeLUT[bytes[1]];

  //First byte is a K3
  if(b0!=SixToEight::K3){return 0;}
  
  //payload
  m_bc = (b1>>4) & 0x3;
  m_command = b1 & 0xF;

  return 2;
}
      
uint32_t FastCommandReset::Pack(uint8_t * bytes){
  //payload
  uint8_t b0 = SixToEight::K3;
  uint8_t b1 = ((m_bc<<4) & 0x30) | (m_command & 0xF);

  //encode
  bytes[0] = SixToEight::encodeLUT[b0];
  bytes[1] = SixToEight::encodeLUT[b1];
  
  return 2;
}
    
