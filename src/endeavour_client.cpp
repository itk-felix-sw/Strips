#include "Strips/EndeavourEncoder.h"
#include "netio/netio.hpp"
#include <cmdl/cmdargs.h>
#include <iostream>
#include <mutex>
#include <thread>
#include <signal.h>
#include <algorithm>
#include <iomanip>

using namespace std;
using namespace Strips;

struct FelixDataHeader{
  uint16_t length;
  uint16_t status;
  uint32_t elink;
};

struct FelixCmdHeader{
  uint32_t length;
  uint32_t reserved;
  uint64_t elink;
};

bool stopScan = false;

void Stop(int){
  cout << endl << "You pressed ctrl+c -> quitting" << endl;
  stopScan = true;
}

int main(int argc, char ** argv) {

  cout << "#################################" << endl
       << "# endeavour_client              #" << endl
       << "#################################" << endl;

  CmdArgBool cVerbose('v',"verbose","turn on verbose mode");
  CmdArgStr cHost('H',"host","host", "felix host");
  CmdArgInt cTxPort('T',"cmd-port","port", "felix data port");
  CmdArgInt cRxPort('R',"data-port","port", "felix data port");
  CmdArgInt cTxElink('t',"cmd-elink","elink", "elink tx", CmdArg::isREQ);
  CmdArgInt cRxElink('r',"data-elink","elink", "elink rx", CmdArg::isREQ);
  CmdArgStr cCmd("command","read,next,write,setid");
  CmdArgInt cAmacId("amacid","amac id");
  CmdArgIntList cOpts("options", "address, address value, fuseid padid", CmdArg::isOPT);
  
  CmdLine cmdl(*argv,&cVerbose,&cHost,&cTxPort,&cRxPort,&cTxElink,&cRxElink,&cCmd,&cAmacId,&cOpts,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);
  
  string host=(cHost.flags()&CmdArg::GIVEN?cHost:"127.0.0.1");
  uint32_t txport=(cTxPort.flags()&CmdArg::GIVEN?cTxPort:12340);
  uint32_t rxport=(cRxPort.flags()&CmdArg::GIVEN?cRxPort:12350);
  
  cout << "Create the context" << endl;
  netio::context * context = new netio::context("posix");
  thread context_thread([&](){context->event_loop()->run_forever();});
  mutex mtx;

  cout << "Create the data stream" << endl;
  vector<uint8_t> data;
  
  cout << "Create the felix header" << endl;
  FelixDataHeader hdr;

  cout << "Create the netio tx" << endl;
  netio::low_latency_send_socket * tx;  
  tx = new netio::low_latency_send_socket(context);

  cout << "Create the netio rx" << endl;
  netio::low_latency_subscribe_socket * rx;
  rx = new netio::low_latency_subscribe_socket(context, [&](netio::endpoint& ep, netio::message& msg){
      mtx.lock();
      if(cVerbose) cout << "Handler::Connect Received data from " << ep.address() << ":" << ep.port() << " size:" << msg.size() << endl;
      data = msg.data_copy();
      memcpy(&hdr,(const void*)&data[0], sizeof (hdr));
      cout << "Reply: 0x";
      for(uint32_t i=0; i<data.size(); i++){
	cout << hex << setw(2) << setfill('0') << (uint32_t)data[i] << dec;
      }
      cout << endl;
      mtx.unlock();
    });
  
  cout << "Connect to command elink: " << cTxElink << " at " << host << ":" << txport << endl;
  tx->connect(netio::endpoint(host,txport));

  cout << "Subscribe to data elink: " << cRxElink << " at " << host << ":" << rxport << endl;
  rx->subscribe(cRxElink, netio::endpoint(host, rxport));


  cout << "Create new encoder" << endl;
  EndeavourEncoder *encoder=new EndeavourEncoder();

  string cmd(cCmd);
  transform(cmd.begin(), cmd.end(), cmd.begin(),[](unsigned char c){ return tolower(c); });
  
  if(cmd=="read" and cOpts.count()>0){
    encoder->AddCommand(new EndeavourRead(cAmacId,cOpts[0]));
  }else if(cmd=="next"){
    encoder->AddCommand(new EndeavourNext(cAmacId));
  }else if(cmd=="write" and cOpts.count()>1){
    encoder->AddCommand(new EndeavourWrite(cAmacId,cOpts[0],cOpts[1]));

    ///* from write amac new
    

    //encoder->AddCommand(new EndeavourWrite(cAmacId,cOpts[0],cOpts[1]));
    //encoder->AddCommand(new EndeavourWrite(cAmacId,cOpts[0],cOpts[1]));


    //*/
  }else if(cmd=="setid" and cOpts.count()>1){
    encoder->AddCommand(new EndeavourSetid(cAmacId,cOpts[0],cOpts[1]));
  }else{
    cout << "Error parsing commands" << endl;
    cout << "commands: " << endl
	 << " read amacid address" << endl
	 << " next amacid" << endl
	 << " write amacid address value" << endl
	 << " setid amacid efuseid padid" << endl;  
  }      
  
  if(encoder->GetCommands().size()>0){
    cout << "Build message" << endl;
    encoder->Encode();
    cout << "Send message: " << encoder->GetByteString() << endl;
    FelixCmdHeader hdr;
    hdr.length=encoder->GetLength();
    hdr.elink=cTxElink;
    netio::message msg;
    msg.add_fragment((uint8_t*)&hdr,sizeof(hdr));
    msg.add_fragment(encoder->GetBytes(),encoder->GetLength());
    tx->send(msg);
    cout << "Wait for answer" << endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
  }
  
  cout << "Stop event loop" << endl;
  context->event_loop()->stop();
  
  cout << "Disconnect from data elink: " << cRxElink << " at " << host << ":" << rxport << endl;
  rx->unsubscribe(cRxElink, netio::endpoint(host, rxport));
  delete rx;
  
  cout << "Join context thread" << endl;
  context_thread.join();
  
  cout << "Delete the context" << endl;
  delete context;
  
  cout << "Delete the encoder" << endl;
  delete encoder;
  
  cout << "Have a nice day" << endl;
  return 0;
}
