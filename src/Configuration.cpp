#include <Strips/Configuration.h>
#include <iomanip>
#include <sstream>
#include <iostream>

using namespace std;
using namespace Strips;

Configuration::Configuration(){}

Configuration::~Configuration(){
  for(auto it : m_subregisters){
    delete it.second;
  }
  m_subregisters.clear();
}

void Configuration::SetID(uint16_t id){
  m_id=id;
}

uint16_t Configuration::GetID(){
  return m_id;
}

Register * Configuration::GetRegister(uint32_t addr){
  return &m_registers[addr];
}

Register * Configuration::GetRegister(string name){
  if(m_registerNames.find(name)==m_registerNames.end()){return 0;}
  return &m_registers[m_registerNames[name]];
}

SubRegister * Configuration::GetSubRegister(uint32_t addr){
  return m_subregisters[addr];
}

SubRegister * Configuration::GetSubRegister(string name){
  if(m_subRegisterNames.find(name)==m_subRegisterNames.end()){return 0;}
  return m_subregisters[m_subRegisterNames[name]];
}

void Configuration::SetRegisterValues(map<string,uint32_t> values){
  for(auto x: values){
    GetRegister(x.first)->SetValue(x.second);
  }
}

void Configuration::SetRegisterStrings(map<string,string> values){
  for(auto x: values){
    char * st;
    cout << "Register: " << x.first << " value:" << x.second << endl;
    GetRegister(x.first)->SetValue(strtoul(x.second.c_str(),&st,16));
  }
}

map<string,uint32_t> Configuration::GetRegisterValues(){
  map<string,uint32_t> values;
  for(auto x: m_registerNames){
    values[x.first] = GetRegister(x.second)->GetValue();
  }
  return values;
}

map<string,string> Configuration::GetRegisterStrings(){
  map<string,string > values;
  for(auto x: m_registerNames){
    ostringstream os;
    os << hex << setfill('0') << setw(8) << GetRegister(x.second)->GetValue(); 
    values[x.first] = os.str();
  }
  return values;
}

map<uint32_t,uint32_t> Configuration::GetUpdatedRegisterValues(){
  map<uint32_t,uint32_t> ret;
  for(uint32_t i=0;i<m_registers.size();i++){
    if(m_registers[i].IsUpdated()){ret[i]=m_registers[i].GetValue();m_registers[i].Update(false);}
  }
  return ret;
}

map<uint32_t,Register*> Configuration::GetUpdatedRegisters(){
  map<uint32_t,Register*> ret;
  for(uint32_t i=0;i<m_registers.size();i++){
    if(m_registers[i].IsUpdated()){ret[i]=&m_registers[i];}
  }
  return ret;
}

map<string,SubRegister*> Configuration::GetGivenSubRegisters(){
  map<string,SubRegister*> ret;
  for(auto x: m_subRegisterNames){
    if(!m_subregisters[x.second]->IsGiven()){continue;}
    ret[x.first] = m_subregisters[x.second];
  }
  return ret;
}

void Configuration::SetDefaults(){}

void Configuration::Backup(){
  for(uint32_t i=0; i<m_registers.size(); i++){
    m_backup[i]=m_registers[i].GetValue();
  }
}

void Configuration::Restore(){
  for(uint32_t i=0; i<m_registers.size(); i++){
    if(m_backup[i]==m_registers[i].GetValue()){continue;}
    m_registers[i].SetValue(m_backup[i]);
  }
}
  
