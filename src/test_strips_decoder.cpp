#include <Strips/Decoder.h>
#include <iostream>
#include <ctime>

using namespace std;
using namespace Strips;

int main() {

  cout << "Test Strips decoder" << endl;
  
  //create decoder
  Decoder * decoder=new Decoder();

  //add commands
  cout << "Adding packets to the decoder" << endl;

  decoder->AddPacket(new ABCLPPRPacket(Packet::TYP_LP,1,1,1,1,{0x1111,0x2222,0x3333,0x4444,0x5555,0x6666,0x7777,0x8888,0x9999,0xAAAA,0xBBBB,0xCCCC,0xDDDD,0xEEEE,0xFFFF}));
  decoder->AddPacket(new ABCLPPRPacket(Packet::TYP_LP,1,1,1,1,{0x1111,0x2222,0x3333,0x4444,0x5555,0x6666,0x7777,0x8888,0x9999,0xAAAA,0xBBBB,0xCCCC,0xDDDD,0xEEEE,0xFFFF}));
  decoder->AddPacket(new ABCLPPRPacket(Packet::TYP_LP,1,1,1,1,{0x1111,0x2222,0x3333,0x4444,0x5555,0x6666,0x7777,0x8888,0x9999,0xAAAA,0xBBBB,0xCCCC,0xDDDD,0xEEEE,0xFFFF}));
  decoder->AddPacket(new ABCLPPRPacket(Packet::TYP_LP,1,1,1,1,{0x1111,0x2222,0x3333,0x4444,0x5555,0x6666,0x7777,0x8888,0x9999,0xAAAA,0xBBBB,0xCCCC,0xDDDD,0xEEEE,0xFFFF}));
  decoder->AddPacket(new ABCLPPRPacket(Packet::TYP_LP,1,1,1,1,{0x1111,0x2222,0x3333,0x4444,0x5555,0x6666,0x7777,0x8888,0x9999,0xAAAA,0xBBBB,0xCCCC,0xDDDD,0xEEEE,0xFFFF}));
  decoder->AddPacket(new ABCLPPRPacket(Packet::TYP_LP,1,1,1,1,{0x1111,0x2222,0x3333,0x4444,0x5555,0x6666,0x7777,0x8888,0x9999,0xAAAA,0xBBBB,0xCCCC,0xDDDD,0xEEEE,0xFFFF}));
  decoder->AddPacket(new TransparentPacket(Packet::TYP_ABC_TRANSP, 1, {1,2,3,4,5,6,7,8}));
  decoder->AddPacket(new ABCRegisterPacket(Packet::TYP_ABC_HPR,1,0xFF,0xAAAA,0x5555));
  decoder->AddPacket(new HCCRegisterPacket(Packet::TYP_HCC_HPR,0xFF,0xAAAA));
  
  //check the contents
  cout << "Initial contents: " << endl;
  for(uint32_t i=0;i<decoder->GetPackets().size();i++){
    cout << "-- " << decoder->GetPackets()[i]->ToString() << endl;
  }
  cout << "Encode" << endl;
  decoder->Encode();
  cout << "Byte string: " << decoder->GetByteString() << endl;
  
  cout << "Get the bytes (from decoder)" << endl;
  uint8_t *bytes = decoder->GetBytes();
  uint32_t len = decoder->GetLength();
  cout << " Length: " << len << endl;

  cout << "Set the bytes (from FELIX)" << endl;
  decoder->SetBytes(bytes,len);

  cout << "Decode" << endl;
  decoder->Decode();
  cout << "Byte string: " << decoder->GetByteString() << endl;
  cout << "Decoded contents: " << endl;
  for(auto command : decoder->GetPackets()){
    cout << "-- " << command->ToString() << endl;
  }


  uint32_t n_test=1E6;
  clock_t start;
  double duration, frequency, bytefreq;

  cout << endl;
  cout << "Statistics:" << endl;
  cout << " Number of Packets: " << decoder->GetPackets().size() << endl;
  cout << " Number of Bytes: " << decoder->GetLength() << endl;
  cout << " Number of Repetitions: " << n_test << endl;
  cout << "Encoding performance: " << endl;
  start = clock();
  for(uint32_t i=0; i<n_test; i++){
    decoder->Encode();
  }
  duration = ( clock() - start ) / (double) CLOCKS_PER_SEC;
  frequency = (double) n_test / duration;
  bytefreq = decoder->GetLength() * frequency;
  
  cout << " CPU time  [s] : " << duration << endl;
  cout << " Frequency [MHz]: " << frequency/1E6 << endl;
  cout << " Byte Freq [MBps]: " << bytefreq/1E6 << endl;

  
  cout << "Decoding performance" << endl;
  start = clock();
  for(uint32_t i=0; i<n_test; i++){
    decoder->Decode();
  }

  duration = ( clock() - start ) / (double) CLOCKS_PER_SEC;
  frequency = (double) n_test / duration;
  bytefreq = decoder->GetLength() * frequency;

  cout << " CPU time  [s] : " << duration << endl;
  cout << " Frequency [MHz]: " << frequency/1E6 << endl;
  cout << " Byte Freq [MBps]: " << bytefreq/1E6 << endl;
  
  cout << "Cleaning the house" << endl;
  delete decoder;

  cout << "Have a nice day" << endl;
  
  return 0;
}

