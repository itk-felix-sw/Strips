#include "Strips/TransparentPacket.h"
#include <iostream>
#include <sstream>

using namespace std;
using namespace Strips;

TransparentPacket::TransparentPacket(){
  m_type=Packet::TYP_ABC_TRANSP;
  m_channel=0;
  m_payload.resize(8,0);
}

TransparentPacket::TransparentPacket(uint32_t typ, uint32_t channel, std::vector<uint8_t> payload){
  m_type=(typ==Packet::TYP_ABC_FULL?Packet::TYP_ABC_FULL:Packet::TYP_ABC_TRANSP);
  m_channel=channel;
  SetPayload(payload);
}

TransparentPacket::TransparentPacket(uint32_t typ, uint32_t channel, uint8_t * payload, uint32_t maxlen){
  m_type=(typ==Packet::TYP_ABC_FULL?Packet::TYP_ABC_FULL:Packet::TYP_ABC_TRANSP);
  m_channel=channel;
  SetPayload(payload,maxlen);  
}

TransparentPacket::TransparentPacket(TransparentPacket *copy){
  m_type=copy->m_type;
  m_channel=copy->m_channel;
  m_payload=copy->m_payload;
}

TransparentPacket::~TransparentPacket(){
  m_payload.clear();
}

string TransparentPacket::ToString(){
  ostringstream os;
  os << "TransparentPacket "
     << "Type: " << Packet::typeNames[m_type] << " (0x" << hex << m_type << dec << ") "
     << "Chan: " << (uint32_t)m_channel << ", "
     << "Payload: 0x";
  for(uint8_t bb: m_payload){
    os << hex << (uint32_t) bb << dec;
  }
  return os.str();
}

void TransparentPacket::SetChannel(uint32_t channel){
  m_channel=channel;
}

uint32_t TransparentPacket::GetChannel(){
  return m_channel;
}

void TransparentPacket::SetPayload(std::vector<uint8_t> & payload){
  uint32_t len=(m_type==TYP_ABC_TRANSP?8:64);
  if(payload.size()<len){return;}
  m_payload=payload;
}

void TransparentPacket::SetPayload(uint8_t * payload, uint32_t maxlen){
  uint32_t len=(m_type==TYP_ABC_TRANSP?8:64);
  if(maxlen<len){return;}
  m_payload.clear();
  for(uint32_t i=0;i<len;i++){m_payload.push_back(payload[i]);}
}

std::vector<uint8_t> * TransparentPacket::GetPayload(){
  return &m_payload;
}

uint32_t TransparentPacket::Pack(uint8_t * bytes){
  uint32_t len=(m_type==TYP_ABC_TRANSP?8:64);

  bytes[0] = ((m_type & 0x0F) << 4) | (m_channel & 0x0F);
  for(uint32_t i=0;i<len;i++){bytes[i+1]=m_payload[i];}
  return len+1;
}

uint32_t TransparentPacket::Unpack(uint8_t * bytes, uint32_t maxlen){
  m_type = (bytes[0]&0xF0) >> 4;

  if(m_type!=Packet::TYP_ABC_TRANSP and m_type!=Packet::TYP_ABC_FULL){
    return 0;
  }
  
  uint32_t len=(m_type==TYP_ABC_TRANSP?8:64);
  if(maxlen<len){return 0;}
  m_channel = bytes[0] & 0x0F;
  m_payload.clear();
  for(uint32_t i=0;i<len;i++){m_payload.push_back(bytes[i+1]);}
  return len+1;
}

