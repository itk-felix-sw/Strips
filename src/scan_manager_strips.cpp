#include <Strips/Handler.h>
#include <Strips/TestScan.h>

#include <cmdl/cmdargs.h>
#include <signal.h>
#include <sstream>
#include <chrono>
#include <map>
#include <fstream>

using namespace std;
using namespace Strips;

int main(int argc, char *argv[]){

  cout << "#####################################" << endl
       << "# Welcome to scan_manager_strips    #" << endl
       << "#####################################" << endl;

  CmdArgStr  cScan(     's',"scan","scan","scan name",CmdArg::isREQ);
  CmdArgStrList cConfs( 'c',"config","chips","chip names");
  CmdArgBool cVerbose(  'v',"verbose","turn on verbose mode");
  CmdArgStr  cBackend(  'b',"backend","type","posix,rdma. Default posix");
  CmdArgStr  cInterface('i',"interface","network-card","Default enp24s0f0");
  CmdArgStr  cNetFile(  'n',"netfile","file","Network file. Default network.json");
  CmdArgInt  cCharge(   'q',"charge","electrons","Injection charge");
  CmdArgBool cRetune(   'r',"retune","enable retune mode");
  CmdArgStr  cOutPath(  'o',"outpath","path","output path. Default $ITK_DATA_PATH");
  CmdArgInt  cToT(      't',"ToT","bc","target ToT");
  CmdArgInt  cThreshold('a',"threshold","electrons","threshold target");
  CmdArgInt  cWait(     'w',"wait","seconds","time to wait between config and run. Default: 3");
  CmdArgStr  cFrontEnd( 'f',"frontend","type","sync, lin, diff, all. Some scans accept frontend range");
  CmdArgBool cStoreHits('H',"hits","turn on the store hits flag");
  
  CmdLine cmdl(*argv,&cScan,&cFrontEnd,&cConfs,&cVerbose,&cBackend,&cInterface,&cNetFile,&cCharge,&cRetune,&cOutPath,&cToT,&cThreshold,&cWait,&cStoreHits,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  string scan(cScan);
  string backend(cBackend.flags()&CmdArg::GIVEN?cBackend:"posix");
  string interface=(cInterface.flags()&CmdArg::GIVEN?cInterface:"enp24s0f0");
  string netfile=(cNetFile.flags()&CmdArg::GIVEN?cNetFile:"connectivity-emu-rd53a.json");

  Handler * handler = 0;
  if(scan.find("Test")!=string::npos){handler=new TestScan();}

  else{
    cout << "Scan Manager: Scan not recognized: " << scan << endl;
    return 0;
  }

  handler->SetVerbose(cVerbose);
  handler->SetContext(backend);
  handler->SetInterface(interface);
  handler->SetMapping(netfile,(cConfs.count()==0));
  handler->SetRetune(cRetune);


  for(uint32_t i=0;i<cConfs.count();i++){
    cout << "Scan Manager: Adding front end #" << i << " " << string(cConfs[i]) << endl;
    handler->LoadFE(string(cConfs[i]));
  }
  
  cout << "Scan Manager: Configure the scan" << endl;
  if(cOutPath.flags()&CmdArg::GIVEN){
    handler->SetOutPath(string(cOutPath));
  }
  if(cCharge.flags()&CmdArg::GIVEN){
    handler->SetCharge(cCharge);
  }
  if(cThreshold.flags()&CmdArg::GIVEN){
    handler->SetThreshold(cThreshold);
  }

  cout << "Scan Manager: Connect" << endl;
  handler->Connect();

  cout << "Scan Manager: InitRun" << endl;
  handler->InitRun();

  cout << "Scan Manager: Config" << endl;
  handler->Config();

  cout << "Scan Manager: PreRun" << endl;
  handler->PreRun();
  
  cout << "Scan Manager: Save configuration before the run" << endl;
  handler->SaveConfig("before");

  cout << "Scan Manager: Wait for run to start: " << endl;
  sleep(cWait);

  cout << "Scan Manager: Run" << endl;
  handler->Run();

  cout << "Scan Manager: Analysis" << endl;
  handler->Analysis();

  cout << "Scan Manager: Disconnect" << endl;
  handler->Disconnect();

  cout << "Scan Manager: Save configuration after the run" << endl;
  handler->SaveConfig("after");

  cout << "Scan Manager: Save the tuned files" << endl;
  handler->SaveConfig("tuned");

  cout << "Scan Manager: Save the histograms" << endl;
  handler->Save();

  cout << "Scan Manager: Cleaning the house" << endl;
  delete handler;

  cout << "Scan Manager: Have a nice day" << endl;
  return 0;
}
