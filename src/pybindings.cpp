#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <Strips/Handler.h>
#include <Strips/FrontEnd.h>
#include <Strips/Encoder.h>
#include <Strips/Decoder.h>
#include <Strips/AMACv2.h>

namespace py = pybind11;
using namespace Strips;
using namespace std;

PYBIND11_MODULE(libStrips, m) {
  m.doc() = "Python binding for Strips itk-felix-sw";

  py::class_<Handler>(m, "Handler")
    .def(py::init())
    .def("SetVerbose", &Handler::SetVerbose)
    .def("SetContext", &Handler::SetContext)
    .def("SetInterface", &Handler::SetInterface)
    .def("SetRetune", &Handler::SetRetune)
    .def("GetRetune", &Handler::GetRetune)
    .def("SetEnableOutput", &Handler::SetEnableOutput)
    .def("GetEnableOutput", &Handler::GetEnableOutput)
    .def("SetThreshold", &Handler::SetThreshold)
    .def("GetThreshold", &Handler::GetThreshold)
    .def("SetOutPath", &Handler::SetOutPath)
    .def("GetOutPath", &Handler::GetOutPath)
    .def("GetFullOutPath", &Handler::GetFullOutPath)
    .def("GetFE", &Handler::GetFE) 
    .def("GetFEs", &Handler::GetFEs) 
    .def("GetFEDataElink", &Handler::GetFEDataElink) 
    .def("GetFECmdElink", &Handler::GetFECmdElink) 
    .def("SetMapping", &Handler::SetMapping)
    .def("AddMapping", &Handler::AddMapping)
    .def("AddFE", py::overload_cast<std::string, FrontEnd*>(&Handler::AddFE)) 
    .def("AddFE", py::overload_cast<std::string, std::string>(&Handler::AddFE))
    .def("LoadFE", &Handler::LoadFE)
    .def("SaveFE", &Handler::SaveFE)
    .def("Connect", &Handler::Connect)
    .def("InitRun", &Handler::InitRun)
    .def("Config", &Handler::Config)
    .def("PreRun", &Handler::PreRun)
    .def("Run", &Handler::Run)
    .def("Analysis", &Handler::Analysis)
    .def("Disconnect", &Handler::Disconnect)
    .def("PrepareTrigger", py::overload_cast<uint32_t,bool>(&Handler::PrepareTrigger))
    .def("PrepareTrigger", py::overload_cast<Encoder*>(&Handler::PrepareTrigger))
    .def("Trigger", &Handler::Trigger)
    .def("Send", &Handler::Send) 
    .def("GetFEConfig", &Handler::GetFEConfig) 
    .def("SaveConfig", &Handler::SaveConfig) 
    .def("GetResults", &Handler::GetResults)
    .def("Save", &Handler::Save);

  py::class_<FrontEnd>(m, "FrontEnd")
    .def(py::init())
    .def("SetVerbose", &FrontEnd::SetVerbose)
    .def("SetName", &FrontEnd::SetName)
    .def("GetName", &FrontEnd::GetName)
    .def("Reset", &FrontEnd::Reset)
    .def("Configure", &FrontEnd::Reset)
    .def("WriteHCCRegisters", &FrontEnd::WriteHCCRegisters)
    .def("WriteABCRegisters", &FrontEnd::WriteABCRegisters)
    .def("GetHCC", &FrontEnd::GetHCC)
    .def("GetABC", &FrontEnd::GetABC)
    .def("GetABCs", &FrontEnd::GetABCs)
    .def("AddABC", &FrontEnd::AddABC)
    .def("ProcessCommands", &FrontEnd::ProcessCommands)
    .def("GetBytes",&FrontEnd::GetBytes)
    .def("GetLength",&FrontEnd::GetLength)
    .def("Clear",&FrontEnd::Clear)
    .def("SetActive",&FrontEnd::SetActive)
    .def("IsActive",&FrontEnd::IsActive)
    .def("GetCluster",&FrontEnd::GetCluster)
    .def("NextCluster",&FrontEnd::NextCluster)
    .def("HasClusters",&FrontEnd::HasClusters)
    .def("HandleData",&FrontEnd::HandleData)
    .def("Trigger",&FrontEnd::Trigger);

  py::class_<Configuration>(m,"Configuration")
    .def(py::init())
    .def("SetID",&Configuration::SetID)
    .def("GetID",&Configuration::GetID)
    .def("GetRegister",py::overload_cast<uint32_t>(&Configuration::GetRegister))
    .def("GetRegister",py::overload_cast<string>(&Configuration::GetRegister))
    .def("GetSubRegister",py::overload_cast<uint32_t>(&Configuration::GetSubRegister))
    .def("GetSubRegister",py::overload_cast<string>(&Configuration::GetSubRegister))
    .def("SetRegisterValues",&Configuration::SetRegisterValues)
    .def("SetRegisterStrings",&Configuration::SetRegisterStrings)
    .def("GetRegisterValues",&Configuration::GetRegisterValues)
    .def("GetRegisterStrings",&Configuration::GetRegisterStrings)
    .def("GetUpdatedRegisters",&Configuration::GetUpdatedRegisters)
    .def("GetGivenSubRegisters",&Configuration::GetGivenSubRegisters)
    .def("SetDefaults",&Configuration::SetDefaults)
    .def("Backup",&Configuration::Backup)
    .def("Restore",&Configuration::Restore);

  py::class_<ABC,Configuration>(m,"ABC")
    .def(py::init());

  py::class_<HCC,Configuration>(m,"HCC")
    .def(py::init());

  py::class_<AMACv2,Configuration>(m,"AMACv2")
    .def(py::init());

  py::class_<Register>(m,"Register")
    .def(py::init())
    .def("SetValue",&Register::SetValue)
    .def("GetValue",&Register::GetValue)
    .def("Update",&Register::Update)
    .def("IsUpdated",&Register::IsUpdated);

  py::class_<SubRegister>(m,"SubRegister")
    .def(py::init<Register*,uint32_t, uint32_t,uint32_t, bool>())
    .def("SetValue",&SubRegister::SetValue)
    .def("GetValue",&SubRegister::GetValue)
    .def("IsGiven",py::overload_cast<bool>(&SubRegister::IsGiven))
    .def("IsGiven",py::overload_cast<>(&SubRegister::IsGiven));

  py::class_<Decoder>(m,"Decoder")
    .def(py::init())
    .def("AddBytes",&Decoder::AddBytes)
    .def("SetBytes",&Decoder::SetBytes)
    .def("AddPacket",&Decoder::AddPacket)
    .def("ClearBytes",&Decoder::ClearBytes)
    .def("ClearPackets",&Decoder::ClearPackets)
    .def("Clear",&Decoder::Clear)
    .def("GetByteString",&Decoder::GetByteString)
    //.def("GetBytes",py::overload_cast<uint8_t*,uint32_t&,bool>(&Decoder::GetBytes))
    .def("GetBytes",py::overload_cast<bool>(&Decoder::GetBytes))
    .def("GetLength",&Decoder::GetLength)
    .def("Encode",&Decoder::Encode)
    .def("Decode",&Decoder::Decode)
    .def("GetPackets",&Decoder::GetPackets);

  py::class_<Encoder>(m,"Encoder")
    .def(py::init())
    .def("AddBytes",&Encoder::AddBytes)
    .def("SetBytes",&Encoder::SetBytes)
    .def("AddCommand",&Encoder::AddCommand)
    .def("ClearBytes",&Encoder::ClearBytes)
    .def("ClearCommands",&Encoder::ClearCommands)
    .def("Clear",&Encoder::Clear)
    .def("GetByteString",&Encoder::GetByteString)
    //.def("GetBytes",py::overload_cast<uint8_t*,uint32_t&length,bool>(&Encoder::GetBytes))
    .def("GetBytes",py::overload_cast<bool>(&Encoder::GetBytes))
    .def("GetLength",&Encoder::GetLength)
    .def("Encode",&Encoder::Encode)
    .def("Decode",&Encoder::Decode)
    .def("GetCommands",&Encoder::GetCommands);

  py::class_<Packet>(m,"Packet")
    .def("GetType",&Packet::GetType)
    .def("SetType",&Packet::SetType)
    .def("ToString",&Packet::ToString)
    .def("Unpack",&Packet::Unpack)
    .def("Pack",&Packet::Pack);
 
  py::class_<ABCLPPRPacket,Packet>(m,"ABCLPPRPacket")
    .def(py::init())
    .def("SetL0ID",&ABCLPPRPacket::SetL0ID)
    .def("GetL0ID",&ABCLPPRPacket::GetL0ID)
    .def("SetFlag",&ABCLPPRPacket::SetFlag)
    .def("GetFlag",&ABCLPPRPacket::GetFlag)
    .def("SetBCID",&ABCLPPRPacket::SetBCID)
    .def("GetBCID",&ABCLPPRPacket::GetBCID)
    .def("SetParity",&ABCLPPRPacket::SetParity)
    .def("GetParity",&ABCLPPRPacket::GetParity)
    .def("SetClusters",&ABCLPPRPacket::SetClusters)
    .def("AddCluster",&ABCLPPRPacket::AddCluster)
    .def("GetClusters",&ABCLPPRPacket::GetClusters);

  py::class_<ABCRegisterPacket,Packet>(m,"ABCRegisterPacket")
    .def(py::init())
    .def("SetChannel",&ABCRegisterPacket::SetChannel)
    .def("GetChannel",&ABCRegisterPacket::GetChannel)
    .def("SetAddress",&ABCRegisterPacket::SetAddress)
    .def("GetAddress",&ABCRegisterPacket::GetAddress)
    .def("SetData",&ABCRegisterPacket::SetData)
    .def("GetData",&ABCRegisterPacket::GetData)
    .def("SetStatusBits",&ABCRegisterPacket::SetStatusBits)
    .def("GetStatusBits",&ABCRegisterPacket::GetStatusBits);
 
  py::class_<HCCRegisterPacket,Packet>(m,"HCCRegisterPacket")
    .def(py::init())
    .def("SetAddress",&HCCRegisterPacket::SetAddress)
    .def("GetAddress",&HCCRegisterPacket::GetAddress)
    .def("SetData",&HCCRegisterPacket::SetData)
    .def("GetData",&HCCRegisterPacket::GetData);
 
  py::class_<TransparentPacket,Packet>(m,"TransparentPacket")
    .def(py::init())
    .def("SetChannel",&TransparentPacket::SetChannel)
    .def("GetChannel",&TransparentPacket::GetChannel)
    .def("SetPayload",py::overload_cast<vector<uint8_t>&>(&TransparentPacket::SetPayload))
    .def("GetPayload",&TransparentPacket::GetPayload);

  py::class_<Command>(m,"Command")
    .def("ToString",&Command::ToString)
    .def("Unpack",&Command::Unpack)
    .def("Pack",&Command::Pack)
    .def("GetType",&Command::GetType)
    .def("SetType",&Command::SetType);
   
  py::class_<FastCommandReset,Command>(m,"FastCommandReset")
    .def(py::init())
    .def("SetBC",&FastCommandReset::SetBC)
    .def("GetBC",&FastCommandReset::GetBC)
    .def("SetCommand",&FastCommandReset::SetCommand)
    .def("GetCommand",&FastCommandReset::GetCommand);

  py::class_<Idle,Command>(m,"Idle")
    .def(py::init());
  
  py::class_<L0A,Command>(m,"L0A")
    .def(py::init())
    .def("SetBCR",&L0A::SetBCR)
    .def("GetBCR",&L0A::GetBCR)
    .def("SetL0A",py::overload_cast<uint8_t>(&L0A::SetL0A))
    .def("SetL0A",py::overload_cast<bool,bool,bool,bool>(&L0A::SetL0A))
    .def("SetL0A",py::overload_cast<uint32_t, bool>(&L0A::SetL0A))
    .def("GetL0A",py::overload_cast<>(&L0A::GetL0A))
    .def("GetL0A",py::overload_cast<uint32_t>(&L0A::GetL0A))
    .def("SetL0Tag",&L0A::SetL0Tag)
    .def("GetL0Tag",&L0A::GetL0Tag);

  py::class_<ReadRegister,Command>(m,"ReadRegister")
    .def(py::init())
    .def("SetChip",&ReadRegister::SetChip)
    .def("GetChip",&ReadRegister::GetChip)
    .def("SetHCCid",&ReadRegister::SetHCCid)
    .def("GetHCCid",&ReadRegister::GetHCCid)
    .def("SetABCid",&ReadRegister::SetABCid)
    .def("GetABCid",&ReadRegister::GetABCid)
    .def("SetAddress",&ReadRegister::SetAddress)
    .def("GetAddress",&ReadRegister::GetAddress);
  
  py::class_<WriteRegister,Command>(m,"WriteRegister")
    .def(py::init())
    .def("SetChip",&WriteRegister::SetChip)
    .def("GetChip",&WriteRegister::GetChip)
    .def("SetHCCid",&WriteRegister::SetHCCid)
    .def("GetHCCid",&WriteRegister::GetHCCid)
    .def("SetABCid",&WriteRegister::SetABCid)
    .def("GetABCid",&WriteRegister::GetABCid)
    .def("SetAddress",&WriteRegister::SetAddress)
    .def("GetAddress",&WriteRegister::GetAddress)
    .def("SetValue",&WriteRegister::SetValue)
    .def("GetValue",&WriteRegister::GetValue);
  
    
 
  
}
