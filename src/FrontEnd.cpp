#include <Strips/FrontEnd.h>
#include <Strips/Encoder.h>
#include <iostream>


using namespace std;
using namespace Strips;

FrontEnd::FrontEnd(){
  m_hcc=new HCC();
  m_name="Star";
  m_encoder = new Encoder();
  m_decoder = new Decoder();
  m_verbose = false;
}

FrontEnd::~FrontEnd(){
  delete m_hcc;
  for(auto abc: m_abcs){
    delete abc;
  }
  m_abcs.clear();
  delete m_encoder;
  delete m_decoder;
  ClearClusters();
}

void FrontEnd::SetVerbose(bool enable){
  m_verbose = enable;
}

void FrontEnd::SetName(string name){
  m_name=name;
}

string FrontEnd::GetName(){
  return m_name;
}

void FrontEnd::Reset(){
  
  uint8_t bc = 0; //2-bit max value, 3 default value 0
  m_encoder->AddCommand(new Idle());
  m_encoder->AddCommand(new Idle());
  m_encoder->AddCommand(new FastCommandReset(bc, FastCommandReset::ABC_REG_RESET));
  m_encoder->AddCommand(new Idle());
  m_encoder->AddCommand(new Idle());
  m_encoder->AddCommand(new FastCommandReset(bc, FastCommandReset::ABC_SLOW_COMMAND_RESET));
  m_encoder->AddCommand(new Idle());
  m_encoder->AddCommand(new Idle());
  m_encoder->AddCommand(new FastCommandReset(bc, FastCommandReset::ABC_SEU_RESET));
  m_encoder->AddCommand(new Idle());
  m_encoder->AddCommand(new Idle());
  m_encoder->AddCommand(new FastCommandReset(bc, FastCommandReset::ABC_HIT_COUNT_RESET));
  m_encoder->AddCommand(new Idle());
  m_encoder->AddCommand(new Idle());
  m_encoder->AddCommand(new FastCommandReset(bc, FastCommandReset::ABC_HIT_COUNT_START));
  m_encoder->AddCommand(new Idle());
  m_encoder->AddCommand(new Idle());
  m_encoder->AddCommand(new FastCommandReset(bc, FastCommandReset::HCC_START_PRLP));
  // reset the BCID counter
  m_encoder->AddCommand(new L0A(true, 0, 0)); 

}

void FrontEnd::Configure(){

  m_encoder->AddCommand(new Idle());
  m_encoder->AddCommand(new Idle());
  m_encoder->AddCommand(new FastCommandReset(0, FastCommandReset::LOGIC_RESET));
  m_encoder->AddCommand(new Idle());
  m_encoder->AddCommand(new Idle());
  m_encoder->AddCommand(new FastCommandReset(0, FastCommandReset::HCC_REG_RESET));

  WriteHCCRegisters();

  Reset();

  WriteABCRegisters();

}

void FrontEnd::WriteABCRegisters(){
  for (unsigned int i =0; i < m_abcs.size(); ++i ){
    for(auto rr : m_abcs[i]->GetUpdatedRegisterValues()){
      m_encoder->AddCommand(new Idle());
      m_encoder->AddCommand(new Idle());
      m_encoder->AddCommand(new WriteRegister(WriteRegister::ABC,m_hcc->GetID(),m_abcs[i]->GetID(),rr.first,rr.second));
    }
  }
}

void FrontEnd::WriteHCCRegisters(){
  for(auto rr : m_hcc->GetUpdatedRegisterValues()){
    m_encoder->AddCommand(new Idle());
    m_encoder->AddCommand(new Idle());
    m_encoder->AddCommand(new WriteRegister(WriteRegister::HCC,m_hcc->GetID(),0,rr.first,rr.second));
  }
}

HCC * FrontEnd::GetHCC(){
  return m_hcc;
}

void FrontEnd::AddABC(ABC* abc){
  if(abc==0){abc=new ABC();}
  m_abcs.push_back(abc);
}

ABC * FrontEnd::GetABC(uint32_t index){
  return m_abcs[index];
}

vector<ABC*> & FrontEnd::GetABCs(){
  return m_abcs;
}

void FrontEnd::ProcessCommands(){
  m_encoder->Encode();
}

uint8_t * FrontEnd::GetBytes(){
  return m_encoder->GetBytes();
}

uint32_t FrontEnd::GetLength(){
  return m_encoder->GetLength();
}

void FrontEnd::Clear(){
  m_encoder->Clear();
}

bool FrontEnd::IsActive(){
  return m_active;
}

void FrontEnd::SetActive(bool active){
  m_active=active;
}

void FrontEnd::HandleData(uint8_t * recv_data, uint32_t recv_size){
  if(m_verbose) cout << "FrontEnd::HandleData" <<endl;
  m_decoder->SetBytes(recv_data,recv_size,true);
  if(m_verbose){cout << "FrontEnd::HandleData Decode bytestream: " << m_decoder->GetByteString() << endl;}
  m_decoder->Decode(m_verbose);
  Cluster cluster;
  for(auto packet: m_decoder->GetPackets()){
    if(m_verbose) cout << "FrontEnd::HandleData: " << packet->ToString() << endl;
    if(packet->GetType()==Packet::TYP_LP || packet->GetType()==Packet::TYP_PR){
      ABCLPPRPacket * lppr=dynamic_cast<ABCLPPRPacket*>(packet);
      cluster.SetBCID(lppr->GetBCID());
      cluster.SetL0ID(lppr->GetL0ID());
      for(auto rawcls: lppr->GetClusters()){
        Cluster * nc = cluster.Clone();
        nc->Parse(rawcls);
        m_mutex.lock();
        m_clusters.push_back(nc);
        m_mutex.unlock();
      }
    }else if(packet->GetType()==Packet::TYP_HCC_RR or packet->GetType()==Packet::TYP_HCC_HPR){
      HCCRegisterPacket * reg=dynamic_cast<HCCRegisterPacket*>(packet);
      m_hcc->GetRegister(reg->GetAddress())->SetValue(reg->GetData());
      m_hcc->GetRegister(reg->GetAddress())->Update(false);
    }else if(packet->GetType()==Packet::TYP_ABC_RR or packet->GetType()==Packet::TYP_ABC_HPR){
      ABCRegisterPacket * reg=dynamic_cast<ABCRegisterPacket*>(packet);
      for(uint32_t i=0;i<m_abcs.size();i++){
        if(m_abcs[i]->GetID()==reg->GetChannel()){
          m_abcs[i]->GetRegister(reg->GetAddress())->SetValue(reg->GetData());
          m_abcs[i]->GetRegister(reg->GetAddress())->Update(false);
        }
      }
    }
  }      

}

void FrontEnd::Trigger(uint32_t delay, bool digital){

  if(delay==0){
    m_encoder->AddCommand(new Idle());
    uint32_t cmd=(digital?FastCommandReset::ABC_DIGITAL_PULSE:FastCommandReset::ABC_CAL_PULSE);
    m_encoder->AddCommand(new FastCommandReset(cmd, 3-(delay%4)));
    for(uint32_t i=0; i<delay/4; i++){m_encoder->AddCommand(new Idle());}
    m_encoder->AddCommand(new L0A(false,1,0));
    m_encoder->AddCommand(new Idle());
  }else{
    for(uint32_t i=0;i<8;i++){m_encoder->AddCommand(new Idle());}
    m_encoder->AddCommand(new L0A(false,4,10));
  }

}

Cluster * FrontEnd::GetCluster(){
  Cluster * tmp = NULL;
  m_mutex.lock();
  tmp = m_clusters.front();
  m_mutex.unlock();
  return tmp;
}

bool FrontEnd::NextCluster(){
  if (m_clusters.empty()) return false;
  m_mutex.lock();
  Cluster * cluster = m_clusters.front();
  m_clusters.pop_front();
  delete cluster;
  m_mutex.unlock();
  return (!m_clusters.empty());
}

bool FrontEnd::HasClusters(){
  return (not m_clusters.empty());
}

void FrontEnd::ClearClusters(){
  while(NextCluster());
}

