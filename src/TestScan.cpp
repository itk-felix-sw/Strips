#include <Strips/TestScan.h>
#include <iostream>

using namespace std;
using namespace Strips;

TestScan::TestScan(){
  m_scan="TestScan";
  m_ntrigs=10;
}

TestScan::~TestScan(){}

void TestScan::PreRun(){

  for(auto fe : GetFEs()){
    fe->GetHCC()->GetSubRegister("MASKHPR")->SetValue(1);
    fe->GetHCC()->GetSubRegister("STOPHPR")->SetValue(1);
    for(auto abc : fe->GetABCs()){
      abc->GetSubRegister("STOPHPR")->SetValue(1);
      abc->GetSubRegister("BCAL")->SetValue(0);
      abc->GetSubRegister("PR_ENABLE")->SetValue(1);
      abc->GetSubRegister("LP_ENABLE")->SetValue(1);
      abc->GetSubRegister("DETMODE")->SetValue(0);
      abc->GetSubRegister("CALPULSE_ENABLE")->SetValue(1);
      abc->GetSubRegister("CALPULSE_POLARITY")->SetValue(0);
      abc->GetSubRegister("CURRDRIV")->SetValue(4);
    }
    Send(fe);

    m_occ[fe->GetName()]=new TH2I(("occ_"+fe->GetName()).c_str(),"Occupancy map;Column;Row;Number of hits", 256,0,256,1,0,1);
  }


}

void TestScan::Run(){

  //Digital scan with latency 20
  PrepareTrigger(20,true);

  for(uint32_t trig=0;trig<m_ntrigs; trig++){

    Trigger();
    //std::cout<< trig << std::endl;

    auto start = chrono::steady_clock::now();

    while(true){
      for(auto fe : GetFEs()){
        if(!fe->HasClusters()) continue;
        Cluster* cls = fe->GetCluster();
        cout << cls->ToString() << endl;
        if(cls!=0){
          cout << cls->ToString() << endl;
          for(auto num: cls->GetStripNumbers()){
            m_occ[fe->GetName()]->Fill(num,1);
          }
        }
        fe->NextCluster();
      }
      auto end = chrono::steady_clock::now();
      uint32_t ms=chrono::duration_cast<chrono::milliseconds>(end - start).count();
      if(ms>300) break;
    }
  }
}

void TestScan::Analysis(){

  for(auto fe: GetFEs()){
    m_occ[fe->GetName()]->Print();
    m_occ[fe->GetName()]->Write();
    delete m_occ[fe->GetName()];
  }

}

