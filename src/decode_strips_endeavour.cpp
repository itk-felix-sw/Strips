#include "Strips/EndeavourEncoder.h"
 
#include <iostream>
#include <ctime>
#include <vector>
#include <iomanip>
#include <fstream>
#include <bitset>
#include <map>
#include <cmdl/cmdargs.h>
#include <algorithm>

using namespace std;
using namespace Strips;

int main(int argc, char ** argv) {

  cout << "#################################" << endl
       << "# decode_strips_commands        #" << endl
       << "#################################" << endl;

  CmdArgBool cVerbose(   'v',"verbose","turn on verbose mode");
  CmdArgBool cReverse(   'r',"reverse","read the bytes backwards");
  CmdArgStr  cFilePath(  'f',"file","path","file to parse");
  CmdArgStr  cFileFormat('F',"format","format","stream,text,binary. Default text");
  CmdArgStrList cStream( 's',"stream","stream","stream of bytes");
  CmdArgBool cEncode(    'e',"encode","encode at the end to check bytestream");

  CmdLine cmdl(*argv,&cVerbose,&cFilePath,&cFileFormat,&cReverse,&cStream,&cEncode,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  string format=(cFileFormat.flags()&CmdArg::GIVEN?cFileFormat:"text");

  cout << "Create a byte stream" << endl;
  vector<uint8_t> bytes;

    map<char,uint32_t> c2n={
        {'0', 0}, {'1', 1}, {'2', 2}, {'3', 3}, {'4', 4},
        {'5', 5}, {'6', 6}, {'7', 7}, {'8', 8}, {'9', 9},
        {'A',10}, {'B',11}, {'C',12}, {'D',13}, {'E',14}, {'F',15},
        {'a',10}, {'b',11}, {'c',12}, {'d',13}, {'e',14}, {'f',15}
    };

  cout << "Loop over file contents" << endl;

  if(format=="stream"){
    for(uint32_t i=0;i<cStream.count();i++){
      uint8_t byte = c2n[cStream[i][0]]*16+c2n[cStream[i][1]];
      if(cVerbose) cout << "byte: 0x" << hex << setw(2) << setfill('0') << (uint32_t) byte << dec << endl;
      bytes.push_back(byte);
    }
  }else if(format=="text"){
    ifstream fr(cFilePath);
    fr.seekg(0, std::ios::end);
    cout << "File size (bytes): " << fr.tellg() << endl;
    fr.seekg(0, std::ios::beg);
    char c1;
    char c2;
    while(fr){
      fr >> c1 >> c2;
      if(fr.eof())break;
      uint8_t byte = c2n[c1]*16+c2n[c2];
      if(cVerbose) cout << "byte: 0x" << hex << setw(2) << setfill('0') << (uint32_t) byte << dec << endl;
      bytes.push_back(byte);
    }
    fr.close();
  }else if(format=="binary"){
    ifstream fr(cFilePath, ios::binary);
    fr.seekg(0, std::ios::end);
    ifstream::pos_type pos = fr.tellg();
    cout << "File size (bytes): " << pos << endl;
    fr.seekg(0, std::ios::beg);
    bytes.resize(pos);
    fr.read((char*)&bytes[0],pos);
    fr.close();
  }

  if(cReverse){
    std::reverse(bytes.begin(),bytes.end());
  }

  cout << "Create new decoder" << endl;
  EndeavourEncoder *encoder=new EndeavourEncoder();

  cout << "Set bytes: size: " << bytes.size() << endl;
  encoder->SetBytes(&bytes[0],bytes.size());

  if(cVerbose) cout << "Byte stream: 0x" << encoder->GetByteString() << endl;

  cout << "Decode" << endl;
  encoder->Decode(cVerbose);

  cout << "Contents" << endl;
  for(uint32_t i=0;i<encoder->GetCommands().size();i++){
    cout <<encoder->GetCommands().at(i)->ToString() << endl;
  }

  if(cEncode){
    cout << "Encode" << endl;
    encoder->Encode();
    cout << "Byte stream: 0x" << encoder->GetByteString() << endl;
  }

  cout << "Delete the encoder" << endl;
  delete encoder;

  cout << "Have a nice day" << endl;
  return 0;
}
