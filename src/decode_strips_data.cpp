#include "Strips/Decoder.h"
 
#include <iostream>
#include <ctime>
#include <vector>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <bitset>
#include <map>
#include <cmdl/cmdargs.h>
#include <algorithm>

using namespace std;
using namespace Strips;

int main(int argc, char ** argv) {

  cout << "#################################" << endl
       << "# decode_strips_data             #" << endl
       << "#################################" << endl;

  CmdArgBool cVerbose(   'v',"verbose","turn on verbose mode");
  CmdArgBool cReverse(   'r',"reverse","read the bytes backwards");
  CmdArgStr  cFilePath(  'f',"file","path","file to parse");
  CmdArgStr  cFileFormat('F',"format","format","stream,text,binary. Default text");
  CmdArgStrList cStream( 's',"stream","stream","stream of bytes");
  CmdArgBool cEncode(    'e',"encode","encode at the end to check bytestream");
  CmdArgInt cSkip(       'k',"skip","number","skip a number of bytes of each line. Default 0");

  CmdLine cmdl(*argv,&cVerbose,&cFilePath,&cFileFormat,&cReverse,&cStream,&cEncode,&cSkip,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  string format=(cFileFormat.flags()&CmdArg::GIVEN?cFileFormat:"text");
  uint32_t skip=(cSkip.flags()&CmdArg::GIVEN?cSkip:0);

  cout << "Create a byte stream" << endl;
  vector<uint8_t> bytes;

    map<char,uint32_t> c2n={
        {'0', 0}, {'1', 1}, {'2', 2}, {'3', 3}, {'4', 4},
        {'5', 5}, {'6', 6}, {'7', 7}, {'8', 8}, {'9', 9},
        {'A',10}, {'B',11}, {'C',12}, {'D',13}, {'E',14}, {'F',15},
        {'a',10}, {'b',11}, {'c',12}, {'d',13}, {'e',14}, {'f',15}
    };

  cout << "Loop over file contents" << endl;

  if(format=="stream"){
    for(uint32_t i=0;i<cStream.count();i++){
      if(i<skip){continue;}
      uint8_t byte = c2n[cStream[i][0]]*16+c2n[cStream[i][1]];
      if(cVerbose) cout << "byte: 0x" << hex << setw(2) << setfill('0') << (uint32_t) byte << dec << endl;
      bytes.push_back(byte);
    }
  }else if(format=="text"){
    ifstream fr(cFilePath);
    fr.seekg(0, std::ios::end);
    cout << "File size (bytes): " << fr.tellg() << endl;
    fr.seekg(0, std::ios::beg);
    string line;
    while(getline(fr,line)){
      stringstream iss(line);
      uint32_t i=0;
      cout << "parse: " << line << endl;
      char c1;
      char c2;
      while(iss){
        iss >> c1 >> c2;
        i++;
        if(iss.eof())break;
        if(fr.eof())break;
        uint8_t byte = c2n[c1]*16+c2n[c2];
        if(cVerbose) cout << "byte: 0x" << hex << setw(2) << setfill('0') << (uint32_t) byte << dec << endl;
        if(i<=skip) continue;
        bytes.push_back(byte);
      }
    }
    fr.close();
  }else if(format=="binary"){
    ifstream fr(cFilePath, ios::binary);
    fr.seekg(0, std::ios::end);
    ifstream::pos_type pos = fr.tellg();
    cout << "File size (bytes): " << pos << endl;
    fr.seekg(0, std::ios::beg);
    bytes.resize(pos);
    fr.read((char*)&bytes[0],pos);
    fr.close();
  }

  if(cReverse){
    std::reverse(bytes.begin(),bytes.end());
  }

  cout << "Create new decoder" << endl;
  Decoder *decoder=new Decoder();

  cout << "Set bytes: size: " << bytes.size() << endl;
  decoder->SetBytes(&bytes[0],bytes.size());

  if(cVerbose) cout << "Byte stream: 0x" << decoder->GetByteString() << endl;

  cout << "Decode" << endl;
  decoder->Decode(cVerbose);

  cout << "Contents" << endl;
  for(uint32_t i=0;i<decoder->GetPackets().size();i++){
    cout <<decoder->GetPackets().at(i)->ToString() << endl;
  }

  if(cEncode){
    cout << "Encode" << endl;
    decoder->Encode();
    cout << "Byte stream: 0x" << decoder->GetByteString() << endl;
  }

  cout << "Delete the decoder" << endl;
  delete decoder;

  cout << "Have a nice day" << endl;
  return 0;
}
