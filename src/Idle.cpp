#include <Strips/Idle.h>
#include <Strips/SixToEight.h>
#include <sstream>

using namespace std;
using namespace Strips;

Idle::Idle(){
  m_type=Command::IDLE;
}

Idle::~Idle(){
  //nothing to do
}

string Idle::ToString(){
  ostringstream os;
  os << "Idle ";
  return os.str();
}

uint32_t Idle::Unpack (uint8_t * bytes, uint32_t maxlen){
  //we need 2 bytes
  if(maxlen<2) return 0;
  
  uint8_t b0=SixToEight::decodeLUT[bytes[0]];
  uint8_t b1=SixToEight::decodeLUT[bytes[1]];

  //Idle is just K0+K1
  if(b0!=SixToEight::K0 or b1!=SixToEight::K1){
    return 0;
  }
  
  return 2;
}
      
uint32_t Idle::Pack(uint8_t * bytes){
  //Idle is just K0+K1
  bytes[0] = SixToEight::encodeLUT[SixToEight::K0];
  bytes[1] = SixToEight::encodeLUT[SixToEight::K1];
  return 2;
}
    
