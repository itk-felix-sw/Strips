#include <Strips/SixToEight.h>

using namespace Strips;

const uint8_t SixToEight::K0;
const uint8_t SixToEight::K1;
const uint8_t SixToEight::K2;
const uint8_t SixToEight::K3;


std::map<uint8_t,uint8_t> SixToEight::lut6b8b={
{0x00,0x59},{0x01,0x71},{0x02,0x72},{0x03,0xc3},
{0x04,0x65},{0x05,0xc5},{0x06,0xc6},{0x07,0x87},
{0x08,0x69},{0x09,0xc9},{0x0a,0xca},{0x0b,0x8b},
{0x0c,0xcc},{0x0d,0x8d},{0x0e,0x8e},{0x0f,0x4b},
{0x10,0x53},{0x11,0xd1},{0x12,0xd2},{0x13,0x93},
{0x14,0xd4},{0x15,0x95},{0x16,0x96},{0x17,0x17},
{0x18,0xd8},{0x19,0x99},{0x1a,0x9a},{0x1b,0x1b},
{0x1c,0x9c},{0x1d,0x1d},{0x1e,0x1e},{0x1f,0x5c},
{0x20,0x63},{0x21,0xe1},{0x22,0xe2},{0x23,0xa3},
{0x24,0xe4},{0x25,0xa5},{0x26,0xa6},{0x27,0x27},
{0x28,0xe8},{0x29,0xa9},{0x2a,0xaa},{0x2b,0x2b},
{0x2c,0xac},{0x2d,0x2d},{0x2e,0x2e},{0x2f,0x6c},
{0x30,0x74},{0x31,0xb1},{0x32,0xb2},{0x33,0x33},
{0x34,0xb4},{0x35,0x35},{0x36,0x36},{0x37,0x56},
{0x38,0xb8},{0x39,0x39},{0x3a,0x3a},{0x3b,0x5a},
{0x3c,0x3c},{0x3d,0x4d},{0x3e,0x4e},{0x3f,0x66},
};

std::map<uint8_t,uint8_t> SixToEight::lut8b6b={
{0x59,0x00},{0x71,0x01},{0x72,0x02},{0xc3,0x03},
{0x65,0x04},{0xc5,0x05},{0xc6,0x06},{0x87,0x07},
{0x69,0x08},{0xc9,0x09},{0xca,0x0a},{0x8b,0x0b},
{0xcc,0x0c},{0x8d,0x0d},{0x8e,0x0e},{0x4b,0x0f},
{0x53,0x10},{0xd1,0x11},{0xd2,0x12},{0x93,0x13},
{0xd4,0x14},{0x95,0x15},{0x96,0x16},{0x17,0x17},
{0xd8,0x18},{0x99,0x19},{0x9a,0x1a},{0x1b,0x1b},
{0x9c,0x1c},{0x1d,0x1d},{0x1e,0x1e},{0x5c,0x1f},
{0x63,0x20},{0xe1,0x21},{0xe2,0x22},{0xa3,0x23},
{0xe4,0x24},{0xa5,0x25},{0xa6,0x26},{0x27,0x27},
{0xe8,0x28},{0xa9,0x29},{0xaa,0x2a},{0x2b,0x2b},
{0xac,0x2c},{0x2d,0x2d},{0x2e,0x2e},{0x6c,0x2f},
{0x74,0x30},{0xb1,0x31},{0xb2,0x32},{0x33,0x33},
{0xb4,0x34},{0x35,0x35},{0x36,0x36},{0x56,0x37},
{0xb8,0x38},{0x39,0x39},{0x3a,0x3a},{0x5a,0x3b},
{0x3c,0x3c},{0x4d,0x3d},{0x4e,0x3e},{0x66,0x3f},
};

std::map<uint8_t,uint8_t> SixToEight::k2word={
{0x00,0x78},{0x01,0x55},{0x02,0x47},{0x03,0x6a},};

std::map<uint8_t,uint8_t> SixToEight::word2k={
{0x78,0x00},{0x55,0x01},{0x47,0x02},{0x6a,0x03},};

std::map<uint8_t,uint8_t> SixToEight::encodeLUT={
{0x00,0x59},{0x01,0x71},{0x02,0x72},{0x03,0xc3},
{0x04,0x65},{0x05,0xc5},{0x06,0xc6},{0x07,0x87},
{0x08,0x69},{0x09,0xc9},{0x0a,0xca},{0x0b,0x8b},
{0x0c,0xcc},{0x0d,0x8d},{0x0e,0x8e},{0x0f,0x4b},
{0x10,0x53},{0x11,0xd1},{0x12,0xd2},{0x13,0x93},
{0x14,0xd4},{0x15,0x95},{0x16,0x96},{0x17,0x17},
{0x18,0xd8},{0x19,0x99},{0x1a,0x9a},{0x1b,0x1b},
{0x1c,0x9c},{0x1d,0x1d},{0x1e,0x1e},{0x1f,0x5c},
{0x20,0x63},{0x21,0xe1},{0x22,0xe2},{0x23,0xa3},
{0x24,0xe4},{0x25,0xa5},{0x26,0xa6},{0x27,0x27},
{0x28,0xe8},{0x29,0xa9},{0x2a,0xaa},{0x2b,0x2b},
{0x2c,0xac},{0x2d,0x2d},{0x2e,0x2e},{0x2f,0x6c},
{0x30,0x74},{0x31,0xb1},{0x32,0xb2},{0x33,0x33},
{0x34,0xb4},{0x35,0x35},{0x36,0x36},{0x37,0x56},
{0x38,0xb8},{0x39,0x39},{0x3a,0x3a},{0x3b,0x5a},
{0x3c,0x3c},{0x3d,0x4d},{0x3e,0x4e},{0x3f,0x66},
{SixToEight::K0,0x78},{SixToEight::K1,0x55},{SixToEight::K2,0x47},{SixToEight::K3,0x6a},
};

std::map<uint8_t,uint8_t> SixToEight::decodeLUT={
{0x59,0x00},{0x71,0x01},{0x72,0x02},{0xc3,0x03},
{0x65,0x04},{0xc5,0x05},{0xc6,0x06},{0x87,0x07},
{0x69,0x08},{0xc9,0x09},{0xca,0x0a},{0x8b,0x0b},
{0xcc,0x0c},{0x8d,0x0d},{0x8e,0x0e},{0x4b,0x0f},
{0x53,0x10},{0xd1,0x11},{0xd2,0x12},{0x93,0x13},
{0xd4,0x14},{0x95,0x15},{0x96,0x16},{0x17,0x17},
{0xd8,0x18},{0x99,0x19},{0x9a,0x1a},{0x1b,0x1b},
{0x9c,0x1c},{0x1d,0x1d},{0x1e,0x1e},{0x5c,0x1f},
{0x63,0x20},{0xe1,0x21},{0xe2,0x22},{0xa3,0x23},
{0xe4,0x24},{0xa5,0x25},{0xa6,0x26},{0x27,0x27},
{0xe8,0x28},{0xa9,0x29},{0xaa,0x2a},{0x2b,0x2b},
{0xac,0x2c},{0x2d,0x2d},{0x2e,0x2e},{0x6c,0x2f},
{0x74,0x30},{0xb1,0x31},{0xb2,0x32},{0x33,0x33},
{0xb4,0x34},{0x35,0x35},{0x36,0x36},{0x56,0x37},
{0xb8,0x38},{0x39,0x39},{0x3a,0x3a},{0x5a,0x3b},
{0x3c,0x3c},{0x4d,0x3d},{0x4e,0x3e},{0x66,0x3f},
{0x78,SixToEight::K0},{0x55,SixToEight::K1},{0x47,SixToEight::K2},{0x6a,SixToEight::K3},
};

