#include "Strips/Decoder.h"
#include "netio/netio.hpp"
#include <cmdl/cmdargs.h>
#include <iostream>
#include <mutex>
#include <thread>
#include <signal.h>

using namespace std;
using namespace Strips;

struct FelixDataHeader{
  uint16_t length;
  uint16_t status;
  uint32_t elink;
};

bool stopScan = false;

void Stop(int){
  cout << endl << "You pressed ctrl+c -> quitting" << endl;
  stopScan = true;
}

int main(int argc, char ** argv) {

  cout << "#################################" << endl
       << "# decode_strips_elink           #" << endl
       << "#################################" << endl;

  CmdArgBool cVerbose('v',"verbose","turn on verbose mode");
  CmdArgInt cElink('e',"elink","elink", "elink rx", CmdArg::isREQ);
  CmdArgStr cHost('H',"host","host", "felix host");
  CmdArgInt cPort('p',"port","port", "felix data port");
  CmdArgBool cReverse('r',"reverse","reverse the byte order");

  CmdLine cmdl(*argv,&cVerbose,&cElink,&cHost,&cPort,&cReverse,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);
  
  string host=(cHost.flags()&CmdArg::GIVEN?cHost:"127.0.0.1");
  uint32_t port=(cPort.flags()&CmdArg::GIVEN?cPort:12350);
  
  cout << "Create the context" << endl;
  netio::context * context = new netio::context("posix");
  thread context_thread([&](){context->event_loop()->run_forever();});
  mutex mtx;
  
  cout << "Create new decoder" << endl;
  Decoder *decoder=new Decoder();
  
  cout << "Create the felix header" << endl;
  FelixDataHeader hdr;
      
  cout << "Create the data stream" << endl;
  vector<uint8_t> data;
  
  netio::low_latency_subscribe_socket * rx;
  
  rx = new netio::low_latency_subscribe_socket(context, [&](netio::endpoint& ep, netio::message& msg){
      mtx.lock();
      if(cVerbose) cout << "Handler::Connect Received data from " << ep.address() << ":" << ep.port() << " size:" << msg.size() << endl;
      data = msg.data_copy();
      memcpy(&hdr,(const void*)&data[0], sizeof (hdr));
      decoder->SetBytes(&data[sizeof(hdr)],data.size()-sizeof(hdr),cReverse);
      if(cVerbose) cout << "Byte stream: 0x" << decoder->GetByteString() << endl;
      cout << "Decode" << endl;
      decoder->Decode(cVerbose);
      cout << "Contents" << endl;
      for(uint32_t i=0;i<decoder->GetPackets().size();i++){
        cout <<decoder->GetPackets().at(i)->ToString() << endl;
      }
      mtx.unlock();
    });
  
  cout << "Subscribe to data elink: " << cElink << " at " << host << ":" << port << endl;
  rx->subscribe(cElink, netio::endpoint(host, port));

  cout << "Loop" << endl;
  signal(SIGINT, Stop);
  while(!stopScan){
    sleep(1);
  }

  cout << "Stop event loop" << endl;
  context->event_loop()->stop();
  
  cout << "Disconnect from data elink: " << cElink << " at " << host << ":" << port << endl;
  rx->unsubscribe(cElink, netio::endpoint(host, port));
  delete rx;
  
  cout << "Join context thread" << endl;
  context_thread.join();
  
  cout << "Delete the context" << endl;
  delete context;
  
  cout << "Delete the sensor" << endl;
  delete decoder;
  
  cout << "Have a nice day" << endl;
  return 0;
}
