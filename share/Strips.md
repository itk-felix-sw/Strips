# Strips software for ITK-FELIX {#PackageStrips}

This package contains all the software necesary to control and read-out a Strips hybrid.

## Strips hybrid

The Strips hybrid is part of a Strips module that contains a HCC Star chip, and up to 10 ABC star chips.
Reading out up to 2560 strips, 256 each ABC star, through a single e-link.

The configuration of the Strips::HCC and each of the Strips::ABC chips is stored in 32-bit Strips::Register memories that are accessed by address with one or many Strips::SubRegister bit-fields associated to them. The Strips::HCC has 49 Strips::HCCRegister addresses, while the Strips::ABC has 192 Strips::ABCRegister addresses. The configuration of each register is sent to the hybrid through a Strips::WriteRegister Strips::Command, and retrieved in the form of a Strips::HCCRegisterPacket or Strips::ABCRegisterPacket Strips::Packet after the sending of a Stripst::ReadRegister Strips::Command.

The data path of the hybrid is made out of Strips::Packet packets. Physics data is contained in an Strips::ABCLPPRPacket Strips::Packet. Another type of packet exists called the Strips::TransparentPacket for debugging purposes.

## Calibration procedure

To be completed.

## Implementation paradigm

The Strips::FrontEnd is implemented with a set of classes (Strips::Encoder, Strips::Decoder, Strips::HCC, Strips::ABC, Strips::Register, Strips::SubRegister) that represent the functionalities of the Hybrid. For example, the Strips::Encoder encodes a byte array from a Strips::Command array, and it can also decode a byte array into a Strips::Command array. This allows closure tests, and the possibility of emulating the front-end behaviour in the Strips::Emulator class.

The communication layer is meant to be external to the Strips::FrontEnd which only deals with byte arrays to send commands and to receive data from the communication layer. Same is true for the Strips::Emulator which receives commands as byte arrays and sends data as byte arrays. 

The Strips::Handler implements the communication layer with FELIX through NETIO for any number of Strips::FrontEnd objects. The execution of the methods of the handler Strips::Handler (Strips::Handler::Connect, Strips::Handler::Config, Strips::Handler::Run) are in series and single threaded, thus smart grouping of Strips::FrontEnd objects into multiple Strips::Handler instances, will allow concurrent processing. 

The steps of a scan are Strips::Handler::InitRun, Strips::Handler::Connect, Strips::Handler::Config, Strips::Handler::PreRun, Strips::Handler::SaveConfig, Strips::Handler::Run, Strips::Handler::Analysis, Strips::Handler::Disconnect, and Strips::Handler::Save. The actual calibration procedure takes place inside the Strips::Handler::Run method.

## How to run a scan

1. Prepare the configuration file. For example: default_star.json

```
{
  "connectivity" : [
    {"name": "default_star", "config" : "default_star.json", "rx" :  0, "tx" :  0, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 0, "locked" : 0}
    ]
}
```

2. Prepare the connectivity file. For example: connectivity_star.json
3. Run the scan manager
   
   ```
   scan_manager_strips -n connectivity_star.json -s scan-name
   ```
   
### Available calibration scans

- Strips::TestScan
