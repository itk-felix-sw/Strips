#!/usr/bin/env python
##################################################
# Create Look Up Tables for 6B/8B encoding/decoding
#
# The maximum number of 6-bit patterns is 64.
# They can be classified according to their disparity, 
# the number of 1 bits minus the number of 0 bits: 
# 
# | Ones | Zeros | Disparity | Number of patterns |  
# | ---- | ----- | --------- | ------------------ | 
# | 0    |  6    | -6        |  1                 | 
# | 1    |  5    | -4        |  6                 | 
# | 2    |  4    | -2        | 15                 | 
# | 3    |  3    |  0        | 20                 | 
# | 4    |  2    | +2        | 15                 | 
# | 5    |  1    | +4        |  6                 | 
# | 6    |  0    | +6        |  1                 | 
# 
# Each of the 6-bit codes has a corresponding 8-bit symbol,
# that does not contain more than four consecutive matching bits.
# Following a look-up table listed below.
# Four additional 8-bit symbols are used in the protocol 
# that do not have a 6-bit representation, called k-words.
#
# | k-word | 8-bit    |
# | ------ | -------- |
# | K0     | 01111000 |
# | K1     | 01010101 |
# | K2     | 01000111 |
# | K3     | 01101010 |
#
# - disparity -6 is assigned 01011001 
# - disparity -4 are complemented using a table
# - disparity -2 are prefixed with 0b11, except 0b110000
# - disparity  0 are prefixed with 0b10
# - disparity +2 are prefixed with 0b00, except 0b001111
# - disparity +4 are complemented using a table 
# - disparity +6 is assigned 01100110
#  
# | Disparity | 6-bit  | 8-bit    | 
# | --------- | ------ | -------- |
# | -6        | 000000 | 01011001 |
# | -4        | 000001 | 01110001 |
# | -4        | 000010 | 01110010 |
# | -4        | 000100 | 01100101 |
# | -4        | 001000 | 01101001 |
# | -4        | 010000 | 01010011 |
# | -4        | 100000 | 01100011 |
# | -2        | 110000 | 01110100 |
# | +2        | 001111 | 01001011 |
# | +4        | 111110 | 01001110 |
# | +4        | 111101 | 01001101 |
# | +4        | 111011 | 01011010 |
# | +4        | 110111 | 01010110 |
# | +4        | 101111 | 01101100 |
# | +4        | 011111 | 01011100 |
# | +6        | 111111 | 01100110 |
#
#
#
# Carlos.Solans@cern.ch
# August 2022
##################################################


import os
import sys
import datetime
import argparse

parser=argparse.ArgumentParser("Create LUTs for 6b/8b encoding/decoding")
parser.add_argument("-v","--verbose",help="enable verbose mode", action="store_true")
parser.add_argument("-c","--classname",help="Class name (default SixToEight)", default="SixToEight")
parser.add_argument("-s","--src",help="source dir (default src)", default="src")
parser.add_argument("-i","--include",help="include dir (default Strips)", default="Strips")
parser.add_argument("-n","--namespace",help="namespace (default same as include dir)", default=None)
parser.add_argument("-o","--outdir",help="Output directory (default=.)", default=".")
args=parser.parse_args()

if not args.namespace: args.namespace = args.include

#LUT with exceptions listed in the comments
lut6to8={
    0b000000: 0b01011001,
    0b000001: 0b01110001,
    0b000010: 0b01110010,
    0b000100: 0b01100101,
    0b001000: 0b01101001,
    0b010000: 0b01010011,
    0b100000: 0b01100011,
    0b110000: 0b01110100,
    0b001111: 0b01001011,
    0b111110: 0b01001110,
    0b111101: 0b01001101,
    0b111011: 0b01011010,
    0b110111: 0b01010110,
    0b101111: 0b01101100,
    0b011111: 0b01011100,
    0b111111: 0b01100110
}

word2k={
    0: 0b01111000,
    1: 0b01010101,
    2: 0b01000111,
    3: 0b01101010,
}

#build the rest of the 6b8b LUT
for n in range(2**6):
    if n in lut6to8: continue
    bs="{0:06b}".format(n)
    n1=bs.count('1')
    n0=bs.count('0')
    dp=n1-n0
    if   dp== 0: lut6to8[n]=int('10%s'%bs,2)
    elif dp==+2: lut6to8[n]=int('00%s'%bs,2)
    elif dp==-2: lut6to8[n]=int('11%s'%bs,2)
    else: print("WFT: n=%i, dp=%i"%(n,dp))
    pass

print("6b8b LUT")
for k in sorted(lut6to8):
    print(" 0x%02x => 0x%02x"%(k,lut6to8[k])) 
    pass
  
print("Write out files")
if not os.path.exists("%s/%s"%(args.outdir,args.include)): os.system("mkdir %s/%s"%(args.outdir,args.include))
if not os.path.exists("%s/%s"%(args.outdir,args.src)):     os.system("mkdir %s/%s"%(args.outdir,args.src))

print("File: %s/%s/%s.h"%(args.outdir,args.include,args.classname))
fw=open("%s/%s/%s.h"%(args.outdir,args.include,args.classname),"w+")
fw.write("#ifndef STRIPS_SIXTOEIGHT_H\n")
fw.write("#define STRIPS_SIXTOEIGHT_H\n")
fw.write("\n")
fw.write("#include <map>\n")
fw.write("#include <vector>\n")
fw.write("#include <cstdint>\n")
fw.write("\n")
fw.write("namespace %s{\n" % args.namespace)
fw.write("\n")
fw.write("/**\n")
fw.write(" *  6b/8b encoding encodes 6-bit patterns in 8-bits.\n")
fw.write(" *  The maximum number of 6-bit patterns is 64.\n")
fw.write(" *  They can be classified according to their disparity, \n")
fw.write(" *  the number of 1 bits minus the number of 0 bits: \n")
fw.write(" *  \n")
fw.write(" *  | Ones | Zeros | Disparity | Number of patterns | \n")
fw.write(" *  | ---- | ----- | --------- | ------------------ | \n")
fw.write(" *  | 0    |  6    | -6        |  1                 | \n")
fw.write(" *  | 1    |  5    | -4        |  6                 | \n")
fw.write(" *  | 2    |  4    | -2        | 15                 | \n")
fw.write(" *  | 3    |  3    |  0        | 20                 | \n")
fw.write(" *  | 4    |  2    | +2        | 15                 | \n")
fw.write(" *  | 5    |  1    | +4        |  6                 | \n")
fw.write(" *  | 6    |  0    | +6        |  1                 | \n")
fw.write(" *  \n")
fw.write(" *  Each of the 6-bit codes has a corresponding 8-bit symbol, \n")
fw.write(" *  that does not contain more than four consecutive matching bits. \n")
fw.write(" *  Following a look-up table listed below. \n")
fw.write(" *  Four additional 8-bit symbols are used in the protocol \n")
fw.write(" *  that do not have a 6-bit representation, called k-words. \n")
fw.write(" *  \n")
fw.write(" *  | k-word | 8-bit    | \n")
fw.write(" *  | ------ | -------- | \n")
fw.write(" *  | K0     | 01111000 | \n")
fw.write(" *  | K1     | 01010101 | \n")
fw.write(" *  | K2     | 01000111 | \n")
fw.write(" *  | K3     | 01101010 | \n")
fw.write(" *  \n")
fw.write(" *  - disparity -6 is assigned 01011001  \n")
fw.write(" *  - disparity -4 are complemented using a table \n")
fw.write(" *  - disparity -2 are prefixed with 0b11, except 0b110000 \n")
fw.write(" *  - disparity  0 are prefixed with 0b10 \n")
fw.write(" *  - disparity +2 are prefixed with 0b00, except 0b001111 \n")
fw.write(" *  - disparity +4 are complemented using a table  \n")
fw.write(" *  - disparity +6 is assigned 01100110 \n")
fw.write(" *    \n")
fw.write(" *  | Disparity | 6-bit  | 8-bit    | \n")
fw.write(" *  | --------- | ------ | -------- | \n")
fw.write(" *  | -6        | 000000 | 01011001 | \n")
fw.write(" *  | -4        | 000001 | 01110001 | \n")
fw.write(" *  | -4        | 000010 | 01110010 | \n")
fw.write(" *  | -4        | 000100 | 01100101 | \n")
fw.write(" *  | -4        | 001000 | 01101001 | \n")
fw.write(" *  | -4        | 010000 | 01010011 | \n")
fw.write(" *  | -4        | 100000 | 01100011 | \n")
fw.write(" *  | -2        | 110000 | 01110100 | \n")
fw.write(" *  | +2        | 001111 | 01001011 | \n")
fw.write(" *  | +4        | 111110 | 01001110 | \n")
fw.write(" *  | +4        | 111101 | 01001101 | \n")
fw.write(" *  | +4        | 111011 | 01011010 | \n")
fw.write(" *  | +4        | 110111 | 01010110 | \n")
fw.write(" *  | +4        | 101111 | 01101100 | \n")
fw.write(" *  | +4        | 011111 | 01011100 | \n")
fw.write(" *  | +6        | 111111 | 01100110 | \n")
fw.write(" *\n")
fw.write(" *\n")
fw.write(" * @brief Look-up-tables for 6b/8b encoding and decoding\n")
fw.write(" * @author Carlos.Solans@cern.ch\n")
fw.write(" * @date Autogenerated on %s\n" % datetime.datetime.now().strftime('%b %Y'))
fw.write(" **/\n")
fw.write("\n")
fw.write("class %s{\n" % args.classname)
fw.write("  public:\n");
fw.write("  /** @brief 6b8b encoding table **/\n")
fw.write("  static std::map<uint8_t,uint8_t> lut6b8b;\n")
fw.write("  /** @brief 6b8b decoding table **/\n")
fw.write("  static std::map<uint8_t,uint8_t> lut8b6b;\n")
fw.write("  /** @brief k-word encoding **/\n")
fw.write("  static std::map<uint8_t,uint8_t> k2word;\n")
fw.write("  /** @brief k-word decoding **/\n")
fw.write("  static std::map<uint8_t,uint8_t> word2k;\n")
fw.write("  \n")
fw.write("  /** @brief full protocol encoding table **/\n")
fw.write("  static std::map<uint8_t,uint8_t> encodeLUT;\n")
fw.write("  /** @brief full protocol decoding table **/\n")
fw.write("  static std::map<uint8_t,uint8_t> decodeLUT;\n")
fw.write("  /**\n")
fw.write("   * Decoded 8-bit out-of-band K0 symbol. Encoded value is 0b01111000.\n")
fw.write("   * @brief K0 symbol \n")
fw.write("   **/\n")
fw.write("  static const uint8_t K0=0x80;\n")
fw.write("  /**\n")
fw.write("   * Decoded 8-bit out-of-band K1 symbol. Encoded value is 0b01010101.\n")
fw.write("   * @brief K0 symbol \n")
fw.write("   **/\n")
fw.write("  static const uint8_t K1=0x81;\n")
fw.write("  /**\n")
fw.write("   * Decoded 8-bit out-of-band K2 symbol. Encoded value is 0b01000111.\n")
fw.write("   * @brief K0 symbol \n")
fw.write("   **/\n")
fw.write("  static const uint8_t K2=0x82;\n")
fw.write("  /**\n")
fw.write("   * Decoded 8-bit out-of-band K3 symbol. Encoded value is 0b01101010.\n")
fw.write("   * @brief K0 symbol \n")
fw.write("   **/\n")
fw.write("  static const uint8_t K3=0x83;\n")
fw.write("  \n")
fw.write("};\n")
fw.write("\n")
fw.write("}\n")
fw.write("#endif\n")
fw.close()

print("File: %s/%s/%s.cpp"%(args.outdir,args.src,args.classname))
fw=open("%s/%s/%s.cpp"%(args.outdir,args.src,args.classname),"w+")

fw.write("#include <%s/%s.h>\n"%(args.include,args.classname))
fw.write("\n")
fw.write("using namespace %s;\n"%args.namespace)
fw.write("\n")
fw.write("const uint8_t %s::K0;\n"%args.classname)
fw.write("const uint8_t %s::K1;\n"%args.classname)
fw.write("const uint8_t %s::K2;\n"%args.classname)
fw.write("const uint8_t %s::K3;\n"%args.classname)
fw.write("\n")
fw.write("\n")
fw.write("std::map<uint8_t,uint8_t> %s::lut6b8b={\n"%args.classname)
i=1
for k in sorted(lut6to8):
    fw.write("{0x%02x,0x%02x},"%(k,lut6to8[k])) 
    if i%4==0: fw.write("\n");
    i+=1
    pass
fw.write("};\n\n")

fw.write("std::map<uint8_t,uint8_t> %s::lut8b6b={\n"%args.classname)
i=1
for k in sorted(lut6to8):
    fw.write("{0x%02x,0x%02x},"%(lut6to8[k],k)) 
    if i%4==0: fw.write("\n");
    i+=1
    pass
fw.write("};\n\n")

fw.write("std::map<uint8_t,uint8_t> %s::k2word={\n"%args.classname)
i=1
for k in word2k:
    fw.write("{0x%02x,0x%02x},"%(k,word2k[k])) 
    if i%5==0: fw.write("\n");
    i+=1
    pass
fw.write("};\n\n")

fw.write("std::map<uint8_t,uint8_t> %s::word2k={\n"%args.classname)
i=1
for k in word2k:
    fw.write("{0x%02x,0x%02x},"%(word2k[k],k)) 
    if i%8==0: fw.write("\n");
    i+=1
    pass
fw.write("};\n\n")

fw.write("std::map<uint8_t,uint8_t> %s::encodeLUT={\n"%args.classname)
i=1
for k in lut6to8:
    fw.write("{0x%02x,0x%02x},"%(k,lut6to8[k])) 
    if i%4==0: fw.write("\n");
    i+=1
    pass
fw.write("{SixToEight::K0,0x%02x},"%(word2k[0])) 
fw.write("{SixToEight::K1,0x%02x},"%(word2k[1])) 
fw.write("{SixToEight::K2,0x%02x},"%(word2k[2])) 
fw.write("{SixToEight::K3,0x%02x},"%(word2k[3])) 
fw.write("\n");
fw.write("};\n\n")

fw.write("std::map<uint8_t,uint8_t> %s::decodeLUT={\n"%args.classname)
i=1
for k in lut6to8:
    fw.write("{0x%02x,0x%02x},"%(lut6to8[k],k)) 
    if i%4==0: fw.write("\n");
    i+=1
    pass
fw.write("{0x%02x,SixToEight::K0},"%(word2k[0])) 
fw.write("{0x%02x,SixToEight::K1},"%(word2k[1])) 
fw.write("{0x%02x,SixToEight::K2},"%(word2k[2])) 
fw.write("{0x%02x,SixToEight::K3},"%(word2k[3])) 
fw.write("\n");
fw.write("};\n\n")


fw.close()
print("Have a nice day")

