#ifndef STRIPS_CONFIGURATION_H
#define STRIPS_CONFIGURATION_H

#include <Strips/Register.h>
#include <Strips/SubRegister.h>
#include <vector>
#include <map>
#include <string>

namespace Strips{

  /**
   * This class contains the basic functionality that is required by the Strips::Handler.
   * And provides the public methods to access Register and SubRegister objects stored in it.
   * The declaration is meant to be done by defining the m_registers and m_subregisters objects
   * in a child class.
   *
   * @brief Base Configuration class
   * @author Carlos.Solans@cern.ch
   * @date August 2022
   **/
  class Configuration{
  public:
    
    /**
     * Create a vector of Register objects and a map of SubRegister pointers.
     * Initialize the ID to 0
     */
    Configuration();

    /**
     * Delete the pointers in the SubRegister map
     */
    ~Configuration();

    /**
     * Set the ID 
     * @param id the ID 
     **/
    void SetID(uint16_t id);
    
    /**
     * Get the ID 
     * @return the ID
     **/
    uint16_t GetID();

    /**
     * Access a register given its address
     * @param addr the Register address
     * @return a pointer to the Register
     **/
    Register * GetRegister(uint32_t addr);

    /**
     * Access the Register given its name
     * @param name the Register name
     * @return a pointer to the Register
     */
    Register * GetRegister(std::string name);

    /**
     * Access a subregister given its address
     * @param addr the SubRegister address
     * @return a pointer to the SubRegister
     **/
    SubRegister * GetSubRegister(uint32_t addr);
    
    /**
     * Access a subregister given its name
     * @param name the SubRegister name
     * @return a pointer to the SubRegister
     **/
    SubRegister * GetSubRegister(std::string name);

    /**
     * Set the value of a range of registers given by a map of strings to integers
     * @param values map of register names to integer values
     **/
    void SetRegisterValues(std::map<std::string,uint32_t> values);

    /**
     * Set the value of a range of registers given by a map of strings to hex strings.
     * The value should not contain the leading "0x" and is parsed into an integer.
     * @param values map of register names to hex string values
     **/
    void SetRegisterStrings(std::map<std::string,std::string> values);

    /**
     * Get the registers that have a name as a map of strings to integers
     * @return map of register names to integer values
     **/
    std::map<std::string,uint32_t> GetRegisterValues();

    /**
     * Get the registers that have a name as a map of strings to hex strings.
     * The value will not contain the leading "0x" and is calculated from the integer value.
     * @return map of register names to hex string values
     **/
    std::map<std::string,std::string> GetRegisterStrings();
    
    /**
     * Get the registers that have been updated (Register::IsUpdated()).
     * This method updates the updated flag of the register.
     * Calling this method for the second time will result in an empty map.
     * @return map of register addresses to register values
     **/
    std::map<uint32_t,uint32_t> GetUpdatedRegisterValues();

    /**
     * Get the registers that have been updated (Register::IsUpdated())
     * @return map of register addresses to register pointers
     **/
    std::map<uint32_t,Register*> GetUpdatedRegisters();

    /**
     * Get the subregisters that have been given (SubRegister::IsGiven())
     * @return map of subregister names to subregister pointers
     **/
    std::map<std::string,SubRegister*> GetGivenSubRegisters();

    /**
     * Make a back-up copy of the registers.
     * This is called by the constructor and contains the default values
     * The back-up is restored by calling Configuration::Restore().
     **/
    void Backup();

    /**
     * Restore the back-up copy of the registers.
     * This updates the update flag of the registers.
     * Then the registers that have been updated can be retrieved with Configuration::GetUpdatedRegisters().
     * The back-up can be made anytime by calling Configuration::Backup().
     **/
    void Restore();
    
    /**
     * Initialize the defaults.
     * Not implemented here.
     **/
    virtual void SetDefaults();

  protected:

    /**
     * The vector of registers that contains the configuration
     **/
    std::vector<Register> m_registers;

    /**
     * The back-up of the registers. 
     * Can be stored calling Configuration::Backup(), 
     * and restored with Configuration::Restore()
     **/
    std::vector<uint32_t> m_backup;

    /**
     * The back-up of the registers. 
     * Can be stored calling Configuration::Backup(), 
     * and restored with Configuration::Backup()
     **/
    std::map<uint32_t,SubRegister*> m_subregisters;

    /**
     * Map of Register names to Register addresses
     **/
    std::map<std::string,uint32_t> m_registerNames;
    
    /**
     * Map of SubRegister names to SubRegister addresses
     **/
    std::map<std::string,uint32_t> m_subRegisterNames;

    /**
     * The ID
     **/
    uint16_t m_id;
  };

}

#endif 
