#ifndef STRIPS_HCCREGISTERPACKET_H
#define STRIPS_HCCREGISTERPACKET_H

#include "Strips/Packet.h"

#include <cstdint>
#include <string>

namespace Strips{

  /**
   * This class represents an HCC register Packet. It can be of type ReadRegister or High Priority Read Register.
   * The format is the following:
   *
   * | Byte    |  0             ||||  1             ||||  2     ||  3     ||  4     ||  5             ||||
   * | Bit     |  0 |  3 |  4 |  7 |  8 | 11 | 12 | 15 | 20 | 23 | 24 | 31 | 32 | 39 | 40 | 43 | 44 | 47 |
   * | ------  | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
   * | Size    |  4     || 8              |||| 32                                     |||||||||| 4      ||
   * | Desc    |  Type  || Address        |||| Data                                   |||||||||| Empty  ||
   *
   **/

  class HCCRegisterPacket: public Packet{
 
  public:

    /**
     * Create and empty ABCRegisterPacket of type Packet::TYP_HCC_RR
     **/
    HCCRegisterPacket();

    /**
     * Create an HCCRegisterPacket with the given parameters
     * @param typ the Type of the Packet (Packet::TYP_HCC_RR or Packet::TYP_HCC_HPR)
     * @param address the HCC register address
     * @param data the HCC register data
     */
    HCCRegisterPacket(uint32_t typ, uint8_t address, uint32_t data);

    /**
     * Copy the HCCRegisterPacket
     * @param copy The pointer to the object to copy
     **/
    HCCRegisterPacket(HCCRegisterPacket *copy);

    /**
     * Delete the HCCRegisterPacket
     **/
    ~HCCRegisterPacket();

    /**
     * Return a human readable representation of the data
     **/
    std::string ToString();
      
    /**
     * Set the register address
     * @param address the HCC register address
     **/
    void SetAddress(uint32_t address);
    
    /**
     * Get the register address
     * @return the HCC register address
     **/
    uint32_t GetAddress();

    /**
     * Set the register data
     * @param data the HCC register data
     **/
    void SetData(uint32_t data);

    /**
     * Get the register data
     * @return the HCC register data
     **/
    uint32_t GetData();
    
    /**
     * extract command from the bytes
     * @param bytes the byte array
     * @param maxlen the maximum number of bytes than can be read
     * @return the number of bytes processed
     **/
    uint32_t Unpack (uint8_t * bytes, uint32_t maxlen);

    /**
     * set command to bytes
     * @param bytes the byte array
     * @return the number of bytes processed
     **/
     uint32_t Pack(uint8_t * bytes);

  private:

    uint8_t m_address = 0;
    uint32_t m_data = 0;

  };

}

#endif
