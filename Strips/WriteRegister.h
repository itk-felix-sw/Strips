#ifndef STRIPS_WRITEREGISTER_H
#define STRIPS_WRITEREGISTER_H

#include <Strips/Command.h>

namespace Strips{

  /**
   * A WriteRegister command is a sequence of 16-bit frames.
   * These frames do not need to be consecutive to be interpreted but must be in order.
   * Frames interleaved may be of any type. However, this implementation does not support it.
   * A WriteRegister will be encoded into a consecutive array of bytes, and decoded from a consecutive array of bytes.
   * The command starts with a header 16-bit frame and ends with a similar 16-bit trailer frame
   * followed by two 16-bit frames containing the ABC ID and the register address.
   *
   * The format is the following:
   *
   * | Bit     | 0| 7|  8 | 15 |16|23|24|31|32|39|40|47|48|55|56|63| 32 | 71 | 72 | 79 | 80 | 87 | 88 | 95 | 96|103|104|111|112|119|128|127|128|135| 136|143|
   * | ------  |--|--| -- | -- |--|--|--|--|--|--|--|--|--|--|--|--| -- | -- | -- | -- | -- | -- | -- | -- | --|---|---|---|---|---|---|---|---|---|----|---|
   * | Byte    | 0  ||  1     || 2  ||  3 || 4  || 5  || 6  || 7  ||  8     ||  9     || 10     || 11     || 12   || 13   || 14   || 15   || 16   ||  17   ||
   * | Size    | 8  ||  8     || 16     |||| 16     ||||  16    |||| 16             |||| 16             |||| 16         |||| 16         |||| 8    || 8     ||
   * | Desc    | K2 || Header || Frame 1|||| Frame 2|||| Frame 3|||| Frame 4        |||| Frame 5        |||| Frame 6    |||| Frame 7    |||| K2   ||Trailer||
   *
   * The header and trailer are 6b/8b (SixToEight) encoded.
   * The payload contains an ABC/HCC select bit (ABC=1, HCC=0), a Start/End bit (Start=1, End=0),
   * and the 4-bit HCC ID (Broadcast=0b1111, Ignore=0b1110).
   * The format is the following:
   *
   * | Bit     |  0      |  1        |  2 |  5 |
   * | ------  | ------- | --------- | -- | -- |
   * | Payload |  1 and 17                  ||||
   * | Size    |  1      |  1        |  4     ||
   * | Desc    | ABC/HCC | Start/End | HCC ID ||
   *
   *
   * The following 2 frames are 6b/8b (SixToEight) encoded too.
   * The first one contains a 5-bit Marker (0b00000), a Read/Write bit (Read=1, Write=0),
   * the 4-bit ABC ID, and the 2-bit MSBs of the Address.
   * The payload is the following:
   *
   * | Bit     |  0 |  4 |  5 |  6 |  9 | 10 | 11      |
   * | ------  | -- | -- | -- | -- | -- | -- | ------- |
   * | Payload |  2         |||  3                  ||||
   * | Size    |  5     ||  1 | 4      || 2           ||
   * | Desc    | Marker || RW | ABC ID || Address MSB ||
   *
   * The second one contains the 5-bit Marker (0b00000), a Read/Write bit (Read=1, Write=0),
   * the 4-bit ABC ID, and the 2-bit MSBs of the Address.
   * The payload is the following:
   *
   * | Bit     |  0 |  4 |  5 |  6 | 10 | 11    |
   * | ------  | -- | -- | -- | -- | -- | ----- |
   * | Payload |  4         |||  5            |||
   * | Size    |  5     || 6          ||| 1     |
   * | Desc    | Marker || Address LSB||| Spare |
   *
   *
   * The next five frames contain the 5-bit Marker (0b00000) followed by a subset of bits of the data.
   * The payload is the following:
   *
   *
   * | Bit     |  0 |  4 |  5 |  6 |  7 |  8    |  11   |
   * | ------  | -- | -- | -- | -- | -- | ----- | ----- |
   * | Payload |  6         |||  7                   ||||
   * | Size    |  5     || 3          |||              ||
   * | Desc    | Marker || Spare      ||| Data [31:28] ||
   *
   * | Bit     |  0 |  4 |  5 |  6 | 11 |
   * | ------  | -- | -- | -- | -- | -- |
   * | Payload |  8         |||  9     ||
   * | Size    |  5     || 6          |||
   * | Desc    | Marker ||Data [27:21]|||
   *
   * | Bit     |  0 |  4 |  5 |  6 | 11 |
   * | ------  | -- | -- | -- | -- | -- |
   * | Payload |  10        ||| 11     ||
   * | Size    |  5     || 6          |||
   * | Desc    | Marker ||Data [20:14]|||
   *
   * | Bit     |  0 |  4 |  5 |  6 | 11 |
   * | ------  | -- | -- | -- | -- | -- |
   * | Payload |  12        ||| 13     ||
   * | Size    |  5     || 6          |||
   * | Desc    | Marker || Data [13:7]|||
   *
   * | Bit     |  0 |  4 |  5 |  6 | 11 |
   * | ------  | -- | -- | -- | -- | -- |
   * | Payload |  14        ||| 15     ||
   * | Size    |  5     || 6          |||
   * | Desc    | Marker || Data [6:0] |||
   *
   *
   * @brief Write Register LCB sequence of Frames for Strips
   * @author Carlos.Solans@cern.ch
   * @date August 2022
   **/

  class WriteRegister: public Command{

  public:

    static const uint32_t HCC = 0;
    static const uint32_t ABC = 1;

    /**
     * Create an empty WriteRegister with the Write bit at one
     * otherwise it will be confused with a ReadRegister
     **/
    WriteRegister();
    
    /**
     * Create a WriteRegister with the given parameters
     * @param chip ABC command if true
     * @param hccid The HCC ID (Broadcast=0xF, ignore=0xE)
     * @param abcid The ABC ID (ignored it is an HCC command)
     * @param address The register address (8-bit)
     * @param value The register value (32-bit)
     **/
    WriteRegister(bool chip, uint32_t hccid, uint32_t abcid, uint8_t address, uint32_t value);

    /**
     * Copy the WriteRegister
     * @param copy The pointer to the object to copy
     **/
    WriteRegister(WriteRegister * copy);

    /**
     * Delete the ReadRegister
     **/
    ~WriteRegister();

    /**
     * Set the chip type
     * @param chip 1 for ABC, 0 for HCC 
     **/

    void SetChip(bool chip);

    /**
     * Get the chip type
     * @return 1 for ABC, 0 for HCC
     **/
 
    bool GetChip();

    /**
     * Set the HCC id
     * @param id the HCC id: 14 is forbidden, 15 talk to all HCC
     **/

    void SetHCCid(uint8_t id);

    /**
     * Get the HCC id
     * @return the hcc id: 14 is forbidden, 15 talk to all HCC
     **/
 
    uint8_t GetHCCid();

    /**
     * Set the ABC id
     * @param id the ABC id
     **/

    void SetABCid(uint8_t id);

    /**
     * Bet the ABC id
     * @return the ABC id
     **/
 
    uint8_t GetABCid();

    /**
     * Set the register address
     * @param address the register address (8-bit)
     **/

    void SetAddress(uint8_t address);

    /**
     * Get the register address
     * @return the register address (8-bit)
     **/
 
    uint8_t GetAddress();

    /**
     * Set the register value
     * @param value the value of the register (32-bit)
     **/

    void SetValue(uint32_t value);

    /**
     * Get the register address of the MSB
     **/
 
    uint32_t GetValue();

    /**
     * Return a human readable representation of the data
     **/

    std::string ToString();

    /**
     * extract command from the bytes
     * @param bytes the byte array
     * @param maxlen the maximum number of bytes than can be read
     * @return the number of bytes processed
     **/

    uint32_t Unpack (uint8_t * bytes, uint32_t maxlen);
      
    /**
     * set command to bytes
     * @param bytes the byte array
     * @return the number of bytes processed
     **/
    uint32_t Pack(uint8_t * bytes);
      
  protected:

    bool m_chip;
    uint8_t m_hccid;
    uint8_t m_abcid;
    uint8_t m_address;
    uint32_t m_value;
  };

}

#endif
