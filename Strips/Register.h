#ifndef STRIPS_REGISTER_H
#define STRIPS_REGISTER_H

#include <cstdint>

namespace Strips{

  /**
   * A Register holds the configuration of a particular ASIC.
   * The value of a Register can be set (Register::SetValue), and retrieved (Register::GetValue).
   * And it also contains a flag (Register::IsUpdated) to know if the register was updated since last reading.
   * The maximum content of a Register is 32 bits.
   *
   * @brief Strips Register
   * @author Carlos.Solans@cern.ch
   * @date June 2022
   **/

  class Register{

  public:

    /**
     * Create a new Register
     **/
    Register();

    /**
     * Delete the Register
     **/
    ~Register();

    /**
     * Set the new value of the Register
     **/
    void SetValue(uint32_t value);

    /**
     * Get the Value of the Register
     * @return The value of the register
     **/
    uint16_t GetValue() const;

    /**
     * Mark the Register as being updated
     * @param enable True if the Register has been updated
     **/
    void Update(bool enable=true);

    /**
     * Get if the Register has been updated
     * @return True if it has been updated
     */
    bool IsUpdated();

  private:

    uint32_t m_value;
    bool m_updated;

  };

}

#endif
