#ifndef STRIPS_ENDEAVOURREAD_H
#define STRIPS_ENDEAVOURREAD_H

#include <Strips/Endeavour.h>

namespace Strips{

  /**
   * This class represents a 16-bit Endeavour::READ command for the AMAC
   *
   * | Bit     |  0 |  3 |  4 |  7 |  8 | 15 |
   * | ------  | -- | -- | -- | -- | -- | -- |
   * | Byte    |  0             ||||  1     ||
   * | Size    |  3     ||  5     ||  8     ||
   * | Desc    | 0b101  || AMACID || Address||
   *
   *
   * @brief Endeavour::READ command for the AMAC
   * @author Carlos.Solans@cern.ch
   * @date August 2022
   **/

  class EndeavourRead: public Endeavour{

  public:

    /**
     * Create an empty EndeavourRead
     **/
    EndeavourRead();
    
    /**
     * Create a EndeavourRead with the given parameters
     * Create a EndeavourWrite with the given parameters
     * @param id the ID to write in the command
     * @param address the address to write in the command
     **/
    EndeavourRead(uint8_t id, uint8_t address);

    /**
     * Copy the EndeavourRead command
     * @param copy The pointer to the object to copy
     **/
    EndeavourRead(EndeavourRead * copy);

    /**
     * Delete the EndeavourRead
     **/
    ~EndeavourRead();

    /**
     * Set the ID of the EndeavourRead
     * @param id The ID of the EndeavourRead
     **/
    void SetID(uint8_t id);

    /**
     * Get the ID of the EndeavourRead
     * @return the ID of the command
     **/
    uint8_t GetID();

    /**
     * Set the Address of the EndeavourRead
     * @param address the address of the command
     **/
    void SetAddress(uint8_t address);

    /**
     * Get the Address of the EndeavourRead
     * @return the of the command
     **/
    uint8_t GetAddress();

    /**
     * Return a human readable representation of the EndeavourRead
     **/
    std::string ToString();

    /**
     * extract command from the bytes
     * @param bytes the byte array
     * @param maxlen the maximum number of bytes than can be read
     * @return the number of bytes processed
     **/
    uint32_t Unpack(uint8_t * bytes, uint32_t maxlen);
      
    /**
     * set command to bytes
     * @param bytes the byte array
     * @return the number of bytes processed
     **/
    uint32_t Pack(uint8_t * bytes);
      
  protected:

    /**
     * Can be set with EndeavourRead::SetID and read with EndeavourRead::GetID.
     * @brief The 4-bit ID of the command. 
     **/
    uint8_t m_id;

    /**
     * Can be set with EndeavourRead::SetAddress and read with EndeavourRead::GetAddress.
     * @brief The 8-bit address of the command. 
     **/
    uint8_t m_address;

  };

}

#endif
