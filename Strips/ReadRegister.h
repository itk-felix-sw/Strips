#ifndef STRIPS_READREGISTER_H
#define STRIPS_READREGISTER_H

#include <Strips/Command.h>

namespace Strips{

  /**
   * A ReadRegister command is a sequence of 16-bit frames.
   * These frames do not need to be consecutive to be interpreted but must be in order.
   * Frames interleaved may be of any type. However, this implementation does not support it.
   * A ReadRegister will be encoded into a consecutive array of bytes, and decoded from a consecutive array of bytes.
   * The command starts with a header 16-bit frame and ends with a similar 16-bit trailer frame
   * followed by two 16-bit frames containing the ABC ID and the register address.
   *
   * The format is the following:
   *
   * | Bit     |  0 |  7 |  8 | 15 | 16 | 23 | 24 | 31 | 32 | 39 | 40 | 47 | 48 | 55 | 56 | 63 |
   * | ------  | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
   * | Byte    |  0     ||  1     ||  2     ||  3     ||  4     ||  5     ||  6     ||  7     ||
   * | Size    |  8     ||  8     || 16             |||| 16             ||||  8     || 8      ||
   * | Desc    |  K2    || Header || Frame 1           | Frame 2        |||| K2     || Trailer||
   *
   * The header and trailer are 6b/8b (SixToEight) encoded.
   * The payload contains an ABC/HCC select bit (ABC=1, HCC=0), a Start/End bit (Start=1, End=0),
   * and the 4-bit HCC ID (Broadcast=0b1111, Ignore=0b1110).
   * The format is the following:
   *
   * | Bit     |  0      |  1        |  2 |  5 |
   * | ------  | ------- | --------- | -- | -- |
   * | Payload |  1 and 7                   ||||
   * | Size    |  1      |  1        |  4     ||
   * | Desc    | ABC/HCC | Start/End | HCC ID ||
   *
   *
   * The following 2 frames are 6b/8b (SixToEight) encoded too.
   * The first one contains a 5-bit Marker (0b00000), a Read/Write bit (Read=1, Write=0),
   * the 4-bit ABC ID, and the 2-bit MSBs of the Address.
   * The payload is the following:
   *
   * | Bit     |  0 |  4 |  5 |  6 |  9 | 10 | 11      |
   * | ------  | -- | -- | -- | -- | -- | -- | ------- |
   * | Payload |  2         |||  3                  ||||
   * | Size    |  1 |  4 |  1 | 4      || 2           ||
   * | Desc    | Marker || RW | ABC ID || Address MSB ||
   *
   * The second one contains a 5-bit Marker (0b00000), a Read/Write bit (Read=1, Write=0),
   * the 4-bit ABC ID, and the 2-bit MSBs of the Address.
   * The payload is the following:
   *
   * | Bit     |  0 |  4 |  5 |  6 | 10 | 11    |
   * | ------  | -- | -- | -- | -- | -- | ----- |
   * | Payload |  4         |||  5            |||
   * | Size    |  1 |  4 | 6          ||| 1     |
   * | Desc    | Marker || Address LSB||| Spare |
   *
   *
   *
   * @brief Read Register LCB sequence of Frames for Strips
   * @author Carlos.Solans@cern.ch
   * @date August 2022
   **/

  class ReadRegister: public Command{

  public:

    /**
     * Create an empty ReadRegister
     **/
    ReadRegister();

    /**
     * Create a WriteRegister with the given parameters
     * @param chip ABC command if true
     * @param hccid The HCC ID (Broadcast=0xF, ignore=0xE)
     * @param abcid The ABC ID (ignored it is an HCC command)
     * @param address The register address (8-bit)
     **/
    ReadRegister(bool chip, uint8_t hccid, uint8_t abcid, uint8_t address);

    /**
     * Copy the WriteRegister
     * @param copy The pointer to the object to copy
     **/
    ReadRegister(ReadRegister * copy);
    
    /**
     * Delete the ReadRegister
     **/
    ~ReadRegister();

    /**
     * Set the chip type
     * @param chip 1 for ABC, 0 for HCC 
     **/
    void SetChip(bool chip);

    /**
     * Get the chip type
     * @param 1 for ABC, 0 for HCC 
     **/
    bool GetChip();

    /**
     * Set the HCC id
     * @param hccid the HCC id: 14 is forbidden, 15 talk to all HCC
     **/
    void SetHCCid(uint8_t hccid);

    /**
     * Get the HCC id 
     **/
    uint8_t GetHCCid();

    /**
     * Set the ABC id
     * @param abcid the ABC id
     **/
    void SetABCid(uint8_t abcid);

    /**
     * Bet the ABC id
     * @return ABC id
     **/
    uint8_t GetABCid();

    /**
     * Set the register address
     * @param address the register address (8-bit)
     **/
    void SetAddress(uint8_t address);

    /**
     * Get the register address
     * @return the register address (8-bit)
     **/
    uint8_t GetAddress();

    /**
     * Return a human readable representation of the data
     **/
    std::string ToString();

    /**
     * extract command from the bytes
     * @param bytes the byte array
     * @param maxlen the maximum number of bytes than can be read
     * @return the number of bytes processed
     **/

    uint32_t Unpack (uint8_t * bytes, uint32_t maxlen);
      
    /**
     * set command to bytes
     * @param bytes the byte array
     * @return the number of bytes processed
     **/
    uint32_t Pack(uint8_t * bytes);
      
  protected:

    bool m_chip;
    uint8_t m_hccid;
    uint8_t m_abcid;
    uint8_t m_address;
  };

}

#endif
