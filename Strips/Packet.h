#ifndef STRIPS_PACKET_H
#define STRIPS_PACKET_H

#include <cstdint>
#include <string>
#include <map>

namespace Strips{

  class Packet{
    public:

	  /**
	   * Create an empty data packet
	   **/
    Packet();

    /**
     * Delete the data packet
     **/
    virtual ~Packet();

    /*
     * packet types
     */
    static const uint32_t TYP_NONE = 0;
    static const uint32_t TYP_PR   = 1;       //!< ABCLPPRPacket
    static const uint32_t TYP_LP   = 2;       //!< ABCLPPRPacket
    static const uint32_t TYP_ABC_RR = 4;     //!< ABCRegisterPacket
    static const uint32_t TYP_ABC_TRANSP = 7; //!< TransparentPacket
    static const uint32_t TYP_HCC_RR = 8;     //!< HCCRegisterPacket
    static const uint32_t TYP_ABC_FULL = 11;  //!< TransparentPacket
    static const uint32_t TYP_ABC_HPR = 13;   //!< ABCRegisterPacket
    static const uint32_t TYP_HCC_HPR = 14;   //!< HCCRegisterPacket
    static const uint32_t TYP_UNKNOWN = 99;   //!< ANDREA dummy values

    static std::map<uint32_t, std::string> typeNames;

    /**
     * Return a human readable representation of the data
     **/
    virtual std::string ToString()=0;

    /**
     * extract command from the bytes
     * @param bytes the byte array
     * @param maxlen the maximum number of bytes than can be read
     * @return the number of bytes processed
     **/
    virtual uint32_t Unpack (uint8_t * bytes, uint32_t maxlen)=0;
      
    /**
     * set command to bytes
     * @param bytes the byte array
     * @return the number of bytes processed
     **/
    virtual uint32_t Pack(uint8_t * bytes)=0;
      
    /**
     * get type of the data
     **/
    virtual uint32_t GetType();

    /**
     * set the type of the data
     **/
    virtual void SetType(uint32_t typ);

    protected:

    uint32_t m_type;

  }; 


}
#endif
