#ifndef STRIPS_ABCREGISTERPACKET_H
#define STRIPS_ABCREGISTERPACKET_H

#include "Strips/Packet.h"

#include <cstdint>
#include <string>

namespace Strips{

  /**
   * This class represents an ABC register Packet. It can be of type ReadRegister or High Priority Read Register.
   * The format is the following:
   *
   * | Bit     | 0 | 3 |  4 | 7           |  8 |  15 | 16 | 19 | 20 | 23 | 24 | 31 | 32 | 39 | 40 | 47 | 48 | 51 | 52 | 55 | 56 | 67 | 68 | 71 |
   * | ------  | - | - | -- | ----------  | -- | --  | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
   * | Byte    | 0                     ||||  1      ||  2             |||| 3      ||  4     ||  5     ||  6             |||| 7              ||||
   * | Size    | 4    || 4               || 8       || 4      || 32                                    |||||||||||   16           |||| 4      ||
   * | Desc    | Type || HCC input chan  || Address || TBD    || Data                                  ||||||||||| Status         |||| Empty  || 
   *
   * @brief ABC Register Packet
   * @author Andrea.Gabrielli@cern.ch
   * @date August 2022
   **/
  class ABCRegisterPacket: public Packet{
 
  public:

    /**
     * Create and empty ABCRegisterPacket of type Packet::TYP_ABC_RR
     **/
    ABCRegisterPacket();

    /**
     * Create an ABCRegisterPacket with the given parameters
     * @param typ the Type of the Packet (Packet::TYP_ABC_RR or Packet::TYP_ABC_HPR)
     * @param channel the ABC channel number in the HCC
     * @param address the ABC register address
     * @param data the ABC register data
     * @param statusbits the ABC status bits
     */
    ABCRegisterPacket(uint32_t typ,uint8_t channel,uint8_t address, uint32_t data,uint32_t statusbits);

    /**
     * Copy the ABCRegisterPacket
     * @param copy The pointer to the object to copy
     **/
    ABCRegisterPacket(ABCRegisterPacket *copy);

    /**
     * Delete the ABCRegisterPacket
     **/
    ~ABCRegisterPacket();

    /**
     * Return a human readable representation of the data
     **/
    std::string ToString();
      
    /**
     * Set the register channel
     * @param channel the ABC channel number in the HCC
     **/
    void SetChannel(uint32_t channel);

    /**
     * Get the register address
     * @return The ABC channel number in the HCC
     **/
    uint32_t GetChannel();

    /**
     * Set the register address
     * @param address the ABC register address
     **/
    void SetAddress(uint32_t address);
    
    /**
     * Get the register address
     * @return the ABC register address
     **/
    uint32_t GetAddress();

    /**
     * Set the register data
     * @param data the ABC register data
     **/
    void SetData(uint32_t data);

    /**
     * Get the register data
     * @return the ABC register data
     **/
    uint32_t GetData();

    /**
     * Set the status bits
     * @param statusbits the ABC status bits
     **/
    void SetStatusBits(uint32_t statusbits);
    
    /**
     * Get the status bits
     * @return the ABC status bits
     **/
    uint32_t GetStatusBits();
    
    /**
     * extract command from the bytes
     * @param bytes the byte array
     * @param maxlen the maximum number of bytes than can be read
     * @return the number of bytes processed
     **/
    uint32_t Unpack (uint8_t * bytes, uint32_t maxlen);

    /**
     * set command to bytes
     * @param bytes the byte array
     * @return the number of bytes processed
     **/
     uint32_t Pack(uint8_t * bytes);

  private:

    uint32_t m_channel = 0;
    uint8_t m_address = 0;
    uint32_t m_data = 0;
    uint32_t m_statusbits = 0;

  };

}

#endif
