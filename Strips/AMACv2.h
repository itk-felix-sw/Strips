#ifndef STRIPS_AMACv2_H
#define STRIPS_AMACv2_H

#include <Strips/Configuration.h>

namespace Strips{

  class AMACv2: public Configuration{
  public:
    
    /**
     * The number of registers available in the AMACv2
     **/
    static const uint32_t NUM_REGISTERS=171;

    enum AMACv2Register{
      REG_Status=0, /**< Status **/
      REG_HxFlags=1, /**< HxFlags **/
      REG_HyFlags=2, /**< HyFlags **/
      REG_HV0Flags=3, /**< HV0Flags **/
      REG_HV2Flags=4, /**< HV2Flags **/
      REG_DCDCflags=5, /**< DCDCflags **/
      REG_WRNflags=6, /**< WRNflags **/
      REG_SynFlags=7, /**< SynFlags **/
      REG_Value0=10, /**< Value0 **/
      REG_Value1=11, /**< Value1 **/
      REG_Value2=12, /**< Value2 **/
      REG_Value3=13, /**< Value3 **/
      REG_Value4=14, /**< Value4 **/
      REG_Value5=15, /**< Value5 **/
      REG_SerNum=31, /**< SerNum **/
      REG_FlagResets=32, /**< FlagResets **/
      REG_LogicReset=33, /**< LogicReset **/
      REG_HardReset=34, /**< HardReset **/
      REG_CntSet=40, /**< CntSet **/
      REG_CntSetC=41, /**< CntSetC **/
      REG_DCDCen=42, /**< DCDCen **/
      REG_DCDCenC=43, /**< DCDCenC **/
      REG_Ilock=44, /**< Ilock **/
      REG_IlockC=45, /**< IlockC **/
      REG_RstCnt=46, /**< RstCnt **/
      REG_RstCntC=47, /**< RstCntC **/
      REG_AMen=48, /**< AMen **/
      REG_AMenC=49, /**< AMenC **/
      REG_AMpwr=50, /**< AMpwr **/
      REG_AMpwrC=51, /**< AMpwrC **/
      REG_BgCnt=52, /**< BgCnt **/
      REG_AMcnt=53, /**< AMcnt **/
      REG_Dacs0=54, /**< Dacs0 **/
      REG_DACbias=55, /**< DACbias **/
      REG_AMACcnt=56, /**< AMACcnt **/
      REG_NTC=57, /**< NTC **/
      REG_LvCurCal=58, /**< LvCurCal **/
      REG_HxICm_cfg=60, /**< HxICm_cfg **/
      REG_HyICm_cfg=61, /**< HyICm_cfg **/
      REG_HV0ICm_cfg=62, /**< HV0ICm_cfg **/
      REG_HV2ICm_cfg=63, /**< HV2ICm_cfg **/
      REG_DCDCICm_cfg=64, /**< DCDCICm_cfg **/
      REG_WRNICm_cfg=65, /**< WRNICm_cfg **/
      REG_HxTlut=70, /**< HxTlut **/
      REG_HxModlut1=71, /**< HxModlut1 **/
      REG_HxModlut2=72, /**< HxModlut2 **/
      REG_HyTlut=73, /**< HyTlut **/
      REG_HyModlut1=74, /**< HyModlut1 **/
      REG_HyModlut2=75, /**< HyModlut2 **/
      REG_HV0Tlut=76, /**< HV0Tlut **/
      REG_HV0Modlut1=77, /**< HV0Modlut1 **/
      REG_HV0Modlut2=78, /**< HV0Modlut2 **/
      REG_HV2Tlut=79, /**< HV2Tlut **/
      REG_HV2Modlut1=80, /**< HV2Modlut1 **/
      REG_HV2Modlut2=81, /**< HV2Modlut2 **/
      REG_DCDCTlut=82, /**< DCDCTlut **/
      REG_DCDCModlut1=83, /**< DCDCModlut1 **/
      REG_DCDCModlut2=84, /**< DCDCModlut2 **/
      REG_WRNTlut=85, /**< WRNTlut **/
      REG_WRNModlut1=86, /**< WRNModlut1 **/
      REG_WRNModlut2=87, /**< WRNModlut2 **/
      REG_HxFlagEn=90, /**< HxFlagEn **/
      REG_HyFlagEn=91, /**< HyFlagEn **/
      REG_HV0FlagEn=92, /**< HV0FlagEn **/
      REG_HV2FlagEn=93, /**< HV2FlagEn **/
      REG_DCDCFlagEn=94, /**< DCDCFlagEn **/
      REG_WRNFlagEn=94, /**< WRNFlagEn **/
      REG_SynFlagEn=95, /**< SynFlagEn **/
      REG_HxLoTh0=100, /**< HxLoTh0 **/
      REG_HxLoTh1=101, /**< HxLoTh1 **/
      REG_HxLoTh2=102, /**< HxLoTh2 **/
      REG_HxLoTh3=103, /**< HxLoTh3 **/
      REG_HxLoTh4=104, /**< HxLoTh4 **/
      REG_HxLoTh5=105, /**< HxLoTh5 **/
      REG_HxHiTh0=106, /**< HxHiTh0 **/
      REG_HxHiTh1=107, /**< HxHiTh1 **/
      REG_HxHiTh2=108, /**< HxHiTh2 **/
      REG_HxHiTh3=109, /**< HxHiTh3 **/
      REG_HxHiTh4=110, /**< HxHiTh4 **/
      REG_HxHiTh5=111, /**< HxHiTh5 **/
      REG_HyLoTh0=112, /**< HyLoTh0 **/
      REG_HyLoTh1=113, /**< HyLoTh1 **/
      REG_HyLoTh2=114, /**< HyLoTh2 **/
      REG_HyLoTh3=115, /**< HyLoTh3 **/
      REG_HyLoTh4=116, /**< HyLoTh4 **/
      REG_HyLoTh5=117, /**< HyLoTh5 **/
      REG_HyHiTh0=118, /**< HyHiTh0 **/
      REG_HyHiTh1=119, /**< HyHiTh1 **/
      REG_HyHiTh2=120, /**< HyHiTh2 **/
      REG_HyHiTh3=121, /**< HyHiTh3 **/
      REG_HyHiTh4=122, /**< HyHiTh4 **/
      REG_HyHiTh5=123, /**< HyHiTh5 **/
      REG_HV0LoTh0=124, /**< HV0LoTh0 **/
      REG_HV0LoTh1=125, /**< HV0LoTh1 **/
      REG_HV0LoTh2=126, /**< HV0LoTh2 **/
      REG_HV0LoTh3=127, /**< HV0LoTh3 **/
      REG_HV0LoTh4=128, /**< HV0LoTh4 **/
      REG_HV0LoTh5=129, /**< HV0LoTh5 **/
      REG_HV0HiTh0=130, /**< HV0HiTh0 **/
      REG_HV0HiTh1=131, /**< HV0HiTh1 **/
      REG_HV0HiTh2=132, /**< HV0HiTh2 **/
      REG_HV0HiTh3=133, /**< HV0HiTh3 **/
      REG_HV0HiTh4=134, /**< HV0HiTh4 **/
      REG_HV0HiTh5=135, /**< HV0HiTh5 **/
      REG_HV2LoTh0=136, /**< HV2LoTh0 **/
      REG_HV2LoTh1=137, /**< HV2LoTh1 **/
      REG_HV2LoTh2=138, /**< HV2LoTh2 **/
      REG_HV2LoTh3=139, /**< HV2LoTh3 **/
      REG_HV2LoTh4=140, /**< HV2LoTh4 **/
      REG_HV2LoTh5=141, /**< HV2LoTh5 **/
      REG_HV2HiTh0=142, /**< HV2HiTh0 **/
      REG_HV2HiTh1=143, /**< HV2HiTh1 **/
      REG_HV2HiTh2=144, /**< HV2HiTh2 **/
      REG_HV2HiTh3=145, /**< HV2HiTh3 **/
      REG_HV2HiTh4=146, /**< HV2HiTh4 **/
      REG_HV2HiTh5=147, /**< HV2HiTh5 **/
      REG_DCDCLoTh0=148, /**< DCDCLoTh0 **/
      REG_DCDCLoTh1=149, /**< DCDCLoTh1 **/
      REG_DCDCLoTh2=150, /**< DCDCLoTh2 **/
      REG_DCDCLoTh3=151, /**< DCDCLoTh3 **/
      REG_DCDCLoTh4=152, /**< DCDCLoTh4 **/
      REG_DCDCLoTh5=153, /**< DCDCLoTh5 **/
      REG_DCDCHiTh0=154, /**< DCDCHiTh0 **/
      REG_DCDCHiTh1=155, /**< DCDCHiTh1 **/
      REG_DCDCHiTh2=156, /**< DCDCHiTh2 **/
      REG_DCDCHiTh3=157, /**< DCDCHiTh3 **/
      REG_DCDCHiTh4=158, /**< DCDCHiTh4 **/
      REG_DCDCHiTh5=159, /**< DCDCHiTh5 **/
      REG_WRNLoTh0=160, /**< WRNLoTh0 **/
      REG_WRNLoTh1=161, /**< WRNLoTh1 **/
      REG_WRNLoTh2=162, /**< WRNLoTh2 **/
      REG_WRNLoTh3=163, /**< WRNLoTh3 **/
      REG_WRNLoTh4=164, /**< WRNLoTh4 **/
      REG_WRNLoTh5=165, /**< WRNLoTh5 **/
      REG_WRNHiTh0=166, /**< WRNHiTh0 **/
      REG_WRNHiTh1=167, /**< WRNHiTh1 **/
      REG_WRNHiTh2=168, /**< WRNHiTh2 **/
      REG_WRNHiTh3=169, /**< WRNHiTh3 **/
      REG_WRNHiTh4=170, /**< WRNHiTh4 **/
      REG_WRNHiTh5=171, /**< WRNHiTh5 **/
    
    };    

    /**
     * Definition of the sub-registers available in the AMACv2
     **/
    enum AMACv2SubRegister{

      StatusAM  ,   /**< Address : 0, bit:  31, size: 1  **/
      StatusWARN,   /**< Address : 0, bit:  29, size: 1  **/
      StatusDCDC,   /**< Address : 0, bit:  28, size: 1  **/
      StatusHV3,   /**< Address : 0, bit:  27, size: 1  **/
      StatusHV2,   /**< Address : 0, bit:  26, size: 1  **/
      StatusHV1,   /**< Address : 0, bit:  25, size: 1  **/
      StatusHV0,   /**< Address : 0, bit:  24, size: 1  **/
      StatusY2LDO,   /**< Address : 0, bit:  22, size: 1  **/
      StatusY1LDO,   /**< Address : 0, bit:  21, size: 1  **/
      StatusY0LDO,   /**< Address : 0, bit:  20, size: 1  **/
      StatusX2LDO,   /**< Address : 0, bit:  18, size: 1  **/
      StatusX1LDO,   /**< Address : 0, bit:  17, size: 1  **/
      StatusX0LDO,   /**< Address : 0, bit:  16, size: 1  **/
      StatusGPI,   /**< Address : 0, bit:  12, size: 1  **/
      StatusPGOOD,   /**< Address : 0, bit:  8, size: 1  **/
      StatusILockWARN,   /**< Address : 0, bit:  5, size: 1  **/
      StatusILockDCDC,   /**< Address : 0, bit:  4, size: 1  **/
      StatusILockHV2,   /**< Address : 0, bit:  3, size: 1  **/
      StatusILockHV0,   /**< Address : 0, bit:  2, size: 1  **/
      StatusILockYLDO,   /**< Address : 0, bit:  1, size: 1  **/
      StatusILockxLDO,   /**< Address : 0, bit:  0, size: 1  **/

      HxFlagsHi,   /**< Address : 1, bit:  16, size: 16  **/
      HxFlagsLo,   /**< Address : 1, bit:  0, size: 16  **/

      HyFlagsHi,   /**< Address : 2, bit:  16, size: 16  **/
      HyFlagsLo,   /**< Address : 2, bit:  0, size: 16  **/

      HV0FlagsHi,   /**< Address : 3, bit:  16, size: 16  **/
      HV0FlagsLo,   /**< Address : 3, bit:  0, size: 16  **/

      HV2FlagsHi,   /**< Address : 4, bit:  16, size: 16  **/
      HV2FlagsLo,   /**< Address : 4, bit:  0, size: 16  **/

      DCDCflagsHi,   /**< Address : 5, bit:  16, size: 16  **/
      DCDCflagsLo,   /**< Address : 5, bit:  0, size: 16  **/

      WRNflagsHi,   /**< Address : 6, bit:  16, size: 16  **/
      WRNflagsLo,   /**< Address : 6, bit:  0, size: 16  **/

      SynFlagsWRN,   /**< Address : 7, bit:  20, size: 2  **/
      SynFlagsDCDC,   /**< Address : 7, bit:  16, size: 2  **/
      SynFlagsHV2,   /**< Address : 7, bit:  12, size: 2  **/
      SynFlagsHV0,   /**< Address : 7, bit:  8, size: 2  **/
      SynFlagsHy,   /**< Address : 7, bit:  4, size: 2  **/
      SynFlagsHx,   /**< Address : 7, bit:  0, size: 2  **/

      Value0AMen,   /**< Address : 10, bit:  31, size: 1  **/
      Ch0Value,   /**< Address : 10, bit:  0, size: 10  **/
      Ch1Value,   /**< Address : 10, bit:  10, size: 10  **/
      Ch2Value,   /**< Address : 10, bit:  20, size: 10  **/

      Value1AMen,   /**< Address : 11, bit:  31, size: 1  **/
      Ch3Value,   /**< Address : 11, bit:  0, size: 10  **/
      Ch4Value,   /**< Address : 11, bit:  10, size: 10  **/
      Ch5Value,   /**< Address : 11, bit:  20, size: 10  **/

      Value2AMen,   /**< Address : 12, bit:  31, size: 1  **/
      Ch6Value,   /**< Address : 12, bit:  0, size: 10  **/
      Ch7Value,   /**< Address : 12, bit:  10, size: 10  **/
      Ch8Value,   /**< Address : 12, bit:  20, size: 10  **/

      Value3AMen,   /**< Address : 13, bit:  31, size: 1  **/
      Ch9Value,   /**< Address : 13, bit:  0, size: 10  **/
      Ch10Value,   /**< Address : 13, bit:  10, size: 10  **/
      Ch11Value,   /**< Address : 13, bit:  20, size: 10  **/

      Value4AMen,   /**< Address : 14, bit:  31, size: 1  **/
      Ch12Value,   /**< Address : 14, bit:  0, size: 10  **/
      Ch13Value,   /**< Address : 14, bit:  10, size: 10  **/
      Ch14Value,   /**< Address : 14, bit:  20, size: 10  **/

      Value5AMen,   /**< Address : 15, bit:  31, size: 1  **/
      Ch15Value,   /**< Address : 15, bit:  0, size: 10  **/

      PadID,   /**< Address : 31, bit:  24, size: 5  **/
      SerNum,   /**< Address : 31, bit:  0, size: 16  **/

      FlagResetWRN,   /**< Address : 32, bit:  5, size: 1  **/
      FlagResetDCDC,   /**< Address : 32, bit:  4, size: 1  **/
      FlagResetHV2,   /**< Address : 32, bit:  3, size: 1  **/
      FlagResetHV0,   /**< Address : 32, bit:  2, size: 1  **/
      FlagResetXLDO,   /**< Address : 32, bit:  1, size: 1  **/
      FlagResetYLDO,   /**< Address : 32, bit:  0, size: 1  **/

      LogicReset,   /**< Address : 33, bit:  0, size: 32  **/

      HardReset,   /**< Address : 34, bit:  0, size: 32  **/

      CntSetHV3frq,   /**< Address : 40, bit:  29, size: 2  **/
      CntSetHV3en,   /**< Address : 40, bit:  28, size: 1  **/
      CntSetHV2frq,   /**< Address : 40, bit:  25, size: 2  **/
      CntSetHV2en,   /**< Address : 40, bit:  24, size: 1  **/
      CntSetHV1frq,   /**< Address : 40, bit:  21, size: 2  **/
      CntSetHV1en,   /**< Address : 40, bit:  20, size: 1  **/
      CntSetHV0frq,   /**< Address : 40, bit:  17, size: 2  **/
      CntSetHV0en,   /**< Address : 40, bit:  16, size: 1  **/
      CntSetHyLDO2en,   /**< Address : 40, bit:  14, size: 1  **/
      CntSetHyLDO1en,   /**< Address : 40, bit:  13, size: 1  **/
      CntSetHyLDO0en,   /**< Address : 40, bit:  12, size: 1  **/
      CntSetHxLDO2en,   /**< Address : 40, bit:  10, size: 1  **/
      CntSetHxLDO1en,   /**< Address : 40, bit:  9, size: 1  **/
      CntSetHxLDO0en,   /**< Address : 40, bit:  8, size: 1  **/
      CntSetWARN,   /**< Address : 40, bit:  4, size: 1  **/

      CntSetCHV3frq,   /**< Address : 41, bit:  29, size: 2  **/
      CntSetCHV3en,   /**< Address : 41, bit:  28, size: 1  **/
      CntSetCHV2frq,   /**< Address : 41, bit:  25, size: 2  **/
      CntSetCHV2en,   /**< Address : 41, bit:  24, size: 1  **/
      CntSetCHV1frq,   /**< Address : 41, bit:  21, size: 2  **/
      CntSetCHV1en,   /**< Address : 41, bit:  20, size: 1  **/
      CntSetCHV0frq,   /**< Address : 41, bit:  17, size: 2  **/
      CntSetCHV0en,   /**< Address : 41, bit:  16, size: 1  **/
      CntSetCHyLDO2en,   /**< Address : 41, bit:  14, size: 1  **/
      CntSetCHyLDO1en,   /**< Address : 41, bit:  13, size: 1  **/
      CntSetCHyLDO0en,   /**< Address : 41, bit:  12, size: 1  **/
      CntSetCHxLDO2en,   /**< Address : 41, bit:  10, size: 1  **/
      CntSetCHxLDO1en,   /**< Address : 41, bit:  9, size: 1  **/
      CntSetCHxLDO0en,   /**< Address : 41, bit:  8, size: 1  **/
      CntSetCWARN,   /**< Address : 41, bit:  4, size: 1  **/

      DCDCAdj,   /**< Address : 42, bit:  4, size: 2  **/
      DCDCen,   /**< Address : 42, bit:  0, size: 1  **/

      DCDCAdjC,   /**< Address : 43, bit:  4, size: 2  **/
      DCDCenC,   /**< Address : 43, bit:  0, size: 1  **/

      IlockWRN,   /**< Address : 44, bit:  5, size: 1  **/
      IlockDCDC,   /**< Address : 44, bit:  4, size: 1  **/
      IlockHV2,   /**< Address : 44, bit:  3, size: 1  **/
      IlockHV0,   /**< Address : 44, bit:  2, size: 1  **/
      IlockHy,   /**< Address : 44, bit:  1, size: 1  **/
      IlockHx,   /**< Address : 44, bit:  0, size: 1  **/

      IlockCWRN,   /**< Address : 45, bit:  5, size: 1  **/
      IlockCDCDC,   /**< Address : 45, bit:  4, size: 1  **/
      IlockCHV2,   /**< Address : 45, bit:  3, size: 1  **/
      IlockCHV0,   /**< Address : 45, bit:  2, size: 1  **/
      IlockCHy,   /**< Address : 45, bit:  1, size: 1  **/
      IlockCHx,   /**< Address : 45, bit:  0, size: 1  **/

      RstCntHyHCCresetB,   /**< Address : 46, bit:  16, size: 1  **/
      RstCntHxHCCresetB,   /**< Address : 46, bit:  8, size: 1  **/
      RstCntOF,   /**< Address : 46, bit:  0, size: 1  **/

      RstCntCHyHCCresetB,   /**< Address : 47, bit:  16, size: 1  **/
      RstCntCHxHCCresetB,   /**< Address : 47, bit:  8, size: 1  **/
      RstCntCOF,   /**< Address : 47, bit:  0, size: 1  **/

      AMzeroCalib,   /**< Address : 48, bit:  8, size: 1  **/
      AMen,   /**< Address : 48, bit:  0, size: 1  **/

      AMzeroCalibC,   /**< Address : 49, bit:  8, size: 1  **/
      AMenC,   /**< Address : 49, bit:  0, size: 1  **/

      ReqDCDCPGOOD,   /**< Address : 50, bit:  8, size: 1  **/
      DCDCenToPwrAMAC,   /**< Address : 50, bit:  0, size: 1  **/

      ReqDCDCPGOODC,   /**< Address : 51, bit:  8, size: 1  **/
      DCDCenToPwrAMACC,   /**< Address : 51, bit:  0, size: 1  **/

      AMbgen ,   /**< Address : 52, bit:  15, size: 1  **/
      AMbg   ,   /**< Address : 52, bit:   8, size: 5  **/
      VDDbgen,   /**< Address : 52, bit:   7, size: 1  **/
      VDDbg  ,   /**< Address : 52, bit:   0, size: 5  **/

      AMintCalib,   /**< Address : 53, bit:  24, size: 4  **/
      Ch13Mux,   /**< Address : 53, bit:  20, size: 2  **/
      Ch12Mux,   /**< Address : 53, bit:  16, size: 2  **/
      Ch5Mux,   /**< Address : 53, bit:  13, size: 2  **/
      Ch4Mux,   /**< Address : 53, bit:  8, size: 2  **/
      Ch3Mux,   /**< Address : 53, bit:  4, size: 2  **/

      DACShunty,   /**< Address : 54, bit:  24, size: 8  **/
      DACShuntx,   /**< Address : 54, bit:  16, size: 8  **/
      DACCALy,   /**< Address : 54, bit:  8, size: 8  **/
      DACCalx,   /**< Address : 54, bit:  0, size: 8  **/

      DACbias,   /**< Address : 55, bit:  0, size: 5  **/

      HVcurGain ,   /**< Address : 56, bit:  16, size: 4  **/
      DRcomMode ,   /**< Address : 56, bit:  12, size: 2  **/
      DRcurr    ,   /**< Address : 56, bit:   8, size: 3  **/
      RingOscFrq,   /**< Address : 56, bit:   0, size: 3  **/

      NTCpbCal       ,   /**< Address : 57, bit:  19, size: 1  **/
      NTCpbSenseRange,   /**< Address : 57, bit:  16, size: 3  **/
      NTCy0Cal       ,   /**< Address : 57, bit:  11, size: 1  **/
      NTCy0SenseRange,   /**< Address : 57, bit:   8, size: 3  **/
      NTCx0Cal       ,   /**< Address : 57, bit:   3, size: 1  **/
      NTCx0SenseRange,   /**< Address : 57, bit:   0, size: 3  **/

      DCDCoOffset     ,   /**< Address : 58, bit:  20, size: 4  **/
      DCDCoZeroReading,   /**< Address : 58, bit:  19, size: 1  **/
      DCDCoN          ,   /**< Address : 58, bit:  17, size: 1  **/
      DCDCoP          ,   /**< Address : 58, bit:  16, size: 1  **/
      DCDCiZeroReading,   /**< Address : 58, bit:  15, size: 1  **/
      DCDCiRangeSW    ,   /**< Address : 58, bit:  12, size: 1  **/
      DCDCiOffset     ,   /**< Address : 58, bit:   8, size: 4  **/
      DCDCiP          ,   /**< Address : 58, bit:   4, size: 3  **/
      DCDCiN          ,   /**< Address : 58, bit:   0, size: 3  **/

      HxLAM,   /**< Address : 60, bit:  16, size: 1  **/
      HxFlagsLatch,   /**< Address : 60, bit:  12, size: 1  **/
      HxFlagsLogic,   /**< Address : 60, bit:  8, size: 1  **/
      HxFlagValid,   /**< Address : 60, bit:  4, size: 1  **/
      HxFlagsValidConf,   /**< Address : 60, bit:  0, size: 2  **/

      HyLAM,   /**< Address : 61, bit:  16, size: 1  **/
      HyFlagsLatch,   /**< Address : 61, bit:  12, size: 1  **/
      HyFlagsLogic,   /**< Address : 61, bit:  8, size: 1  **/
      HyFlagValid,   /**< Address : 61, bit:  4, size: 1  **/
      HyFlagsValidConf,   /**< Address : 61, bit:  0, size: 2  **/

      HV0LAM,   /**< Address : 62, bit:  16, size: 1  **/
      HV0FlagsLatch,   /**< Address : 62, bit:  12, size: 1  **/
      HV0FlagsLogic,   /**< Address : 62, bit:  8, size: 1  **/
      HV0FlagValid,   /**< Address : 62, bit:  4, size: 1  **/
      HV0FlagsValidConf,   /**< Address : 62, bit:  0, size: 2  **/

      HV2LAM,   /**< Address : 63, bit:  16, size: 1  **/
      HV2FlagsLatch,   /**< Address : 63, bit:  12, size: 1  **/
      HV2FlagsLogic,   /**< Address : 63, bit:  8, size: 1  **/
      HV2FlagValid,   /**< Address : 63, bit:  4, size: 1  **/
      HV2FlagsValidConf,   /**< Address : 63, bit:  0, size: 2  **/

      DCDCLAM,   /**< Address : 64, bit:  16, size: 1  **/
      DCDCFlagsLatch,   /**< Address : 64, bit:  12, size: 1  **/
      DCDCFlagsLogic,   /**< Address : 64, bit:  8, size: 1  **/
      DCDCFlagValid,   /**< Address : 64, bit:  4, size: 1  **/
      DCDCFlagsValidConf,   /**< Address : 64, bit:  0, size: 2  **/

      WRNLAM,   /**< Address : 65, bit:  16, size: 1  **/
      WRNFlagsLatch,   /**< Address : 65, bit:  12, size: 1  **/
      WRNFlagsLogic,   /**< Address : 65, bit:  8, size: 1  **/
      WRNFlagValid,   /**< Address : 65, bit:  4, size: 1  **/
      WRNFlagsValidConf,   /**< Address : 65, bit:  0, size: 2  **/

      HxTlut,   /**< Address : 70, bit:  0, size: 8  **/

      HxModlut1,   /**< Address : 71, bit:  0, size: 32  **/

      HxModlut2,   /**< Address : 72, bit:  0, size: 32  **/

      HyTlut,   /**< Address : 73, bit:  0, size: 8  **/

      HyModlut1,   /**< Address : 74, bit:  0, size: 32  **/

      HyModlut2,   /**< Address : 75, bit:  0, size: 32  **/

      HV0Tlut,   /**< Address : 76, bit:  0, size: 8  **/

      HV0Modlut1,   /**< Address : 77, bit:  0, size: 32  **/

      HV0Modlut2,   /**< Address : 78, bit:  0, size: 32  **/

      HV2Tlut,   /**< Address : 79, bit:  0, size: 8  **/

      HV2Modlut1,   /**< Address : 80, bit:  0, size: 32  **/

      HV2Modlut2,   /**< Address : 81, bit:  0, size: 32  **/

      DCDCTlut,   /**< Address : 82, bit:  0, size: 8  **/

      DCDCModlut1,   /**< Address : 83, bit:  0, size: 32  **/

      DCDCModlut2,   /**< Address : 84, bit:  0, size: 32  **/

      WRNTlut,   /**< Address : 85, bit:  0, size: 8  **/

      WRNModlut1,   /**< Address : 86, bit:  0, size: 32  **/

      WRNModlut2,   /**< Address : 87, bit:  0, size: 32  **/

      HxFlagsEnHi,   /**< Address : 90, bit:  16, size: 16  **/
      HxFlagsEnLo,   /**< Address : 90, bit:  16, size: 16  **/

      HyFlagsEnHi,   /**< Address : 91, bit:  16, size: 16  **/
      HyFlagsEnLo,   /**< Address : 91, bit:  16, size: 16  **/

      HV0FlagsEnHi,   /**< Address : 92, bit:  16, size: 16  **/
      HV0FlagsEnLo,   /**< Address : 92, bit:  16, size: 16  **/

      HV2FlagsEnHi,   /**< Address : 93, bit:  16, size: 16  **/
      HV2FlagsEnLo,   /**< Address : 93, bit:  16, size: 16  **/

      DCDCFlagsEnHi,   /**< Address : 94, bit:  16, size: 16  **/
      DCDCFlagsEnLo,   /**< Address : 94, bit:  16, size: 16  **/

      WRNFlagsEnHi,   /**< Address : 95, bit:  16, size: 16  **/
      WRNFlagsEnLo,   /**< Address : 95, bit:  16, size: 16  **/

      WRNsynFlagEnHi,   /**< Address : 95, bit:  21, size: 1  **/
      WRNsynFlagEnLo,   /**< Address : 95, bit:  20, size: 1  **/
      DCDCsynFlagEnHi,   /**< Address : 95, bit:  17, size: 1  **/
      DCDCsynFlagEnLo,   /**< Address : 95, bit:  16, size: 1  **/
      HV2synFlagEnHi,   /**< Address : 95, bit:  13, size: 1  **/
      HV2synFlagEnLo,   /**< Address : 95, bit:  12, size: 1  **/
      HV0synFlagEnHi,   /**< Address : 95, bit:  9, size: 1  **/
      HV0synFlagEnLo,   /**< Address : 95, bit:  8, size: 1  **/
      HysynFlagEnHi,   /**< Address : 95, bit:  5, size: 1  **/
      HysynFlagEnLo,   /**< Address : 95, bit:  4, size: 1  **/
      HxsynFlagEnHi,   /**< Address : 95, bit:  1, size: 1  **/
      HxsynFlagEnLo,   /**< Address : 95, bit:  0, size: 1  **/

      HxLoThCh0,   /**< Address : 100, bit:  0, size: 10  **/
      HxLoThCh1,   /**< Address : 100, bit:  10, size: 10  **/
      HxLoThCh2,   /**< Address : 100, bit:  20, size: 10  **/

      HxLoThCh3,   /**< Address : 101, bit:  0, size: 10  **/
      HxLoThCh4,   /**< Address : 101, bit:  10, size: 10  **/
      HxLoThCh5,   /**< Address : 101, bit:  20, size: 10  **/

      HxLoThCh6,   /**< Address : 102, bit:  0, size: 10  **/
      HxLoThCh7,   /**< Address : 102, bit:  10, size: 10  **/
      HxLoThCh8,   /**< Address : 102, bit:  20, size: 10  **/

      HxLoThCh9,   /**< Address : 103, bit:  0, size: 10  **/
      HxLoThCh10,   /**< Address : 103, bit:  10, size: 10  **/
      HxLoThCh11,   /**< Address : 103, bit:  20, size: 10  **/

      HxLoThCh12,   /**< Address : 104, bit:  0, size: 10  **/
      HxLoThCh13,   /**< Address : 104, bit:  10, size: 10  **/
      HxLoThCh14,   /**< Address : 104, bit:  20, size: 10  **/

      HxLoThCh15,   /**< Address : 105, bit:  0, size: 10  **/

      HxHiThCh0,   /**< Address : 106, bit:  0, size: 10  **/
      HxHiThCh1,   /**< Address : 106, bit:  10, size: 10  **/
      HxHiThCh2,   /**< Address : 106, bit:  20, size: 10  **/

      HxHiThCh3,   /**< Address : 107, bit:  0, size: 10  **/
      HxHiThCh4,   /**< Address : 107, bit:  10, size: 10  **/
      HxHiThCh5,   /**< Address : 107, bit:  20, size: 10  **/

      HxHiThCh6,   /**< Address : 108, bit:  0, size: 10  **/
      HxHiThCh7,   /**< Address : 108, bit:  10, size: 10  **/
      HxHiThCh8,   /**< Address : 108, bit:  20, size: 10  **/

      HxHiThCh9,   /**< Address : 109, bit:  0, size: 10  **/
      HxHiThCh10,   /**< Address : 109, bit:  10, size: 10  **/
      HxHiThCh11,   /**< Address : 109, bit:  20, size: 10  **/

      HxHiThCh12,   /**< Address : 110, bit:  0, size: 10  **/
      HxHiThCh13,   /**< Address : 110, bit:  10, size: 10  **/
      HxHiThCh14,   /**< Address : 110, bit:  20, size: 10  **/

      HxHiThCh15,   /**< Address : 111, bit:  0, size: 10  **/

      HyLoThCh0,   /**< Address : 112, bit:  0, size: 10  **/
      HyLoThCh1,   /**< Address : 112, bit:  10, size: 10  **/
      HyLoThCh2,   /**< Address : 112, bit:  20, size: 10  **/

      HyLoThCh3,   /**< Address : 113, bit:  0, size: 10  **/
      HyLoThCh4,   /**< Address : 113, bit:  10, size: 10  **/
      HyLoThCh5,   /**< Address : 113, bit:  20, size: 10  **/

      HyLoThCh6,   /**< Address : 114, bit:  0, size: 10  **/
      HyLoThCh7,   /**< Address : 114, bit:  10, size: 10  **/
      HyLoThCh8,   /**< Address : 114, bit:  20, size: 10  **/

      HyLoThCh9,   /**< Address : 115, bit:  0, size: 10  **/
      HyLoThCh10,   /**< Address : 115, bit:  10, size: 10  **/
      HyLoThCh11,   /**< Address : 115, bit:  20, size: 10  **/

      HyLoThCh12,   /**< Address : 116, bit:  0, size: 10  **/
      HyLoThCh13,   /**< Address : 116, bit:  10, size: 10  **/
      HyLoThCh14,   /**< Address : 116, bit:  20, size: 10  **/

      HyLoThCh15,   /**< Address : 117, bit:  0, size: 10  **/

      HyHiThCh0,   /**< Address : 118, bit:  0, size: 10  **/
      HyHiThCh1,   /**< Address : 118, bit:  10, size: 10  **/
      HyHiThCh2,   /**< Address : 118, bit:  20, size: 10  **/

      HyHiThCh3,   /**< Address : 119, bit:  0, size: 10  **/
      HyHiThCh4,   /**< Address : 119, bit:  10, size: 10  **/
      HyHiThCh5,   /**< Address : 119, bit:  20, size: 10  **/

      HyHiThCh6,   /**< Address : 120, bit:  0, size: 10  **/
      HyHiThCh7,   /**< Address : 120, bit:  10, size: 10  **/
      HyHiThCh8,   /**< Address : 120, bit:  20, size: 10  **/

      HyHiThCh9,   /**< Address : 121, bit:  0, size: 10  **/
      HyHiThCh10,   /**< Address : 121, bit:  10, size: 10  **/
      HyHiThCh11,   /**< Address : 121, bit:  20, size: 10  **/

      HyHiThCh12,   /**< Address : 122, bit:  0, size: 10  **/
      HyHiThCh13,   /**< Address : 122, bit:  10, size: 10  **/
      HyHiThCh14,   /**< Address : 122, bit:  20, size: 10  **/

      HyHiThCh15,   /**< Address : 123, bit:  0, size: 10  **/

      HV0LoThCh0,   /**< Address : 124, bit:  0, size: 10  **/
      HV0LoThCh1,   /**< Address : 124, bit:  10, size: 10  **/
      HV0LoThCh2,   /**< Address : 124, bit:  20, size: 10  **/

      HV0LoThCh3,   /**< Address : 125, bit:  0, size: 10  **/
      HV0LoThCh4,   /**< Address : 125, bit:  10, size: 10  **/
      HV0LoThCh5,   /**< Address : 125, bit:  20, size: 10  **/

      HV0LoThCh6,   /**< Address : 126, bit:  0, size: 10  **/
      HV0LoThCh7,   /**< Address : 126, bit:  10, size: 10  **/
      HV0LoThCh8,   /**< Address : 126, bit:  20, size: 10  **/

      HV0LoThCh9,   /**< Address : 127, bit:  0, size: 10  **/
      HV0LoThCh10,   /**< Address : 127, bit:  10, size: 10  **/
      HV0LoThCh11,   /**< Address : 127, bit:  20, size: 10  **/

      HV0LoThCh12,   /**< Address : 128, bit:  0, size: 10  **/
      HV0LoThCh13,   /**< Address : 128, bit:  10, size: 10  **/
      HV0LoThCh14,   /**< Address : 128, bit:  20, size: 10  **/

      HV0LoThCh15,   /**< Address : 129, bit:  0, size: 10  **/

      HV0HiThCh0,   /**< Address : 130, bit:  0, size: 10  **/
      HV0HiThCh1,   /**< Address : 130, bit:  10, size: 10  **/
      HV0HiThCh2,   /**< Address : 130, bit:  20, size: 10  **/

      HV0HiThCh3,   /**< Address : 131, bit:  0, size: 10  **/
      HV0HiThCh4,   /**< Address : 131, bit:  10, size: 10  **/
      HV0HiThCh5,   /**< Address : 131, bit:  20, size: 10  **/

      HV0HiThCh6,   /**< Address : 132, bit:  0, size: 10  **/
      HV0HiThCh7,   /**< Address : 132, bit:  10, size: 10  **/
      HV0HiThCh8,   /**< Address : 132, bit:  20, size: 10  **/

      HV0HiThCh9,   /**< Address : 133, bit:  0, size: 10  **/
      HV0HiThCh10,   /**< Address : 133, bit:  10, size: 10  **/
      HV0HiThCh11,   /**< Address : 133, bit:  20, size: 10  **/

      HV0HiThCh12,   /**< Address : 134, bit:  0, size: 10  **/
      HV0HiThCh13,   /**< Address : 134, bit:  10, size: 10  **/
      HV0HiThCh14,   /**< Address : 134, bit:  20, size: 10  **/

      HV0HiThCh15,   /**< Address : 135, bit:  0, size: 10  **/

      HV2LoThCh0,   /**< Address : 136, bit:  0, size: 10  **/
      HV2LoThCh1,   /**< Address : 136, bit:  10, size: 10  **/
      HV2LoThCh2,   /**< Address : 136, bit:  20, size: 10  **/

      HV2LoThCh3,   /**< Address : 137, bit:  0, size: 10  **/
      HV2LoThCh4,   /**< Address : 137, bit:  10, size: 10  **/
      HV2LoThCh5,   /**< Address : 137, bit:  20, size: 10  **/

      HV2LoThCh6,   /**< Address : 138, bit:  0, size: 10  **/
      HV2LoThCh7,   /**< Address : 138, bit:  10, size: 10  **/
      HV2LoThCh8,   /**< Address : 138, bit:  20, size: 10  **/

      HV2LoThCh9,   /**< Address : 139, bit:  0, size: 10  **/
      HV2LoThCh10,   /**< Address : 139, bit:  10, size: 10  **/
      HV2LoThCh11,   /**< Address : 139, bit:  20, size: 10  **/

      HV2LoThCh12,   /**< Address : 140, bit:  0, size: 10  **/
      HV2LoThCh13,   /**< Address : 140, bit:  10, size: 10  **/
      HV2LoThCh14,   /**< Address : 140, bit:  20, size: 10  **/

      HV2LoThCh15,   /**< Address : 141, bit:  0, size: 10  **/

      HV2HiThCh0,   /**< Address : 142, bit:  0, size: 10  **/
      HV2HiThCh1,   /**< Address : 142, bit:  10, size: 10  **/
      HV2HiThCh2,   /**< Address : 142, bit:  20, size: 10  **/

      HV2HiThCh3,   /**< Address : 143, bit:  0, size: 10  **/
      HV2HiThCh4,   /**< Address : 143, bit:  10, size: 10  **/
      HV2HiThCh5,   /**< Address : 143, bit:  20, size: 10  **/

      HV2HiThCh6,   /**< Address : 144, bit:  0, size: 10  **/
      HV2HiThCh7,   /**< Address : 144, bit:  10, size: 10  **/
      HV2HiThCh8,   /**< Address : 144, bit:  20, size: 10  **/

      HV2HiThCh9,   /**< Address : 145, bit:  0, size: 10  **/
      HV2HiThCh10,   /**< Address : 145, bit:  10, size: 10  **/
      HV2HiThCh11,   /**< Address : 145, bit:  20, size: 10  **/

      HV2HiThCh12,   /**< Address : 146, bit:  0, size: 10  **/
      HV2HiThCh13,   /**< Address : 146, bit:  10, size: 10  **/
      HV2HiThCh14,   /**< Address : 146, bit:  20, size: 10  **/

      HV2HiThCh15,   /**< Address : 147, bit:  0, size: 10  **/

      DCDCLoThCh0,   /**< Address : 148, bit:  0, size: 10  **/
      DCDCLoThCh1,   /**< Address : 148, bit:  10, size: 10  **/
      DCDCLoThCh2,   /**< Address : 148, bit:  20, size: 10  **/

      DCDCLoThCh3,   /**< Address : 149, bit:  0, size: 10  **/
      DCDCLoThCh4,   /**< Address : 149, bit:  10, size: 10  **/
      DCDCLoThCh5,   /**< Address : 149, bit:  20, size: 10  **/

      DCDCLoThCh6,   /**< Address : 150, bit:  0, size: 10  **/
      DCDCLoThCh7,   /**< Address : 150, bit:  10, size: 10  **/
      DCDCLoThCh8,   /**< Address : 150, bit:  20, size: 10  **/

      DCDCLoThCh9,   /**< Address : 151, bit:  0, size: 10  **/
      DCDCLoThCh10,   /**< Address : 151, bit:  10, size: 10  **/
      DCDCLoThCh11,   /**< Address : 151, bit:  20, size: 10  **/

      DCDCLoThCh12,   /**< Address : 152, bit:  0, size: 10  **/
      DCDCLoThCh13,   /**< Address : 152, bit:  10, size: 10  **/
      DCDCLoThCh14,   /**< Address : 152, bit:  20, size: 10  **/

      DCDCLoThCh15,   /**< Address : 153, bit:  0, size: 10  **/

      DCDCHiThCh0,   /**< Address : 154, bit:  0, size: 10  **/
      DCDCHiThCh1,   /**< Address : 154, bit:  10, size: 10  **/
      DCDCHiThCh2,   /**< Address : 154, bit:  20, size: 10  **/

      DCDCHiThCh3,   /**< Address : 155, bit:  0, size: 10  **/
      DCDCHiThCh4,   /**< Address : 155, bit:  10, size: 10  **/
      DCDCHiThCh5,   /**< Address : 155, bit:  20, size: 10  **/

      DCDCHiThCh6,   /**< Address : 156, bit:  0, size: 10  **/
      DCDCHiThCh7,   /**< Address : 156, bit:  10, size: 10  **/
      DCDCHiThCh8,   /**< Address : 156, bit:  20, size: 10  **/

      DCDCHiThCh9,   /**< Address : 157, bit:  0, size: 10  **/
      DCDCHiThCh10,   /**< Address : 157, bit:  10, size: 10  **/
      DCDCHiThCh11,   /**< Address : 157, bit:  20, size: 10  **/

      DCDCHiThCh12,   /**< Address : 158, bit:  0, size: 10  **/
      DCDCHiThCh13,   /**< Address : 158, bit:  10, size: 10  **/
      DCDCHiThCh14,   /**< Address : 158, bit:  20, size: 10  **/

      DCDCHiThCh15,   /**< Address : 159, bit:  0, size: 10  **/

      WRNLoThCh0,   /**< Address : 160, bit:  0, size: 10  **/
      WRNLoThCh1,   /**< Address : 160, bit:  10, size: 10  **/
      WRNLoThCh2,   /**< Address : 160, bit:  20, size: 10  **/

      WRNLoThCh3,   /**< Address : 161, bit:  0, size: 10  **/
      WRNLoThCh4,   /**< Address : 161, bit:  10, size: 10  **/
      WRNLoThCh5,   /**< Address : 161, bit:  20, size: 10  **/

      WRNLoThCh6,   /**< Address : 162, bit:  0, size: 10  **/
      WRNLoThCh7,   /**< Address : 162, bit:  10, size: 10  **/
      WRNLoThCh8,   /**< Address : 162, bit:  20, size: 10  **/

      WRNLoThCh9,   /**< Address : 163, bit:  0, size: 10  **/
      WRNLoThCh10,   /**< Address : 163, bit:  10, size: 10  **/
      WRNLoThCh11,   /**< Address : 163, bit:  20, size: 10  **/

      WRNLoThCh12,   /**< Address : 164, bit:  0, size: 10  **/
      WRNLoThCh13,   /**< Address : 164, bit:  10, size: 10  **/
      WRNLoThCh14,   /**< Address : 164, bit:  20, size: 10  **/
      WRNLoThCh15,   /**< Address : 165, bit:  0, size: 10  **/
      WRNHiThCh0,   /**< Address : 166, bit:  0, size: 10  **/
      WRNHiThCh1,   /**< Address : 166, bit:  10, size: 10  **/
      WRNHiThCh2,   /**< Address : 166, bit:  20, size: 10  **/
      WRNHiThCh3,   /**< Address : 167, bit:  0, size: 10  **/
      WRNHiThCh4,   /**< Address : 167, bit:  10, size: 10  **/
      WRNHiThCh5,   /**< Address : 167, bit:  20, size: 10  **/
      WRNHiThCh6,   /**< Address : 168, bit:  0, size: 10  **/
      WRNHiThCh7,   /**< Address : 168, bit:  10, size: 10  **/
      WRNHiThCh8,   /**< Address : 168, bit:  20, size: 10  **/
      WRNHiThCh9,   /**< Address : 169, bit:  0, size: 10  **/
      WRNHiThCh10,   /**< Address : 169, bit:  10, size: 10  **/
      WRNHiThCh11,   /**< Address : 169, bit:  20, size: 10  **/
      WRNHiThCh12,   /**< Address : 170, bit:  0, size: 10  **/
      WRNHiThCh13,   /**< Address : 170, bit:  10, size: 10  **/
      WRNHiThCh14,   /**< Address : 170, bit:  20, size: 10  **/
      WRNHiThCh15,   /**< Address : 171, bit:  0, size: 10  **/
    };

    /**
     * Create a vector of Register objects and a map of SubRegister pointers.
     * Initialize the ID to 0
     * @brief Create an AMACv2 object with default values for the registers
     */
    AMACv2();

    /**
     * Delete the pointers in the SubRegister map
     */
    ~AMACv2();

  };

}

#endif 
