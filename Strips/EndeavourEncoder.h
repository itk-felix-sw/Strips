#ifndef STRIPS_ENDEAVOURENCODER_H
#define STRIPS_ENDEAVOURENCODER_H

#include "Strips/Endeavour.h"
#include "Strips/EndeavourRead.h"
#include "Strips/EndeavourWrite.h"
#include "Strips/EndeavourNext.h"
#include "Strips/EndeavourSetid.h"

#include <cstdint>
#include <vector>

namespace Strips{

/**
 * This class is designed to decode a byte stream (pointer of bytes)
 * into a vector of Strips specific data packets (Packet), and
 * encode a byte stream from packets through the Decoder::Decode and Decoder::Encode.
 *
 * Packets are added to a vector of packets with Decoder::AddPacket.
 * Once added will be owned by the Decoder and should not be deleted by the user. 
 * The bytestream is compiled on request with Decoder::Encode.
 * The order of the packets in the byte stream is preserved.
 *
 * The byte stream is accessible through Decoder::GetBytes.
 * The resulting pointer is owned by the Decoder, and should not be deleted by the user.
 * Similarly, a byte stream can be decoded by the Decoder::SetBytes.
 *
 * The packets are available from Decoder::GetPackets.
 *
 * @verbatim
 
   Decoder decoder;
   decoder.AddPacket(new TransparentPacket());
   decoder.AddPacket(new ABCRegisterPacket());
   decoder.AddPacket(new HCCRegisterPacket());
 
   //Encode into bytes
   decoder.Encode();
   uint8_t * bytes = decoder.GetBytes();
   uin32_t length = decoder.GetLength();

   //Decode into commands
   decoder.SetBytes(bytes, length);
   decoder.Decode();
   vector<Packet*> packets = decoder.GetPackets();

   @endverbatim
 *
 * @brief Strips data Packet decoder/encoder
 * @author Carlos.Solans@cern.ch
 * @date August 2022
 **/

class EndeavourEncoder{
    
 public:

  /**
   * Initialize the Encoder
   **/
  EndeavourEncoder();
  
  /**
   * Delete the commands in memory
   **/
  ~EndeavourEncoder();
  
  /**
   * Clear the byte array and the Command list
   **/
  void Clear();

  /**
   * Clear the byte array
   **/
  void ClearBytes();

  /**
   * Clear the frame list by deleting each
   * object in the list.
  **/
  void ClearCommands();

  /**
   * Add bytes to the already existing byte array
   * @param bytes byte array
   * @param pos starting index of the byte array
   * @param len number of bytes to add
   **/
  void AddBytes(uint8_t *bytes, uint32_t pos, uint32_t len);

  /**
   * Replace the bytes of the byte array.
   * @param bytes byte array
   * @param len number of bytes to add
   * @param reversed add the bytes from the last to the first
   **/
  void SetBytes(uint8_t *bytes, uint32_t len, bool reversed=false);
  
  /**
   * Add a Command to the end of the list of commands
   * @param cmd the Command to add to the list
   **/
  void AddCommand(Endeavour *cmd);
  
  /**
   * Get the list of commands
   * @return vector the list of Command pointers
   **/
  std::vector<Endeavour*> & GetCommands();

  /**
   * Get a string representation of the bytes. 
   * @return a string in hexadecimal
   **/
  std::string GetByteString();

  /**
   * Fill in the bytes in a byte array pointer. 
   * The size of the pointer is also returned by reference.
   * Bytes can be returned in from first to last (regular), or from last to first (reversed)
   * @param bytes byte array to be filled with the bytes
   * @param length number of bytes to add. Will be updated by the method.
   * @param reversed add the bytes from the last to the first
   **/
  void GetBytes(uint8_t * bytes, uint32_t& length, bool reversed=false);
  
  /**
   * Get the byte array pointer that cannot be deleted by the user. 
   * This can be used to create a message for the communication layer.
   * The array can be returned in reverse order (last byte first), in this case
   * the byte array is internally copied to a second array increasing the memory allocation.
   * Each time this method is requested with the reveresed flag
   * the memory is copied to the reversed byte array.
   * @param reversed add the bytes from the last to the first
   * @return the byte array as a pointer
   **/
  uint8_t * GetBytes(bool reversed=false);
  
  /**
   * @return the size of the byte array
   **/
  uint32_t GetLength();

  /**
   * Encode the Command list into a byte array
   **/
  void Encode();

  /**
   * Decode the byte array into Commands
   * @param verbose enable the verbose flag
   **/
  void Decode(bool verbose=false);

 private:
  
  std::vector<Endeavour*> m_cmds;
  std::vector<uint8_t> m_bytes;
  std::vector<uint8_t> m_rbytes;
  uint32_t m_length;
  uint32_t m_mode;
  uint32_t m_size;
  
  EndeavourRead * m_read;
  EndeavourNext * m_next;
  EndeavourWrite * m_write;
  EndeavourSetid * m_setid;

};

}

#endif
