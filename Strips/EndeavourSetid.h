#ifndef STRIPS_ENDEAVOURSETID_H
#define STRIPS_ENDEAVOURSETID_H

#include <Strips/Endeavour.h>

namespace Strips{

  /**
   * This class represents a 56-bit Endeavour::SETID command for the AMAC.
   *
   * The EndeavourSetid only works after a power-on reset, when AMAC does not yet know its communications ID.
   * In this initial state, SETID is the only command AMAC will process. 
   * Once a given s communications ID is set, it cannot be changed until the next power-on reset.
   * Subsequent SETID commands are ignored by this AMAC.
   *
   * The format is the following:
   *
   * | Bit     |  0 |  3 |  4 |  7 |  8 | 10 | 11 | 15 | 16 | 19 | 20 | 23 | 24 | 31 | 32 | 39 | 40 | 42 | 43 | 47 | 48 | 55 | 
   * | ------  | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
   * | Byte    |  0             ||||  1             ||||  2             ||||  3     ||  4     ||  5     ||  6     ||  7     || 
   * | Size    |  3     ||  5     ||  3     || 5      || 4      || 20                     ||||||  3     ||  4     ||  8     ||
   * | Desc    | 0b110  ||0b11111 || 0x111  || new ID || 0x1111 || efuse ID               |||||| 0x111  || idpads || CRC    ||
   *
   *
   * To match the given AMAC, two possible conditions are allowed:
   *  - either idpads is 0b11111 and efuseid matches the 20-bit identifier programmed into the AMAC
   *  - or the efuseid is 0b11111111111111111111 and idpads matches the values wired to the padID pads.
   *
   * @brief Endeavour::SETID command for the AMAC
   * @author Carlos.Solans@cern.ch
   * @date August 2022
   **/

  class EndeavourSetid: public Endeavour{

  public:

    /**
     * Broadcast Efuse ID
     **/
    static const uint32_t EFUSEID_BROADCAST = 0xFFFFF;
    
    /**
     * Broadcast Pad ID
     **/
    static const uint8_t PADID_BROADCAST = 0x1F;

    /**
     * Create an empty EndeavourSetid
     **/
    EndeavourSetid();
    
    /**
     * Create a EndeavourSetid with the given parameters
     * @param newid the new ID to write in the command
     * @param efuseid the new ID to write in the command
     * @param padid the new ID to write in the command
     **/
    EndeavourSetid(uint8_t newid, uint32_t efuseid, uint8_t padid);

    /**
     * Copy the EndeavourSetid command
     * @param copy The pointer to the object to copy
     **/
    EndeavourSetid(EndeavourSetid * copy);

    /**
     * Delete the EndeavourSetid
     **/
    ~EndeavourSetid();

    /**
     * Set the new ID of the EndeavourSetid
     * @param newid The new ID of the EndeavourSetid
     **/
    void SetNewID(uint8_t newid);

    /**
     * Get the ID of the EndeavourSetid
     * @return the ID of the EndeavourSetid
     **/
    uint8_t GetNewID();

    /**
     * Set the efuse ID of the EndeavourSetid
     * @param efuseid the efuse ID of the EndeavourSetid
     **/
    void SetEfuseID(uint32_t efuseid);

    /**
     * Get the efuse ID of the EndeavourSetid
     * @return efuseid the efuse ID of the EndeavourSetid
     **/
    uint32_t GetEfuseID();

    /**
     * Set the pad ID of the EndeavourSetid
     * @param padid the pad ID of the EndeavourSetid
     **/
    void SetPadID(uint8_t padid);

    /**
     * Get the efuse ID of the EndeavourSetid
     * @return efuseid the efuse ID of the EndeavourSetid
     **/
    uint8_t GetPadID();

    /**
     * Return a human readable representation of the EndeavourSetid
     **/
    std::string ToString();

    /**
     * extract command from the bytes
     * @param bytes the byte array
     * @param maxlen the maximum number of bytes than can be read
     * @return the number of bytes processed
     **/
    uint32_t Unpack(uint8_t * bytes, uint32_t maxlen);
      
    /**
     * set command to bytes
     * @param bytes the byte array
     * @return the number of bytes processed
     **/
    uint32_t Pack(uint8_t * bytes);
      
  protected:

    /**
     * Can be set with EndeavourSetid::SetNewID and read with EndeavourSetid::GetNewID.
     * @brief The 4-bit new ID of the command. 
     **/
    uint8_t m_newid;

    /**
     * Can be set with EndeavourSetid::SetEfuseID and read with EndeavourSetid::GetEfuseID.
     * @brief The 20-bit efuse ID of the command. 
     **/
    uint32_t m_efuseid;

    /**
     * Can be set with EndeavourSetid::SetPadID and read with EndeavourSetid::GetPadID.
     * @brief The 4-bit pad ID of the command. 
     **/
    uint8_t m_padid;

  };

}

#endif
