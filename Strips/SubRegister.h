#ifndef STRIPS_SUBREGISTER_H
#define STRIPS_SUBREGISTER_H

#include <cstdint>
#include <vector>
#include <string>

#include "Strips/Register.h"

namespace Strips{

  /**
   * A SubRegister is a quick access to a sub-set of bits in a Register
   *
   * @brief Strips SubRegister
   * @author Carlos.Solans@cern.ch
   * @date June 2022
   **/
  class SubRegister{
    
  public:
    
    /**
     * Construct a Field given a pointer to the Configuration register.
     * @param data pointer to the Configuration register
     * @param start First bit of the data
     * @param len Length of the Field in bits
     * @param defval Default value for the Field
     * @param reversed Flip the bit order of the Field if true
     **/
    SubRegister(Register * data, uint32_t start, uint32_t len, uint32_t defval=0, bool reversed=false);

    /**
     * Empty Destructor
     **/
    ~SubRegister();

    /**
     * Set the value of the Field. Extra bits will be truncated.
     * @param value New value of the Field
     **/
    void SetValue(uint32_t value);

    /**
     * Get the value of the Field
     * @return The value of the Field
     **/
    uint32_t GetValue();
  
    /**
     * Set if the SubRegister is given.
     * Used by higher level classes to track if a SubRegister was provided or not
     * to build the same array of registers
     * @param given true if given
     **/
    void IsGiven(bool given);

    /**
     * Check if the SubRegister is given
     * @return true if the SubRegister has the given flag enabled.
     **/
    bool IsGiven();

    /**
     * Get the value as a hex string
     * @return the hex string
     **/
    std::string GetValueAsString();

    /**
     * Set the value from a hex string
     * @param value a hex string
     **/
    void SetValueFromString(std::string value);

  private:

    /**
     * Pointer to the Register holding the actual data
     **/
    Register * m_data;
    
    /**
     * Start bit of the SubRegister
     **/
    uint32_t m_start;
    
    /**
     * Length of the SubRegister in bits
     **/
    uint32_t m_len;
    
    /**
     * Default value of the SubRegister
     **/
    uint32_t m_defval;
    
    /**
     * Mask containing the bits that belong to the SubRegister
     **/
    uint32_t m_mask;
    
    /**
     * Mask containing the bits and the position in the Register
     **/
    uint32_t m_smask;
    
    /**
     * Reverse flag. Used to write the value in reverse order of bits
     **/
    bool m_reversed;
    
    /**
     * Given flag. Used to identify if the SubRegister value was provided externally
     **/
    bool m_given;

    /**
     * List of masks in number of bits 
     **/
    static std::vector<uint32_t> m_masks;
    
    /**
     * Reverse the bit value of an integer given its size
     * @param value the integer value to reverse
     * @param sz the size of the integer in bits
     **/
    uint32_t Reverse(uint32_t value, uint32_t sz);

  };

}

#endif
