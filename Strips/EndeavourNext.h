#ifndef STRIPS_ENDEAVOURNEXT_H
#define STRIPS_ENDEAVOURNEXT_H

#include <Strips/Endeavour.h>

namespace Strips{

  /**
   * This class represents a 16-bit Endeavour::NEXT command for the AMAC
   *
   * | Bit     |  0 |  3 |  4 |  7 |
   * | ------  | -- | -- | -- | -- |
   * | Byte    |  0             ||||
   * | Size    |  3     ||  5     ||
   * | Desc    | 0b100  || AMACID ||
   *
   *
   * @brief Endeavour::NEXT command for the AMAC
   * @author Carlos.Solans@cern.ch
   * @date August 2022
   **/

  class EndeavourNext: public Endeavour{

  public:

    /**
     * Create an empty EndeavourNext
     **/
    EndeavourNext();
    
    /**
     * Create a EndeavourNext with the given parameters
     * @param id the ID of the EndeavourNext
     **/
    EndeavourNext(uint8_t id);

    /**
     * Copy the EndeavourNext command
     * @param copy The pointer to the object to copy
     **/
    EndeavourNext(EndeavourNext * copy);

    /**
     * Delete the EndeavourNext
     **/
    ~EndeavourNext();

    /**
     * Set the ID of the EndeavourNext
     * @param id The ID of the EndeavourNext
     **/
    void SetID(uint8_t id);

    /**
     * Get the ID of the EndeavourNext
     * @return the ID of the EndeavourNext
     **/
    uint8_t GetID();

    /**
     * Return a human readable representation of the EndeavourNext
     **/
    std::string ToString();

    /**
     * extract command from the bytes
     * @param bytes the byte array
     * @param maxlen the maximum number of bytes than can be read
     * @return the number of bytes processed
     **/
    uint32_t Unpack(uint8_t * bytes, uint32_t maxlen);
      
    /**
     * set command to bytes
     * @param bytes the byte array
     * @return the number of bytes processed
     **/
    uint32_t Pack(uint8_t * bytes);
      
  protected:

    /**
     * The ID to be used in the command.
     * Can be set with EndeavourNext::SetID and retrieved with EndeavourNext::GetID
     **/
    uint8_t m_id;
    
  };

}

#endif
