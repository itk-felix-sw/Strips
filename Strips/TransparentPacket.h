#ifndef STRIPS_TRANSPARENTPACKET_H
#define STRIPS_TRANSPARENTPACKET_H

#include "Strips/Packet.h"

#include <cstdint>
#include <string>
#include <vector>

namespace Strips{

  /**
   * This class represents a Transparent Packet or a Full Transparent packet.
   * The format of the Transparent Packet is the following:
   *
   * | Bit     | 0 | 3 |  4 |  7          |  8 |  15 | 16 | 23 | 24 | 31 | 32 | 39 | 40 | 47 | 48 | 56 | 67 | 71 | 72 | 79 |
   * | ------  | - | - | -- | ----------  | -- | --  | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
   * | Byte    | 0                     ||||  1      ||  2     || 3      ||  4     ||  5     ||  6     || 7      ||  8     ||
   * | Size    | 4    || 4               || 64                                                              ||||||||||||||||
   * | Desc    | 7    || HCC input chan  || Payload                                                         ||||||||||||||||
   *
   * The header will be the 4-bit TYP code plus the 4-bit Input Channel number.
   * Following this header will be the full, unmodified 64-bit ABCStar packet, with the framing bits removed.
   *
   * The format of the Full Transparent Packet is the following:
   *
   * | Bit     | 0 | 3 |  4 |  7          |  8 |  15 | 16 | 23 | ... | 504 | 511 | 512 | 519 |
   * | ------  | - | - | -- | ----------  | -- | --  | -- | -- | --- | --- | --- | --- | --- |
   * | Byte    | 0                     ||||  1      ||  2     || ... |  63      ||  64      ||
   * | Size    | 4    || 4               || 512                                      |||||||||
   * | Desc    | 11   || HCC input chan  || Payload                                  |||||||||
   *
   * The header will be the 4-bit TYP code plus the 4-bit Input Channel number.
   * This header will be followed by a 512-bit chunk of data.
   *
   * @author Carlos.Solans@cern.ch
   * @date August 2022
   **/

  class TransparentPacket: public Packet{
 
  public:

    /**
     * Create and empty TransparentPacket of type Packet::TYP_ABC_TRANSP
     **/
    TransparentPacket();

    /**
     * Create a Transparent Packet with the given parameters
     * @param typ type of Transparent Packet, valid values are Packet::TYP_ABC_TRANSP and Packet::TYP_ABC_FULL 
     * @param channel input channel 
     * @param payload the payload of the packet (8 bytes if type is TYP_ABC_TRANSP, or 64 if it is TYP_ABC_FULL)
    **/
    TransparentPacket(uint32_t typ, uint32_t channel, std::vector<uint8_t> payload);

    /**
     * Create a Transparent Packet with the given parameters
     * @param typ type of Transparent Packet, valid values are Packet::TYP_ABC_TRANSP and Packet::TYP_ABC_FULL 
     * @param channel input channel 
     * @param payload the payload of the packet 
     * @param maxlen  the maximum number of bytes than can be read (optional) 
    **/
    TransparentPacket(uint32_t typ, uint32_t channel, uint8_t * payload, uint32_t maxlen=64);

    /**
     * Copy the TransparentPacket
     * @param copy The pointer to the object to copy
     **/
    TransparentPacket(TransparentPacket *copy);

    /**
     * Delete the TransparentPacket
     **/
    ~TransparentPacket();

    /**
     * Return a human readable representation of the data
     **/
    std::string ToString();
      
    /**
     * Set the input channel
     * @param channel the ABC channel number in the HCC
     **/
    void SetChannel(uint32_t channel);

    /**
     * Get the input channel
     * @return The ABC channel number in the HCC
     **/
    uint32_t GetChannel();

    /**
     * Copy the payload of the transparent packet from a vector of bytes.
     * 8 bytes will be copied for transparent packets (Packet::TYP_ABC_TRANSP)
     * and 64 bytes for full transparent packets (Packet::TYP_ABC_FULL).
     * @param payload reference to a vector of bytes containing the payload
     **/
    void SetPayload(std::vector<uint8_t> & payload);

    /**
     * Copy the payload of the transparent packet from a pointer of bytes.
     * 8 bytes will be copied for transparent packets (Packet::TYP_ABC_TRANSP)
     * and 64 bytes for full transparent packets (Packet::TYP_ABC_FULL).
     * @param payload pointer of bytes containing the payload
     * @param maxlen the maximum number of bytes than can be read (optional)
     **/
    void SetPayload(uint8_t * payload, uint32_t maxlen=64);

    /**
     * Get the register channel
     * @return channel the ABC channel number in the HCC
     **/
    std::vector<uint8_t> * GetPayload();
    
    /**
     * extract command from the bytes
     * @param bytes the byte array
     * @param maxlen the maximum number of bytes than can be read
     * @return the number of bytes processed
     **/
    uint32_t Unpack (uint8_t * bytes, uint32_t maxlen);

    /**
     * set command to bytes
     * @param bytes the byte array
     * @return the number of bytes processed
     **/
     uint32_t Pack(uint8_t * bytes);

  private:

    uint32_t m_channel = 0;
    std::vector<uint8_t> m_payload;

  };

}

#endif
