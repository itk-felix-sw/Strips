#ifndef STRIPS_L0A_H
#define STRIPS_L0A_H

#include <Strips/Command.h>

namespace Strips{

  /**
   * This class represents a L0A Command frame.
   * It is 16-bit long spanning 4 bunch crossings,
   * thus it contains 4 L0As through a 4-bit L0Mask,
   * a 7-bit L0Tag, and a BCR signal.
   * The format of a L0A command is the following:
   *
   * | Bit     |  0 |  7 |  8 | 15  |
   * | ------  | -- | -- | -- | --- |
   * | Byte    |  0     ||  1      ||
   * | Size    |  7     || 7       ||
   * | Payload | 0 to 5  | 6 to 11 ||
   *
   *
   * The payload is encoded using the 6b8b protocol with the following meaning:
   *
   * | Bit     |  0  |  1 |  4 |  5 |  6 | 11 |
   * | ------  | --- | -- | -- | -- | -- | -- |
   * | Payload |  0              ||||  1     ||
   * | Size    |  1  |  4     || 7          |||
   * | Desc    | BCR | L0Mask || L0Tag      |||
   *
   *
   * @brief L0A LCB Frame for Strips
   * @author Carlos.Solans@cern.ch
   * @date August 2022
   **/

  class L0A: public Command{

  public:

    /**
     * Create an empty L0A with one trigger
     * otherwise the frame will be confused with a register write
     **/
    L0A();
    
    /**
     * Delete the L0A
     **/
    ~L0A();

    /**
     * Create a L0A with the given parameters
     * @param bcr enable the BCR
     * @param mask the L0A 4-bit mask (up to 4 BCs)
     * @param tag the L0Tag to use
     **/
    L0A(bool bcr, uint8_t mask, uint8_t tag);

    /**
      * Create a L0A with the given parameters
      * @param bcr enable the BCR
      * @param bc1 trigger on the first BC
      * @param bc2 trigger on the first BC
      * @param bc3 trigger on the first BC
      * @param bc4 trigger on the first BC
      * @param tag the L0Tag to use
      **/
     L0A(bool bcr, bool bc1, bool bc2, bool bc3, bool bc4, uint8_t tag);

     /**
     * Copy the WriteRegister
     * @param copy The pointer to the object to copy
     **/
    L0A(L0A * copy);

    /**
     * Set the BCR
     * @param enable enable the BCR
     **/
    void SetBCR(bool enable);

    /**
     * Get the BCR
     * @return true if the BCR is enabled
     **/
    bool GetBCR();

    /**
     * Set the L0A mask, bits at one represent a trigger
     * @param mask the L0A mask
     **/
    void SetL0A(uint8_t mask);

    /**
     * Set the L0A mask
     * @param bc1 enable the L0A on the first BC
     * @param bc2 enable the L0A on the second BC
     * @param bc3 enable the L0A on the third BC
     * @param bc4 enable the L0A on the fourth BC
     **/
    void SetL0A(bool bc1, bool bc2, bool bc3, bool bc4);

    /**
     * Set the L0A mask
     * @param bc enable the L0A on the first BC
     * @param enable enable the L0A on the second BC
     **/
    void SetL0A(uint32_t bc, bool enable);

    /**
     * Get the L0A mask, bits at one represent a trigger
     * @return the L0A mask
     **/
    uint32_t GetL0A();

    /**
     * Check if a given BC is enabled in the L0A mask
     * @param bc the BC in which to trigger
     * @return true if the BC is enabled
     **/
    bool GetL0A(uint32_t bc);

    /**
     * Set the L0Tag
     * @param tag the L0tag
     **/
    void SetL0Tag(uint32_t tag);

    /**
     * Get the L0Tag
     * @return the L0Tag
     **/
    uint32_t GetL0Tag();
    
    /**
     * Return a human readable representation of the data
     **/
    std::string ToString();

    /**
     * extract command from the bytes
     * @param bytes the byte array
     * @param maxlen the maximum number of bytes than can be read
     * @return the number of bytes processed
     **/
    uint32_t Unpack (uint8_t * bytes, uint32_t maxlen);
      
    /**
     * set command to bytes
     * @param bytes the byte array
     * @return the number of bytes processed
     **/
    uint32_t Pack(uint8_t * bytes);
      
  protected:

    bool m_bcr;
    uint8_t m_l0a;
    uint8_t m_tag;

  };

}

#endif
