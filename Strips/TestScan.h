#ifndef STRIPS_TESTSCAN_H
#define STRIPS_TESTSCAN_H

#include <Strips/Handler.h>
#include "TH2I.h"

namespace Strips{

  /**
   * This is a test scan. It is used to debug the code.
   *
   * @brief Strips TestScan
   * @author Carlos.Solans@cern.ch
   * @date August 2022
   */

  class TestScan: public Handler{

  public:

    /**
     * Create the scan
     */
    TestScan();

    /**
     * Delete the scan
     */
    virtual ~TestScan();

    /**
     * Set the value of the variables for this scan, create histograms.
     */
    virtual void PreRun();

    /**
     * Loop the matrix in groups of pixels and core columns.
     * Trigger few events and collect the hits per trigger.
     * An occupancy histogram is filled.
     */
    virtual void Run();

    /**
     * Write and delete histograms.
     */
    virtual void Analysis();

  private:
    uint32_t m_ntrigs;
    std::map<std::string,TH2I*> m_occ;
  };

}

#endif
