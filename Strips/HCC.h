#ifndef STRIPS_HCC_H
#define STRIPS_HCC_H

#include <Strips/Configuration.h>
#include <Strips/Register.h>
#include <Strips/SubRegister.h>
#include <vector>
#include <map>
#include <string>

namespace Strips{

  /**
   * The HCC represents the configuration of the HCC chip.
   * It can be accessed via the registers or the sub-registers.
   * 
   * @verbatim
     HCC * hcc = new HCC();
     uint32_t vv = hcc->GetRegister(HCC::Cfg1)->GetValue();
     hcc->GetSubRegister(HCC::LCBOUTEN)->SetValue(2);
     for(auto rr: hcc->GetUpdatedRegisters()){ 
       cout << "Register address: 0x" << hex << rr.first 
            << ", value:" << rr.second << dec << endl;
     }
     delete hcc;
   * @endverbatim  
   * @author Andrea.Gabrielli@cern.ch
   * @author Carlos.Solans@cern.ch
   * @date August 2022 
   **/
  class HCC: public Configuration{
  public:
    
    /**
     * The number of registers available in the HCC
     **/
    static const uint32_t NUM_REGISTERS=49;

    /**
     * Definition of the registers available in the HCC
     **/
    enum HCCRegister{
      SEU1=0, SEU2=1, SEU3=2, FrameRaw=3, LCBerr=4, ADCStatus=5, Status=6, HPR=15, Pulse=16, Addressing=17, Delay1=32, Delay2=33, Delay3=34, 
      PLL1=35, PLL2=36, PLL3=37, DRV1=38, DRV2=39, ICenable=40, OPmode=41, OPmodeC=42, Cfg1=43, Cfg2=44, ExtRst=45, ExtRstC=46, ErrCfg=47, ADCcfg=48
    };
    
    /**
     * Definition of the sub-registers available in the HCC
     **/
    enum HCCSubRegister{
      STOPHPR=1, TESTHPR, CFD_BC_FINEDELAY, CFD_BC_COARSEDELAY, CFD_PRLP_FINEDELAY, CFD_PRLP_COARSEDELAY, HFD_LCBA_FINEDELAY, FD_RCLK_FINEDELAY, 
      LCBA_DELAY160, FD_DATAIN0_FINEDELAY, FD_DATAIN1_FINEDELAY, FD_DATAIN2_FINEDELAY, FD_DATAIN3_FINEDELAY, FD_DATAIN4_FINEDELAY, FD_DATAIN5_FINEDELAY, 
      FD_DATAIN6_FINEDELAY, FD_DATAIN7_FINEDELAY, FD_DATAIN8_FINEDELAY, FD_DATAIN9_FINEDELAY, FD_DATAIN10_FINEDELAY, EPLLICP, EPLLCAP, EPLLRES, 
      EPLLREFFREQ, EPLLENABLEPHASE, EPLLPHASE320A, EPLLPHASE320B, EPLLPHASE320C, EPLLPHASE160A, EPLLPHASE160B, EPLLPHASE160C, STVCLKOUTCUR, STVCLKOUTEN, 
      LCBOUTCUR, LCBOUTEN, R3L1OUTCUR, R3L1OUTEN, BCHYBCUR, BCHYBEN, LCBAHYBCUR, LCBAHYBEN, PRLPHYBCUR, PRLPHYBEN, RCLKHYBCUR, RCLKHYBEN, DATA1CUR, 
      DATA1ENPRE, DATA1ENABLE, DATA1TERM, DATACLKCUR, DATACLKENPRE, DATACLKENABLE, ICENABLE, ICTRANSSEL, TRIGMODE, ROSPEED, OPMODE, MAXNPACKETS, 
      ENCODECNTL, ENCODE8B10B, PRBSMODE, TRIGMODEC, ROSPEEDC, OPMODEC, MAXNPACKETSC, ENCODECNTLC, ENCODE8B10BC, PRBSMODEC, BGSETTING, MASKHPR, GPO0, GPO1, 
      EFUSEPROGBIT, BCIDRSTDELAY, BCMMSQUELCH, ABCRESETB, AMACSSSH, ABCRESETBC, AMACSSSHC, LCBERRCOUNTTHR, R3L1ERRCOUNTTHR, AMENABLE, AMCALIB, AMSW0, AMSW1, 
      AMSW2, AMSW60, AMSW80, AMSW100, ANASET, THERMOFFSET
    };

    /**
     * Create a vector of Register objects and a map of SubRegister pointers.
     * Initialize the HCC ID to 0
     * @brief Create an HCC object with default values for the registers
     */
    HCC();

    /**
     * Delete the pointers in the SubRegister map
     */
    virtual ~HCC();

    
  private:

  };

}

#endif 
