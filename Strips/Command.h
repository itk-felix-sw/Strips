#ifndef STRIPS_COMMAND_H
#define STRIPS_COMMAND_H

#include <cstdint>
#include <string>
#include <map>

namespace Strips{

  class Command{

  public:

    /**
     * Create and empty Command
     **/
    Command();

    /**
     * Delete the data packet
     **/
    virtual ~Command();

    /**
     * command types
     */
    static const uint32_t NONE = 0;
    static const uint32_t L0A  = 1;
    static const uint32_t FAST = 2;
    static const uint32_t READ = 3;
    static const uint32_t WRITE = 4;
    static const uint32_t IDLE = 5;

    static const std::map<uint32_t, std::string> typeNames;

    /**
     * Return a human readable representation of the data
     **/
    virtual std::string ToString()=0;

    /**
     * extract command from the bytes
     * @param bytes the byte array
     * @param maxlen the maximum number of bytes than can be read
     * @return the number of bytes processed
     **/
    virtual uint32_t Unpack (uint8_t * bytes, uint32_t maxlen)=0;
      
    /**
     * set command to bytes
     * @param bytes the byte array
     * @return the number of bytes processed
     **/
    virtual uint32_t Pack(uint8_t * bytes)=0;
      
    /**
     * get type of the data
     **/
    virtual uint32_t GetType();

    /**
     * set the type of the data
     **/
    virtual void SetType(uint32_t typ);

  protected:

    uint32_t m_type;

  };


}

#endif
