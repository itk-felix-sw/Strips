#ifndef STRIPS_CLUSTER_H
#define STRIPS_CLUSTER_H

#include <vector>
#include <string>
#include <cstdint>

namespace Strips{

  /**
   * Representation of a Cluster contains L0ID and BCID
   * that can be extracted from the Strips::ABCLPPRPacket header,
   * and the strip numbers that belong to the cluster 
   * extracted from the raw cluster information.
   *  
   * @brief Strips Cluster
   * @author Andrea.Gabrielli@cern.ch
   * @author Carlos.Solans@cern.ch
   * @date August 2022
   **/
  class Cluster{
 
    public:
    
    /**
     * Create a new Cluster with all values at zero
     */
    Cluster();
    
    /**
     * Delete the Cluster
     */
    ~Cluster();

    /**
     * Clone the Cluster
     * @return a Cluster pointer that is a copy of this one
     */
    Cluster * Clone();
    
    /**
     * Update the L0ID and the BCID of the Cluster in one go
     * @param l0id The L0ID 
     * @param bcid_parity The BCID parity
     * @param bcid The BCID counter
     */    
    void Update(uint32_t l0id, bool bcid_parity, uint32_t bcid);

    /**
     * Parse the raw cluster information into channel, address, and pattern 
     * @param rawcls the raw cluster information
     **/
    void Parse(uint16_t rawcls);
        
    /**
     * Set the L0ID of the Cluster
     * @param l0id the L0ID of the Cluster
     **/
    void SetL0ID(uint32_t l0id);

    /**
     * Get the L0ID of the Cluster
     * @return the L0ID of the Cluster
     **/
    uint32_t GetL0ID();
    
    /**
     * Set the BCID of the Cluster
     * @param bcid the BCID of the Cluster
     **/
    void SetBCID(uint32_t bcid);

    /**
     * Get the BCID of the Cluster
     * @return the BCID of the Cluster
     **/
    uint32_t GetBCID();
    
    /**
     * Set the BCID parity of the Cluster
     * @param bcid_parity the BCID parity of the Cluster
     **/
    void SetBCIDParity(bool bcid_parity);

    /**
     * Get the BCID parity of the Cluster
     * @return the BCID parity of the Cluster
     **/
    bool GetBCIDParity();

    /**
     * Set the channel number
     * @param channel the channel number 
     **/
    void SetChannel(uint32_t channel);
    
    /**
     * Get the channel number
     * @return the channel number
     **/
    uint32_t GetChannel();
    
    /**
     * Set the address
     * @param address the address
     **/
    void SetAddress(uint32_t address);
    
    /**
     * Get the address
     * @return the address
     **/
    uint32_t GetAddress();
    
    /**
     * Set the raw pattern of the Cluster
     * @param pattern the raw pattern of the Cluster
     **/
    void SetPattern(uint32_t pattern);
    
    /**
     * Get the raw pattern of the Cluster
     * @return the raw pattern of the Cluster
     **/
    uint32_t GetPattern();
        
    /**
     * Get a vector of the strip numbers belonging to this Cluster
     * It always returns the address, followed by the addresses 
     * decoded from the pattern.
     **/
    std::vector<uint32_t> GetStripNumbers();
    
    
    /**
     * Print Cluster info in a human readable format 
     * @return a string representation of this Cluster
     **/      
    std::string ToString();

  private:
      
    /**
     * BCID of the cluster
     **/
    uint32_t m_bcid;
    
    /**
     * L0ID of the cluster
     **/
    uint32_t m_l0id;
    
    /**
     *  BCID parity of the cluster
     **/
    bool m_bcid_parity;
    
    /**
     * ABC channel where the Cluster is located
     **/
    uint32_t m_channel;
    
    /**
     * Address of the Cluster in the ABC
     **/
    uint32_t m_address;
    
    /**
     * Pattern of the Cluster that indicates if the next, 
     * next-to-next, and next-to-next strip have been hit
     **/
    uint32_t m_pattern;
    
  };
}

#endif 
