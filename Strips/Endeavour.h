#ifndef STRIPS_ENDEAVOUR_H
#define STRIPS_ENDEAVOUR_H

#include <cstdint>
#include <string>
#include <map>

namespace Strips{

  class Endeavour{

  public:

    static const uint32_t NONE  = 0x00; /**< None type. Default by base class. **/
    static const uint32_t READ  = 0xA0; /**< Read type. Read a register from AMAC. **/
    static const uint32_t NEXT  = 0x80; /**< Read next type. Read the next register from AMAC. **/
    static const uint32_t WRITE = 0xE0; /**< Write type. Write a register to AMAC. **/
    static const uint32_t SETID = 0xC0; /**< Set ID type. Initialize the AMAC. **/

    static const std::map<uint32_t, std::string> typeNames; /**< Map of types to strings **/

    /**
     * Create and empty object
     **/
    Endeavour();

    /**
     * Delete the object
     **/
    virtual ~Endeavour();

    /**
     * get type of the data
     **/
    virtual uint32_t GetType();

    /**
     * set the type of the data
     **/
    virtual void SetType(uint32_t typ);

    /**
     * Return a human readable representation of the data
     **/
    virtual std::string ToString()=0;

    /**
     * extract command from the bytes
     * @param bytes the byte array
     * @param maxlen the maximum number of bytes than can be read
     * @return the number of bytes processed
     **/
    virtual uint32_t Unpack (uint8_t * bytes, uint32_t maxlen)=0;
      
    /**
     * set command to bytes
     * @param bytes the byte array
     * @return the number of bytes processed
     **/
    virtual uint32_t Pack(uint8_t * bytes)=0;
      
    /**
     * Simple checksum intended to prevent spurious writes.
     * Bit crc[0] is the XOR of message bits 0, 8, 16, 24, 32, and 40.
     * Bit i of the CRC is the XOR of message bits 8n + i for 0 n <= 5.
     * @brief Calculate the CRC of a set of bytes.
     *
     */
    uint8_t CRC(uint8_t * bytes);

  protected:

    uint32_t m_type;

  };


}

#endif
