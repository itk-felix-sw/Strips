#ifndef STRIPS_FRONTEND_H
#define STRIPS_FRONTEND_H

#include <Strips/HCC.h>
#include <Strips/ABC.h>
#include <Strips/Encoder.h>
#include <Strips/Decoder.h>
#include <Strips/Cluster.h>
#include <vector>
#include <cstdint>
#include <string>
#include <mutex>
#include <queue>

namespace Strips{

  
  class FrontEnd{

  public:
    
    FrontEnd();
    ~FrontEnd();
    void SetVerbose(bool enable);
    void SetName(std::string name);
    std::string GetName();
    void Reset();
    void Configure();
    void WriteHCCRegisters();
    void WriteABCRegisters();
    HCC * GetHCC();
    ABC * GetABC(uint32_t index);
    std::vector<ABC*> & GetABCs();
    void AddABC(ABC* abc=0);
    void ProcessCommands();
    uint8_t * GetBytes();
    uint32_t GetLength();
    void Clear();
    void SetActive(bool active);
    bool IsActive();

    Cluster * GetCluster();

    bool NextCluster();

    bool HasClusters();

    void ClearClusters();
 

    void HandleData(uint8_t * data, uint32_t maxlen);
    void Trigger(uint32_t latency=0, bool digital=false);


  private:

    bool m_verbose;
    bool m_active;
    HCC * m_hcc;
    std::vector<ABC *> m_abcs;
    Encoder * m_encoder;
    Decoder * m_decoder;
    std::string m_name;
    std::mutex m_mutex;
    std::deque<Cluster*> m_clusters;
  };

}

#endif
