#ifndef STRIPS_ENDEAVOURWRITE_H
#define STRIPS_ENDEAVOURWRITE_H

#include <Strips/Endeavour.h>

namespace Strips{

  /**
   * This class represents a 16-bit Endeavour::WRITE command for the AMAC
   *
   * | Bit     |  0 |  3 |  4 |  7 |  8 | 15 | 16 | 23 | 24 | 31 | 32 | 39 | 40 | 47 | 48 | 55 | 
   * | ------  | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
   * | Byte    |  0             ||||  1     ||  2     ||  3     ||  4     ||  5     ||  6     ||
   * | Size    |  3     ||  5     ||  8     || 32                             ||||||||  8     ||
   * | Desc    | 0b101  || AMACID || Address|| Data                           |||||||| CRC    ||
   *
   *
   * @brief Endeavour::WRITE command for the AMAC
   * @author Carlos.Solans@cern.ch
   * @date August 2022
   **/

  class EndeavourWrite: public Endeavour{

  public:

    /**
     * Create an empty EndeavourWrite
     **/
    EndeavourWrite();
    
    /**
     * Create a EndeavourWrite with the given parameters
     * @param id the ID to write in the command
     * @param address the address to write in the command
     * @param data the data to write in the command
     **/
    EndeavourWrite(uint8_t id, uint8_t address, uint32_t data);

    /**
     * Copy the EndeavourWrite command
     * @param copy The pointer to the object to copy
     **/
    EndeavourWrite(EndeavourWrite * copy);

    /**
     * Delete the EndeavourWrite
     **/
    ~EndeavourWrite();

    /**
     * Set the ID of the EndeavourWrite
     * @param id The ID of the EndeavourWrite
     **/
    void SetID(uint8_t id);

    /**
     * Get the ID of the EndeavourWrite
     * @return the ID of the EndeavourWrite
     **/
    uint8_t GetID();

    /**
     * Set the Address of the EndeavourWrite
     * @param address the address of the EndeavourWrite
     **/
    void SetAddress(uint8_t address);

    /**
     * Get the Address of the EndeavourWrite
     * @return the address of the EndeavourWrite
     **/
    uint8_t GetAddress();

    /**
     * Set the data of the EndeavourWrite
     * @param data the data of the EndeavourWrite
     **/
    void SetData(uint32_t data);

    /**
     * Get the Address of the EndeavourWrite
     * @return the data of the EndeavourWrite
     **/
    uint32_t GetData();

    /**
     * Return a human writeable representation of the EndeavourWrite
     **/
    std::string ToString();

    /**
     * extract command from the bytes
     * @param bytes the byte array
     * @param maxlen the maximum number of bytes than can be read
     * @return the number of bytes processed
     **/
    uint32_t Unpack(uint8_t * bytes, uint32_t maxlen);
      
    /**
     * set command to bytes
     * @param bytes the byte array
     * @return the number of bytes processed
     **/
    uint32_t Pack(uint8_t * bytes);
      
  protected:

    /**
     * Can be set with EndeavourWrite::SetID and read with EndeavourWrite::GetID.
     * @brief The 4-bit ID of the command. 
     **/
    uint8_t m_id;
    
    /**
     * Can be set with EndeavourWrite::SetAddress and read with EndeavourWrite::GetAddress.
     * @brief The 8-bit new ID of the command. 
     **/
    uint8_t m_address;

    /**
     * Can be set with EndeavourWrite::SetData and read with EndeavourWrite::GetData.
     * @brief The 32-bit new ID of the command. 
     **/
    uint32_t m_data;

  };

}

#endif
