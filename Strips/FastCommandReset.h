#ifndef STRIPS_FASTCOMMANDRESET_H
#define STRIPS_FASTCOMMANDRESET_H

#include <Strips/Command.h>
#include <map>

namespace Strips{

  /**
   * This class represents a 16-bit frame composed of a K3 symbol and a 8-bit payload that spans 4 BC and contains a fast commands.
   * The format of a FastCommandReset command is the following:
   *
   * | Bit     |  0 |  7 |  8 | 15  |
   * | ------  | -- | -- | -- | --- |
   * | Byte    |  0     ||  1      ||
   * | Size    |  8     ||  8      ||
   * | Desc    |  K3    || Payload ||
   *
   * The payload contains 2-bit BC selector indicating in which BC (First=0, Second=1, Third=2, Fourt=3)
   * the command is to be generated, and the 4-bit command to generate.
   * The payload is the following
   *
   * | Bit     | 0  | 1  | 2  | 5  |
   * | ------  | -- | -- | -- | -- |
   * | Payload |  1             ||||
   * | Size    | 2      ||  4     ||
   * | Desc    | BC     || Command||
   *
   *
   * @brief Fast Command and Reset LCB Frame for Strips
   * @author Andrea.Gabrielli@cern.ch
   * @date August 2022
   **/

  class FastCommandReset: public Command{

  public:

    static const uint32_t NONE = 0;
    static const uint32_t RESVD = 1;
    static const uint32_t LOGIC_RESET = 2;
    static const uint32_t ABC_REG_RESET = 3;
    static const uint32_t ABC_SEU_RESET = 4;
    static const uint32_t ABC_CAL_PULSE = 5;
    static const uint32_t ABC_DIGITAL_PULSE = 6;
    static const uint32_t ABC_HIT_COUNT_RESET = 7;
    static const uint32_t ABC_HIT_COUNT_START = 8;
    static const uint32_t ABC_HIT_COUNT_STOP = 9;
    static const uint32_t ABC_SLOW_COMMAND_RESET = 10;
    static const uint32_t HCC_STOP_PRLP = 11;
    static const uint32_t HCC_REG_RESET = 12;
    static const uint32_t HCC_SEU_RESET = 13;
    static const uint32_t HCC_PLL_RESET = 14;
    static const uint32_t HCC_START_PRLP = 15;

    static std::map<uint32_t,std::string> typeNames;

    /**
     * Create an empty FastCommandReset
     * otherwise the frame will be confused with a register write
     **/
    FastCommandReset();
    
    /**
     * Create a FastCommandReset with the given parameters
     * @param bc the BC where to execute the command
     * @param command the command to execute
     **/
    FastCommandReset(uint8_t bc, uint8_t command);

    /**
     * Copy the FastCommandReset
     * @param copy The pointer to the object to copy
     **/
    FastCommandReset(FastCommandReset * copy);

    /**
     * Delete the FastCommandReset
     **/
    ~FastCommandReset();

    /**
     * Set the BC from 0 to 3. Higher values will be ignored.
     * @param bc selector indicating in which BC the command or reset is to be generated
     **/
    void SetBC(uint8_t bc);

    /**
     * Get the BC
     * @return the value of the BC selector
     **/
    uint8_t GetBC();

    /**
     * Set the Command
     * @param command the command to execute
     **/
    void SetCommand(uint8_t command);

    /**
     * Get the Command
     * @return the command to execute
     **/
    uint8_t GetCommand();

    /**
     * Return a human readable representation of the data
     **/
    std::string ToString();

    /**
     * extract command from the bytes
     * @param bytes the byte array
     * @param maxlen the maximum number of bytes than can be read
     * @return the number of bytes processed
     **/

    uint32_t Unpack (uint8_t * bytes, uint32_t maxlen);
      
    /**
     * set command to bytes
     * @param bytes the byte array
     * @return the number of bytes processed
     **/
    uint32_t Pack(uint8_t * bytes);
      
  protected:

    uint8_t m_bc;
    uint8_t m_command;

  };

}

#endif
