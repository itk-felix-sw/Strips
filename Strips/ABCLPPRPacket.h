#ifndef STRIPS_ABCLPPRPACKET_H
#define STRIPS_ABCLPPRPACKET_H

#include "Strips/Packet.h"

#include <cstdint>
#include <string>
#include <vector>

namespace Strips{
  /**
   * This class represents a physics event package. It starts with a header composed of a 4-bit TYP code,
   * a 1-bit Error Flag, a 7-bit L0ID, a 1-bit BCID parity flag, and a 3-bit BCID field that are the least
   * significant bits BCID of the full 8-bit BCID counter. The parity flag is calculated over the entire 8-bit
   * width of the counter.
   *
   * The format is the following:
   *
   * | Bit     |  0 |  3 |  4   |  5 |  7 |  8 | 11 | 12     | 13 | 15 |
   * | ------  | -- | -- | ---- | -- | -- | -- | -- | ------ | -- | -- |
   * | Byte    |  0                   |||||  1                     |||||
   * | Size    |  4     ||  1   |  7             ||||  1     | 3      ||
   * | Desc    | Type   || Flag | L0ID           |||| Parity | BCIDs  ||
   *
   * See page 22 https://edms.cern.ch/ui/file/1809590/1/HCCStar_Spec_10.pdf
   *
   * 
   * @brief Physics Packet
   * @author Andrea.Gabrielli@cern.ch
   * @date August 2022
   **/

  class ABCLPPRPacket: public Packet{
 
  public:

    /**
     * Create and empty ABCLPPRPacket of type Packet::TYP_ABC_RR
     **/
    ABCLPPRPacket();

    /**
     * Create and ABCLPPRPacket with the given parameters
     * @param typ type of Packet, valid values are Packet::TYP_LP and Packet::TYP_PR
     * @param l0id the L0ID of this packet
     * @param flag the error flag for this packet
     * @param bcid the 3 LSBs of the BCID
     * @param parity the BCID parity of this packet
     * @param clusters vector of 16-bit integers describing the clusters
     **/
    ABCLPPRPacket(uint32_t typ, uint32_t l0id, bool flag, uint32_t bcid, bool parity, std::vector<uint16_t> clusters);

    /**
     * Copy the ABCLPPRPacket
     * @param copy The pointer to the object to copy
     **/
    ABCLPPRPacket(ABCLPPRPacket *copy);

    /**
     * Delete the ABCLPPRPacket
     **/
    ~ABCLPPRPacket();

    /**
     * Return a human readable representation of the data
     **/
    std::string ToString();
      
    /**
     * Set the L0ID
     * @param l0id the L0ID of this packet
     **/
    void SetL0ID(uint32_t l0id);

    /**
     * Get the L0ID
     * @return The L0ID of this packet
     **/
    uint32_t GetL0ID();

    /**
     * Set the error flag for this packet
     * @param flag the error flag for this packet
     **/
    void SetFlag(bool flag);
    
    /**
     * Get the error flag for this packet
     * @return the error flag for this packet
     **/
    bool GetFlag();

    /**
     * Set the BCID of this packet (only 3 LSBs)
     * @param bcid the 3 LSBs of the BCID
     **/
    void SetBCID(uint32_t bcid);

    /**
     * Get the BCID of this packet (only 3 LSBs)
     * @return The BCID of this packet
     **/
    uint32_t GetBCID();

    /**
     * Set the BCID parity of this packet
     * @param parity the BCID parity of this packet
     **/
    void SetParity(bool parity);

    /**
     * Get the BCID parity of this packet
     * @return the BCID parity of this packet
     **/
    bool GetParity();

    /**
     * Set the clusters for this event in raw data format
     * @param clusters vector of 16-bit integers describing the clusters
     **/
    void SetClusters(std::vector<uint16_t> &clusters);

    /**
     * Add a cluster to this packet in raw data format
     * @param cluster a 16-bit integer describing the cluster
     **/
    void AddCluster(uint16_t cluster);

    /**
     * Get the clusters for this event in raw data format
     * @return vector of 16-bit integers describing the clusters
     **/
    std::vector<uint16_t> & GetClusters();
    
    /**
     * extract command from the bytes
     * @param bytes the byte array
     * @param maxlen the maximum number of bytes than can be read
     * @return the number of bytes processed
     **/
    uint32_t Unpack (uint8_t * bytes, uint32_t maxlen);

    /**
     * set command to bytes
     * @param bytes the byte array
     * @return the number of bytes processed
     **/
     uint32_t Pack(uint8_t * bytes);

  private:

     uint32_t m_l0id;
     bool m_flag;
     uint32_t m_bcid;
     bool m_parity;
     std::vector<uint16_t> m_clusters;

  };

}

#endif
