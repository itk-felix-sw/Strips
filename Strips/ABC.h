#ifndef STRIPS_ABC_H
#define STRIPS_ABC_H

#include <Strips/Configuration.h>
//#include <Strips/Register.h>
//#include <Strips/SubRegister.h>

//#include <vector>
//#include <map>
//#include <string>

namespace Strips{

  /**
   * The ABC represents the configuration of the ABC chip.
   * It can be accessed via the registers or the sub-registers.
   * 
   * @verbatim
     ABC * abc = new ABC();
     uint32_t vv = hcc->GetRegister(ABC::HPR)->GetValue();
     abc->GetSubRegister(ABC::STOPHPR)->SetValue(2);
     for(auto rr: hcc->GetUpdatedRegisters()){ 
       cout << "Register address: 0x" << hex << rr.first 
            << ", value:" << rr.second << dec << endl;
     }
     delete abc;
   * @endverbatim
   * 
   * @brief Strips ABC representation  
   * @author Andrea.Gabrielli@cern.ch
   * @author Carlos.Solans@cern.ch
   * @date August 2022 
   **/
  class ABC: public Configuration{
  public:

    static const uint32_t NUM_ABC_REGISTERS=192;

    enum ABCRegister{
      SCReg=0, ADCS1=1, ADCS2=2, ADCS3=3, ADCS4=4, ADCS5=5, ADCS6=6, ADCS7=7,
      MaskInput0=16, MaskInput1=17, MaskInput2=18, MaskInput3=19, MaskInput4=20, MaskInput5=21, MaskInput6=22, MaskInput7=23,
      CREG0=32, CREG1=33, CREG2=34, CREG3=35, CREG4=36,
      /* CREG5=37,  */ // This is used for fusing only
      CREG6=38,
      STAT0=48, STAT1=49, STAT2=50, STAT3=51, STAT4=52, HPR=63,
      TrimDAC0=64, TrimDAC1=65, TrimDAC2=66, TrimDAC3=67, TrimDAC4=68, TrimDAC5=69, TrimDAC6=70, TrimDAC7=71, TrimDAC8=72, TrimDAC9=73, 
      TrimDAC10=74, TrimDAC11=75, TrimDAC12=76, TrimDAC13=77, TrimDAC14=78, TrimDAC15=79, TrimDAC16=80, TrimDAC17=81, TrimDAC18=82, TrimDAC19=83, 
      TrimDAC20=84, TrimDAC21=85, TrimDAC22=86, TrimDAC23=87, TrimDAC24=88, TrimDAC25=89, TrimDAC26=90, TrimDAC27=91, TrimDAC28=92, TrimDAC29=93, 
      TrimDAC30=94, TrimDAC31=95, TrimDAC32=96, TrimDAC33=97, TrimDAC34=98, TrimDAC35=99, TrimDAC36=100, TrimDAC37=101, TrimDAC38=102, TrimDAC39=103,
      CalREG0=104, CalREG1=105, CalREG2=106, CalREG3=107, CalREG4=108, CalREG5=109, CalREG6=110, CalREG7=111,
      HitCountREG0=128, HitCountREG1=129, HitCountREG2=130, HitCountREG3=131, HitCountREG4=132, HitCountREG5=133, HitCountREG6=134, HitCountREG7=135, 
      HitCountREG8=136, HitCountREG9=137, HitCountREG10=138, HitCountREG11=139, HitCountREG12=140, HitCountREG13=141, HitCountREG14=142, HitCountREG15=143, 
      HitCountREG16=144, HitCountREG17=145, HitCountREG18=146, HitCountREG19=147, HitCountREG20=148, HitCountREG21=149, HitCountREG22=150, HitCountREG23=151, 
      HitCountREG24=152, HitCountREG25=153, HitCountREG26=154, HitCountREG27=155, HitCountREG28=156, HitCountREG29=157, HitCountREG30=158, HitCountREG31=159, 
      HitCountREG32=160, HitCountREG33=161, HitCountREG34=162, HitCountREG35=163, HitCountREG36=164, HitCountREG37=165, HitCountREG38=166, HitCountREG39=167, 
      HitCountREG40=168, HitCountREG41=169, HitCountREG42=170, HitCountREG43=171, HitCountREG44=172, HitCountREG45=173, HitCountREG46=174, HitCountREG47=175, 
      HitCountREG48=176, HitCountREG49=177, HitCountREG50=178, HitCountREG51=179, HitCountREG52=180, HitCountREG53=181, HitCountREG54=182, HitCountREG55=183, 
      HitCountREG56=184, HitCountREG57=185, HitCountREG58=186, HitCountREG59=187, HitCountREG60=188, HitCountREG61=189, HitCountREG62=190, HitCountREG63=191
    };

    enum ABCSubRegister{
      RRFORCE=1, WRITEDISABLE, STOPHPR, TESTHPR, EFUSEL, LCBERRCNTCLR,
      BVREF, BIREF, B8BREF, BTRANGE,
      BVT, COMBIAS, BIFEED, BIPRE,
      STR_DEL_R, STR_DEL, BCAL, BCAL_RANGE,
      ADC_BIAS, ADC_CH, ADC_ENABLE,
      D_S, D_LOW, D_EN_CTRL,
      BTMUX, BTMUXD, A_S, A_EN_CTRL,
      TEST_PULSE_ENABLE, ENCOUNT, MASKHPR, PR_ENABLE, LP_ENABLE, RRMODE, TM, TESTPATT_ENABLE, TESTPATT1, TESTPATT2,
      CURRDRIV, CALPULSE_ENABLE, CALPULSE_POLARITY,
      LATENCY, BCFLAG_ENABLE, BCOFFSET,
      DETMODE, MAX_CLUSTER, MAX_CLUSTER_ENABLE,
      EN_CLUSTER_EMPTY, EN_CLUSTER_FULL, EN_CLUSTER_OVFL, EN_REGFIFO_EMPTY, EN_REGFIFO_FULL, EN_REGFIFO_OVFL, EN_LPFIFO_EMPTY, 
      EN_LPFIFO_FULL, EN_PRFIFO_EMPTY, EN_PRFIFO_FULL, EN_LCB_LOCKED, EN_LCB_DECODE_ERR, EN_LCB_ERRCNT_OVFL, EN_LCB_SCMD_ERR,
      // DOFUSE, // Not needed
      LCB_ERRCOUNT_THR
    };
    
    /**
     * Create a vector of Register objects and a map of SubRegister pointers. Initialize the ID to 0
     **/ 
    ABC();
 
    /**
     * Delete the pointers in the SubRegister map 
     **/
    virtual ~ABC();
    
    /**
     * Initialize the registers with their default values
     **/
    virtual void SetDefaults(); 


  };

}

#endif 
