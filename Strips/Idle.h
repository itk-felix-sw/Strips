#ifndef STRIPS_IDLE_H
#define STRIPS_IDLE_H

#include <Strips/Command.h>

namespace Strips{

  /**
   * This class represents an IDLE frame composed of the control symbols K0 and K1.
   * The format is the following:
   *
   * | Bit     |  0 |  7 |  8 | 15 |
   * | ------  | -- | -- | -- | -- |
   * | Byte    |  0     ||  1     ||
   * | Size    |  8     ||  8     ||
   * | Desc    |  K0    ||  K1    ||
   *
   *
   * @brief Idle LCB Frame for Strips
   * @author Carlos.Solans@cern.ch
   * @date August 2022
   **/

  class Idle: public Command{

  public:

    /**
     * Create an empty L0A with one trigger
     * otherwise the frame will be confused with a register write
     **/
    Idle();
    
    /**
     * Delete the L0A
     **/
    ~Idle();

    /**
     * Return a human readable representation of the data
     **/
    std::string ToString();

    /**
     * extract command from the bytes
     * @param bytes the byte array
     * @param maxlen the maximum number of bytes than can be read
     * @return the number of bytes processed
     **/
    uint32_t Unpack (uint8_t * bytes, uint32_t maxlen);
      
    /**
     * set command to bytes
     * @param bytes the byte array
     * @return the number of bytes processed
     **/
    uint32_t Pack(uint8_t * bytes);
      
  };

}

#endif
